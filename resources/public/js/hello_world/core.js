// Compiled by ClojureScript 1.9.521 {}
goog.provide('hello_world.core');
goog.require('cljs.core');
goog.require('goog.dom');
goog.require('goog.dom.classes');
goog.require('goog.events');
goog.require('goog.Timer');
var element_11242 = goog.dom.createDom("div","some-class","Hello, World!");
goog.dom.classes.enable(element_11242,"another-class",true);

goog.dom.appendChild(goog.dom.getDocument().body,element_11242);

var G__11241_11243 = (new goog.Timer((1000)));
goog.events.listen(G__11241_11243,"tick",((function (G__11241_11243,element_11242){
return (function (){
return console.warn("still here!");
});})(G__11241_11243,element_11242))
);

G__11241_11243.start();

