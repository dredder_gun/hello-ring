// Compiled by ClojureScript 1.9.521 {}
goog.provide('schema.spec.leaf');
goog.require('cljs.core');
goog.require('schema.spec.core');

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {schema.spec.core.CoreSpec}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
schema.spec.leaf.LeafSpec = (function (pre,__meta,__extmap,__hash){
this.pre = pre;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2229667594;
this.cljs$lang$protocol_mask$partition1$ = 8192;
})
schema.spec.leaf.LeafSpec.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__7887__auto__,k__7888__auto__){
var self__ = this;
var this__7887__auto____$1 = this;
return this__7887__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__7888__auto__,null);
});

schema.spec.leaf.LeafSpec.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__7889__auto__,k9944,else__7890__auto__){
var self__ = this;
var this__7889__auto____$1 = this;
var G__9946 = (((k9944 instanceof cljs.core.Keyword))?k9944.fqn:null);
switch (G__9946) {
case "pre":
return self__.pre;

break;
default:
return cljs.core.get.call(null,self__.__extmap,k9944,else__7890__auto__);

}
});

schema.spec.leaf.LeafSpec.prototype.schema$spec$core$CoreSpec$ = cljs.core.PROTOCOL_SENTINEL;

schema.spec.leaf.LeafSpec.prototype.schema$spec$core$CoreSpec$subschemas$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return null;
});

schema.spec.leaf.LeafSpec.prototype.schema$spec$core$CoreSpec$checker$arity$2 = (function (this$,params){
var self__ = this;
var this$__$1 = this;
return ((function (this$__$1){
return (function (x){
var or__7260__auto__ = self__.pre.call(null,x);
if(cljs.core.truth_(or__7260__auto__)){
return or__7260__auto__;
} else {
return x;
}
});
;})(this$__$1))
});

schema.spec.leaf.LeafSpec.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__7901__auto__,writer__7902__auto__,opts__7903__auto__){
var self__ = this;
var this__7901__auto____$1 = this;
var pr_pair__7904__auto__ = ((function (this__7901__auto____$1){
return (function (keyval__7905__auto__){
return cljs.core.pr_sequential_writer.call(null,writer__7902__auto__,cljs.core.pr_writer,""," ","",opts__7903__auto__,keyval__7905__auto__);
});})(this__7901__auto____$1))
;
return cljs.core.pr_sequential_writer.call(null,writer__7902__auto__,pr_pair__7904__auto__,"#schema.spec.leaf.LeafSpec{",", ","}",opts__7903__auto__,cljs.core.concat.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"pre","pre",2118456869),self__.pre],null))], null),self__.__extmap));
});

schema.spec.leaf.LeafSpec.prototype.cljs$core$IIterable$ = cljs.core.PROTOCOL_SENTINEL;

schema.spec.leaf.LeafSpec.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__9943){
var self__ = this;
var G__9943__$1 = this;
return (new cljs.core.RecordIter((0),G__9943__$1,1,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pre","pre",2118456869)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator.call(null,self__.__extmap):cljs.core.nil_iter.call(null))));
});

schema.spec.leaf.LeafSpec.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__7885__auto__){
var self__ = this;
var this__7885__auto____$1 = this;
return self__.__meta;
});

schema.spec.leaf.LeafSpec.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__7881__auto__){
var self__ = this;
var this__7881__auto____$1 = this;
return (new schema.spec.leaf.LeafSpec(self__.pre,self__.__meta,self__.__extmap,self__.__hash));
});

schema.spec.leaf.LeafSpec.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__7891__auto__){
var self__ = this;
var this__7891__auto____$1 = this;
return (1 + cljs.core.count.call(null,self__.__extmap));
});

schema.spec.leaf.LeafSpec.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__7882__auto__){
var self__ = this;
var this__7882__auto____$1 = this;
var h__7700__auto__ = self__.__hash;
if(!((h__7700__auto__ == null))){
return h__7700__auto__;
} else {
var h__7700__auto____$1 = cljs.core.hash_imap.call(null,this__7882__auto____$1);
self__.__hash = h__7700__auto____$1;

return h__7700__auto____$1;
}
});

schema.spec.leaf.LeafSpec.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this__7883__auto__,other__7884__auto__){
var self__ = this;
var this__7883__auto____$1 = this;
if(cljs.core.truth_((function (){var and__7248__auto__ = other__7884__auto__;
if(cljs.core.truth_(and__7248__auto__)){
return ((this__7883__auto____$1.constructor === other__7884__auto__.constructor)) && (cljs.core.equiv_map.call(null,this__7883__auto____$1,other__7884__auto__));
} else {
return and__7248__auto__;
}
})())){
return true;
} else {
return false;
}
});

schema.spec.leaf.LeafSpec.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__7896__auto__,k__7897__auto__){
var self__ = this;
var this__7896__auto____$1 = this;
if(cljs.core.contains_QMARK_.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"pre","pre",2118456869),null], null), null),k__7897__auto__)){
return cljs.core.dissoc.call(null,cljs.core.with_meta.call(null,cljs.core.into.call(null,cljs.core.PersistentArrayMap.EMPTY,this__7896__auto____$1),self__.__meta),k__7897__auto__);
} else {
return (new schema.spec.leaf.LeafSpec(self__.pre,self__.__meta,cljs.core.not_empty.call(null,cljs.core.dissoc.call(null,self__.__extmap,k__7897__auto__)),null));
}
});

schema.spec.leaf.LeafSpec.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__7894__auto__,k__7895__auto__,G__9943){
var self__ = this;
var this__7894__auto____$1 = this;
var pred__9947 = cljs.core.keyword_identical_QMARK_;
var expr__9948 = k__7895__auto__;
if(cljs.core.truth_(pred__9947.call(null,new cljs.core.Keyword(null,"pre","pre",2118456869),expr__9948))){
return (new schema.spec.leaf.LeafSpec(G__9943,self__.__meta,self__.__extmap,null));
} else {
return (new schema.spec.leaf.LeafSpec(self__.pre,self__.__meta,cljs.core.assoc.call(null,self__.__extmap,k__7895__auto__,G__9943),null));
}
});

schema.spec.leaf.LeafSpec.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__7899__auto__){
var self__ = this;
var this__7899__auto____$1 = this;
return cljs.core.seq.call(null,cljs.core.concat.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"pre","pre",2118456869),self__.pre],null))], null),self__.__extmap));
});

schema.spec.leaf.LeafSpec.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__7886__auto__,G__9943){
var self__ = this;
var this__7886__auto____$1 = this;
return (new schema.spec.leaf.LeafSpec(self__.pre,G__9943,self__.__extmap,self__.__hash));
});

schema.spec.leaf.LeafSpec.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__7892__auto__,entry__7893__auto__){
var self__ = this;
var this__7892__auto____$1 = this;
if(cljs.core.vector_QMARK_.call(null,entry__7893__auto__)){
return this__7892__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth.call(null,entry__7893__auto__,(0)),cljs.core._nth.call(null,entry__7893__auto__,(1)));
} else {
return cljs.core.reduce.call(null,cljs.core._conj,this__7892__auto____$1,entry__7893__auto__);
}
});

schema.spec.leaf.LeafSpec.getBasis = (function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"pre","pre",-535978900,null)], null);
});

schema.spec.leaf.LeafSpec.cljs$lang$type = true;

schema.spec.leaf.LeafSpec.cljs$lang$ctorPrSeq = (function (this__7921__auto__){
return cljs.core._conj.call(null,cljs.core.List.EMPTY,"schema.spec.leaf/LeafSpec");
});

schema.spec.leaf.LeafSpec.cljs$lang$ctorPrWriter = (function (this__7921__auto__,writer__7922__auto__){
return cljs.core._write.call(null,writer__7922__auto__,"schema.spec.leaf/LeafSpec");
});

schema.spec.leaf.__GT_LeafSpec = (function schema$spec$leaf$__GT_LeafSpec(pre){
return (new schema.spec.leaf.LeafSpec(pre,null,null,null));
});

schema.spec.leaf.map__GT_LeafSpec = (function schema$spec$leaf$map__GT_LeafSpec(G__9945){
return (new schema.spec.leaf.LeafSpec(new cljs.core.Keyword(null,"pre","pre",2118456869).cljs$core$IFn$_invoke$arity$1(G__9945),null,cljs.core.dissoc.call(null,G__9945,new cljs.core.Keyword(null,"pre","pre",2118456869)),null));
});

/**
 * A leaf spec represents an atomic datum that is checked completely
 * with a single precondition, and is otherwise a black box to Schema.
 */
schema.spec.leaf.leaf_spec = (function schema$spec$leaf$leaf_spec(pre){
return schema.spec.leaf.__GT_LeafSpec.call(null,pre);
});
