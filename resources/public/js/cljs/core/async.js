// Compiled by ClojureScript 1.9.521 {}
goog.provide('cljs.core.async');
goog.require('cljs.core');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.timers');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.ioc_helpers');
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var args19320 = [];
var len__8373__auto___19326 = arguments.length;
var i__8374__auto___19327 = (0);
while(true){
if((i__8374__auto___19327 < len__8373__auto___19326)){
args19320.push((arguments[i__8374__auto___19327]));

var G__19328 = (i__8374__auto___19327 + (1));
i__8374__auto___19327 = G__19328;
continue;
} else {
}
break;
}

var G__19322 = args19320.length;
switch (G__19322) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args19320.length)].join('')));

}
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.call(null,f,true);
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
if(typeof cljs.core.async.t_cljs$core$async19323 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async19323 = (function (f,blockable,meta19324){
this.f = f;
this.blockable = blockable;
this.meta19324 = meta19324;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async19323.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_19325,meta19324__$1){
var self__ = this;
var _19325__$1 = this;
return (new cljs.core.async.t_cljs$core$async19323(self__.f,self__.blockable,meta19324__$1));
});

cljs.core.async.t_cljs$core$async19323.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_19325){
var self__ = this;
var _19325__$1 = this;
return self__.meta19324;
});

cljs.core.async.t_cljs$core$async19323.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async19323.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async19323.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
});

cljs.core.async.t_cljs$core$async19323.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
});

cljs.core.async.t_cljs$core$async19323.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta19324","meta19324",1246866294,null)], null);
});

cljs.core.async.t_cljs$core$async19323.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async19323.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async19323";

cljs.core.async.t_cljs$core$async19323.cljs$lang$ctorPrWriter = (function (this__7871__auto__,writer__7872__auto__,opt__7873__auto__){
return cljs.core._write.call(null,writer__7872__auto__,"cljs.core.async/t_cljs$core$async19323");
});

cljs.core.async.__GT_t_cljs$core$async19323 = (function cljs$core$async$__GT_t_cljs$core$async19323(f__$1,blockable__$1,meta19324){
return (new cljs.core.async.t_cljs$core$async19323(f__$1,blockable__$1,meta19324));
});

}

return (new cljs.core.async.t_cljs$core$async19323(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
});

cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2;

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer.call(null,n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer.call(null,n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer.call(null,n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if(!((buff == null))){
if((false) || ((cljs.core.PROTOCOL_SENTINEL === buff.cljs$core$async$impl$protocols$UnblockingBuffer$))){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var args19332 = [];
var len__8373__auto___19335 = arguments.length;
var i__8374__auto___19336 = (0);
while(true){
if((i__8374__auto___19336 < len__8373__auto___19335)){
args19332.push((arguments[i__8374__auto___19336]));

var G__19337 = (i__8374__auto___19336 + (1));
i__8374__auto___19336 = G__19337;
continue;
} else {
}
break;
}

var G__19334 = args19332.length;
switch (G__19334) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args19332.length)].join('')));

}
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.call(null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.call(null,buf_or_n,null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.call(null,buf_or_n,xform,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.call(null,buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Assert failed: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1("buffer must be supplied when transducer is"),cljs.core.str.cljs$core$IFn$_invoke$arity$1("\n"),cljs.core.str.cljs$core$IFn$_invoke$arity$1("buf-or-n")].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.call(null,((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer.call(null,buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
});

cljs.core.async.chan.cljs$lang$maxFixedArity = 3;

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var args19339 = [];
var len__8373__auto___19342 = arguments.length;
var i__8374__auto___19343 = (0);
while(true){
if((i__8374__auto___19343 < len__8373__auto___19342)){
args19339.push((arguments[i__8374__auto___19343]));

var G__19344 = (i__8374__auto___19343 + (1));
i__8374__auto___19343 = G__19344;
continue;
} else {
}
break;
}

var G__19341 = args19339.length;
switch (G__19341) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args19339.length)].join('')));

}
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.call(null,null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.call(null,xform,null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.call(null,cljs.core.async.impl.buffers.promise_buffer.call(null),xform,ex_handler);
});

cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2;

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout.call(null,msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var args19346 = [];
var len__8373__auto___19349 = arguments.length;
var i__8374__auto___19350 = (0);
while(true){
if((i__8374__auto___19350 < len__8373__auto___19349)){
args19346.push((arguments[i__8374__auto___19350]));

var G__19351 = (i__8374__auto___19350 + (1));
i__8374__auto___19350 = G__19351;
continue;
} else {
}
break;
}

var G__19348 = args19346.length;
switch (G__19348) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args19346.length)].join('')));

}
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.call(null,port,fn1,true);
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,fn1));
if(cljs.core.truth_(ret)){
var val_19353 = cljs.core.deref.call(null,ret);
if(cljs.core.truth_(on_caller_QMARK_)){
fn1.call(null,val_19353);
} else {
cljs.core.async.impl.dispatch.run.call(null,((function (val_19353,ret){
return (function (){
return fn1.call(null,val_19353);
});})(val_19353,ret))
);
}
} else {
}

return null;
});

cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3;

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.call(null,cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn0 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn0 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var args19354 = [];
var len__8373__auto___19357 = arguments.length;
var i__8374__auto___19358 = (0);
while(true){
if((i__8374__auto___19358 < len__8373__auto___19357)){
args19354.push((arguments[i__8374__auto___19358]));

var G__19359 = (i__8374__auto___19358 + (1));
i__8374__auto___19358 = G__19359;
continue;
} else {
}
break;
}

var G__19356 = args19354.length;
switch (G__19356) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args19354.length)].join('')));

}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__4655__auto__ = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__4655__auto__)){
var ret = temp__4655__auto__;
return cljs.core.deref.call(null,ret);
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.call(null,port,val,fn1,true);
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__4655__auto__ = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,fn1));
if(cljs.core.truth_(temp__4655__auto__)){
var retb = temp__4655__auto__;
var ret = cljs.core.deref.call(null,retb);
if(cljs.core.truth_(on_caller_QMARK_)){
fn1.call(null,ret);
} else {
cljs.core.async.impl.dispatch.run.call(null,((function (ret,retb,temp__4655__auto__){
return (function (){
return fn1.call(null,ret);
});})(ret,retb,temp__4655__auto__))
);
}

return ret;
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4;

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_.call(null,port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__8183__auto___19361 = n;
var x_19362 = (0);
while(true){
if((x_19362 < n__8183__auto___19361)){
(a[x_19362] = (0));

var G__19363 = (x_19362 + (1));
x_19362 = G__19363;
continue;
} else {
}
break;
}

var i = (1);
while(true){
if(cljs.core._EQ_.call(null,i,n)){
return a;
} else {
var j = cljs.core.rand_int.call(null,i);
(a[i] = (a[j]));

(a[j] = i);

var G__19364 = (i + (1));
i = G__19364;
continue;
}
break;
}
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.call(null,true);
if(typeof cljs.core.async.t_cljs$core$async19368 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async19368 = (function (flag,meta19369){
this.flag = flag;
this.meta19369 = meta19369;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async19368.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (flag){
return (function (_19370,meta19369__$1){
var self__ = this;
var _19370__$1 = this;
return (new cljs.core.async.t_cljs$core$async19368(self__.flag,meta19369__$1));
});})(flag))
;

cljs.core.async.t_cljs$core$async19368.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (flag){
return (function (_19370){
var self__ = this;
var _19370__$1 = this;
return self__.meta19369;
});})(flag))
;

cljs.core.async.t_cljs$core$async19368.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async19368.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref.call(null,self__.flag);
});})(flag))
;

cljs.core.async.t_cljs$core$async19368.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async19368.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.flag,null);

return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async19368.getBasis = ((function (flag){
return (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta19369","meta19369",-1641844083,null)], null);
});})(flag))
;

cljs.core.async.t_cljs$core$async19368.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async19368.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async19368";

cljs.core.async.t_cljs$core$async19368.cljs$lang$ctorPrWriter = ((function (flag){
return (function (this__7871__auto__,writer__7872__auto__,opt__7873__auto__){
return cljs.core._write.call(null,writer__7872__auto__,"cljs.core.async/t_cljs$core$async19368");
});})(flag))
;

cljs.core.async.__GT_t_cljs$core$async19368 = ((function (flag){
return (function cljs$core$async$alt_flag_$___GT_t_cljs$core$async19368(flag__$1,meta19369){
return (new cljs.core.async.t_cljs$core$async19368(flag__$1,meta19369));
});})(flag))
;

}

return (new cljs.core.async.t_cljs$core$async19368(flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if(typeof cljs.core.async.t_cljs$core$async19374 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async19374 = (function (flag,cb,meta19375){
this.flag = flag;
this.cb = cb;
this.meta19375 = meta19375;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async19374.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_19376,meta19375__$1){
var self__ = this;
var _19376__$1 = this;
return (new cljs.core.async.t_cljs$core$async19374(self__.flag,self__.cb,meta19375__$1));
});

cljs.core.async.t_cljs$core$async19374.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_19376){
var self__ = this;
var _19376__$1 = this;
return self__.meta19375;
});

cljs.core.async.t_cljs$core$async19374.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async19374.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.flag);
});

cljs.core.async.t_cljs$core$async19374.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async19374.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit.call(null,self__.flag);

return self__.cb;
});

cljs.core.async.t_cljs$core$async19374.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta19375","meta19375",-1007482254,null)], null);
});

cljs.core.async.t_cljs$core$async19374.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async19374.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async19374";

cljs.core.async.t_cljs$core$async19374.cljs$lang$ctorPrWriter = (function (this__7871__auto__,writer__7872__auto__,opt__7873__auto__){
return cljs.core._write.call(null,writer__7872__auto__,"cljs.core.async/t_cljs$core$async19374");
});

cljs.core.async.__GT_t_cljs$core$async19374 = (function cljs$core$async$alt_handler_$___GT_t_cljs$core$async19374(flag__$1,cb__$1,meta19375){
return (new cljs.core.async.t_cljs$core$async19374(flag__$1,cb__$1,meta19375));
});

}

return (new cljs.core.async.t_cljs$core$async19374(flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
var flag = cljs.core.async.alt_flag.call(null);
var n = cljs.core.count.call(null,ports);
var idxs = cljs.core.async.random_array.call(null,n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.call(null,ports,idx);
var wport = ((cljs.core.vector_QMARK_.call(null,port))?port.call(null,(0)):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = port.call(null,(1));
return cljs.core.async.impl.protocols.put_BANG_.call(null,wport,val,cljs.core.async.alt_handler.call(null,flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__19377_SHARP_){
return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__19377_SHARP_,wport], null));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.alt_handler.call(null,flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__19378_SHARP_){
return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__19378_SHARP_,port], null));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref.call(null,vbox),(function (){var or__7260__auto__ = wport;
if(cljs.core.truth_(or__7260__auto__)){
return or__7260__auto__;
} else {
return port;
}
})()], null));
} else {
var G__19379 = (i + (1));
i = G__19379;
continue;
}
} else {
return null;
}
break;
}
})();
var or__7260__auto__ = ret;
if(cljs.core.truth_(or__7260__auto__)){
return or__7260__auto__;
} else {
if(cljs.core.contains_QMARK_.call(null,opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__4657__auto__ = (function (){var and__7248__auto__ = cljs.core.async.impl.protocols.active_QMARK_.call(null,flag);
if(cljs.core.truth_(and__7248__auto__)){
return cljs.core.async.impl.protocols.commit.call(null,flag);
} else {
return and__7248__auto__;
}
})();
if(cljs.core.truth_(temp__4657__auto__)){
var got = temp__4657__auto__;
return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__8380__auto__ = [];
var len__8373__auto___19385 = arguments.length;
var i__8374__auto___19386 = (0);
while(true){
if((i__8374__auto___19386 < len__8373__auto___19385)){
args__8380__auto__.push((arguments[i__8374__auto___19386]));

var G__19387 = (i__8374__auto___19386 + (1));
i__8374__auto___19386 = G__19387;
continue;
} else {
}
break;
}

var argseq__8381__auto__ = ((((1) < args__8380__auto__.length))?(new cljs.core.IndexedSeq(args__8380__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__8381__auto__);
});

cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__19382){
var map__19383 = p__19382;
var map__19383__$1 = ((((!((map__19383 == null)))?((((map__19383.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__19383.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__19383):map__19383);
var opts = map__19383__$1;
throw (new Error("alts! used not in (go ...) block"));
});

cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1);

cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq19380){
var G__19381 = cljs.core.first.call(null,seq19380);
var seq19380__$1 = cljs.core.next.call(null,seq19380);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__19381,seq19380__$1);
});

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref.call(null,ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref.call(null,ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var args19388 = [];
var len__8373__auto___19438 = arguments.length;
var i__8374__auto___19439 = (0);
while(true){
if((i__8374__auto___19439 < len__8373__auto___19438)){
args19388.push((arguments[i__8374__auto___19439]));

var G__19440 = (i__8374__auto___19439 + (1));
i__8374__auto___19439 = G__19440;
continue;
} else {
}
break;
}

var G__19390 = args19388.length;
switch (G__19390) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args19388.length)].join('')));

}
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.call(null,from,to,true);
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__19275__auto___19442 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__19275__auto___19442){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (c__19275__auto___19442){
return (function (state_19414){
var state_val_19415 = (state_19414[(1)]);
if((state_val_19415 === (7))){
var inst_19410 = (state_19414[(2)]);
var state_19414__$1 = state_19414;
var statearr_19416_19443 = state_19414__$1;
(statearr_19416_19443[(2)] = inst_19410);

(statearr_19416_19443[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19415 === (1))){
var state_19414__$1 = state_19414;
var statearr_19417_19444 = state_19414__$1;
(statearr_19417_19444[(2)] = null);

(statearr_19417_19444[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19415 === (4))){
var inst_19393 = (state_19414[(7)]);
var inst_19393__$1 = (state_19414[(2)]);
var inst_19394 = (inst_19393__$1 == null);
var state_19414__$1 = (function (){var statearr_19418 = state_19414;
(statearr_19418[(7)] = inst_19393__$1);

return statearr_19418;
})();
if(cljs.core.truth_(inst_19394)){
var statearr_19419_19445 = state_19414__$1;
(statearr_19419_19445[(1)] = (5));

} else {
var statearr_19420_19446 = state_19414__$1;
(statearr_19420_19446[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19415 === (13))){
var state_19414__$1 = state_19414;
var statearr_19421_19447 = state_19414__$1;
(statearr_19421_19447[(2)] = null);

(statearr_19421_19447[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19415 === (6))){
var inst_19393 = (state_19414[(7)]);
var state_19414__$1 = state_19414;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_19414__$1,(11),to,inst_19393);
} else {
if((state_val_19415 === (3))){
var inst_19412 = (state_19414[(2)]);
var state_19414__$1 = state_19414;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_19414__$1,inst_19412);
} else {
if((state_val_19415 === (12))){
var state_19414__$1 = state_19414;
var statearr_19422_19448 = state_19414__$1;
(statearr_19422_19448[(2)] = null);

(statearr_19422_19448[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19415 === (2))){
var state_19414__$1 = state_19414;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_19414__$1,(4),from);
} else {
if((state_val_19415 === (11))){
var inst_19403 = (state_19414[(2)]);
var state_19414__$1 = state_19414;
if(cljs.core.truth_(inst_19403)){
var statearr_19423_19449 = state_19414__$1;
(statearr_19423_19449[(1)] = (12));

} else {
var statearr_19424_19450 = state_19414__$1;
(statearr_19424_19450[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19415 === (9))){
var state_19414__$1 = state_19414;
var statearr_19425_19451 = state_19414__$1;
(statearr_19425_19451[(2)] = null);

(statearr_19425_19451[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19415 === (5))){
var state_19414__$1 = state_19414;
if(cljs.core.truth_(close_QMARK_)){
var statearr_19426_19452 = state_19414__$1;
(statearr_19426_19452[(1)] = (8));

} else {
var statearr_19427_19453 = state_19414__$1;
(statearr_19427_19453[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19415 === (14))){
var inst_19408 = (state_19414[(2)]);
var state_19414__$1 = state_19414;
var statearr_19428_19454 = state_19414__$1;
(statearr_19428_19454[(2)] = inst_19408);

(statearr_19428_19454[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19415 === (10))){
var inst_19400 = (state_19414[(2)]);
var state_19414__$1 = state_19414;
var statearr_19429_19455 = state_19414__$1;
(statearr_19429_19455[(2)] = inst_19400);

(statearr_19429_19455[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19415 === (8))){
var inst_19397 = cljs.core.async.close_BANG_.call(null,to);
var state_19414__$1 = state_19414;
var statearr_19430_19456 = state_19414__$1;
(statearr_19430_19456[(2)] = inst_19397);

(statearr_19430_19456[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__19275__auto___19442))
;
return ((function (switch__19163__auto__,c__19275__auto___19442){
return (function() {
var cljs$core$async$state_machine__19164__auto__ = null;
var cljs$core$async$state_machine__19164__auto____0 = (function (){
var statearr_19434 = [null,null,null,null,null,null,null,null];
(statearr_19434[(0)] = cljs$core$async$state_machine__19164__auto__);

(statearr_19434[(1)] = (1));

return statearr_19434;
});
var cljs$core$async$state_machine__19164__auto____1 = (function (state_19414){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_19414);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e19435){if((e19435 instanceof Object)){
var ex__19167__auto__ = e19435;
var statearr_19436_19457 = state_19414;
(statearr_19436_19457[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_19414);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e19435;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__19458 = state_19414;
state_19414 = G__19458;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$state_machine__19164__auto__ = function(state_19414){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19164__auto____1.call(this,state_19414);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19164__auto____0;
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19164__auto____1;
return cljs$core$async$state_machine__19164__auto__;
})()
;})(switch__19163__auto__,c__19275__auto___19442))
})();
var state__19277__auto__ = (function (){var statearr_19437 = f__19276__auto__.call(null);
(statearr_19437[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto___19442);

return statearr_19437;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(c__19275__auto___19442))
);


return to;
});

cljs.core.async.pipe.cljs$lang$maxFixedArity = 3;

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.call(null,n);
var results = cljs.core.async.chan.call(null,n);
var process = ((function (jobs,results){
return (function (p__19646){
var vec__19647 = p__19646;
var v = cljs.core.nth.call(null,vec__19647,(0),null);
var p = cljs.core.nth.call(null,vec__19647,(1),null);
var job = vec__19647;
if((job == null)){
cljs.core.async.close_BANG_.call(null,results);

return null;
} else {
var res = cljs.core.async.chan.call(null,(1),xf,ex_handler);
var c__19275__auto___19833 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__19275__auto___19833,res,vec__19647,v,p,job,jobs,results){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (c__19275__auto___19833,res,vec__19647,v,p,job,jobs,results){
return (function (state_19654){
var state_val_19655 = (state_19654[(1)]);
if((state_val_19655 === (1))){
var state_19654__$1 = state_19654;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_19654__$1,(2),res,v);
} else {
if((state_val_19655 === (2))){
var inst_19651 = (state_19654[(2)]);
var inst_19652 = cljs.core.async.close_BANG_.call(null,res);
var state_19654__$1 = (function (){var statearr_19656 = state_19654;
(statearr_19656[(7)] = inst_19651);

return statearr_19656;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_19654__$1,inst_19652);
} else {
return null;
}
}
});})(c__19275__auto___19833,res,vec__19647,v,p,job,jobs,results))
;
return ((function (switch__19163__auto__,c__19275__auto___19833,res,vec__19647,v,p,job,jobs,results){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____0 = (function (){
var statearr_19660 = [null,null,null,null,null,null,null,null];
(statearr_19660[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__);

(statearr_19660[(1)] = (1));

return statearr_19660;
});
var cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____1 = (function (state_19654){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_19654);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e19661){if((e19661 instanceof Object)){
var ex__19167__auto__ = e19661;
var statearr_19662_19834 = state_19654;
(statearr_19662_19834[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_19654);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e19661;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__19835 = state_19654;
state_19654 = G__19835;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__ = function(state_19654){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____1.call(this,state_19654);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__;
})()
;})(switch__19163__auto__,c__19275__auto___19833,res,vec__19647,v,p,job,jobs,results))
})();
var state__19277__auto__ = (function (){var statearr_19663 = f__19276__auto__.call(null);
(statearr_19663[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto___19833);

return statearr_19663;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(c__19275__auto___19833,res,vec__19647,v,p,job,jobs,results))
);


cljs.core.async.put_BANG_.call(null,p,res);

return true;
}
});})(jobs,results))
;
var async = ((function (jobs,results,process){
return (function (p__19664){
var vec__19665 = p__19664;
var v = cljs.core.nth.call(null,vec__19665,(0),null);
var p = cljs.core.nth.call(null,vec__19665,(1),null);
var job = vec__19665;
if((job == null)){
cljs.core.async.close_BANG_.call(null,results);

return null;
} else {
var res = cljs.core.async.chan.call(null,(1));
xf.call(null,v,res);

cljs.core.async.put_BANG_.call(null,p,res);

return true;
}
});})(jobs,results,process))
;
var n__8183__auto___19836 = n;
var __19837 = (0);
while(true){
if((__19837 < n__8183__auto___19836)){
var G__19668_19838 = (((type instanceof cljs.core.Keyword))?type.fqn:null);
switch (G__19668_19838) {
case "compute":
var c__19275__auto___19840 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (__19837,c__19275__auto___19840,G__19668_19838,n__8183__auto___19836,jobs,results,process,async){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (__19837,c__19275__auto___19840,G__19668_19838,n__8183__auto___19836,jobs,results,process,async){
return (function (state_19681){
var state_val_19682 = (state_19681[(1)]);
if((state_val_19682 === (1))){
var state_19681__$1 = state_19681;
var statearr_19683_19841 = state_19681__$1;
(statearr_19683_19841[(2)] = null);

(statearr_19683_19841[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19682 === (2))){
var state_19681__$1 = state_19681;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_19681__$1,(4),jobs);
} else {
if((state_val_19682 === (3))){
var inst_19679 = (state_19681[(2)]);
var state_19681__$1 = state_19681;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_19681__$1,inst_19679);
} else {
if((state_val_19682 === (4))){
var inst_19671 = (state_19681[(2)]);
var inst_19672 = process.call(null,inst_19671);
var state_19681__$1 = state_19681;
if(cljs.core.truth_(inst_19672)){
var statearr_19684_19842 = state_19681__$1;
(statearr_19684_19842[(1)] = (5));

} else {
var statearr_19685_19843 = state_19681__$1;
(statearr_19685_19843[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19682 === (5))){
var state_19681__$1 = state_19681;
var statearr_19686_19844 = state_19681__$1;
(statearr_19686_19844[(2)] = null);

(statearr_19686_19844[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19682 === (6))){
var state_19681__$1 = state_19681;
var statearr_19687_19845 = state_19681__$1;
(statearr_19687_19845[(2)] = null);

(statearr_19687_19845[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19682 === (7))){
var inst_19677 = (state_19681[(2)]);
var state_19681__$1 = state_19681;
var statearr_19688_19846 = state_19681__$1;
(statearr_19688_19846[(2)] = inst_19677);

(statearr_19688_19846[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__19837,c__19275__auto___19840,G__19668_19838,n__8183__auto___19836,jobs,results,process,async))
;
return ((function (__19837,switch__19163__auto__,c__19275__auto___19840,G__19668_19838,n__8183__auto___19836,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____0 = (function (){
var statearr_19692 = [null,null,null,null,null,null,null];
(statearr_19692[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__);

(statearr_19692[(1)] = (1));

return statearr_19692;
});
var cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____1 = (function (state_19681){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_19681);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e19693){if((e19693 instanceof Object)){
var ex__19167__auto__ = e19693;
var statearr_19694_19847 = state_19681;
(statearr_19694_19847[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_19681);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e19693;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__19848 = state_19681;
state_19681 = G__19848;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__ = function(state_19681){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____1.call(this,state_19681);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__;
})()
;})(__19837,switch__19163__auto__,c__19275__auto___19840,G__19668_19838,n__8183__auto___19836,jobs,results,process,async))
})();
var state__19277__auto__ = (function (){var statearr_19695 = f__19276__auto__.call(null);
(statearr_19695[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto___19840);

return statearr_19695;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(__19837,c__19275__auto___19840,G__19668_19838,n__8183__auto___19836,jobs,results,process,async))
);


break;
case "async":
var c__19275__auto___19849 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (__19837,c__19275__auto___19849,G__19668_19838,n__8183__auto___19836,jobs,results,process,async){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (__19837,c__19275__auto___19849,G__19668_19838,n__8183__auto___19836,jobs,results,process,async){
return (function (state_19708){
var state_val_19709 = (state_19708[(1)]);
if((state_val_19709 === (1))){
var state_19708__$1 = state_19708;
var statearr_19710_19850 = state_19708__$1;
(statearr_19710_19850[(2)] = null);

(statearr_19710_19850[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19709 === (2))){
var state_19708__$1 = state_19708;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_19708__$1,(4),jobs);
} else {
if((state_val_19709 === (3))){
var inst_19706 = (state_19708[(2)]);
var state_19708__$1 = state_19708;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_19708__$1,inst_19706);
} else {
if((state_val_19709 === (4))){
var inst_19698 = (state_19708[(2)]);
var inst_19699 = async.call(null,inst_19698);
var state_19708__$1 = state_19708;
if(cljs.core.truth_(inst_19699)){
var statearr_19711_19851 = state_19708__$1;
(statearr_19711_19851[(1)] = (5));

} else {
var statearr_19712_19852 = state_19708__$1;
(statearr_19712_19852[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19709 === (5))){
var state_19708__$1 = state_19708;
var statearr_19713_19853 = state_19708__$1;
(statearr_19713_19853[(2)] = null);

(statearr_19713_19853[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19709 === (6))){
var state_19708__$1 = state_19708;
var statearr_19714_19854 = state_19708__$1;
(statearr_19714_19854[(2)] = null);

(statearr_19714_19854[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19709 === (7))){
var inst_19704 = (state_19708[(2)]);
var state_19708__$1 = state_19708;
var statearr_19715_19855 = state_19708__$1;
(statearr_19715_19855[(2)] = inst_19704);

(statearr_19715_19855[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__19837,c__19275__auto___19849,G__19668_19838,n__8183__auto___19836,jobs,results,process,async))
;
return ((function (__19837,switch__19163__auto__,c__19275__auto___19849,G__19668_19838,n__8183__auto___19836,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____0 = (function (){
var statearr_19719 = [null,null,null,null,null,null,null];
(statearr_19719[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__);

(statearr_19719[(1)] = (1));

return statearr_19719;
});
var cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____1 = (function (state_19708){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_19708);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e19720){if((e19720 instanceof Object)){
var ex__19167__auto__ = e19720;
var statearr_19721_19856 = state_19708;
(statearr_19721_19856[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_19708);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e19720;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__19857 = state_19708;
state_19708 = G__19857;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__ = function(state_19708){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____1.call(this,state_19708);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__;
})()
;})(__19837,switch__19163__auto__,c__19275__auto___19849,G__19668_19838,n__8183__auto___19836,jobs,results,process,async))
})();
var state__19277__auto__ = (function (){var statearr_19722 = f__19276__auto__.call(null);
(statearr_19722[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto___19849);

return statearr_19722;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(__19837,c__19275__auto___19849,G__19668_19838,n__8183__auto___19836,jobs,results,process,async))
);


break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("No matching clause: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(type)].join('')));

}

var G__19858 = (__19837 + (1));
__19837 = G__19858;
continue;
} else {
}
break;
}

var c__19275__auto___19859 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__19275__auto___19859,jobs,results,process,async){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (c__19275__auto___19859,jobs,results,process,async){
return (function (state_19744){
var state_val_19745 = (state_19744[(1)]);
if((state_val_19745 === (1))){
var state_19744__$1 = state_19744;
var statearr_19746_19860 = state_19744__$1;
(statearr_19746_19860[(2)] = null);

(statearr_19746_19860[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19745 === (2))){
var state_19744__$1 = state_19744;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_19744__$1,(4),from);
} else {
if((state_val_19745 === (3))){
var inst_19742 = (state_19744[(2)]);
var state_19744__$1 = state_19744;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_19744__$1,inst_19742);
} else {
if((state_val_19745 === (4))){
var inst_19725 = (state_19744[(7)]);
var inst_19725__$1 = (state_19744[(2)]);
var inst_19726 = (inst_19725__$1 == null);
var state_19744__$1 = (function (){var statearr_19747 = state_19744;
(statearr_19747[(7)] = inst_19725__$1);

return statearr_19747;
})();
if(cljs.core.truth_(inst_19726)){
var statearr_19748_19861 = state_19744__$1;
(statearr_19748_19861[(1)] = (5));

} else {
var statearr_19749_19862 = state_19744__$1;
(statearr_19749_19862[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19745 === (5))){
var inst_19728 = cljs.core.async.close_BANG_.call(null,jobs);
var state_19744__$1 = state_19744;
var statearr_19750_19863 = state_19744__$1;
(statearr_19750_19863[(2)] = inst_19728);

(statearr_19750_19863[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19745 === (6))){
var inst_19730 = (state_19744[(8)]);
var inst_19725 = (state_19744[(7)]);
var inst_19730__$1 = cljs.core.async.chan.call(null,(1));
var inst_19731 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_19732 = [inst_19725,inst_19730__$1];
var inst_19733 = (new cljs.core.PersistentVector(null,2,(5),inst_19731,inst_19732,null));
var state_19744__$1 = (function (){var statearr_19751 = state_19744;
(statearr_19751[(8)] = inst_19730__$1);

return statearr_19751;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_19744__$1,(8),jobs,inst_19733);
} else {
if((state_val_19745 === (7))){
var inst_19740 = (state_19744[(2)]);
var state_19744__$1 = state_19744;
var statearr_19752_19864 = state_19744__$1;
(statearr_19752_19864[(2)] = inst_19740);

(statearr_19752_19864[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19745 === (8))){
var inst_19730 = (state_19744[(8)]);
var inst_19735 = (state_19744[(2)]);
var state_19744__$1 = (function (){var statearr_19753 = state_19744;
(statearr_19753[(9)] = inst_19735);

return statearr_19753;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_19744__$1,(9),results,inst_19730);
} else {
if((state_val_19745 === (9))){
var inst_19737 = (state_19744[(2)]);
var state_19744__$1 = (function (){var statearr_19754 = state_19744;
(statearr_19754[(10)] = inst_19737);

return statearr_19754;
})();
var statearr_19755_19865 = state_19744__$1;
(statearr_19755_19865[(2)] = null);

(statearr_19755_19865[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});})(c__19275__auto___19859,jobs,results,process,async))
;
return ((function (switch__19163__auto__,c__19275__auto___19859,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____0 = (function (){
var statearr_19759 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_19759[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__);

(statearr_19759[(1)] = (1));

return statearr_19759;
});
var cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____1 = (function (state_19744){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_19744);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e19760){if((e19760 instanceof Object)){
var ex__19167__auto__ = e19760;
var statearr_19761_19866 = state_19744;
(statearr_19761_19866[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_19744);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e19760;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__19867 = state_19744;
state_19744 = G__19867;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__ = function(state_19744){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____1.call(this,state_19744);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__;
})()
;})(switch__19163__auto__,c__19275__auto___19859,jobs,results,process,async))
})();
var state__19277__auto__ = (function (){var statearr_19762 = f__19276__auto__.call(null);
(statearr_19762[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto___19859);

return statearr_19762;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(c__19275__auto___19859,jobs,results,process,async))
);


var c__19275__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__19275__auto__,jobs,results,process,async){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (c__19275__auto__,jobs,results,process,async){
return (function (state_19800){
var state_val_19801 = (state_19800[(1)]);
if((state_val_19801 === (7))){
var inst_19796 = (state_19800[(2)]);
var state_19800__$1 = state_19800;
var statearr_19802_19868 = state_19800__$1;
(statearr_19802_19868[(2)] = inst_19796);

(statearr_19802_19868[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19801 === (20))){
var state_19800__$1 = state_19800;
var statearr_19803_19869 = state_19800__$1;
(statearr_19803_19869[(2)] = null);

(statearr_19803_19869[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19801 === (1))){
var state_19800__$1 = state_19800;
var statearr_19804_19870 = state_19800__$1;
(statearr_19804_19870[(2)] = null);

(statearr_19804_19870[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19801 === (4))){
var inst_19765 = (state_19800[(7)]);
var inst_19765__$1 = (state_19800[(2)]);
var inst_19766 = (inst_19765__$1 == null);
var state_19800__$1 = (function (){var statearr_19805 = state_19800;
(statearr_19805[(7)] = inst_19765__$1);

return statearr_19805;
})();
if(cljs.core.truth_(inst_19766)){
var statearr_19806_19871 = state_19800__$1;
(statearr_19806_19871[(1)] = (5));

} else {
var statearr_19807_19872 = state_19800__$1;
(statearr_19807_19872[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19801 === (15))){
var inst_19778 = (state_19800[(8)]);
var state_19800__$1 = state_19800;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_19800__$1,(18),to,inst_19778);
} else {
if((state_val_19801 === (21))){
var inst_19791 = (state_19800[(2)]);
var state_19800__$1 = state_19800;
var statearr_19808_19873 = state_19800__$1;
(statearr_19808_19873[(2)] = inst_19791);

(statearr_19808_19873[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19801 === (13))){
var inst_19793 = (state_19800[(2)]);
var state_19800__$1 = (function (){var statearr_19809 = state_19800;
(statearr_19809[(9)] = inst_19793);

return statearr_19809;
})();
var statearr_19810_19874 = state_19800__$1;
(statearr_19810_19874[(2)] = null);

(statearr_19810_19874[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19801 === (6))){
var inst_19765 = (state_19800[(7)]);
var state_19800__$1 = state_19800;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_19800__$1,(11),inst_19765);
} else {
if((state_val_19801 === (17))){
var inst_19786 = (state_19800[(2)]);
var state_19800__$1 = state_19800;
if(cljs.core.truth_(inst_19786)){
var statearr_19811_19875 = state_19800__$1;
(statearr_19811_19875[(1)] = (19));

} else {
var statearr_19812_19876 = state_19800__$1;
(statearr_19812_19876[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19801 === (3))){
var inst_19798 = (state_19800[(2)]);
var state_19800__$1 = state_19800;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_19800__$1,inst_19798);
} else {
if((state_val_19801 === (12))){
var inst_19775 = (state_19800[(10)]);
var state_19800__$1 = state_19800;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_19800__$1,(14),inst_19775);
} else {
if((state_val_19801 === (2))){
var state_19800__$1 = state_19800;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_19800__$1,(4),results);
} else {
if((state_val_19801 === (19))){
var state_19800__$1 = state_19800;
var statearr_19813_19877 = state_19800__$1;
(statearr_19813_19877[(2)] = null);

(statearr_19813_19877[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19801 === (11))){
var inst_19775 = (state_19800[(2)]);
var state_19800__$1 = (function (){var statearr_19814 = state_19800;
(statearr_19814[(10)] = inst_19775);

return statearr_19814;
})();
var statearr_19815_19878 = state_19800__$1;
(statearr_19815_19878[(2)] = null);

(statearr_19815_19878[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19801 === (9))){
var state_19800__$1 = state_19800;
var statearr_19816_19879 = state_19800__$1;
(statearr_19816_19879[(2)] = null);

(statearr_19816_19879[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19801 === (5))){
var state_19800__$1 = state_19800;
if(cljs.core.truth_(close_QMARK_)){
var statearr_19817_19880 = state_19800__$1;
(statearr_19817_19880[(1)] = (8));

} else {
var statearr_19818_19881 = state_19800__$1;
(statearr_19818_19881[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19801 === (14))){
var inst_19778 = (state_19800[(8)]);
var inst_19780 = (state_19800[(11)]);
var inst_19778__$1 = (state_19800[(2)]);
var inst_19779 = (inst_19778__$1 == null);
var inst_19780__$1 = cljs.core.not.call(null,inst_19779);
var state_19800__$1 = (function (){var statearr_19819 = state_19800;
(statearr_19819[(8)] = inst_19778__$1);

(statearr_19819[(11)] = inst_19780__$1);

return statearr_19819;
})();
if(inst_19780__$1){
var statearr_19820_19882 = state_19800__$1;
(statearr_19820_19882[(1)] = (15));

} else {
var statearr_19821_19883 = state_19800__$1;
(statearr_19821_19883[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19801 === (16))){
var inst_19780 = (state_19800[(11)]);
var state_19800__$1 = state_19800;
var statearr_19822_19884 = state_19800__$1;
(statearr_19822_19884[(2)] = inst_19780);

(statearr_19822_19884[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19801 === (10))){
var inst_19772 = (state_19800[(2)]);
var state_19800__$1 = state_19800;
var statearr_19823_19885 = state_19800__$1;
(statearr_19823_19885[(2)] = inst_19772);

(statearr_19823_19885[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19801 === (18))){
var inst_19783 = (state_19800[(2)]);
var state_19800__$1 = state_19800;
var statearr_19824_19886 = state_19800__$1;
(statearr_19824_19886[(2)] = inst_19783);

(statearr_19824_19886[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19801 === (8))){
var inst_19769 = cljs.core.async.close_BANG_.call(null,to);
var state_19800__$1 = state_19800;
var statearr_19825_19887 = state_19800__$1;
(statearr_19825_19887[(2)] = inst_19769);

(statearr_19825_19887[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__19275__auto__,jobs,results,process,async))
;
return ((function (switch__19163__auto__,c__19275__auto__,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____0 = (function (){
var statearr_19829 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_19829[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__);

(statearr_19829[(1)] = (1));

return statearr_19829;
});
var cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____1 = (function (state_19800){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_19800);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e19830){if((e19830 instanceof Object)){
var ex__19167__auto__ = e19830;
var statearr_19831_19888 = state_19800;
(statearr_19831_19888[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_19800);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e19830;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__19889 = state_19800;
state_19800 = G__19889;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__ = function(state_19800){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____1.call(this,state_19800);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__19164__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__19164__auto__;
})()
;})(switch__19163__auto__,c__19275__auto__,jobs,results,process,async))
})();
var state__19277__auto__ = (function (){var statearr_19832 = f__19276__auto__.call(null);
(statearr_19832[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto__);

return statearr_19832;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(c__19275__auto__,jobs,results,process,async))
);

return c__19275__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). af must close!
 *   the channel before returning.  The presumption is that af will
 *   return immediately, having launched some asynchronous operation
 *   whose completion/callback will manipulate the result channel. Outputs
 *   will be returned in order relative to  the inputs. By default, the to
 *   channel will be closed when the from channel closes, but can be
 *   determined by the close?  parameter. Will stop consuming the from
 *   channel if the to channel closes.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var args19890 = [];
var len__8373__auto___19893 = arguments.length;
var i__8374__auto___19894 = (0);
while(true){
if((i__8374__auto___19894 < len__8373__auto___19893)){
args19890.push((arguments[i__8374__auto___19894]));

var G__19895 = (i__8374__auto___19894 + (1));
i__8374__auto___19894 = G__19895;
continue;
} else {
}
break;
}

var G__19892 = args19890.length;
switch (G__19892) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args19890.length)].join('')));

}
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.call(null,n,to,af,from,true);
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_.call(null,n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
});

cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5;

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var args19897 = [];
var len__8373__auto___19900 = arguments.length;
var i__8374__auto___19901 = (0);
while(true){
if((i__8374__auto___19901 < len__8373__auto___19900)){
args19897.push((arguments[i__8374__auto___19901]));

var G__19902 = (i__8374__auto___19901 + (1));
i__8374__auto___19901 = G__19902;
continue;
} else {
}
break;
}

var G__19899 = args19897.length;
switch (G__19899) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args19897.length)].join('')));

}
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.call(null,n,to,xf,from,true);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.call(null,n,to,xf,from,close_QMARK_,null);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_.call(null,n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
});

cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6;

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var args19904 = [];
var len__8373__auto___19957 = arguments.length;
var i__8374__auto___19958 = (0);
while(true){
if((i__8374__auto___19958 < len__8373__auto___19957)){
args19904.push((arguments[i__8374__auto___19958]));

var G__19959 = (i__8374__auto___19958 + (1));
i__8374__auto___19958 = G__19959;
continue;
} else {
}
break;
}

var G__19906 = args19904.length;
switch (G__19906) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args19904.length)].join('')));

}
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.call(null,p,ch,null,null);
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.call(null,t_buf_or_n);
var fc = cljs.core.async.chan.call(null,f_buf_or_n);
var c__19275__auto___19961 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__19275__auto___19961,tc,fc){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (c__19275__auto___19961,tc,fc){
return (function (state_19932){
var state_val_19933 = (state_19932[(1)]);
if((state_val_19933 === (7))){
var inst_19928 = (state_19932[(2)]);
var state_19932__$1 = state_19932;
var statearr_19934_19962 = state_19932__$1;
(statearr_19934_19962[(2)] = inst_19928);

(statearr_19934_19962[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19933 === (1))){
var state_19932__$1 = state_19932;
var statearr_19935_19963 = state_19932__$1;
(statearr_19935_19963[(2)] = null);

(statearr_19935_19963[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19933 === (4))){
var inst_19909 = (state_19932[(7)]);
var inst_19909__$1 = (state_19932[(2)]);
var inst_19910 = (inst_19909__$1 == null);
var state_19932__$1 = (function (){var statearr_19936 = state_19932;
(statearr_19936[(7)] = inst_19909__$1);

return statearr_19936;
})();
if(cljs.core.truth_(inst_19910)){
var statearr_19937_19964 = state_19932__$1;
(statearr_19937_19964[(1)] = (5));

} else {
var statearr_19938_19965 = state_19932__$1;
(statearr_19938_19965[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19933 === (13))){
var state_19932__$1 = state_19932;
var statearr_19939_19966 = state_19932__$1;
(statearr_19939_19966[(2)] = null);

(statearr_19939_19966[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19933 === (6))){
var inst_19909 = (state_19932[(7)]);
var inst_19915 = p.call(null,inst_19909);
var state_19932__$1 = state_19932;
if(cljs.core.truth_(inst_19915)){
var statearr_19940_19967 = state_19932__$1;
(statearr_19940_19967[(1)] = (9));

} else {
var statearr_19941_19968 = state_19932__$1;
(statearr_19941_19968[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19933 === (3))){
var inst_19930 = (state_19932[(2)]);
var state_19932__$1 = state_19932;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_19932__$1,inst_19930);
} else {
if((state_val_19933 === (12))){
var state_19932__$1 = state_19932;
var statearr_19942_19969 = state_19932__$1;
(statearr_19942_19969[(2)] = null);

(statearr_19942_19969[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19933 === (2))){
var state_19932__$1 = state_19932;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_19932__$1,(4),ch);
} else {
if((state_val_19933 === (11))){
var inst_19909 = (state_19932[(7)]);
var inst_19919 = (state_19932[(2)]);
var state_19932__$1 = state_19932;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_19932__$1,(8),inst_19919,inst_19909);
} else {
if((state_val_19933 === (9))){
var state_19932__$1 = state_19932;
var statearr_19943_19970 = state_19932__$1;
(statearr_19943_19970[(2)] = tc);

(statearr_19943_19970[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19933 === (5))){
var inst_19912 = cljs.core.async.close_BANG_.call(null,tc);
var inst_19913 = cljs.core.async.close_BANG_.call(null,fc);
var state_19932__$1 = (function (){var statearr_19944 = state_19932;
(statearr_19944[(8)] = inst_19912);

return statearr_19944;
})();
var statearr_19945_19971 = state_19932__$1;
(statearr_19945_19971[(2)] = inst_19913);

(statearr_19945_19971[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19933 === (14))){
var inst_19926 = (state_19932[(2)]);
var state_19932__$1 = state_19932;
var statearr_19946_19972 = state_19932__$1;
(statearr_19946_19972[(2)] = inst_19926);

(statearr_19946_19972[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19933 === (10))){
var state_19932__$1 = state_19932;
var statearr_19947_19973 = state_19932__$1;
(statearr_19947_19973[(2)] = fc);

(statearr_19947_19973[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_19933 === (8))){
var inst_19921 = (state_19932[(2)]);
var state_19932__$1 = state_19932;
if(cljs.core.truth_(inst_19921)){
var statearr_19948_19974 = state_19932__$1;
(statearr_19948_19974[(1)] = (12));

} else {
var statearr_19949_19975 = state_19932__$1;
(statearr_19949_19975[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__19275__auto___19961,tc,fc))
;
return ((function (switch__19163__auto__,c__19275__auto___19961,tc,fc){
return (function() {
var cljs$core$async$state_machine__19164__auto__ = null;
var cljs$core$async$state_machine__19164__auto____0 = (function (){
var statearr_19953 = [null,null,null,null,null,null,null,null,null];
(statearr_19953[(0)] = cljs$core$async$state_machine__19164__auto__);

(statearr_19953[(1)] = (1));

return statearr_19953;
});
var cljs$core$async$state_machine__19164__auto____1 = (function (state_19932){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_19932);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e19954){if((e19954 instanceof Object)){
var ex__19167__auto__ = e19954;
var statearr_19955_19976 = state_19932;
(statearr_19955_19976[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_19932);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e19954;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__19977 = state_19932;
state_19932 = G__19977;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$state_machine__19164__auto__ = function(state_19932){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19164__auto____1.call(this,state_19932);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19164__auto____0;
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19164__auto____1;
return cljs$core$async$state_machine__19164__auto__;
})()
;})(switch__19163__auto__,c__19275__auto___19961,tc,fc))
})();
var state__19277__auto__ = (function (){var statearr_19956 = f__19276__auto__.call(null);
(statearr_19956[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto___19961);

return statearr_19956;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(c__19275__auto___19961,tc,fc))
);


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
});

cljs.core.async.split.cljs$lang$maxFixedArity = 4;

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__19275__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__19275__auto__){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (c__19275__auto__){
return (function (state_20041){
var state_val_20042 = (state_20041[(1)]);
if((state_val_20042 === (7))){
var inst_20037 = (state_20041[(2)]);
var state_20041__$1 = state_20041;
var statearr_20043_20064 = state_20041__$1;
(statearr_20043_20064[(2)] = inst_20037);

(statearr_20043_20064[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20042 === (1))){
var inst_20021 = init;
var state_20041__$1 = (function (){var statearr_20044 = state_20041;
(statearr_20044[(7)] = inst_20021);

return statearr_20044;
})();
var statearr_20045_20065 = state_20041__$1;
(statearr_20045_20065[(2)] = null);

(statearr_20045_20065[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20042 === (4))){
var inst_20024 = (state_20041[(8)]);
var inst_20024__$1 = (state_20041[(2)]);
var inst_20025 = (inst_20024__$1 == null);
var state_20041__$1 = (function (){var statearr_20046 = state_20041;
(statearr_20046[(8)] = inst_20024__$1);

return statearr_20046;
})();
if(cljs.core.truth_(inst_20025)){
var statearr_20047_20066 = state_20041__$1;
(statearr_20047_20066[(1)] = (5));

} else {
var statearr_20048_20067 = state_20041__$1;
(statearr_20048_20067[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20042 === (6))){
var inst_20028 = (state_20041[(9)]);
var inst_20021 = (state_20041[(7)]);
var inst_20024 = (state_20041[(8)]);
var inst_20028__$1 = f.call(null,inst_20021,inst_20024);
var inst_20029 = cljs.core.reduced_QMARK_.call(null,inst_20028__$1);
var state_20041__$1 = (function (){var statearr_20049 = state_20041;
(statearr_20049[(9)] = inst_20028__$1);

return statearr_20049;
})();
if(inst_20029){
var statearr_20050_20068 = state_20041__$1;
(statearr_20050_20068[(1)] = (8));

} else {
var statearr_20051_20069 = state_20041__$1;
(statearr_20051_20069[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20042 === (3))){
var inst_20039 = (state_20041[(2)]);
var state_20041__$1 = state_20041;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_20041__$1,inst_20039);
} else {
if((state_val_20042 === (2))){
var state_20041__$1 = state_20041;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_20041__$1,(4),ch);
} else {
if((state_val_20042 === (9))){
var inst_20028 = (state_20041[(9)]);
var inst_20021 = inst_20028;
var state_20041__$1 = (function (){var statearr_20052 = state_20041;
(statearr_20052[(7)] = inst_20021);

return statearr_20052;
})();
var statearr_20053_20070 = state_20041__$1;
(statearr_20053_20070[(2)] = null);

(statearr_20053_20070[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20042 === (5))){
var inst_20021 = (state_20041[(7)]);
var state_20041__$1 = state_20041;
var statearr_20054_20071 = state_20041__$1;
(statearr_20054_20071[(2)] = inst_20021);

(statearr_20054_20071[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20042 === (10))){
var inst_20035 = (state_20041[(2)]);
var state_20041__$1 = state_20041;
var statearr_20055_20072 = state_20041__$1;
(statearr_20055_20072[(2)] = inst_20035);

(statearr_20055_20072[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20042 === (8))){
var inst_20028 = (state_20041[(9)]);
var inst_20031 = cljs.core.deref.call(null,inst_20028);
var state_20041__$1 = state_20041;
var statearr_20056_20073 = state_20041__$1;
(statearr_20056_20073[(2)] = inst_20031);

(statearr_20056_20073[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});})(c__19275__auto__))
;
return ((function (switch__19163__auto__,c__19275__auto__){
return (function() {
var cljs$core$async$reduce_$_state_machine__19164__auto__ = null;
var cljs$core$async$reduce_$_state_machine__19164__auto____0 = (function (){
var statearr_20060 = [null,null,null,null,null,null,null,null,null,null];
(statearr_20060[(0)] = cljs$core$async$reduce_$_state_machine__19164__auto__);

(statearr_20060[(1)] = (1));

return statearr_20060;
});
var cljs$core$async$reduce_$_state_machine__19164__auto____1 = (function (state_20041){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_20041);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e20061){if((e20061 instanceof Object)){
var ex__19167__auto__ = e20061;
var statearr_20062_20074 = state_20041;
(statearr_20062_20074[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_20041);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e20061;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__20075 = state_20041;
state_20041 = G__20075;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__19164__auto__ = function(state_20041){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__19164__auto____1.call(this,state_20041);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$reduce_$_state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__19164__auto____0;
cljs$core$async$reduce_$_state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__19164__auto____1;
return cljs$core$async$reduce_$_state_machine__19164__auto__;
})()
;})(switch__19163__auto__,c__19275__auto__))
})();
var state__19277__auto__ = (function (){var statearr_20063 = f__19276__auto__.call(null);
(statearr_20063[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto__);

return statearr_20063;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(c__19275__auto__))
);

return c__19275__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = xform.call(null,f);
var c__19275__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__19275__auto__,f__$1){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (c__19275__auto__,f__$1){
return (function (state_20095){
var state_val_20096 = (state_20095[(1)]);
if((state_val_20096 === (1))){
var inst_20090 = cljs.core.async.reduce.call(null,f__$1,init,ch);
var state_20095__$1 = state_20095;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_20095__$1,(2),inst_20090);
} else {
if((state_val_20096 === (2))){
var inst_20092 = (state_20095[(2)]);
var inst_20093 = f__$1.call(null,inst_20092);
var state_20095__$1 = state_20095;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_20095__$1,inst_20093);
} else {
return null;
}
}
});})(c__19275__auto__,f__$1))
;
return ((function (switch__19163__auto__,c__19275__auto__,f__$1){
return (function() {
var cljs$core$async$transduce_$_state_machine__19164__auto__ = null;
var cljs$core$async$transduce_$_state_machine__19164__auto____0 = (function (){
var statearr_20100 = [null,null,null,null,null,null,null];
(statearr_20100[(0)] = cljs$core$async$transduce_$_state_machine__19164__auto__);

(statearr_20100[(1)] = (1));

return statearr_20100;
});
var cljs$core$async$transduce_$_state_machine__19164__auto____1 = (function (state_20095){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_20095);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e20101){if((e20101 instanceof Object)){
var ex__19167__auto__ = e20101;
var statearr_20102_20104 = state_20095;
(statearr_20102_20104[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_20095);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e20101;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__20105 = state_20095;
state_20095 = G__20105;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__19164__auto__ = function(state_20095){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__19164__auto____1.call(this,state_20095);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$transduce_$_state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__19164__auto____0;
cljs$core$async$transduce_$_state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__19164__auto____1;
return cljs$core$async$transduce_$_state_machine__19164__auto__;
})()
;})(switch__19163__auto__,c__19275__auto__,f__$1))
})();
var state__19277__auto__ = (function (){var statearr_20103 = f__19276__auto__.call(null);
(statearr_20103[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto__);

return statearr_20103;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(c__19275__auto__,f__$1))
);

return c__19275__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var args20106 = [];
var len__8373__auto___20158 = arguments.length;
var i__8374__auto___20159 = (0);
while(true){
if((i__8374__auto___20159 < len__8373__auto___20158)){
args20106.push((arguments[i__8374__auto___20159]));

var G__20160 = (i__8374__auto___20159 + (1));
i__8374__auto___20159 = G__20160;
continue;
} else {
}
break;
}

var G__20108 = args20106.length;
switch (G__20108) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args20106.length)].join('')));

}
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan.call(null,ch,coll,true);
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__19275__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__19275__auto__){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (c__19275__auto__){
return (function (state_20133){
var state_val_20134 = (state_20133[(1)]);
if((state_val_20134 === (7))){
var inst_20115 = (state_20133[(2)]);
var state_20133__$1 = state_20133;
var statearr_20135_20162 = state_20133__$1;
(statearr_20135_20162[(2)] = inst_20115);

(statearr_20135_20162[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20134 === (1))){
var inst_20109 = cljs.core.seq.call(null,coll);
var inst_20110 = inst_20109;
var state_20133__$1 = (function (){var statearr_20136 = state_20133;
(statearr_20136[(7)] = inst_20110);

return statearr_20136;
})();
var statearr_20137_20163 = state_20133__$1;
(statearr_20137_20163[(2)] = null);

(statearr_20137_20163[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20134 === (4))){
var inst_20110 = (state_20133[(7)]);
var inst_20113 = cljs.core.first.call(null,inst_20110);
var state_20133__$1 = state_20133;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_20133__$1,(7),ch,inst_20113);
} else {
if((state_val_20134 === (13))){
var inst_20127 = (state_20133[(2)]);
var state_20133__$1 = state_20133;
var statearr_20138_20164 = state_20133__$1;
(statearr_20138_20164[(2)] = inst_20127);

(statearr_20138_20164[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20134 === (6))){
var inst_20118 = (state_20133[(2)]);
var state_20133__$1 = state_20133;
if(cljs.core.truth_(inst_20118)){
var statearr_20139_20165 = state_20133__$1;
(statearr_20139_20165[(1)] = (8));

} else {
var statearr_20140_20166 = state_20133__$1;
(statearr_20140_20166[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20134 === (3))){
var inst_20131 = (state_20133[(2)]);
var state_20133__$1 = state_20133;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_20133__$1,inst_20131);
} else {
if((state_val_20134 === (12))){
var state_20133__$1 = state_20133;
var statearr_20141_20167 = state_20133__$1;
(statearr_20141_20167[(2)] = null);

(statearr_20141_20167[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20134 === (2))){
var inst_20110 = (state_20133[(7)]);
var state_20133__$1 = state_20133;
if(cljs.core.truth_(inst_20110)){
var statearr_20142_20168 = state_20133__$1;
(statearr_20142_20168[(1)] = (4));

} else {
var statearr_20143_20169 = state_20133__$1;
(statearr_20143_20169[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20134 === (11))){
var inst_20124 = cljs.core.async.close_BANG_.call(null,ch);
var state_20133__$1 = state_20133;
var statearr_20144_20170 = state_20133__$1;
(statearr_20144_20170[(2)] = inst_20124);

(statearr_20144_20170[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20134 === (9))){
var state_20133__$1 = state_20133;
if(cljs.core.truth_(close_QMARK_)){
var statearr_20145_20171 = state_20133__$1;
(statearr_20145_20171[(1)] = (11));

} else {
var statearr_20146_20172 = state_20133__$1;
(statearr_20146_20172[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20134 === (5))){
var inst_20110 = (state_20133[(7)]);
var state_20133__$1 = state_20133;
var statearr_20147_20173 = state_20133__$1;
(statearr_20147_20173[(2)] = inst_20110);

(statearr_20147_20173[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20134 === (10))){
var inst_20129 = (state_20133[(2)]);
var state_20133__$1 = state_20133;
var statearr_20148_20174 = state_20133__$1;
(statearr_20148_20174[(2)] = inst_20129);

(statearr_20148_20174[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20134 === (8))){
var inst_20110 = (state_20133[(7)]);
var inst_20120 = cljs.core.next.call(null,inst_20110);
var inst_20110__$1 = inst_20120;
var state_20133__$1 = (function (){var statearr_20149 = state_20133;
(statearr_20149[(7)] = inst_20110__$1);

return statearr_20149;
})();
var statearr_20150_20175 = state_20133__$1;
(statearr_20150_20175[(2)] = null);

(statearr_20150_20175[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__19275__auto__))
;
return ((function (switch__19163__auto__,c__19275__auto__){
return (function() {
var cljs$core$async$state_machine__19164__auto__ = null;
var cljs$core$async$state_machine__19164__auto____0 = (function (){
var statearr_20154 = [null,null,null,null,null,null,null,null];
(statearr_20154[(0)] = cljs$core$async$state_machine__19164__auto__);

(statearr_20154[(1)] = (1));

return statearr_20154;
});
var cljs$core$async$state_machine__19164__auto____1 = (function (state_20133){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_20133);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e20155){if((e20155 instanceof Object)){
var ex__19167__auto__ = e20155;
var statearr_20156_20176 = state_20133;
(statearr_20156_20176[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_20133);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e20155;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__20177 = state_20133;
state_20133 = G__20177;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$state_machine__19164__auto__ = function(state_20133){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19164__auto____1.call(this,state_20133);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19164__auto____0;
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19164__auto____1;
return cljs$core$async$state_machine__19164__auto__;
})()
;})(switch__19163__auto__,c__19275__auto__))
})();
var state__19277__auto__ = (function (){var statearr_20157 = f__19276__auto__.call(null);
(statearr_20157[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto__);

return statearr_20157;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(c__19275__auto__))
);

return c__19275__auto__;
});

cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3;

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
var ch = cljs.core.async.chan.call(null,cljs.core.bounded_count.call(null,(100),coll));
cljs.core.async.onto_chan.call(null,ch,coll);

return ch;
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((!((_ == null))) && (!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
var x__7928__auto__ = (((_ == null))?null:_);
var m__7929__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__7928__auto__)]);
if(!((m__7929__auto__ == null))){
return m__7929__auto__.call(null,_);
} else {
var m__7929__auto____$1 = (cljs.core.async.muxch_STAR_["_"]);
if(!((m__7929__auto____$1 == null))){
return m__7929__auto____$1.call(null,_);
} else {
throw cljs.core.missing_protocol.call(null,"Mux.muxch*",_);
}
}
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((!((m == null))) && (!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
var x__7928__auto__ = (((m == null))?null:m);
var m__7929__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__7928__auto__)]);
if(!((m__7929__auto__ == null))){
return m__7929__auto__.call(null,m,ch,close_QMARK_);
} else {
var m__7929__auto____$1 = (cljs.core.async.tap_STAR_["_"]);
if(!((m__7929__auto____$1 == null))){
return m__7929__auto____$1.call(null,m,ch,close_QMARK_);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.tap*",m);
}
}
}
});

cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
var x__7928__auto__ = (((m == null))?null:m);
var m__7929__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__7928__auto__)]);
if(!((m__7929__auto__ == null))){
return m__7929__auto__.call(null,m,ch);
} else {
var m__7929__auto____$1 = (cljs.core.async.untap_STAR_["_"]);
if(!((m__7929__auto____$1 == null))){
return m__7929__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.untap*",m);
}
}
}
});

cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((!((m == null))) && (!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
var x__7928__auto__ = (((m == null))?null:m);
var m__7929__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__7928__auto__)]);
if(!((m__7929__auto__ == null))){
return m__7929__auto__.call(null,m);
} else {
var m__7929__auto____$1 = (cljs.core.async.untap_all_STAR_["_"]);
if(!((m__7929__auto____$1 == null))){
return m__7929__auto____$1.call(null,m);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.untap-all*",m);
}
}
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if(typeof cljs.core.async.t_cljs$core$async20403 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async20403 = (function (ch,cs,meta20404){
this.ch = ch;
this.cs = cs;
this.meta20404 = meta20404;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async20403.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs){
return (function (_20405,meta20404__$1){
var self__ = this;
var _20405__$1 = this;
return (new cljs.core.async.t_cljs$core$async20403(self__.ch,self__.cs,meta20404__$1));
});})(cs))
;

cljs.core.async.t_cljs$core$async20403.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs){
return (function (_20405){
var self__ = this;
var _20405__$1 = this;
return self__.meta20404;
});})(cs))
;

cljs.core.async.t_cljs$core$async20403.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async20403.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(cs))
;

cljs.core.async.t_cljs$core$async20403.prototype.cljs$core$async$Mult$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async20403.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = ((function (cs){
return (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async20403.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = ((function (cs){
return (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch__$1);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async20403.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async20403.getBasis = ((function (cs){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta20404","meta20404",-1937497357,null)], null);
});})(cs))
;

cljs.core.async.t_cljs$core$async20403.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async20403.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async20403";

cljs.core.async.t_cljs$core$async20403.cljs$lang$ctorPrWriter = ((function (cs){
return (function (this__7871__auto__,writer__7872__auto__,opt__7873__auto__){
return cljs.core._write.call(null,writer__7872__auto__,"cljs.core.async/t_cljs$core$async20403");
});})(cs))
;

cljs.core.async.__GT_t_cljs$core$async20403 = ((function (cs){
return (function cljs$core$async$mult_$___GT_t_cljs$core$async20403(ch__$1,cs__$1,meta20404){
return (new cljs.core.async.t_cljs$core$async20403(ch__$1,cs__$1,meta20404));
});})(cs))
;

}

return (new cljs.core.async.t_cljs$core$async20403(ch,cs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.call(null,(1));
var dctr = cljs.core.atom.call(null,null);
var done = ((function (cs,m,dchan,dctr){
return (function (_){
if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.call(null,dchan,true);
} else {
return null;
}
});})(cs,m,dchan,dctr))
;
var c__19275__auto___20628 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__19275__auto___20628,cs,m,dchan,dctr,done){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (c__19275__auto___20628,cs,m,dchan,dctr,done){
return (function (state_20540){
var state_val_20541 = (state_20540[(1)]);
if((state_val_20541 === (7))){
var inst_20536 = (state_20540[(2)]);
var state_20540__$1 = state_20540;
var statearr_20542_20629 = state_20540__$1;
(statearr_20542_20629[(2)] = inst_20536);

(statearr_20542_20629[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (20))){
var inst_20439 = (state_20540[(7)]);
var inst_20451 = cljs.core.first.call(null,inst_20439);
var inst_20452 = cljs.core.nth.call(null,inst_20451,(0),null);
var inst_20453 = cljs.core.nth.call(null,inst_20451,(1),null);
var state_20540__$1 = (function (){var statearr_20543 = state_20540;
(statearr_20543[(8)] = inst_20452);

return statearr_20543;
})();
if(cljs.core.truth_(inst_20453)){
var statearr_20544_20630 = state_20540__$1;
(statearr_20544_20630[(1)] = (22));

} else {
var statearr_20545_20631 = state_20540__$1;
(statearr_20545_20631[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (27))){
var inst_20483 = (state_20540[(9)]);
var inst_20408 = (state_20540[(10)]);
var inst_20481 = (state_20540[(11)]);
var inst_20488 = (state_20540[(12)]);
var inst_20488__$1 = cljs.core._nth.call(null,inst_20481,inst_20483);
var inst_20489 = cljs.core.async.put_BANG_.call(null,inst_20488__$1,inst_20408,done);
var state_20540__$1 = (function (){var statearr_20546 = state_20540;
(statearr_20546[(12)] = inst_20488__$1);

return statearr_20546;
})();
if(cljs.core.truth_(inst_20489)){
var statearr_20547_20632 = state_20540__$1;
(statearr_20547_20632[(1)] = (30));

} else {
var statearr_20548_20633 = state_20540__$1;
(statearr_20548_20633[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (1))){
var state_20540__$1 = state_20540;
var statearr_20549_20634 = state_20540__$1;
(statearr_20549_20634[(2)] = null);

(statearr_20549_20634[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (24))){
var inst_20439 = (state_20540[(7)]);
var inst_20458 = (state_20540[(2)]);
var inst_20459 = cljs.core.next.call(null,inst_20439);
var inst_20417 = inst_20459;
var inst_20418 = null;
var inst_20419 = (0);
var inst_20420 = (0);
var state_20540__$1 = (function (){var statearr_20550 = state_20540;
(statearr_20550[(13)] = inst_20420);

(statearr_20550[(14)] = inst_20417);

(statearr_20550[(15)] = inst_20418);

(statearr_20550[(16)] = inst_20458);

(statearr_20550[(17)] = inst_20419);

return statearr_20550;
})();
var statearr_20551_20635 = state_20540__$1;
(statearr_20551_20635[(2)] = null);

(statearr_20551_20635[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (39))){
var state_20540__$1 = state_20540;
var statearr_20555_20636 = state_20540__$1;
(statearr_20555_20636[(2)] = null);

(statearr_20555_20636[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (4))){
var inst_20408 = (state_20540[(10)]);
var inst_20408__$1 = (state_20540[(2)]);
var inst_20409 = (inst_20408__$1 == null);
var state_20540__$1 = (function (){var statearr_20556 = state_20540;
(statearr_20556[(10)] = inst_20408__$1);

return statearr_20556;
})();
if(cljs.core.truth_(inst_20409)){
var statearr_20557_20637 = state_20540__$1;
(statearr_20557_20637[(1)] = (5));

} else {
var statearr_20558_20638 = state_20540__$1;
(statearr_20558_20638[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (15))){
var inst_20420 = (state_20540[(13)]);
var inst_20417 = (state_20540[(14)]);
var inst_20418 = (state_20540[(15)]);
var inst_20419 = (state_20540[(17)]);
var inst_20435 = (state_20540[(2)]);
var inst_20436 = (inst_20420 + (1));
var tmp20552 = inst_20417;
var tmp20553 = inst_20418;
var tmp20554 = inst_20419;
var inst_20417__$1 = tmp20552;
var inst_20418__$1 = tmp20553;
var inst_20419__$1 = tmp20554;
var inst_20420__$1 = inst_20436;
var state_20540__$1 = (function (){var statearr_20559 = state_20540;
(statearr_20559[(13)] = inst_20420__$1);

(statearr_20559[(14)] = inst_20417__$1);

(statearr_20559[(15)] = inst_20418__$1);

(statearr_20559[(18)] = inst_20435);

(statearr_20559[(17)] = inst_20419__$1);

return statearr_20559;
})();
var statearr_20560_20639 = state_20540__$1;
(statearr_20560_20639[(2)] = null);

(statearr_20560_20639[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (21))){
var inst_20462 = (state_20540[(2)]);
var state_20540__$1 = state_20540;
var statearr_20564_20640 = state_20540__$1;
(statearr_20564_20640[(2)] = inst_20462);

(statearr_20564_20640[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (31))){
var inst_20488 = (state_20540[(12)]);
var inst_20492 = done.call(null,null);
var inst_20493 = cljs.core.async.untap_STAR_.call(null,m,inst_20488);
var state_20540__$1 = (function (){var statearr_20565 = state_20540;
(statearr_20565[(19)] = inst_20492);

return statearr_20565;
})();
var statearr_20566_20641 = state_20540__$1;
(statearr_20566_20641[(2)] = inst_20493);

(statearr_20566_20641[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (32))){
var inst_20483 = (state_20540[(9)]);
var inst_20482 = (state_20540[(20)]);
var inst_20480 = (state_20540[(21)]);
var inst_20481 = (state_20540[(11)]);
var inst_20495 = (state_20540[(2)]);
var inst_20496 = (inst_20483 + (1));
var tmp20561 = inst_20482;
var tmp20562 = inst_20480;
var tmp20563 = inst_20481;
var inst_20480__$1 = tmp20562;
var inst_20481__$1 = tmp20563;
var inst_20482__$1 = tmp20561;
var inst_20483__$1 = inst_20496;
var state_20540__$1 = (function (){var statearr_20567 = state_20540;
(statearr_20567[(9)] = inst_20483__$1);

(statearr_20567[(20)] = inst_20482__$1);

(statearr_20567[(21)] = inst_20480__$1);

(statearr_20567[(11)] = inst_20481__$1);

(statearr_20567[(22)] = inst_20495);

return statearr_20567;
})();
var statearr_20568_20642 = state_20540__$1;
(statearr_20568_20642[(2)] = null);

(statearr_20568_20642[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (40))){
var inst_20508 = (state_20540[(23)]);
var inst_20512 = done.call(null,null);
var inst_20513 = cljs.core.async.untap_STAR_.call(null,m,inst_20508);
var state_20540__$1 = (function (){var statearr_20569 = state_20540;
(statearr_20569[(24)] = inst_20512);

return statearr_20569;
})();
var statearr_20570_20643 = state_20540__$1;
(statearr_20570_20643[(2)] = inst_20513);

(statearr_20570_20643[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (33))){
var inst_20499 = (state_20540[(25)]);
var inst_20501 = cljs.core.chunked_seq_QMARK_.call(null,inst_20499);
var state_20540__$1 = state_20540;
if(inst_20501){
var statearr_20571_20644 = state_20540__$1;
(statearr_20571_20644[(1)] = (36));

} else {
var statearr_20572_20645 = state_20540__$1;
(statearr_20572_20645[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (13))){
var inst_20429 = (state_20540[(26)]);
var inst_20432 = cljs.core.async.close_BANG_.call(null,inst_20429);
var state_20540__$1 = state_20540;
var statearr_20573_20646 = state_20540__$1;
(statearr_20573_20646[(2)] = inst_20432);

(statearr_20573_20646[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (22))){
var inst_20452 = (state_20540[(8)]);
var inst_20455 = cljs.core.async.close_BANG_.call(null,inst_20452);
var state_20540__$1 = state_20540;
var statearr_20574_20647 = state_20540__$1;
(statearr_20574_20647[(2)] = inst_20455);

(statearr_20574_20647[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (36))){
var inst_20499 = (state_20540[(25)]);
var inst_20503 = cljs.core.chunk_first.call(null,inst_20499);
var inst_20504 = cljs.core.chunk_rest.call(null,inst_20499);
var inst_20505 = cljs.core.count.call(null,inst_20503);
var inst_20480 = inst_20504;
var inst_20481 = inst_20503;
var inst_20482 = inst_20505;
var inst_20483 = (0);
var state_20540__$1 = (function (){var statearr_20575 = state_20540;
(statearr_20575[(9)] = inst_20483);

(statearr_20575[(20)] = inst_20482);

(statearr_20575[(21)] = inst_20480);

(statearr_20575[(11)] = inst_20481);

return statearr_20575;
})();
var statearr_20576_20648 = state_20540__$1;
(statearr_20576_20648[(2)] = null);

(statearr_20576_20648[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (41))){
var inst_20499 = (state_20540[(25)]);
var inst_20515 = (state_20540[(2)]);
var inst_20516 = cljs.core.next.call(null,inst_20499);
var inst_20480 = inst_20516;
var inst_20481 = null;
var inst_20482 = (0);
var inst_20483 = (0);
var state_20540__$1 = (function (){var statearr_20577 = state_20540;
(statearr_20577[(9)] = inst_20483);

(statearr_20577[(20)] = inst_20482);

(statearr_20577[(21)] = inst_20480);

(statearr_20577[(27)] = inst_20515);

(statearr_20577[(11)] = inst_20481);

return statearr_20577;
})();
var statearr_20578_20649 = state_20540__$1;
(statearr_20578_20649[(2)] = null);

(statearr_20578_20649[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (43))){
var state_20540__$1 = state_20540;
var statearr_20579_20650 = state_20540__$1;
(statearr_20579_20650[(2)] = null);

(statearr_20579_20650[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (29))){
var inst_20524 = (state_20540[(2)]);
var state_20540__$1 = state_20540;
var statearr_20580_20651 = state_20540__$1;
(statearr_20580_20651[(2)] = inst_20524);

(statearr_20580_20651[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (44))){
var inst_20533 = (state_20540[(2)]);
var state_20540__$1 = (function (){var statearr_20581 = state_20540;
(statearr_20581[(28)] = inst_20533);

return statearr_20581;
})();
var statearr_20582_20652 = state_20540__$1;
(statearr_20582_20652[(2)] = null);

(statearr_20582_20652[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (6))){
var inst_20472 = (state_20540[(29)]);
var inst_20471 = cljs.core.deref.call(null,cs);
var inst_20472__$1 = cljs.core.keys.call(null,inst_20471);
var inst_20473 = cljs.core.count.call(null,inst_20472__$1);
var inst_20474 = cljs.core.reset_BANG_.call(null,dctr,inst_20473);
var inst_20479 = cljs.core.seq.call(null,inst_20472__$1);
var inst_20480 = inst_20479;
var inst_20481 = null;
var inst_20482 = (0);
var inst_20483 = (0);
var state_20540__$1 = (function (){var statearr_20583 = state_20540;
(statearr_20583[(9)] = inst_20483);

(statearr_20583[(20)] = inst_20482);

(statearr_20583[(21)] = inst_20480);

(statearr_20583[(30)] = inst_20474);

(statearr_20583[(11)] = inst_20481);

(statearr_20583[(29)] = inst_20472__$1);

return statearr_20583;
})();
var statearr_20584_20653 = state_20540__$1;
(statearr_20584_20653[(2)] = null);

(statearr_20584_20653[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (28))){
var inst_20499 = (state_20540[(25)]);
var inst_20480 = (state_20540[(21)]);
var inst_20499__$1 = cljs.core.seq.call(null,inst_20480);
var state_20540__$1 = (function (){var statearr_20585 = state_20540;
(statearr_20585[(25)] = inst_20499__$1);

return statearr_20585;
})();
if(inst_20499__$1){
var statearr_20586_20654 = state_20540__$1;
(statearr_20586_20654[(1)] = (33));

} else {
var statearr_20587_20655 = state_20540__$1;
(statearr_20587_20655[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (25))){
var inst_20483 = (state_20540[(9)]);
var inst_20482 = (state_20540[(20)]);
var inst_20485 = (inst_20483 < inst_20482);
var inst_20486 = inst_20485;
var state_20540__$1 = state_20540;
if(cljs.core.truth_(inst_20486)){
var statearr_20588_20656 = state_20540__$1;
(statearr_20588_20656[(1)] = (27));

} else {
var statearr_20589_20657 = state_20540__$1;
(statearr_20589_20657[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (34))){
var state_20540__$1 = state_20540;
var statearr_20590_20658 = state_20540__$1;
(statearr_20590_20658[(2)] = null);

(statearr_20590_20658[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (17))){
var state_20540__$1 = state_20540;
var statearr_20591_20659 = state_20540__$1;
(statearr_20591_20659[(2)] = null);

(statearr_20591_20659[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (3))){
var inst_20538 = (state_20540[(2)]);
var state_20540__$1 = state_20540;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_20540__$1,inst_20538);
} else {
if((state_val_20541 === (12))){
var inst_20467 = (state_20540[(2)]);
var state_20540__$1 = state_20540;
var statearr_20592_20660 = state_20540__$1;
(statearr_20592_20660[(2)] = inst_20467);

(statearr_20592_20660[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (2))){
var state_20540__$1 = state_20540;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_20540__$1,(4),ch);
} else {
if((state_val_20541 === (23))){
var state_20540__$1 = state_20540;
var statearr_20593_20661 = state_20540__$1;
(statearr_20593_20661[(2)] = null);

(statearr_20593_20661[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (35))){
var inst_20522 = (state_20540[(2)]);
var state_20540__$1 = state_20540;
var statearr_20594_20662 = state_20540__$1;
(statearr_20594_20662[(2)] = inst_20522);

(statearr_20594_20662[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (19))){
var inst_20439 = (state_20540[(7)]);
var inst_20443 = cljs.core.chunk_first.call(null,inst_20439);
var inst_20444 = cljs.core.chunk_rest.call(null,inst_20439);
var inst_20445 = cljs.core.count.call(null,inst_20443);
var inst_20417 = inst_20444;
var inst_20418 = inst_20443;
var inst_20419 = inst_20445;
var inst_20420 = (0);
var state_20540__$1 = (function (){var statearr_20595 = state_20540;
(statearr_20595[(13)] = inst_20420);

(statearr_20595[(14)] = inst_20417);

(statearr_20595[(15)] = inst_20418);

(statearr_20595[(17)] = inst_20419);

return statearr_20595;
})();
var statearr_20596_20663 = state_20540__$1;
(statearr_20596_20663[(2)] = null);

(statearr_20596_20663[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (11))){
var inst_20417 = (state_20540[(14)]);
var inst_20439 = (state_20540[(7)]);
var inst_20439__$1 = cljs.core.seq.call(null,inst_20417);
var state_20540__$1 = (function (){var statearr_20597 = state_20540;
(statearr_20597[(7)] = inst_20439__$1);

return statearr_20597;
})();
if(inst_20439__$1){
var statearr_20598_20664 = state_20540__$1;
(statearr_20598_20664[(1)] = (16));

} else {
var statearr_20599_20665 = state_20540__$1;
(statearr_20599_20665[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (9))){
var inst_20469 = (state_20540[(2)]);
var state_20540__$1 = state_20540;
var statearr_20600_20666 = state_20540__$1;
(statearr_20600_20666[(2)] = inst_20469);

(statearr_20600_20666[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (5))){
var inst_20415 = cljs.core.deref.call(null,cs);
var inst_20416 = cljs.core.seq.call(null,inst_20415);
var inst_20417 = inst_20416;
var inst_20418 = null;
var inst_20419 = (0);
var inst_20420 = (0);
var state_20540__$1 = (function (){var statearr_20601 = state_20540;
(statearr_20601[(13)] = inst_20420);

(statearr_20601[(14)] = inst_20417);

(statearr_20601[(15)] = inst_20418);

(statearr_20601[(17)] = inst_20419);

return statearr_20601;
})();
var statearr_20602_20667 = state_20540__$1;
(statearr_20602_20667[(2)] = null);

(statearr_20602_20667[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (14))){
var state_20540__$1 = state_20540;
var statearr_20603_20668 = state_20540__$1;
(statearr_20603_20668[(2)] = null);

(statearr_20603_20668[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (45))){
var inst_20530 = (state_20540[(2)]);
var state_20540__$1 = state_20540;
var statearr_20604_20669 = state_20540__$1;
(statearr_20604_20669[(2)] = inst_20530);

(statearr_20604_20669[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (26))){
var inst_20472 = (state_20540[(29)]);
var inst_20526 = (state_20540[(2)]);
var inst_20527 = cljs.core.seq.call(null,inst_20472);
var state_20540__$1 = (function (){var statearr_20605 = state_20540;
(statearr_20605[(31)] = inst_20526);

return statearr_20605;
})();
if(inst_20527){
var statearr_20606_20670 = state_20540__$1;
(statearr_20606_20670[(1)] = (42));

} else {
var statearr_20607_20671 = state_20540__$1;
(statearr_20607_20671[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (16))){
var inst_20439 = (state_20540[(7)]);
var inst_20441 = cljs.core.chunked_seq_QMARK_.call(null,inst_20439);
var state_20540__$1 = state_20540;
if(inst_20441){
var statearr_20608_20672 = state_20540__$1;
(statearr_20608_20672[(1)] = (19));

} else {
var statearr_20609_20673 = state_20540__$1;
(statearr_20609_20673[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (38))){
var inst_20519 = (state_20540[(2)]);
var state_20540__$1 = state_20540;
var statearr_20610_20674 = state_20540__$1;
(statearr_20610_20674[(2)] = inst_20519);

(statearr_20610_20674[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (30))){
var state_20540__$1 = state_20540;
var statearr_20611_20675 = state_20540__$1;
(statearr_20611_20675[(2)] = null);

(statearr_20611_20675[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (10))){
var inst_20420 = (state_20540[(13)]);
var inst_20418 = (state_20540[(15)]);
var inst_20428 = cljs.core._nth.call(null,inst_20418,inst_20420);
var inst_20429 = cljs.core.nth.call(null,inst_20428,(0),null);
var inst_20430 = cljs.core.nth.call(null,inst_20428,(1),null);
var state_20540__$1 = (function (){var statearr_20612 = state_20540;
(statearr_20612[(26)] = inst_20429);

return statearr_20612;
})();
if(cljs.core.truth_(inst_20430)){
var statearr_20613_20676 = state_20540__$1;
(statearr_20613_20676[(1)] = (13));

} else {
var statearr_20614_20677 = state_20540__$1;
(statearr_20614_20677[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (18))){
var inst_20465 = (state_20540[(2)]);
var state_20540__$1 = state_20540;
var statearr_20615_20678 = state_20540__$1;
(statearr_20615_20678[(2)] = inst_20465);

(statearr_20615_20678[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (42))){
var state_20540__$1 = state_20540;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_20540__$1,(45),dchan);
} else {
if((state_val_20541 === (37))){
var inst_20499 = (state_20540[(25)]);
var inst_20408 = (state_20540[(10)]);
var inst_20508 = (state_20540[(23)]);
var inst_20508__$1 = cljs.core.first.call(null,inst_20499);
var inst_20509 = cljs.core.async.put_BANG_.call(null,inst_20508__$1,inst_20408,done);
var state_20540__$1 = (function (){var statearr_20616 = state_20540;
(statearr_20616[(23)] = inst_20508__$1);

return statearr_20616;
})();
if(cljs.core.truth_(inst_20509)){
var statearr_20617_20679 = state_20540__$1;
(statearr_20617_20679[(1)] = (39));

} else {
var statearr_20618_20680 = state_20540__$1;
(statearr_20618_20680[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20541 === (8))){
var inst_20420 = (state_20540[(13)]);
var inst_20419 = (state_20540[(17)]);
var inst_20422 = (inst_20420 < inst_20419);
var inst_20423 = inst_20422;
var state_20540__$1 = state_20540;
if(cljs.core.truth_(inst_20423)){
var statearr_20619_20681 = state_20540__$1;
(statearr_20619_20681[(1)] = (10));

} else {
var statearr_20620_20682 = state_20540__$1;
(statearr_20620_20682[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__19275__auto___20628,cs,m,dchan,dctr,done))
;
return ((function (switch__19163__auto__,c__19275__auto___20628,cs,m,dchan,dctr,done){
return (function() {
var cljs$core$async$mult_$_state_machine__19164__auto__ = null;
var cljs$core$async$mult_$_state_machine__19164__auto____0 = (function (){
var statearr_20624 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_20624[(0)] = cljs$core$async$mult_$_state_machine__19164__auto__);

(statearr_20624[(1)] = (1));

return statearr_20624;
});
var cljs$core$async$mult_$_state_machine__19164__auto____1 = (function (state_20540){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_20540);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e20625){if((e20625 instanceof Object)){
var ex__19167__auto__ = e20625;
var statearr_20626_20683 = state_20540;
(statearr_20626_20683[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_20540);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e20625;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__20684 = state_20540;
state_20540 = G__20684;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__19164__auto__ = function(state_20540){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__19164__auto____1.call(this,state_20540);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$mult_$_state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__19164__auto____0;
cljs$core$async$mult_$_state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__19164__auto____1;
return cljs$core$async$mult_$_state_machine__19164__auto__;
})()
;})(switch__19163__auto__,c__19275__auto___20628,cs,m,dchan,dctr,done))
})();
var state__19277__auto__ = (function (){var statearr_20627 = f__19276__auto__.call(null);
(statearr_20627[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto___20628);

return statearr_20627;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(c__19275__auto___20628,cs,m,dchan,dctr,done))
);


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var args20685 = [];
var len__8373__auto___20688 = arguments.length;
var i__8374__auto___20689 = (0);
while(true){
if((i__8374__auto___20689 < len__8373__auto___20688)){
args20685.push((arguments[i__8374__auto___20689]));

var G__20690 = (i__8374__auto___20689 + (1));
i__8374__auto___20689 = G__20690;
continue;
} else {
}
break;
}

var G__20687 = args20685.length;
switch (G__20687) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args20685.length)].join('')));

}
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.call(null,mult,ch,true);
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_.call(null,mult,ch,close_QMARK_);

return ch;
});

cljs.core.async.tap.cljs$lang$maxFixedArity = 3;

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_.call(null,mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_.call(null,mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
var x__7928__auto__ = (((m == null))?null:m);
var m__7929__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__7928__auto__)]);
if(!((m__7929__auto__ == null))){
return m__7929__auto__.call(null,m,ch);
} else {
var m__7929__auto____$1 = (cljs.core.async.admix_STAR_["_"]);
if(!((m__7929__auto____$1 == null))){
return m__7929__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.admix*",m);
}
}
}
});

cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
var x__7928__auto__ = (((m == null))?null:m);
var m__7929__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__7928__auto__)]);
if(!((m__7929__auto__ == null))){
return m__7929__auto__.call(null,m,ch);
} else {
var m__7929__auto____$1 = (cljs.core.async.unmix_STAR_["_"]);
if(!((m__7929__auto____$1 == null))){
return m__7929__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.unmix*",m);
}
}
}
});

cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((!((m == null))) && (!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
var x__7928__auto__ = (((m == null))?null:m);
var m__7929__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__7928__auto__)]);
if(!((m__7929__auto__ == null))){
return m__7929__auto__.call(null,m);
} else {
var m__7929__auto____$1 = (cljs.core.async.unmix_all_STAR_["_"]);
if(!((m__7929__auto____$1 == null))){
return m__7929__auto____$1.call(null,m);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.unmix-all*",m);
}
}
}
});

cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((!((m == null))) && (!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
var x__7928__auto__ = (((m == null))?null:m);
var m__7929__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__7928__auto__)]);
if(!((m__7929__auto__ == null))){
return m__7929__auto__.call(null,m,state_map);
} else {
var m__7929__auto____$1 = (cljs.core.async.toggle_STAR_["_"]);
if(!((m__7929__auto____$1 == null))){
return m__7929__auto____$1.call(null,m,state_map);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.toggle*",m);
}
}
}
});

cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((!((m == null))) && (!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
var x__7928__auto__ = (((m == null))?null:m);
var m__7929__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__7928__auto__)]);
if(!((m__7929__auto__ == null))){
return m__7929__auto__.call(null,m,mode);
} else {
var m__7929__auto____$1 = (cljs.core.async.solo_mode_STAR_["_"]);
if(!((m__7929__auto____$1 == null))){
return m__7929__auto____$1.call(null,m,mode);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.solo-mode*",m);
}
}
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__8380__auto__ = [];
var len__8373__auto___20702 = arguments.length;
var i__8374__auto___20703 = (0);
while(true){
if((i__8374__auto___20703 < len__8373__auto___20702)){
args__8380__auto__.push((arguments[i__8374__auto___20703]));

var G__20704 = (i__8374__auto___20703 + (1));
i__8374__auto___20703 = G__20704;
continue;
} else {
}
break;
}

var argseq__8381__auto__ = ((((3) < args__8380__auto__.length))?(new cljs.core.IndexedSeq(args__8380__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__8381__auto__);
});

cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__20696){
var map__20697 = p__20696;
var map__20697__$1 = ((((!((map__20697 == null)))?((((map__20697.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__20697.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__20697):map__20697);
var opts = map__20697__$1;
var statearr_20699_20705 = state;
(statearr_20699_20705[cljs.core.async.impl.ioc_helpers.STATE_IDX] = cont_block);


var temp__4657__auto__ = cljs.core.async.do_alts.call(null,((function (map__20697,map__20697__$1,opts){
return (function (val){
var statearr_20700_20706 = state;
(statearr_20700_20706[cljs.core.async.impl.ioc_helpers.VALUE_IDX] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state);
});})(map__20697,map__20697__$1,opts))
,ports,opts);
if(cljs.core.truth_(temp__4657__auto__)){
var cb = temp__4657__auto__;
var statearr_20701_20707 = state;
(statearr_20701_20707[cljs.core.async.impl.ioc_helpers.VALUE_IDX] = cljs.core.deref.call(null,cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
});

cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3);

cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq20692){
var G__20693 = cljs.core.first.call(null,seq20692);
var seq20692__$1 = cljs.core.next.call(null,seq20692);
var G__20694 = cljs.core.first.call(null,seq20692__$1);
var seq20692__$2 = cljs.core.next.call(null,seq20692__$1);
var G__20695 = cljs.core.first.call(null,seq20692__$2);
var seq20692__$3 = cljs.core.next.call(null,seq20692__$2);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__20693,G__20694,G__20695,seq20692__$3);
});

/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.call(null,solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.call(null,new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.call(null);
var changed = ((function (cs,solo_modes,attrs,solo_mode,change){
return (function (){
return cljs.core.async.put_BANG_.call(null,change,true);
});})(cs,solo_modes,attrs,solo_mode,change))
;
var pick = ((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (attr,chs){
return cljs.core.reduce_kv.call(null,((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (ret,c,v){
if(cljs.core.truth_(attr.call(null,v))){
return cljs.core.conj.call(null,ret,c);
} else {
return ret;
}
});})(cs,solo_modes,attrs,solo_mode,change,changed))
,cljs.core.PersistentHashSet.EMPTY,chs);
});})(cs,solo_modes,attrs,solo_mode,change,changed))
;
var calc_state = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick){
return (function (){
var chs = cljs.core.deref.call(null,cs);
var mode = cljs.core.deref.call(null,solo_mode);
var solos = pick.call(null,new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick.call(null,new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick.call(null,new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.call(null,(((cljs.core._EQ_.call(null,mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && (!(cljs.core.empty_QMARK_.call(null,solos))))?cljs.core.vec.call(null,solos):cljs.core.vec.call(null,cljs.core.remove.call(null,pauses,cljs.core.keys.call(null,chs)))),change)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick))
;
var m = (function (){
if(typeof cljs.core.async.t_cljs$core$async20875 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async20875 = (function (out,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,meta20876){
this.out = out;
this.cs = cs;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.solo_mode = solo_mode;
this.change = change;
this.changed = changed;
this.pick = pick;
this.calc_state = calc_state;
this.meta20876 = meta20876;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async20875.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_20877,meta20876__$1){
var self__ = this;
var _20877__$1 = this;
return (new cljs.core.async.t_cljs$core$async20875(self__.out,self__.cs,self__.solo_modes,self__.attrs,self__.solo_mode,self__.change,self__.changed,self__.pick,self__.calc_state,meta20876__$1));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async20875.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_20877){
var self__ = this;
var _20877__$1 = this;
return self__.meta20876;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async20875.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async20875.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async20875.prototype.cljs$core$async$Mix$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async20875.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async20875.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async20875.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async20875.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.partial.call(null,cljs.core.merge_with,cljs.core.merge),state_map);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async20875.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_(self__.solo_modes.call(null,mode))){
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Assert failed: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1([cljs.core.str.cljs$core$IFn$_invoke$arity$1("mode must be one of: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)].join('')),cljs.core.str.cljs$core$IFn$_invoke$arity$1("\n"),cljs.core.str.cljs$core$IFn$_invoke$arity$1("(solo-modes mode)")].join('')));
}

cljs.core.reset_BANG_.call(null,self__.solo_mode,mode);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async20875.getBasis = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"change","change",477485025,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"meta20876","meta20876",-1287726593,null)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async20875.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async20875.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async20875";

cljs.core.async.t_cljs$core$async20875.cljs$lang$ctorPrWriter = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (this__7871__auto__,writer__7872__auto__,opt__7873__auto__){
return cljs.core._write.call(null,writer__7872__auto__,"cljs.core.async/t_cljs$core$async20875");
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.__GT_t_cljs$core$async20875 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function cljs$core$async$mix_$___GT_t_cljs$core$async20875(out__$1,cs__$1,solo_modes__$1,attrs__$1,solo_mode__$1,change__$1,changed__$1,pick__$1,calc_state__$1,meta20876){
return (new cljs.core.async.t_cljs$core$async20875(out__$1,cs__$1,solo_modes__$1,attrs__$1,solo_mode__$1,change__$1,changed__$1,pick__$1,calc_state__$1,meta20876));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

}

return (new cljs.core.async.t_cljs$core$async20875(out,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__19275__auto___21042 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__19275__auto___21042,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (c__19275__auto___21042,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (state_20979){
var state_val_20980 = (state_20979[(1)]);
if((state_val_20980 === (7))){
var inst_20894 = (state_20979[(2)]);
var state_20979__$1 = state_20979;
var statearr_20981_21043 = state_20979__$1;
(statearr_20981_21043[(2)] = inst_20894);

(statearr_20981_21043[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (20))){
var inst_20906 = (state_20979[(7)]);
var state_20979__$1 = state_20979;
var statearr_20982_21044 = state_20979__$1;
(statearr_20982_21044[(2)] = inst_20906);

(statearr_20982_21044[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (27))){
var state_20979__$1 = state_20979;
var statearr_20983_21045 = state_20979__$1;
(statearr_20983_21045[(2)] = null);

(statearr_20983_21045[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (1))){
var inst_20881 = (state_20979[(8)]);
var inst_20881__$1 = calc_state.call(null);
var inst_20883 = (inst_20881__$1 == null);
var inst_20884 = cljs.core.not.call(null,inst_20883);
var state_20979__$1 = (function (){var statearr_20984 = state_20979;
(statearr_20984[(8)] = inst_20881__$1);

return statearr_20984;
})();
if(inst_20884){
var statearr_20985_21046 = state_20979__$1;
(statearr_20985_21046[(1)] = (2));

} else {
var statearr_20986_21047 = state_20979__$1;
(statearr_20986_21047[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (24))){
var inst_20930 = (state_20979[(9)]);
var inst_20939 = (state_20979[(10)]);
var inst_20953 = (state_20979[(11)]);
var inst_20953__$1 = inst_20930.call(null,inst_20939);
var state_20979__$1 = (function (){var statearr_20987 = state_20979;
(statearr_20987[(11)] = inst_20953__$1);

return statearr_20987;
})();
if(cljs.core.truth_(inst_20953__$1)){
var statearr_20988_21048 = state_20979__$1;
(statearr_20988_21048[(1)] = (29));

} else {
var statearr_20989_21049 = state_20979__$1;
(statearr_20989_21049[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (4))){
var inst_20897 = (state_20979[(2)]);
var state_20979__$1 = state_20979;
if(cljs.core.truth_(inst_20897)){
var statearr_20990_21050 = state_20979__$1;
(statearr_20990_21050[(1)] = (8));

} else {
var statearr_20991_21051 = state_20979__$1;
(statearr_20991_21051[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (15))){
var inst_20924 = (state_20979[(2)]);
var state_20979__$1 = state_20979;
if(cljs.core.truth_(inst_20924)){
var statearr_20992_21052 = state_20979__$1;
(statearr_20992_21052[(1)] = (19));

} else {
var statearr_20993_21053 = state_20979__$1;
(statearr_20993_21053[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (21))){
var inst_20929 = (state_20979[(12)]);
var inst_20929__$1 = (state_20979[(2)]);
var inst_20930 = cljs.core.get.call(null,inst_20929__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_20931 = cljs.core.get.call(null,inst_20929__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_20932 = cljs.core.get.call(null,inst_20929__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_20979__$1 = (function (){var statearr_20994 = state_20979;
(statearr_20994[(9)] = inst_20930);

(statearr_20994[(12)] = inst_20929__$1);

(statearr_20994[(13)] = inst_20931);

return statearr_20994;
})();
return cljs.core.async.ioc_alts_BANG_.call(null,state_20979__$1,(22),inst_20932);
} else {
if((state_val_20980 === (31))){
var inst_20961 = (state_20979[(2)]);
var state_20979__$1 = state_20979;
if(cljs.core.truth_(inst_20961)){
var statearr_20995_21054 = state_20979__$1;
(statearr_20995_21054[(1)] = (32));

} else {
var statearr_20996_21055 = state_20979__$1;
(statearr_20996_21055[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (32))){
var inst_20938 = (state_20979[(14)]);
var state_20979__$1 = state_20979;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_20979__$1,(35),out,inst_20938);
} else {
if((state_val_20980 === (33))){
var inst_20929 = (state_20979[(12)]);
var inst_20906 = inst_20929;
var state_20979__$1 = (function (){var statearr_20997 = state_20979;
(statearr_20997[(7)] = inst_20906);

return statearr_20997;
})();
var statearr_20998_21056 = state_20979__$1;
(statearr_20998_21056[(2)] = null);

(statearr_20998_21056[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (13))){
var inst_20906 = (state_20979[(7)]);
var inst_20913 = inst_20906.cljs$lang$protocol_mask$partition0$;
var inst_20914 = (inst_20913 & (64));
var inst_20915 = inst_20906.cljs$core$ISeq$;
var inst_20916 = (cljs.core.PROTOCOL_SENTINEL === inst_20915);
var inst_20917 = (inst_20914) || (inst_20916);
var state_20979__$1 = state_20979;
if(cljs.core.truth_(inst_20917)){
var statearr_20999_21057 = state_20979__$1;
(statearr_20999_21057[(1)] = (16));

} else {
var statearr_21000_21058 = state_20979__$1;
(statearr_21000_21058[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (22))){
var inst_20939 = (state_20979[(10)]);
var inst_20938 = (state_20979[(14)]);
var inst_20937 = (state_20979[(2)]);
var inst_20938__$1 = cljs.core.nth.call(null,inst_20937,(0),null);
var inst_20939__$1 = cljs.core.nth.call(null,inst_20937,(1),null);
var inst_20940 = (inst_20938__$1 == null);
var inst_20941 = cljs.core._EQ_.call(null,inst_20939__$1,change);
var inst_20942 = (inst_20940) || (inst_20941);
var state_20979__$1 = (function (){var statearr_21001 = state_20979;
(statearr_21001[(10)] = inst_20939__$1);

(statearr_21001[(14)] = inst_20938__$1);

return statearr_21001;
})();
if(cljs.core.truth_(inst_20942)){
var statearr_21002_21059 = state_20979__$1;
(statearr_21002_21059[(1)] = (23));

} else {
var statearr_21003_21060 = state_20979__$1;
(statearr_21003_21060[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (36))){
var inst_20929 = (state_20979[(12)]);
var inst_20906 = inst_20929;
var state_20979__$1 = (function (){var statearr_21004 = state_20979;
(statearr_21004[(7)] = inst_20906);

return statearr_21004;
})();
var statearr_21005_21061 = state_20979__$1;
(statearr_21005_21061[(2)] = null);

(statearr_21005_21061[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (29))){
var inst_20953 = (state_20979[(11)]);
var state_20979__$1 = state_20979;
var statearr_21006_21062 = state_20979__$1;
(statearr_21006_21062[(2)] = inst_20953);

(statearr_21006_21062[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (6))){
var state_20979__$1 = state_20979;
var statearr_21007_21063 = state_20979__$1;
(statearr_21007_21063[(2)] = false);

(statearr_21007_21063[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (28))){
var inst_20949 = (state_20979[(2)]);
var inst_20950 = calc_state.call(null);
var inst_20906 = inst_20950;
var state_20979__$1 = (function (){var statearr_21008 = state_20979;
(statearr_21008[(7)] = inst_20906);

(statearr_21008[(15)] = inst_20949);

return statearr_21008;
})();
var statearr_21009_21064 = state_20979__$1;
(statearr_21009_21064[(2)] = null);

(statearr_21009_21064[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (25))){
var inst_20975 = (state_20979[(2)]);
var state_20979__$1 = state_20979;
var statearr_21010_21065 = state_20979__$1;
(statearr_21010_21065[(2)] = inst_20975);

(statearr_21010_21065[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (34))){
var inst_20973 = (state_20979[(2)]);
var state_20979__$1 = state_20979;
var statearr_21011_21066 = state_20979__$1;
(statearr_21011_21066[(2)] = inst_20973);

(statearr_21011_21066[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (17))){
var state_20979__$1 = state_20979;
var statearr_21012_21067 = state_20979__$1;
(statearr_21012_21067[(2)] = false);

(statearr_21012_21067[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (3))){
var state_20979__$1 = state_20979;
var statearr_21013_21068 = state_20979__$1;
(statearr_21013_21068[(2)] = false);

(statearr_21013_21068[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (12))){
var inst_20977 = (state_20979[(2)]);
var state_20979__$1 = state_20979;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_20979__$1,inst_20977);
} else {
if((state_val_20980 === (2))){
var inst_20881 = (state_20979[(8)]);
var inst_20886 = inst_20881.cljs$lang$protocol_mask$partition0$;
var inst_20887 = (inst_20886 & (64));
var inst_20888 = inst_20881.cljs$core$ISeq$;
var inst_20889 = (cljs.core.PROTOCOL_SENTINEL === inst_20888);
var inst_20890 = (inst_20887) || (inst_20889);
var state_20979__$1 = state_20979;
if(cljs.core.truth_(inst_20890)){
var statearr_21014_21069 = state_20979__$1;
(statearr_21014_21069[(1)] = (5));

} else {
var statearr_21015_21070 = state_20979__$1;
(statearr_21015_21070[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (23))){
var inst_20938 = (state_20979[(14)]);
var inst_20944 = (inst_20938 == null);
var state_20979__$1 = state_20979;
if(cljs.core.truth_(inst_20944)){
var statearr_21016_21071 = state_20979__$1;
(statearr_21016_21071[(1)] = (26));

} else {
var statearr_21017_21072 = state_20979__$1;
(statearr_21017_21072[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (35))){
var inst_20964 = (state_20979[(2)]);
var state_20979__$1 = state_20979;
if(cljs.core.truth_(inst_20964)){
var statearr_21018_21073 = state_20979__$1;
(statearr_21018_21073[(1)] = (36));

} else {
var statearr_21019_21074 = state_20979__$1;
(statearr_21019_21074[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (19))){
var inst_20906 = (state_20979[(7)]);
var inst_20926 = cljs.core.apply.call(null,cljs.core.hash_map,inst_20906);
var state_20979__$1 = state_20979;
var statearr_21020_21075 = state_20979__$1;
(statearr_21020_21075[(2)] = inst_20926);

(statearr_21020_21075[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (11))){
var inst_20906 = (state_20979[(7)]);
var inst_20910 = (inst_20906 == null);
var inst_20911 = cljs.core.not.call(null,inst_20910);
var state_20979__$1 = state_20979;
if(inst_20911){
var statearr_21021_21076 = state_20979__$1;
(statearr_21021_21076[(1)] = (13));

} else {
var statearr_21022_21077 = state_20979__$1;
(statearr_21022_21077[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (9))){
var inst_20881 = (state_20979[(8)]);
var state_20979__$1 = state_20979;
var statearr_21023_21078 = state_20979__$1;
(statearr_21023_21078[(2)] = inst_20881);

(statearr_21023_21078[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (5))){
var state_20979__$1 = state_20979;
var statearr_21024_21079 = state_20979__$1;
(statearr_21024_21079[(2)] = true);

(statearr_21024_21079[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (14))){
var state_20979__$1 = state_20979;
var statearr_21025_21080 = state_20979__$1;
(statearr_21025_21080[(2)] = false);

(statearr_21025_21080[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (26))){
var inst_20939 = (state_20979[(10)]);
var inst_20946 = cljs.core.swap_BANG_.call(null,cs,cljs.core.dissoc,inst_20939);
var state_20979__$1 = state_20979;
var statearr_21026_21081 = state_20979__$1;
(statearr_21026_21081[(2)] = inst_20946);

(statearr_21026_21081[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (16))){
var state_20979__$1 = state_20979;
var statearr_21027_21082 = state_20979__$1;
(statearr_21027_21082[(2)] = true);

(statearr_21027_21082[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (38))){
var inst_20969 = (state_20979[(2)]);
var state_20979__$1 = state_20979;
var statearr_21028_21083 = state_20979__$1;
(statearr_21028_21083[(2)] = inst_20969);

(statearr_21028_21083[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (30))){
var inst_20930 = (state_20979[(9)]);
var inst_20939 = (state_20979[(10)]);
var inst_20931 = (state_20979[(13)]);
var inst_20956 = cljs.core.empty_QMARK_.call(null,inst_20930);
var inst_20957 = inst_20931.call(null,inst_20939);
var inst_20958 = cljs.core.not.call(null,inst_20957);
var inst_20959 = (inst_20956) && (inst_20958);
var state_20979__$1 = state_20979;
var statearr_21029_21084 = state_20979__$1;
(statearr_21029_21084[(2)] = inst_20959);

(statearr_21029_21084[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (10))){
var inst_20881 = (state_20979[(8)]);
var inst_20902 = (state_20979[(2)]);
var inst_20903 = cljs.core.get.call(null,inst_20902,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_20904 = cljs.core.get.call(null,inst_20902,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_20905 = cljs.core.get.call(null,inst_20902,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_20906 = inst_20881;
var state_20979__$1 = (function (){var statearr_21030 = state_20979;
(statearr_21030[(16)] = inst_20903);

(statearr_21030[(17)] = inst_20905);

(statearr_21030[(18)] = inst_20904);

(statearr_21030[(7)] = inst_20906);

return statearr_21030;
})();
var statearr_21031_21085 = state_20979__$1;
(statearr_21031_21085[(2)] = null);

(statearr_21031_21085[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (18))){
var inst_20921 = (state_20979[(2)]);
var state_20979__$1 = state_20979;
var statearr_21032_21086 = state_20979__$1;
(statearr_21032_21086[(2)] = inst_20921);

(statearr_21032_21086[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (37))){
var state_20979__$1 = state_20979;
var statearr_21033_21087 = state_20979__$1;
(statearr_21033_21087[(2)] = null);

(statearr_21033_21087[(1)] = (38));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_20980 === (8))){
var inst_20881 = (state_20979[(8)]);
var inst_20899 = cljs.core.apply.call(null,cljs.core.hash_map,inst_20881);
var state_20979__$1 = state_20979;
var statearr_21034_21088 = state_20979__$1;
(statearr_21034_21088[(2)] = inst_20899);

(statearr_21034_21088[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__19275__auto___21042,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
;
return ((function (switch__19163__auto__,c__19275__auto___21042,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function() {
var cljs$core$async$mix_$_state_machine__19164__auto__ = null;
var cljs$core$async$mix_$_state_machine__19164__auto____0 = (function (){
var statearr_21038 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_21038[(0)] = cljs$core$async$mix_$_state_machine__19164__auto__);

(statearr_21038[(1)] = (1));

return statearr_21038;
});
var cljs$core$async$mix_$_state_machine__19164__auto____1 = (function (state_20979){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_20979);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e21039){if((e21039 instanceof Object)){
var ex__19167__auto__ = e21039;
var statearr_21040_21089 = state_20979;
(statearr_21040_21089[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_20979);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e21039;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__21090 = state_20979;
state_20979 = G__21090;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__19164__auto__ = function(state_20979){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__19164__auto____1.call(this,state_20979);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$mix_$_state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__19164__auto____0;
cljs$core$async$mix_$_state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__19164__auto____1;
return cljs$core$async$mix_$_state_machine__19164__auto__;
})()
;})(switch__19163__auto__,c__19275__auto___21042,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
})();
var state__19277__auto__ = (function (){var statearr_21041 = f__19276__auto__.call(null);
(statearr_21041[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto___21042);

return statearr_21041;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(c__19275__auto___21042,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
);


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_.call(null,mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_.call(null,mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_.call(null,mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_.call(null,mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_.call(null,mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((!((p == null))) && (!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
var x__7928__auto__ = (((p == null))?null:p);
var m__7929__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__7928__auto__)]);
if(!((m__7929__auto__ == null))){
return m__7929__auto__.call(null,p,v,ch,close_QMARK_);
} else {
var m__7929__auto____$1 = (cljs.core.async.sub_STAR_["_"]);
if(!((m__7929__auto____$1 == null))){
return m__7929__auto____$1.call(null,p,v,ch,close_QMARK_);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.sub*",p);
}
}
}
});

cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
var x__7928__auto__ = (((p == null))?null:p);
var m__7929__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__7928__auto__)]);
if(!((m__7929__auto__ == null))){
return m__7929__auto__.call(null,p,v,ch);
} else {
var m__7929__auto____$1 = (cljs.core.async.unsub_STAR_["_"]);
if(!((m__7929__auto____$1 == null))){
return m__7929__auto____$1.call(null,p,v,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var args21091 = [];
var len__8373__auto___21094 = arguments.length;
var i__8374__auto___21095 = (0);
while(true){
if((i__8374__auto___21095 < len__8373__auto___21094)){
args21091.push((arguments[i__8374__auto___21095]));

var G__21096 = (i__8374__auto___21095 + (1));
i__8374__auto___21095 = G__21096;
continue;
} else {
}
break;
}

var G__21093 = args21091.length;
switch (G__21093) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args21091.length)].join('')));

}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
var x__7928__auto__ = (((p == null))?null:p);
var m__7929__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__7928__auto__)]);
if(!((m__7929__auto__ == null))){
return m__7929__auto__.call(null,p);
} else {
var m__7929__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);
if(!((m__7929__auto____$1 == null))){
return m__7929__auto____$1.call(null,p);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
var x__7928__auto__ = (((p == null))?null:p);
var m__7929__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__7928__auto__)]);
if(!((m__7929__auto__ == null))){
return m__7929__auto__.call(null,p,v);
} else {
var m__7929__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);
if(!((m__7929__auto____$1 == null))){
return m__7929__auto____$1.call(null,p,v);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2;


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var args21099 = [];
var len__8373__auto___21224 = arguments.length;
var i__8374__auto___21225 = (0);
while(true){
if((i__8374__auto___21225 < len__8373__auto___21224)){
args21099.push((arguments[i__8374__auto___21225]));

var G__21226 = (i__8374__auto___21225 + (1));
i__8374__auto___21225 = G__21226;
continue;
} else {
}
break;
}

var G__21101 = args21099.length;
switch (G__21101) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args21099.length)].join('')));

}
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.call(null,ch,topic_fn,cljs.core.constantly.call(null,null));
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = ((function (mults){
return (function (topic){
var or__7260__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,mults),topic);
if(cljs.core.truth_(or__7260__auto__)){
return or__7260__auto__;
} else {
return cljs.core.get.call(null,cljs.core.swap_BANG_.call(null,mults,((function (or__7260__auto__,mults){
return (function (p1__21098_SHARP_){
if(cljs.core.truth_(p1__21098_SHARP_.call(null,topic))){
return p1__21098_SHARP_;
} else {
return cljs.core.assoc.call(null,p1__21098_SHARP_,topic,cljs.core.async.mult.call(null,cljs.core.async.chan.call(null,buf_fn.call(null,topic))));
}
});})(or__7260__auto__,mults))
),topic);
}
});})(mults))
;
var p = (function (){
if(typeof cljs.core.async.t_cljs$core$async21102 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async21102 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta21103){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta21103 = meta21103;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async21102.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (mults,ensure_mult){
return (function (_21104,meta21103__$1){
var self__ = this;
var _21104__$1 = this;
return (new cljs.core.async.t_cljs$core$async21102(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta21103__$1));
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async21102.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (mults,ensure_mult){
return (function (_21104){
var self__ = this;
var _21104__$1 = this;
return self__.meta21103;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async21102.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async21102.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async21102.prototype.cljs$core$async$Pub$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async21102.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = self__.ensure_mult.call(null,topic);
return cljs.core.async.tap.call(null,m,ch__$1,close_QMARK_);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async21102.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__4657__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,self__.mults),topic);
if(cljs.core.truth_(temp__4657__auto__)){
var m = temp__4657__auto__;
return cljs.core.async.untap.call(null,m,ch__$1);
} else {
return null;
}
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async21102.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_.call(null,self__.mults,cljs.core.PersistentArrayMap.EMPTY);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async21102.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = ((function (mults,ensure_mult){
return (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.call(null,self__.mults,cljs.core.dissoc,topic);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async21102.getBasis = ((function (mults,ensure_mult){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta21103","meta21103",451748497,null)], null);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async21102.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async21102.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async21102";

cljs.core.async.t_cljs$core$async21102.cljs$lang$ctorPrWriter = ((function (mults,ensure_mult){
return (function (this__7871__auto__,writer__7872__auto__,opt__7873__auto__){
return cljs.core._write.call(null,writer__7872__auto__,"cljs.core.async/t_cljs$core$async21102");
});})(mults,ensure_mult))
;

cljs.core.async.__GT_t_cljs$core$async21102 = ((function (mults,ensure_mult){
return (function cljs$core$async$__GT_t_cljs$core$async21102(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta21103){
return (new cljs.core.async.t_cljs$core$async21102(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta21103));
});})(mults,ensure_mult))
;

}

return (new cljs.core.async.t_cljs$core$async21102(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__19275__auto___21228 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__19275__auto___21228,mults,ensure_mult,p){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (c__19275__auto___21228,mults,ensure_mult,p){
return (function (state_21176){
var state_val_21177 = (state_21176[(1)]);
if((state_val_21177 === (7))){
var inst_21172 = (state_21176[(2)]);
var state_21176__$1 = state_21176;
var statearr_21178_21229 = state_21176__$1;
(statearr_21178_21229[(2)] = inst_21172);

(statearr_21178_21229[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (20))){
var state_21176__$1 = state_21176;
var statearr_21179_21230 = state_21176__$1;
(statearr_21179_21230[(2)] = null);

(statearr_21179_21230[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (1))){
var state_21176__$1 = state_21176;
var statearr_21180_21231 = state_21176__$1;
(statearr_21180_21231[(2)] = null);

(statearr_21180_21231[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (24))){
var inst_21155 = (state_21176[(7)]);
var inst_21164 = cljs.core.swap_BANG_.call(null,mults,cljs.core.dissoc,inst_21155);
var state_21176__$1 = state_21176;
var statearr_21181_21232 = state_21176__$1;
(statearr_21181_21232[(2)] = inst_21164);

(statearr_21181_21232[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (4))){
var inst_21107 = (state_21176[(8)]);
var inst_21107__$1 = (state_21176[(2)]);
var inst_21108 = (inst_21107__$1 == null);
var state_21176__$1 = (function (){var statearr_21182 = state_21176;
(statearr_21182[(8)] = inst_21107__$1);

return statearr_21182;
})();
if(cljs.core.truth_(inst_21108)){
var statearr_21183_21233 = state_21176__$1;
(statearr_21183_21233[(1)] = (5));

} else {
var statearr_21184_21234 = state_21176__$1;
(statearr_21184_21234[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (15))){
var inst_21149 = (state_21176[(2)]);
var state_21176__$1 = state_21176;
var statearr_21185_21235 = state_21176__$1;
(statearr_21185_21235[(2)] = inst_21149);

(statearr_21185_21235[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (21))){
var inst_21169 = (state_21176[(2)]);
var state_21176__$1 = (function (){var statearr_21186 = state_21176;
(statearr_21186[(9)] = inst_21169);

return statearr_21186;
})();
var statearr_21187_21236 = state_21176__$1;
(statearr_21187_21236[(2)] = null);

(statearr_21187_21236[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (13))){
var inst_21131 = (state_21176[(10)]);
var inst_21133 = cljs.core.chunked_seq_QMARK_.call(null,inst_21131);
var state_21176__$1 = state_21176;
if(inst_21133){
var statearr_21188_21237 = state_21176__$1;
(statearr_21188_21237[(1)] = (16));

} else {
var statearr_21189_21238 = state_21176__$1;
(statearr_21189_21238[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (22))){
var inst_21161 = (state_21176[(2)]);
var state_21176__$1 = state_21176;
if(cljs.core.truth_(inst_21161)){
var statearr_21190_21239 = state_21176__$1;
(statearr_21190_21239[(1)] = (23));

} else {
var statearr_21191_21240 = state_21176__$1;
(statearr_21191_21240[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (6))){
var inst_21157 = (state_21176[(11)]);
var inst_21107 = (state_21176[(8)]);
var inst_21155 = (state_21176[(7)]);
var inst_21155__$1 = topic_fn.call(null,inst_21107);
var inst_21156 = cljs.core.deref.call(null,mults);
var inst_21157__$1 = cljs.core.get.call(null,inst_21156,inst_21155__$1);
var state_21176__$1 = (function (){var statearr_21192 = state_21176;
(statearr_21192[(11)] = inst_21157__$1);

(statearr_21192[(7)] = inst_21155__$1);

return statearr_21192;
})();
if(cljs.core.truth_(inst_21157__$1)){
var statearr_21193_21241 = state_21176__$1;
(statearr_21193_21241[(1)] = (19));

} else {
var statearr_21194_21242 = state_21176__$1;
(statearr_21194_21242[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (25))){
var inst_21166 = (state_21176[(2)]);
var state_21176__$1 = state_21176;
var statearr_21195_21243 = state_21176__$1;
(statearr_21195_21243[(2)] = inst_21166);

(statearr_21195_21243[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (17))){
var inst_21131 = (state_21176[(10)]);
var inst_21140 = cljs.core.first.call(null,inst_21131);
var inst_21141 = cljs.core.async.muxch_STAR_.call(null,inst_21140);
var inst_21142 = cljs.core.async.close_BANG_.call(null,inst_21141);
var inst_21143 = cljs.core.next.call(null,inst_21131);
var inst_21117 = inst_21143;
var inst_21118 = null;
var inst_21119 = (0);
var inst_21120 = (0);
var state_21176__$1 = (function (){var statearr_21196 = state_21176;
(statearr_21196[(12)] = inst_21120);

(statearr_21196[(13)] = inst_21142);

(statearr_21196[(14)] = inst_21118);

(statearr_21196[(15)] = inst_21119);

(statearr_21196[(16)] = inst_21117);

return statearr_21196;
})();
var statearr_21197_21244 = state_21176__$1;
(statearr_21197_21244[(2)] = null);

(statearr_21197_21244[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (3))){
var inst_21174 = (state_21176[(2)]);
var state_21176__$1 = state_21176;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_21176__$1,inst_21174);
} else {
if((state_val_21177 === (12))){
var inst_21151 = (state_21176[(2)]);
var state_21176__$1 = state_21176;
var statearr_21198_21245 = state_21176__$1;
(statearr_21198_21245[(2)] = inst_21151);

(statearr_21198_21245[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (2))){
var state_21176__$1 = state_21176;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_21176__$1,(4),ch);
} else {
if((state_val_21177 === (23))){
var state_21176__$1 = state_21176;
var statearr_21199_21246 = state_21176__$1;
(statearr_21199_21246[(2)] = null);

(statearr_21199_21246[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (19))){
var inst_21157 = (state_21176[(11)]);
var inst_21107 = (state_21176[(8)]);
var inst_21159 = cljs.core.async.muxch_STAR_.call(null,inst_21157);
var state_21176__$1 = state_21176;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_21176__$1,(22),inst_21159,inst_21107);
} else {
if((state_val_21177 === (11))){
var inst_21131 = (state_21176[(10)]);
var inst_21117 = (state_21176[(16)]);
var inst_21131__$1 = cljs.core.seq.call(null,inst_21117);
var state_21176__$1 = (function (){var statearr_21200 = state_21176;
(statearr_21200[(10)] = inst_21131__$1);

return statearr_21200;
})();
if(inst_21131__$1){
var statearr_21201_21247 = state_21176__$1;
(statearr_21201_21247[(1)] = (13));

} else {
var statearr_21202_21248 = state_21176__$1;
(statearr_21202_21248[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (9))){
var inst_21153 = (state_21176[(2)]);
var state_21176__$1 = state_21176;
var statearr_21203_21249 = state_21176__$1;
(statearr_21203_21249[(2)] = inst_21153);

(statearr_21203_21249[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (5))){
var inst_21114 = cljs.core.deref.call(null,mults);
var inst_21115 = cljs.core.vals.call(null,inst_21114);
var inst_21116 = cljs.core.seq.call(null,inst_21115);
var inst_21117 = inst_21116;
var inst_21118 = null;
var inst_21119 = (0);
var inst_21120 = (0);
var state_21176__$1 = (function (){var statearr_21204 = state_21176;
(statearr_21204[(12)] = inst_21120);

(statearr_21204[(14)] = inst_21118);

(statearr_21204[(15)] = inst_21119);

(statearr_21204[(16)] = inst_21117);

return statearr_21204;
})();
var statearr_21205_21250 = state_21176__$1;
(statearr_21205_21250[(2)] = null);

(statearr_21205_21250[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (14))){
var state_21176__$1 = state_21176;
var statearr_21209_21251 = state_21176__$1;
(statearr_21209_21251[(2)] = null);

(statearr_21209_21251[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (16))){
var inst_21131 = (state_21176[(10)]);
var inst_21135 = cljs.core.chunk_first.call(null,inst_21131);
var inst_21136 = cljs.core.chunk_rest.call(null,inst_21131);
var inst_21137 = cljs.core.count.call(null,inst_21135);
var inst_21117 = inst_21136;
var inst_21118 = inst_21135;
var inst_21119 = inst_21137;
var inst_21120 = (0);
var state_21176__$1 = (function (){var statearr_21210 = state_21176;
(statearr_21210[(12)] = inst_21120);

(statearr_21210[(14)] = inst_21118);

(statearr_21210[(15)] = inst_21119);

(statearr_21210[(16)] = inst_21117);

return statearr_21210;
})();
var statearr_21211_21252 = state_21176__$1;
(statearr_21211_21252[(2)] = null);

(statearr_21211_21252[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (10))){
var inst_21120 = (state_21176[(12)]);
var inst_21118 = (state_21176[(14)]);
var inst_21119 = (state_21176[(15)]);
var inst_21117 = (state_21176[(16)]);
var inst_21125 = cljs.core._nth.call(null,inst_21118,inst_21120);
var inst_21126 = cljs.core.async.muxch_STAR_.call(null,inst_21125);
var inst_21127 = cljs.core.async.close_BANG_.call(null,inst_21126);
var inst_21128 = (inst_21120 + (1));
var tmp21206 = inst_21118;
var tmp21207 = inst_21119;
var tmp21208 = inst_21117;
var inst_21117__$1 = tmp21208;
var inst_21118__$1 = tmp21206;
var inst_21119__$1 = tmp21207;
var inst_21120__$1 = inst_21128;
var state_21176__$1 = (function (){var statearr_21212 = state_21176;
(statearr_21212[(12)] = inst_21120__$1);

(statearr_21212[(17)] = inst_21127);

(statearr_21212[(14)] = inst_21118__$1);

(statearr_21212[(15)] = inst_21119__$1);

(statearr_21212[(16)] = inst_21117__$1);

return statearr_21212;
})();
var statearr_21213_21253 = state_21176__$1;
(statearr_21213_21253[(2)] = null);

(statearr_21213_21253[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (18))){
var inst_21146 = (state_21176[(2)]);
var state_21176__$1 = state_21176;
var statearr_21214_21254 = state_21176__$1;
(statearr_21214_21254[(2)] = inst_21146);

(statearr_21214_21254[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21177 === (8))){
var inst_21120 = (state_21176[(12)]);
var inst_21119 = (state_21176[(15)]);
var inst_21122 = (inst_21120 < inst_21119);
var inst_21123 = inst_21122;
var state_21176__$1 = state_21176;
if(cljs.core.truth_(inst_21123)){
var statearr_21215_21255 = state_21176__$1;
(statearr_21215_21255[(1)] = (10));

} else {
var statearr_21216_21256 = state_21176__$1;
(statearr_21216_21256[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__19275__auto___21228,mults,ensure_mult,p))
;
return ((function (switch__19163__auto__,c__19275__auto___21228,mults,ensure_mult,p){
return (function() {
var cljs$core$async$state_machine__19164__auto__ = null;
var cljs$core$async$state_machine__19164__auto____0 = (function (){
var statearr_21220 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_21220[(0)] = cljs$core$async$state_machine__19164__auto__);

(statearr_21220[(1)] = (1));

return statearr_21220;
});
var cljs$core$async$state_machine__19164__auto____1 = (function (state_21176){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_21176);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e21221){if((e21221 instanceof Object)){
var ex__19167__auto__ = e21221;
var statearr_21222_21257 = state_21176;
(statearr_21222_21257[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_21176);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e21221;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__21258 = state_21176;
state_21176 = G__21258;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$state_machine__19164__auto__ = function(state_21176){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19164__auto____1.call(this,state_21176);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19164__auto____0;
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19164__auto____1;
return cljs$core$async$state_machine__19164__auto__;
})()
;})(switch__19163__auto__,c__19275__auto___21228,mults,ensure_mult,p))
})();
var state__19277__auto__ = (function (){var statearr_21223 = f__19276__auto__.call(null);
(statearr_21223[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto___21228);

return statearr_21223;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(c__19275__auto___21228,mults,ensure_mult,p))
);


return p;
});

cljs.core.async.pub.cljs$lang$maxFixedArity = 3;

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var args21259 = [];
var len__8373__auto___21262 = arguments.length;
var i__8374__auto___21263 = (0);
while(true){
if((i__8374__auto___21263 < len__8373__auto___21262)){
args21259.push((arguments[i__8374__auto___21263]));

var G__21264 = (i__8374__auto___21263 + (1));
i__8374__auto___21263 = G__21264;
continue;
} else {
}
break;
}

var G__21261 = args21259.length;
switch (G__21261) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args21259.length)].join('')));

}
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.call(null,p,topic,ch,true);
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_.call(null,p,topic,ch,close_QMARK_);
});

cljs.core.async.sub.cljs$lang$maxFixedArity = 4;

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_.call(null,p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var args21266 = [];
var len__8373__auto___21269 = arguments.length;
var i__8374__auto___21270 = (0);
while(true){
if((i__8374__auto___21270 < len__8373__auto___21269)){
args21266.push((arguments[i__8374__auto___21270]));

var G__21271 = (i__8374__auto___21270 + (1));
i__8374__auto___21270 = G__21271;
continue;
} else {
}
break;
}

var G__21268 = args21266.length;
switch (G__21268) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args21266.length)].join('')));

}
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_.call(null,p);
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_.call(null,p,topic);
});

cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2;

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var args21273 = [];
var len__8373__auto___21344 = arguments.length;
var i__8374__auto___21345 = (0);
while(true){
if((i__8374__auto___21345 < len__8373__auto___21344)){
args21273.push((arguments[i__8374__auto___21345]));

var G__21346 = (i__8374__auto___21345 + (1));
i__8374__auto___21345 = G__21346;
continue;
} else {
}
break;
}

var G__21275 = args21273.length;
switch (G__21275) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args21273.length)].join('')));

}
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.call(null,f,chs,null);
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec.call(null,chs);
var out = cljs.core.async.chan.call(null,buf_or_n);
var cnt = cljs.core.count.call(null,chs__$1);
var rets = cljs.core.object_array.call(null,cnt);
var dchan = cljs.core.async.chan.call(null,(1));
var dctr = cljs.core.atom.call(null,null);
var done = cljs.core.mapv.call(null,((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (i){
return ((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.call(null,dchan,rets.slice((0)));
} else {
return null;
}
});
;})(chs__$1,out,cnt,rets,dchan,dctr))
});})(chs__$1,out,cnt,rets,dchan,dctr))
,cljs.core.range.call(null,cnt));
var c__19275__auto___21348 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__19275__auto___21348,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (c__19275__auto___21348,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (state_21314){
var state_val_21315 = (state_21314[(1)]);
if((state_val_21315 === (7))){
var state_21314__$1 = state_21314;
var statearr_21316_21349 = state_21314__$1;
(statearr_21316_21349[(2)] = null);

(statearr_21316_21349[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21315 === (1))){
var state_21314__$1 = state_21314;
var statearr_21317_21350 = state_21314__$1;
(statearr_21317_21350[(2)] = null);

(statearr_21317_21350[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21315 === (4))){
var inst_21278 = (state_21314[(7)]);
var inst_21280 = (inst_21278 < cnt);
var state_21314__$1 = state_21314;
if(cljs.core.truth_(inst_21280)){
var statearr_21318_21351 = state_21314__$1;
(statearr_21318_21351[(1)] = (6));

} else {
var statearr_21319_21352 = state_21314__$1;
(statearr_21319_21352[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21315 === (15))){
var inst_21310 = (state_21314[(2)]);
var state_21314__$1 = state_21314;
var statearr_21320_21353 = state_21314__$1;
(statearr_21320_21353[(2)] = inst_21310);

(statearr_21320_21353[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21315 === (13))){
var inst_21303 = cljs.core.async.close_BANG_.call(null,out);
var state_21314__$1 = state_21314;
var statearr_21321_21354 = state_21314__$1;
(statearr_21321_21354[(2)] = inst_21303);

(statearr_21321_21354[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21315 === (6))){
var state_21314__$1 = state_21314;
var statearr_21322_21355 = state_21314__$1;
(statearr_21322_21355[(2)] = null);

(statearr_21322_21355[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21315 === (3))){
var inst_21312 = (state_21314[(2)]);
var state_21314__$1 = state_21314;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_21314__$1,inst_21312);
} else {
if((state_val_21315 === (12))){
var inst_21300 = (state_21314[(8)]);
var inst_21300__$1 = (state_21314[(2)]);
var inst_21301 = cljs.core.some.call(null,cljs.core.nil_QMARK_,inst_21300__$1);
var state_21314__$1 = (function (){var statearr_21323 = state_21314;
(statearr_21323[(8)] = inst_21300__$1);

return statearr_21323;
})();
if(cljs.core.truth_(inst_21301)){
var statearr_21324_21356 = state_21314__$1;
(statearr_21324_21356[(1)] = (13));

} else {
var statearr_21325_21357 = state_21314__$1;
(statearr_21325_21357[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21315 === (2))){
var inst_21277 = cljs.core.reset_BANG_.call(null,dctr,cnt);
var inst_21278 = (0);
var state_21314__$1 = (function (){var statearr_21326 = state_21314;
(statearr_21326[(9)] = inst_21277);

(statearr_21326[(7)] = inst_21278);

return statearr_21326;
})();
var statearr_21327_21358 = state_21314__$1;
(statearr_21327_21358[(2)] = null);

(statearr_21327_21358[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21315 === (11))){
var inst_21278 = (state_21314[(7)]);
var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_21314,(10),Object,null,(9));
var inst_21287 = chs__$1.call(null,inst_21278);
var inst_21288 = done.call(null,inst_21278);
var inst_21289 = cljs.core.async.take_BANG_.call(null,inst_21287,inst_21288);
var state_21314__$1 = state_21314;
var statearr_21328_21359 = state_21314__$1;
(statearr_21328_21359[(2)] = inst_21289);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_21314__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21315 === (9))){
var inst_21278 = (state_21314[(7)]);
var inst_21291 = (state_21314[(2)]);
var inst_21292 = (inst_21278 + (1));
var inst_21278__$1 = inst_21292;
var state_21314__$1 = (function (){var statearr_21329 = state_21314;
(statearr_21329[(10)] = inst_21291);

(statearr_21329[(7)] = inst_21278__$1);

return statearr_21329;
})();
var statearr_21330_21360 = state_21314__$1;
(statearr_21330_21360[(2)] = null);

(statearr_21330_21360[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21315 === (5))){
var inst_21298 = (state_21314[(2)]);
var state_21314__$1 = (function (){var statearr_21331 = state_21314;
(statearr_21331[(11)] = inst_21298);

return statearr_21331;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_21314__$1,(12),dchan);
} else {
if((state_val_21315 === (14))){
var inst_21300 = (state_21314[(8)]);
var inst_21305 = cljs.core.apply.call(null,f,inst_21300);
var state_21314__$1 = state_21314;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_21314__$1,(16),out,inst_21305);
} else {
if((state_val_21315 === (16))){
var inst_21307 = (state_21314[(2)]);
var state_21314__$1 = (function (){var statearr_21332 = state_21314;
(statearr_21332[(12)] = inst_21307);

return statearr_21332;
})();
var statearr_21333_21361 = state_21314__$1;
(statearr_21333_21361[(2)] = null);

(statearr_21333_21361[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21315 === (10))){
var inst_21282 = (state_21314[(2)]);
var inst_21283 = cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec);
var state_21314__$1 = (function (){var statearr_21334 = state_21314;
(statearr_21334[(13)] = inst_21282);

return statearr_21334;
})();
var statearr_21335_21362 = state_21314__$1;
(statearr_21335_21362[(2)] = inst_21283);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_21314__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21315 === (8))){
var inst_21296 = (state_21314[(2)]);
var state_21314__$1 = state_21314;
var statearr_21336_21363 = state_21314__$1;
(statearr_21336_21363[(2)] = inst_21296);

(statearr_21336_21363[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__19275__auto___21348,chs__$1,out,cnt,rets,dchan,dctr,done))
;
return ((function (switch__19163__auto__,c__19275__auto___21348,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function() {
var cljs$core$async$state_machine__19164__auto__ = null;
var cljs$core$async$state_machine__19164__auto____0 = (function (){
var statearr_21340 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_21340[(0)] = cljs$core$async$state_machine__19164__auto__);

(statearr_21340[(1)] = (1));

return statearr_21340;
});
var cljs$core$async$state_machine__19164__auto____1 = (function (state_21314){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_21314);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e21341){if((e21341 instanceof Object)){
var ex__19167__auto__ = e21341;
var statearr_21342_21364 = state_21314;
(statearr_21342_21364[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_21314);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e21341;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__21365 = state_21314;
state_21314 = G__21365;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$state_machine__19164__auto__ = function(state_21314){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19164__auto____1.call(this,state_21314);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19164__auto____0;
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19164__auto____1;
return cljs$core$async$state_machine__19164__auto__;
})()
;})(switch__19163__auto__,c__19275__auto___21348,chs__$1,out,cnt,rets,dchan,dctr,done))
})();
var state__19277__auto__ = (function (){var statearr_21343 = f__19276__auto__.call(null);
(statearr_21343[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto___21348);

return statearr_21343;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(c__19275__auto___21348,chs__$1,out,cnt,rets,dchan,dctr,done))
);


return out;
});

cljs.core.async.map.cljs$lang$maxFixedArity = 3;

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var args21367 = [];
var len__8373__auto___21425 = arguments.length;
var i__8374__auto___21426 = (0);
while(true){
if((i__8374__auto___21426 < len__8373__auto___21425)){
args21367.push((arguments[i__8374__auto___21426]));

var G__21427 = (i__8374__auto___21426 + (1));
i__8374__auto___21426 = G__21427;
continue;
} else {
}
break;
}

var G__21369 = args21367.length;
switch (G__21369) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args21367.length)].join('')));

}
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.call(null,chs,null);
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__19275__auto___21429 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__19275__auto___21429,out){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (c__19275__auto___21429,out){
return (function (state_21401){
var state_val_21402 = (state_21401[(1)]);
if((state_val_21402 === (7))){
var inst_21380 = (state_21401[(7)]);
var inst_21381 = (state_21401[(8)]);
var inst_21380__$1 = (state_21401[(2)]);
var inst_21381__$1 = cljs.core.nth.call(null,inst_21380__$1,(0),null);
var inst_21382 = cljs.core.nth.call(null,inst_21380__$1,(1),null);
var inst_21383 = (inst_21381__$1 == null);
var state_21401__$1 = (function (){var statearr_21403 = state_21401;
(statearr_21403[(9)] = inst_21382);

(statearr_21403[(7)] = inst_21380__$1);

(statearr_21403[(8)] = inst_21381__$1);

return statearr_21403;
})();
if(cljs.core.truth_(inst_21383)){
var statearr_21404_21430 = state_21401__$1;
(statearr_21404_21430[(1)] = (8));

} else {
var statearr_21405_21431 = state_21401__$1;
(statearr_21405_21431[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21402 === (1))){
var inst_21370 = cljs.core.vec.call(null,chs);
var inst_21371 = inst_21370;
var state_21401__$1 = (function (){var statearr_21406 = state_21401;
(statearr_21406[(10)] = inst_21371);

return statearr_21406;
})();
var statearr_21407_21432 = state_21401__$1;
(statearr_21407_21432[(2)] = null);

(statearr_21407_21432[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21402 === (4))){
var inst_21371 = (state_21401[(10)]);
var state_21401__$1 = state_21401;
return cljs.core.async.ioc_alts_BANG_.call(null,state_21401__$1,(7),inst_21371);
} else {
if((state_val_21402 === (6))){
var inst_21397 = (state_21401[(2)]);
var state_21401__$1 = state_21401;
var statearr_21408_21433 = state_21401__$1;
(statearr_21408_21433[(2)] = inst_21397);

(statearr_21408_21433[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21402 === (3))){
var inst_21399 = (state_21401[(2)]);
var state_21401__$1 = state_21401;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_21401__$1,inst_21399);
} else {
if((state_val_21402 === (2))){
var inst_21371 = (state_21401[(10)]);
var inst_21373 = cljs.core.count.call(null,inst_21371);
var inst_21374 = (inst_21373 > (0));
var state_21401__$1 = state_21401;
if(cljs.core.truth_(inst_21374)){
var statearr_21410_21434 = state_21401__$1;
(statearr_21410_21434[(1)] = (4));

} else {
var statearr_21411_21435 = state_21401__$1;
(statearr_21411_21435[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21402 === (11))){
var inst_21371 = (state_21401[(10)]);
var inst_21390 = (state_21401[(2)]);
var tmp21409 = inst_21371;
var inst_21371__$1 = tmp21409;
var state_21401__$1 = (function (){var statearr_21412 = state_21401;
(statearr_21412[(11)] = inst_21390);

(statearr_21412[(10)] = inst_21371__$1);

return statearr_21412;
})();
var statearr_21413_21436 = state_21401__$1;
(statearr_21413_21436[(2)] = null);

(statearr_21413_21436[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21402 === (9))){
var inst_21381 = (state_21401[(8)]);
var state_21401__$1 = state_21401;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_21401__$1,(11),out,inst_21381);
} else {
if((state_val_21402 === (5))){
var inst_21395 = cljs.core.async.close_BANG_.call(null,out);
var state_21401__$1 = state_21401;
var statearr_21414_21437 = state_21401__$1;
(statearr_21414_21437[(2)] = inst_21395);

(statearr_21414_21437[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21402 === (10))){
var inst_21393 = (state_21401[(2)]);
var state_21401__$1 = state_21401;
var statearr_21415_21438 = state_21401__$1;
(statearr_21415_21438[(2)] = inst_21393);

(statearr_21415_21438[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21402 === (8))){
var inst_21382 = (state_21401[(9)]);
var inst_21371 = (state_21401[(10)]);
var inst_21380 = (state_21401[(7)]);
var inst_21381 = (state_21401[(8)]);
var inst_21385 = (function (){var cs = inst_21371;
var vec__21376 = inst_21380;
var v = inst_21381;
var c = inst_21382;
return ((function (cs,vec__21376,v,c,inst_21382,inst_21371,inst_21380,inst_21381,state_val_21402,c__19275__auto___21429,out){
return (function (p1__21366_SHARP_){
return cljs.core.not_EQ_.call(null,c,p1__21366_SHARP_);
});
;})(cs,vec__21376,v,c,inst_21382,inst_21371,inst_21380,inst_21381,state_val_21402,c__19275__auto___21429,out))
})();
var inst_21386 = cljs.core.filterv.call(null,inst_21385,inst_21371);
var inst_21371__$1 = inst_21386;
var state_21401__$1 = (function (){var statearr_21416 = state_21401;
(statearr_21416[(10)] = inst_21371__$1);

return statearr_21416;
})();
var statearr_21417_21439 = state_21401__$1;
(statearr_21417_21439[(2)] = null);

(statearr_21417_21439[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__19275__auto___21429,out))
;
return ((function (switch__19163__auto__,c__19275__auto___21429,out){
return (function() {
var cljs$core$async$state_machine__19164__auto__ = null;
var cljs$core$async$state_machine__19164__auto____0 = (function (){
var statearr_21421 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_21421[(0)] = cljs$core$async$state_machine__19164__auto__);

(statearr_21421[(1)] = (1));

return statearr_21421;
});
var cljs$core$async$state_machine__19164__auto____1 = (function (state_21401){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_21401);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e21422){if((e21422 instanceof Object)){
var ex__19167__auto__ = e21422;
var statearr_21423_21440 = state_21401;
(statearr_21423_21440[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_21401);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e21422;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__21441 = state_21401;
state_21401 = G__21441;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$state_machine__19164__auto__ = function(state_21401){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19164__auto____1.call(this,state_21401);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19164__auto____0;
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19164__auto____1;
return cljs$core$async$state_machine__19164__auto__;
})()
;})(switch__19163__auto__,c__19275__auto___21429,out))
})();
var state__19277__auto__ = (function (){var statearr_21424 = f__19276__auto__.call(null);
(statearr_21424[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto___21429);

return statearr_21424;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(c__19275__auto___21429,out))
);


return out;
});

cljs.core.async.merge.cljs$lang$maxFixedArity = 2;

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce.call(null,cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var args21442 = [];
var len__8373__auto___21491 = arguments.length;
var i__8374__auto___21492 = (0);
while(true){
if((i__8374__auto___21492 < len__8373__auto___21491)){
args21442.push((arguments[i__8374__auto___21492]));

var G__21493 = (i__8374__auto___21492 + (1));
i__8374__auto___21492 = G__21493;
continue;
} else {
}
break;
}

var G__21444 = args21442.length;
switch (G__21444) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args21442.length)].join('')));

}
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.call(null,n,ch,null);
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__19275__auto___21495 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__19275__auto___21495,out){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (c__19275__auto___21495,out){
return (function (state_21468){
var state_val_21469 = (state_21468[(1)]);
if((state_val_21469 === (7))){
var inst_21450 = (state_21468[(7)]);
var inst_21450__$1 = (state_21468[(2)]);
var inst_21451 = (inst_21450__$1 == null);
var inst_21452 = cljs.core.not.call(null,inst_21451);
var state_21468__$1 = (function (){var statearr_21470 = state_21468;
(statearr_21470[(7)] = inst_21450__$1);

return statearr_21470;
})();
if(inst_21452){
var statearr_21471_21496 = state_21468__$1;
(statearr_21471_21496[(1)] = (8));

} else {
var statearr_21472_21497 = state_21468__$1;
(statearr_21472_21497[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21469 === (1))){
var inst_21445 = (0);
var state_21468__$1 = (function (){var statearr_21473 = state_21468;
(statearr_21473[(8)] = inst_21445);

return statearr_21473;
})();
var statearr_21474_21498 = state_21468__$1;
(statearr_21474_21498[(2)] = null);

(statearr_21474_21498[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21469 === (4))){
var state_21468__$1 = state_21468;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_21468__$1,(7),ch);
} else {
if((state_val_21469 === (6))){
var inst_21463 = (state_21468[(2)]);
var state_21468__$1 = state_21468;
var statearr_21475_21499 = state_21468__$1;
(statearr_21475_21499[(2)] = inst_21463);

(statearr_21475_21499[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21469 === (3))){
var inst_21465 = (state_21468[(2)]);
var inst_21466 = cljs.core.async.close_BANG_.call(null,out);
var state_21468__$1 = (function (){var statearr_21476 = state_21468;
(statearr_21476[(9)] = inst_21465);

return statearr_21476;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_21468__$1,inst_21466);
} else {
if((state_val_21469 === (2))){
var inst_21445 = (state_21468[(8)]);
var inst_21447 = (inst_21445 < n);
var state_21468__$1 = state_21468;
if(cljs.core.truth_(inst_21447)){
var statearr_21477_21500 = state_21468__$1;
(statearr_21477_21500[(1)] = (4));

} else {
var statearr_21478_21501 = state_21468__$1;
(statearr_21478_21501[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21469 === (11))){
var inst_21445 = (state_21468[(8)]);
var inst_21455 = (state_21468[(2)]);
var inst_21456 = (inst_21445 + (1));
var inst_21445__$1 = inst_21456;
var state_21468__$1 = (function (){var statearr_21479 = state_21468;
(statearr_21479[(8)] = inst_21445__$1);

(statearr_21479[(10)] = inst_21455);

return statearr_21479;
})();
var statearr_21480_21502 = state_21468__$1;
(statearr_21480_21502[(2)] = null);

(statearr_21480_21502[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21469 === (9))){
var state_21468__$1 = state_21468;
var statearr_21481_21503 = state_21468__$1;
(statearr_21481_21503[(2)] = null);

(statearr_21481_21503[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21469 === (5))){
var state_21468__$1 = state_21468;
var statearr_21482_21504 = state_21468__$1;
(statearr_21482_21504[(2)] = null);

(statearr_21482_21504[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21469 === (10))){
var inst_21460 = (state_21468[(2)]);
var state_21468__$1 = state_21468;
var statearr_21483_21505 = state_21468__$1;
(statearr_21483_21505[(2)] = inst_21460);

(statearr_21483_21505[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21469 === (8))){
var inst_21450 = (state_21468[(7)]);
var state_21468__$1 = state_21468;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_21468__$1,(11),out,inst_21450);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__19275__auto___21495,out))
;
return ((function (switch__19163__auto__,c__19275__auto___21495,out){
return (function() {
var cljs$core$async$state_machine__19164__auto__ = null;
var cljs$core$async$state_machine__19164__auto____0 = (function (){
var statearr_21487 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_21487[(0)] = cljs$core$async$state_machine__19164__auto__);

(statearr_21487[(1)] = (1));

return statearr_21487;
});
var cljs$core$async$state_machine__19164__auto____1 = (function (state_21468){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_21468);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e21488){if((e21488 instanceof Object)){
var ex__19167__auto__ = e21488;
var statearr_21489_21506 = state_21468;
(statearr_21489_21506[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_21468);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e21488;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__21507 = state_21468;
state_21468 = G__21507;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$state_machine__19164__auto__ = function(state_21468){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19164__auto____1.call(this,state_21468);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19164__auto____0;
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19164__auto____1;
return cljs$core$async$state_machine__19164__auto__;
})()
;})(switch__19163__auto__,c__19275__auto___21495,out))
})();
var state__19277__auto__ = (function (){var statearr_21490 = f__19276__auto__.call(null);
(statearr_21490[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto___21495);

return statearr_21490;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(c__19275__auto___21495,out))
);


return out;
});

cljs.core.async.take.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if(typeof cljs.core.async.t_cljs$core$async21515 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async21515 = (function (f,ch,meta21516){
this.f = f;
this.ch = ch;
this.meta21516 = meta21516;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async21515.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_21517,meta21516__$1){
var self__ = this;
var _21517__$1 = this;
return (new cljs.core.async.t_cljs$core$async21515(self__.f,self__.ch,meta21516__$1));
});

cljs.core.async.t_cljs$core$async21515.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_21517){
var self__ = this;
var _21517__$1 = this;
return self__.meta21516;
});

cljs.core.async.t_cljs$core$async21515.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async21515.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async21515.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async21515.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async21515.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,(function (){
if(typeof cljs.core.async.t_cljs$core$async21518 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async21518 = (function (f,ch,meta21516,_,fn1,meta21519){
this.f = f;
this.ch = ch;
this.meta21516 = meta21516;
this._ = _;
this.fn1 = fn1;
this.meta21519 = meta21519;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async21518.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (___$1){
return (function (_21520,meta21519__$1){
var self__ = this;
var _21520__$1 = this;
return (new cljs.core.async.t_cljs$core$async21518(self__.f,self__.ch,self__.meta21516,self__._,self__.fn1,meta21519__$1));
});})(___$1))
;

cljs.core.async.t_cljs$core$async21518.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (___$1){
return (function (_21520){
var self__ = this;
var _21520__$1 = this;
return self__.meta21519;
});})(___$1))
;

cljs.core.async.t_cljs$core$async21518.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async21518.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.fn1);
});})(___$1))
;

cljs.core.async.t_cljs$core$async21518.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
});})(___$1))
;

cljs.core.async.t_cljs$core$async21518.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit.call(null,self__.fn1);
return ((function (f1,___$2,___$1){
return (function (p1__21508_SHARP_){
return f1.call(null,(((p1__21508_SHARP_ == null))?null:self__.f.call(null,p1__21508_SHARP_)));
});
;})(f1,___$2,___$1))
});})(___$1))
;

cljs.core.async.t_cljs$core$async21518.getBasis = ((function (___$1){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta21516","meta21516",-1244220491,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async21515","cljs.core.async/t_cljs$core$async21515",-531501825,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta21519","meta21519",545181031,null)], null);
});})(___$1))
;

cljs.core.async.t_cljs$core$async21518.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async21518.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async21518";

cljs.core.async.t_cljs$core$async21518.cljs$lang$ctorPrWriter = ((function (___$1){
return (function (this__7871__auto__,writer__7872__auto__,opt__7873__auto__){
return cljs.core._write.call(null,writer__7872__auto__,"cljs.core.async/t_cljs$core$async21518");
});})(___$1))
;

cljs.core.async.__GT_t_cljs$core$async21518 = ((function (___$1){
return (function cljs$core$async$map_LT__$___GT_t_cljs$core$async21518(f__$1,ch__$1,meta21516__$1,___$2,fn1__$1,meta21519){
return (new cljs.core.async.t_cljs$core$async21518(f__$1,ch__$1,meta21516__$1,___$2,fn1__$1,meta21519));
});})(___$1))
;

}

return (new cljs.core.async.t_cljs$core$async21518(self__.f,self__.ch,self__.meta21516,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__7248__auto__ = ret;
if(cljs.core.truth_(and__7248__auto__)){
return !((cljs.core.deref.call(null,ret) == null));
} else {
return and__7248__auto__;
}
})())){
return cljs.core.async.impl.channels.box.call(null,self__.f.call(null,cljs.core.deref.call(null,ret)));
} else {
return ret;
}
});

cljs.core.async.t_cljs$core$async21515.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async21515.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn1);
});

cljs.core.async.t_cljs$core$async21515.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta21516","meta21516",-1244220491,null)], null);
});

cljs.core.async.t_cljs$core$async21515.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async21515.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async21515";

cljs.core.async.t_cljs$core$async21515.cljs$lang$ctorPrWriter = (function (this__7871__auto__,writer__7872__auto__,opt__7873__auto__){
return cljs.core._write.call(null,writer__7872__auto__,"cljs.core.async/t_cljs$core$async21515");
});

cljs.core.async.__GT_t_cljs$core$async21515 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async21515(f__$1,ch__$1,meta21516){
return (new cljs.core.async.t_cljs$core$async21515(f__$1,ch__$1,meta21516));
});

}

return (new cljs.core.async.t_cljs$core$async21515(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if(typeof cljs.core.async.t_cljs$core$async21524 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async21524 = (function (f,ch,meta21525){
this.f = f;
this.ch = ch;
this.meta21525 = meta21525;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async21524.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_21526,meta21525__$1){
var self__ = this;
var _21526__$1 = this;
return (new cljs.core.async.t_cljs$core$async21524(self__.f,self__.ch,meta21525__$1));
});

cljs.core.async.t_cljs$core$async21524.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_21526){
var self__ = this;
var _21526__$1 = this;
return self__.meta21525;
});

cljs.core.async.t_cljs$core$async21524.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async21524.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async21524.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async21524.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async21524.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async21524.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,self__.f.call(null,val),fn1);
});

cljs.core.async.t_cljs$core$async21524.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta21525","meta21525",-106083908,null)], null);
});

cljs.core.async.t_cljs$core$async21524.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async21524.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async21524";

cljs.core.async.t_cljs$core$async21524.cljs$lang$ctorPrWriter = (function (this__7871__auto__,writer__7872__auto__,opt__7873__auto__){
return cljs.core._write.call(null,writer__7872__auto__,"cljs.core.async/t_cljs$core$async21524");
});

cljs.core.async.__GT_t_cljs$core$async21524 = (function cljs$core$async$map_GT__$___GT_t_cljs$core$async21524(f__$1,ch__$1,meta21525){
return (new cljs.core.async.t_cljs$core$async21524(f__$1,ch__$1,meta21525));
});

}

return (new cljs.core.async.t_cljs$core$async21524(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if(typeof cljs.core.async.t_cljs$core$async21530 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async21530 = (function (p,ch,meta21531){
this.p = p;
this.ch = ch;
this.meta21531 = meta21531;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async21530.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_21532,meta21531__$1){
var self__ = this;
var _21532__$1 = this;
return (new cljs.core.async.t_cljs$core$async21530(self__.p,self__.ch,meta21531__$1));
});

cljs.core.async.t_cljs$core$async21530.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_21532){
var self__ = this;
var _21532__$1 = this;
return self__.meta21531;
});

cljs.core.async.t_cljs$core$async21530.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async21530.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async21530.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async21530.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async21530.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async21530.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async21530.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_(self__.p.call(null,val))){
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box.call(null,cljs.core.not.call(null,cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch)));
}
});

cljs.core.async.t_cljs$core$async21530.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta21531","meta21531",-246729065,null)], null);
});

cljs.core.async.t_cljs$core$async21530.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async21530.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async21530";

cljs.core.async.t_cljs$core$async21530.cljs$lang$ctorPrWriter = (function (this__7871__auto__,writer__7872__auto__,opt__7873__auto__){
return cljs.core._write.call(null,writer__7872__auto__,"cljs.core.async/t_cljs$core$async21530");
});

cljs.core.async.__GT_t_cljs$core$async21530 = (function cljs$core$async$filter_GT__$___GT_t_cljs$core$async21530(p__$1,ch__$1,meta21531){
return (new cljs.core.async.t_cljs$core$async21530(p__$1,ch__$1,meta21531));
});

}

return (new cljs.core.async.t_cljs$core$async21530(p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_.call(null,cljs.core.complement.call(null,p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var args21533 = [];
var len__8373__auto___21577 = arguments.length;
var i__8374__auto___21578 = (0);
while(true){
if((i__8374__auto___21578 < len__8373__auto___21577)){
args21533.push((arguments[i__8374__auto___21578]));

var G__21579 = (i__8374__auto___21578 + (1));
i__8374__auto___21578 = G__21579;
continue;
} else {
}
break;
}

var G__21535 = args21533.length;
switch (G__21535) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args21533.length)].join('')));

}
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.call(null,p,ch,null);
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__19275__auto___21581 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__19275__auto___21581,out){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (c__19275__auto___21581,out){
return (function (state_21556){
var state_val_21557 = (state_21556[(1)]);
if((state_val_21557 === (7))){
var inst_21552 = (state_21556[(2)]);
var state_21556__$1 = state_21556;
var statearr_21558_21582 = state_21556__$1;
(statearr_21558_21582[(2)] = inst_21552);

(statearr_21558_21582[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21557 === (1))){
var state_21556__$1 = state_21556;
var statearr_21559_21583 = state_21556__$1;
(statearr_21559_21583[(2)] = null);

(statearr_21559_21583[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21557 === (4))){
var inst_21538 = (state_21556[(7)]);
var inst_21538__$1 = (state_21556[(2)]);
var inst_21539 = (inst_21538__$1 == null);
var state_21556__$1 = (function (){var statearr_21560 = state_21556;
(statearr_21560[(7)] = inst_21538__$1);

return statearr_21560;
})();
if(cljs.core.truth_(inst_21539)){
var statearr_21561_21584 = state_21556__$1;
(statearr_21561_21584[(1)] = (5));

} else {
var statearr_21562_21585 = state_21556__$1;
(statearr_21562_21585[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21557 === (6))){
var inst_21538 = (state_21556[(7)]);
var inst_21543 = p.call(null,inst_21538);
var state_21556__$1 = state_21556;
if(cljs.core.truth_(inst_21543)){
var statearr_21563_21586 = state_21556__$1;
(statearr_21563_21586[(1)] = (8));

} else {
var statearr_21564_21587 = state_21556__$1;
(statearr_21564_21587[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21557 === (3))){
var inst_21554 = (state_21556[(2)]);
var state_21556__$1 = state_21556;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_21556__$1,inst_21554);
} else {
if((state_val_21557 === (2))){
var state_21556__$1 = state_21556;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_21556__$1,(4),ch);
} else {
if((state_val_21557 === (11))){
var inst_21546 = (state_21556[(2)]);
var state_21556__$1 = state_21556;
var statearr_21565_21588 = state_21556__$1;
(statearr_21565_21588[(2)] = inst_21546);

(statearr_21565_21588[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21557 === (9))){
var state_21556__$1 = state_21556;
var statearr_21566_21589 = state_21556__$1;
(statearr_21566_21589[(2)] = null);

(statearr_21566_21589[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21557 === (5))){
var inst_21541 = cljs.core.async.close_BANG_.call(null,out);
var state_21556__$1 = state_21556;
var statearr_21567_21590 = state_21556__$1;
(statearr_21567_21590[(2)] = inst_21541);

(statearr_21567_21590[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21557 === (10))){
var inst_21549 = (state_21556[(2)]);
var state_21556__$1 = (function (){var statearr_21568 = state_21556;
(statearr_21568[(8)] = inst_21549);

return statearr_21568;
})();
var statearr_21569_21591 = state_21556__$1;
(statearr_21569_21591[(2)] = null);

(statearr_21569_21591[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21557 === (8))){
var inst_21538 = (state_21556[(7)]);
var state_21556__$1 = state_21556;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_21556__$1,(11),out,inst_21538);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__19275__auto___21581,out))
;
return ((function (switch__19163__auto__,c__19275__auto___21581,out){
return (function() {
var cljs$core$async$state_machine__19164__auto__ = null;
var cljs$core$async$state_machine__19164__auto____0 = (function (){
var statearr_21573 = [null,null,null,null,null,null,null,null,null];
(statearr_21573[(0)] = cljs$core$async$state_machine__19164__auto__);

(statearr_21573[(1)] = (1));

return statearr_21573;
});
var cljs$core$async$state_machine__19164__auto____1 = (function (state_21556){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_21556);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e21574){if((e21574 instanceof Object)){
var ex__19167__auto__ = e21574;
var statearr_21575_21592 = state_21556;
(statearr_21575_21592[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_21556);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e21574;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__21593 = state_21556;
state_21556 = G__21593;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$state_machine__19164__auto__ = function(state_21556){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19164__auto____1.call(this,state_21556);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19164__auto____0;
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19164__auto____1;
return cljs$core$async$state_machine__19164__auto__;
})()
;})(switch__19163__auto__,c__19275__auto___21581,out))
})();
var state__19277__auto__ = (function (){var statearr_21576 = f__19276__auto__.call(null);
(statearr_21576[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto___21581);

return statearr_21576;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(c__19275__auto___21581,out))
);


return out;
});

cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var args21594 = [];
var len__8373__auto___21597 = arguments.length;
var i__8374__auto___21598 = (0);
while(true){
if((i__8374__auto___21598 < len__8373__auto___21597)){
args21594.push((arguments[i__8374__auto___21598]));

var G__21599 = (i__8374__auto___21598 + (1));
i__8374__auto___21598 = G__21599;
continue;
} else {
}
break;
}

var G__21596 = args21594.length;
switch (G__21596) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args21594.length)].join('')));

}
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.call(null,p,ch,null);
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.call(null,cljs.core.complement.call(null,p),ch,buf_or_n);
});

cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3;

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__19275__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__19275__auto__){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (c__19275__auto__){
return (function (state_21766){
var state_val_21767 = (state_21766[(1)]);
if((state_val_21767 === (7))){
var inst_21762 = (state_21766[(2)]);
var state_21766__$1 = state_21766;
var statearr_21768_21809 = state_21766__$1;
(statearr_21768_21809[(2)] = inst_21762);

(statearr_21768_21809[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21767 === (20))){
var inst_21732 = (state_21766[(7)]);
var inst_21743 = (state_21766[(2)]);
var inst_21744 = cljs.core.next.call(null,inst_21732);
var inst_21718 = inst_21744;
var inst_21719 = null;
var inst_21720 = (0);
var inst_21721 = (0);
var state_21766__$1 = (function (){var statearr_21769 = state_21766;
(statearr_21769[(8)] = inst_21719);

(statearr_21769[(9)] = inst_21718);

(statearr_21769[(10)] = inst_21721);

(statearr_21769[(11)] = inst_21743);

(statearr_21769[(12)] = inst_21720);

return statearr_21769;
})();
var statearr_21770_21810 = state_21766__$1;
(statearr_21770_21810[(2)] = null);

(statearr_21770_21810[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21767 === (1))){
var state_21766__$1 = state_21766;
var statearr_21771_21811 = state_21766__$1;
(statearr_21771_21811[(2)] = null);

(statearr_21771_21811[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21767 === (4))){
var inst_21707 = (state_21766[(13)]);
var inst_21707__$1 = (state_21766[(2)]);
var inst_21708 = (inst_21707__$1 == null);
var state_21766__$1 = (function (){var statearr_21772 = state_21766;
(statearr_21772[(13)] = inst_21707__$1);

return statearr_21772;
})();
if(cljs.core.truth_(inst_21708)){
var statearr_21773_21812 = state_21766__$1;
(statearr_21773_21812[(1)] = (5));

} else {
var statearr_21774_21813 = state_21766__$1;
(statearr_21774_21813[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21767 === (15))){
var state_21766__$1 = state_21766;
var statearr_21778_21814 = state_21766__$1;
(statearr_21778_21814[(2)] = null);

(statearr_21778_21814[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21767 === (21))){
var state_21766__$1 = state_21766;
var statearr_21779_21815 = state_21766__$1;
(statearr_21779_21815[(2)] = null);

(statearr_21779_21815[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21767 === (13))){
var inst_21719 = (state_21766[(8)]);
var inst_21718 = (state_21766[(9)]);
var inst_21721 = (state_21766[(10)]);
var inst_21720 = (state_21766[(12)]);
var inst_21728 = (state_21766[(2)]);
var inst_21729 = (inst_21721 + (1));
var tmp21775 = inst_21719;
var tmp21776 = inst_21718;
var tmp21777 = inst_21720;
var inst_21718__$1 = tmp21776;
var inst_21719__$1 = tmp21775;
var inst_21720__$1 = tmp21777;
var inst_21721__$1 = inst_21729;
var state_21766__$1 = (function (){var statearr_21780 = state_21766;
(statearr_21780[(8)] = inst_21719__$1);

(statearr_21780[(9)] = inst_21718__$1);

(statearr_21780[(10)] = inst_21721__$1);

(statearr_21780[(14)] = inst_21728);

(statearr_21780[(12)] = inst_21720__$1);

return statearr_21780;
})();
var statearr_21781_21816 = state_21766__$1;
(statearr_21781_21816[(2)] = null);

(statearr_21781_21816[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21767 === (22))){
var state_21766__$1 = state_21766;
var statearr_21782_21817 = state_21766__$1;
(statearr_21782_21817[(2)] = null);

(statearr_21782_21817[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21767 === (6))){
var inst_21707 = (state_21766[(13)]);
var inst_21716 = f.call(null,inst_21707);
var inst_21717 = cljs.core.seq.call(null,inst_21716);
var inst_21718 = inst_21717;
var inst_21719 = null;
var inst_21720 = (0);
var inst_21721 = (0);
var state_21766__$1 = (function (){var statearr_21783 = state_21766;
(statearr_21783[(8)] = inst_21719);

(statearr_21783[(9)] = inst_21718);

(statearr_21783[(10)] = inst_21721);

(statearr_21783[(12)] = inst_21720);

return statearr_21783;
})();
var statearr_21784_21818 = state_21766__$1;
(statearr_21784_21818[(2)] = null);

(statearr_21784_21818[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21767 === (17))){
var inst_21732 = (state_21766[(7)]);
var inst_21736 = cljs.core.chunk_first.call(null,inst_21732);
var inst_21737 = cljs.core.chunk_rest.call(null,inst_21732);
var inst_21738 = cljs.core.count.call(null,inst_21736);
var inst_21718 = inst_21737;
var inst_21719 = inst_21736;
var inst_21720 = inst_21738;
var inst_21721 = (0);
var state_21766__$1 = (function (){var statearr_21785 = state_21766;
(statearr_21785[(8)] = inst_21719);

(statearr_21785[(9)] = inst_21718);

(statearr_21785[(10)] = inst_21721);

(statearr_21785[(12)] = inst_21720);

return statearr_21785;
})();
var statearr_21786_21819 = state_21766__$1;
(statearr_21786_21819[(2)] = null);

(statearr_21786_21819[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21767 === (3))){
var inst_21764 = (state_21766[(2)]);
var state_21766__$1 = state_21766;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_21766__$1,inst_21764);
} else {
if((state_val_21767 === (12))){
var inst_21752 = (state_21766[(2)]);
var state_21766__$1 = state_21766;
var statearr_21787_21820 = state_21766__$1;
(statearr_21787_21820[(2)] = inst_21752);

(statearr_21787_21820[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21767 === (2))){
var state_21766__$1 = state_21766;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_21766__$1,(4),in$);
} else {
if((state_val_21767 === (23))){
var inst_21760 = (state_21766[(2)]);
var state_21766__$1 = state_21766;
var statearr_21788_21821 = state_21766__$1;
(statearr_21788_21821[(2)] = inst_21760);

(statearr_21788_21821[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21767 === (19))){
var inst_21747 = (state_21766[(2)]);
var state_21766__$1 = state_21766;
var statearr_21789_21822 = state_21766__$1;
(statearr_21789_21822[(2)] = inst_21747);

(statearr_21789_21822[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21767 === (11))){
var inst_21718 = (state_21766[(9)]);
var inst_21732 = (state_21766[(7)]);
var inst_21732__$1 = cljs.core.seq.call(null,inst_21718);
var state_21766__$1 = (function (){var statearr_21790 = state_21766;
(statearr_21790[(7)] = inst_21732__$1);

return statearr_21790;
})();
if(inst_21732__$1){
var statearr_21791_21823 = state_21766__$1;
(statearr_21791_21823[(1)] = (14));

} else {
var statearr_21792_21824 = state_21766__$1;
(statearr_21792_21824[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21767 === (9))){
var inst_21754 = (state_21766[(2)]);
var inst_21755 = cljs.core.async.impl.protocols.closed_QMARK_.call(null,out);
var state_21766__$1 = (function (){var statearr_21793 = state_21766;
(statearr_21793[(15)] = inst_21754);

return statearr_21793;
})();
if(cljs.core.truth_(inst_21755)){
var statearr_21794_21825 = state_21766__$1;
(statearr_21794_21825[(1)] = (21));

} else {
var statearr_21795_21826 = state_21766__$1;
(statearr_21795_21826[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21767 === (5))){
var inst_21710 = cljs.core.async.close_BANG_.call(null,out);
var state_21766__$1 = state_21766;
var statearr_21796_21827 = state_21766__$1;
(statearr_21796_21827[(2)] = inst_21710);

(statearr_21796_21827[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21767 === (14))){
var inst_21732 = (state_21766[(7)]);
var inst_21734 = cljs.core.chunked_seq_QMARK_.call(null,inst_21732);
var state_21766__$1 = state_21766;
if(inst_21734){
var statearr_21797_21828 = state_21766__$1;
(statearr_21797_21828[(1)] = (17));

} else {
var statearr_21798_21829 = state_21766__$1;
(statearr_21798_21829[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21767 === (16))){
var inst_21750 = (state_21766[(2)]);
var state_21766__$1 = state_21766;
var statearr_21799_21830 = state_21766__$1;
(statearr_21799_21830[(2)] = inst_21750);

(statearr_21799_21830[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21767 === (10))){
var inst_21719 = (state_21766[(8)]);
var inst_21721 = (state_21766[(10)]);
var inst_21726 = cljs.core._nth.call(null,inst_21719,inst_21721);
var state_21766__$1 = state_21766;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_21766__$1,(13),out,inst_21726);
} else {
if((state_val_21767 === (18))){
var inst_21732 = (state_21766[(7)]);
var inst_21741 = cljs.core.first.call(null,inst_21732);
var state_21766__$1 = state_21766;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_21766__$1,(20),out,inst_21741);
} else {
if((state_val_21767 === (8))){
var inst_21721 = (state_21766[(10)]);
var inst_21720 = (state_21766[(12)]);
var inst_21723 = (inst_21721 < inst_21720);
var inst_21724 = inst_21723;
var state_21766__$1 = state_21766;
if(cljs.core.truth_(inst_21724)){
var statearr_21800_21831 = state_21766__$1;
(statearr_21800_21831[(1)] = (10));

} else {
var statearr_21801_21832 = state_21766__$1;
(statearr_21801_21832[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__19275__auto__))
;
return ((function (switch__19163__auto__,c__19275__auto__){
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__19164__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__19164__auto____0 = (function (){
var statearr_21805 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_21805[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__19164__auto__);

(statearr_21805[(1)] = (1));

return statearr_21805;
});
var cljs$core$async$mapcat_STAR__$_state_machine__19164__auto____1 = (function (state_21766){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_21766);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e21806){if((e21806 instanceof Object)){
var ex__19167__auto__ = e21806;
var statearr_21807_21833 = state_21766;
(statearr_21807_21833[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_21766);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e21806;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__21834 = state_21766;
state_21766 = G__21834;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__19164__auto__ = function(state_21766){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__19164__auto____1.call(this,state_21766);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$mapcat_STAR__$_state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__19164__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__19164__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__19164__auto__;
})()
;})(switch__19163__auto__,c__19275__auto__))
})();
var state__19277__auto__ = (function (){var statearr_21808 = f__19276__auto__.call(null);
(statearr_21808[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto__);

return statearr_21808;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(c__19275__auto__))
);

return c__19275__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var args21835 = [];
var len__8373__auto___21838 = arguments.length;
var i__8374__auto___21839 = (0);
while(true){
if((i__8374__auto___21839 < len__8373__auto___21838)){
args21835.push((arguments[i__8374__auto___21839]));

var G__21840 = (i__8374__auto___21839 + (1));
i__8374__auto___21839 = G__21840;
continue;
} else {
}
break;
}

var G__21837 = args21835.length;
switch (G__21837) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args21835.length)].join('')));

}
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.call(null,f,in$,null);
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
cljs.core.async.mapcat_STAR_.call(null,f,in$,out);

return out;
});

cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var args21842 = [];
var len__8373__auto___21845 = arguments.length;
var i__8374__auto___21846 = (0);
while(true){
if((i__8374__auto___21846 < len__8373__auto___21845)){
args21842.push((arguments[i__8374__auto___21846]));

var G__21847 = (i__8374__auto___21846 + (1));
i__8374__auto___21846 = G__21847;
continue;
} else {
}
break;
}

var G__21844 = args21842.length;
switch (G__21844) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args21842.length)].join('')));

}
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.call(null,f,out,null);
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.call(null,buf_or_n);
cljs.core.async.mapcat_STAR_.call(null,f,in$,out);

return in$;
});

cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var args21849 = [];
var len__8373__auto___21900 = arguments.length;
var i__8374__auto___21901 = (0);
while(true){
if((i__8374__auto___21901 < len__8373__auto___21900)){
args21849.push((arguments[i__8374__auto___21901]));

var G__21902 = (i__8374__auto___21901 + (1));
i__8374__auto___21901 = G__21902;
continue;
} else {
}
break;
}

var G__21851 = args21849.length;
switch (G__21851) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args21849.length)].join('')));

}
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.call(null,ch,null);
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__19275__auto___21904 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__19275__auto___21904,out){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (c__19275__auto___21904,out){
return (function (state_21875){
var state_val_21876 = (state_21875[(1)]);
if((state_val_21876 === (7))){
var inst_21870 = (state_21875[(2)]);
var state_21875__$1 = state_21875;
var statearr_21877_21905 = state_21875__$1;
(statearr_21877_21905[(2)] = inst_21870);

(statearr_21877_21905[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21876 === (1))){
var inst_21852 = null;
var state_21875__$1 = (function (){var statearr_21878 = state_21875;
(statearr_21878[(7)] = inst_21852);

return statearr_21878;
})();
var statearr_21879_21906 = state_21875__$1;
(statearr_21879_21906[(2)] = null);

(statearr_21879_21906[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21876 === (4))){
var inst_21855 = (state_21875[(8)]);
var inst_21855__$1 = (state_21875[(2)]);
var inst_21856 = (inst_21855__$1 == null);
var inst_21857 = cljs.core.not.call(null,inst_21856);
var state_21875__$1 = (function (){var statearr_21880 = state_21875;
(statearr_21880[(8)] = inst_21855__$1);

return statearr_21880;
})();
if(inst_21857){
var statearr_21881_21907 = state_21875__$1;
(statearr_21881_21907[(1)] = (5));

} else {
var statearr_21882_21908 = state_21875__$1;
(statearr_21882_21908[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21876 === (6))){
var state_21875__$1 = state_21875;
var statearr_21883_21909 = state_21875__$1;
(statearr_21883_21909[(2)] = null);

(statearr_21883_21909[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21876 === (3))){
var inst_21872 = (state_21875[(2)]);
var inst_21873 = cljs.core.async.close_BANG_.call(null,out);
var state_21875__$1 = (function (){var statearr_21884 = state_21875;
(statearr_21884[(9)] = inst_21872);

return statearr_21884;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_21875__$1,inst_21873);
} else {
if((state_val_21876 === (2))){
var state_21875__$1 = state_21875;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_21875__$1,(4),ch);
} else {
if((state_val_21876 === (11))){
var inst_21855 = (state_21875[(8)]);
var inst_21864 = (state_21875[(2)]);
var inst_21852 = inst_21855;
var state_21875__$1 = (function (){var statearr_21885 = state_21875;
(statearr_21885[(10)] = inst_21864);

(statearr_21885[(7)] = inst_21852);

return statearr_21885;
})();
var statearr_21886_21910 = state_21875__$1;
(statearr_21886_21910[(2)] = null);

(statearr_21886_21910[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21876 === (9))){
var inst_21855 = (state_21875[(8)]);
var state_21875__$1 = state_21875;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_21875__$1,(11),out,inst_21855);
} else {
if((state_val_21876 === (5))){
var inst_21855 = (state_21875[(8)]);
var inst_21852 = (state_21875[(7)]);
var inst_21859 = cljs.core._EQ_.call(null,inst_21855,inst_21852);
var state_21875__$1 = state_21875;
if(inst_21859){
var statearr_21888_21911 = state_21875__$1;
(statearr_21888_21911[(1)] = (8));

} else {
var statearr_21889_21912 = state_21875__$1;
(statearr_21889_21912[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21876 === (10))){
var inst_21867 = (state_21875[(2)]);
var state_21875__$1 = state_21875;
var statearr_21890_21913 = state_21875__$1;
(statearr_21890_21913[(2)] = inst_21867);

(statearr_21890_21913[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21876 === (8))){
var inst_21852 = (state_21875[(7)]);
var tmp21887 = inst_21852;
var inst_21852__$1 = tmp21887;
var state_21875__$1 = (function (){var statearr_21891 = state_21875;
(statearr_21891[(7)] = inst_21852__$1);

return statearr_21891;
})();
var statearr_21892_21914 = state_21875__$1;
(statearr_21892_21914[(2)] = null);

(statearr_21892_21914[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__19275__auto___21904,out))
;
return ((function (switch__19163__auto__,c__19275__auto___21904,out){
return (function() {
var cljs$core$async$state_machine__19164__auto__ = null;
var cljs$core$async$state_machine__19164__auto____0 = (function (){
var statearr_21896 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_21896[(0)] = cljs$core$async$state_machine__19164__auto__);

(statearr_21896[(1)] = (1));

return statearr_21896;
});
var cljs$core$async$state_machine__19164__auto____1 = (function (state_21875){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_21875);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e21897){if((e21897 instanceof Object)){
var ex__19167__auto__ = e21897;
var statearr_21898_21915 = state_21875;
(statearr_21898_21915[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_21875);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e21897;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__21916 = state_21875;
state_21875 = G__21916;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$state_machine__19164__auto__ = function(state_21875){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19164__auto____1.call(this,state_21875);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19164__auto____0;
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19164__auto____1;
return cljs$core$async$state_machine__19164__auto__;
})()
;})(switch__19163__auto__,c__19275__auto___21904,out))
})();
var state__19277__auto__ = (function (){var statearr_21899 = f__19276__auto__.call(null);
(statearr_21899[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto___21904);

return statearr_21899;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(c__19275__auto___21904,out))
);


return out;
});

cljs.core.async.unique.cljs$lang$maxFixedArity = 2;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var args21917 = [];
var len__8373__auto___21987 = arguments.length;
var i__8374__auto___21988 = (0);
while(true){
if((i__8374__auto___21988 < len__8373__auto___21987)){
args21917.push((arguments[i__8374__auto___21988]));

var G__21989 = (i__8374__auto___21988 + (1));
i__8374__auto___21988 = G__21989;
continue;
} else {
}
break;
}

var G__21919 = args21917.length;
switch (G__21919) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args21917.length)].join('')));

}
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.call(null,n,ch,null);
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__19275__auto___21991 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__19275__auto___21991,out){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (c__19275__auto___21991,out){
return (function (state_21957){
var state_val_21958 = (state_21957[(1)]);
if((state_val_21958 === (7))){
var inst_21953 = (state_21957[(2)]);
var state_21957__$1 = state_21957;
var statearr_21959_21992 = state_21957__$1;
(statearr_21959_21992[(2)] = inst_21953);

(statearr_21959_21992[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21958 === (1))){
var inst_21920 = (new Array(n));
var inst_21921 = inst_21920;
var inst_21922 = (0);
var state_21957__$1 = (function (){var statearr_21960 = state_21957;
(statearr_21960[(7)] = inst_21921);

(statearr_21960[(8)] = inst_21922);

return statearr_21960;
})();
var statearr_21961_21993 = state_21957__$1;
(statearr_21961_21993[(2)] = null);

(statearr_21961_21993[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21958 === (4))){
var inst_21925 = (state_21957[(9)]);
var inst_21925__$1 = (state_21957[(2)]);
var inst_21926 = (inst_21925__$1 == null);
var inst_21927 = cljs.core.not.call(null,inst_21926);
var state_21957__$1 = (function (){var statearr_21962 = state_21957;
(statearr_21962[(9)] = inst_21925__$1);

return statearr_21962;
})();
if(inst_21927){
var statearr_21963_21994 = state_21957__$1;
(statearr_21963_21994[(1)] = (5));

} else {
var statearr_21964_21995 = state_21957__$1;
(statearr_21964_21995[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21958 === (15))){
var inst_21947 = (state_21957[(2)]);
var state_21957__$1 = state_21957;
var statearr_21965_21996 = state_21957__$1;
(statearr_21965_21996[(2)] = inst_21947);

(statearr_21965_21996[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21958 === (13))){
var state_21957__$1 = state_21957;
var statearr_21966_21997 = state_21957__$1;
(statearr_21966_21997[(2)] = null);

(statearr_21966_21997[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21958 === (6))){
var inst_21922 = (state_21957[(8)]);
var inst_21943 = (inst_21922 > (0));
var state_21957__$1 = state_21957;
if(cljs.core.truth_(inst_21943)){
var statearr_21967_21998 = state_21957__$1;
(statearr_21967_21998[(1)] = (12));

} else {
var statearr_21968_21999 = state_21957__$1;
(statearr_21968_21999[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21958 === (3))){
var inst_21955 = (state_21957[(2)]);
var state_21957__$1 = state_21957;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_21957__$1,inst_21955);
} else {
if((state_val_21958 === (12))){
var inst_21921 = (state_21957[(7)]);
var inst_21945 = cljs.core.vec.call(null,inst_21921);
var state_21957__$1 = state_21957;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_21957__$1,(15),out,inst_21945);
} else {
if((state_val_21958 === (2))){
var state_21957__$1 = state_21957;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_21957__$1,(4),ch);
} else {
if((state_val_21958 === (11))){
var inst_21937 = (state_21957[(2)]);
var inst_21938 = (new Array(n));
var inst_21921 = inst_21938;
var inst_21922 = (0);
var state_21957__$1 = (function (){var statearr_21969 = state_21957;
(statearr_21969[(7)] = inst_21921);

(statearr_21969[(8)] = inst_21922);

(statearr_21969[(10)] = inst_21937);

return statearr_21969;
})();
var statearr_21970_22000 = state_21957__$1;
(statearr_21970_22000[(2)] = null);

(statearr_21970_22000[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21958 === (9))){
var inst_21921 = (state_21957[(7)]);
var inst_21935 = cljs.core.vec.call(null,inst_21921);
var state_21957__$1 = state_21957;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_21957__$1,(11),out,inst_21935);
} else {
if((state_val_21958 === (5))){
var inst_21921 = (state_21957[(7)]);
var inst_21930 = (state_21957[(11)]);
var inst_21922 = (state_21957[(8)]);
var inst_21925 = (state_21957[(9)]);
var inst_21929 = (inst_21921[inst_21922] = inst_21925);
var inst_21930__$1 = (inst_21922 + (1));
var inst_21931 = (inst_21930__$1 < n);
var state_21957__$1 = (function (){var statearr_21971 = state_21957;
(statearr_21971[(12)] = inst_21929);

(statearr_21971[(11)] = inst_21930__$1);

return statearr_21971;
})();
if(cljs.core.truth_(inst_21931)){
var statearr_21972_22001 = state_21957__$1;
(statearr_21972_22001[(1)] = (8));

} else {
var statearr_21973_22002 = state_21957__$1;
(statearr_21973_22002[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21958 === (14))){
var inst_21950 = (state_21957[(2)]);
var inst_21951 = cljs.core.async.close_BANG_.call(null,out);
var state_21957__$1 = (function (){var statearr_21975 = state_21957;
(statearr_21975[(13)] = inst_21950);

return statearr_21975;
})();
var statearr_21976_22003 = state_21957__$1;
(statearr_21976_22003[(2)] = inst_21951);

(statearr_21976_22003[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21958 === (10))){
var inst_21941 = (state_21957[(2)]);
var state_21957__$1 = state_21957;
var statearr_21977_22004 = state_21957__$1;
(statearr_21977_22004[(2)] = inst_21941);

(statearr_21977_22004[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_21958 === (8))){
var inst_21921 = (state_21957[(7)]);
var inst_21930 = (state_21957[(11)]);
var tmp21974 = inst_21921;
var inst_21921__$1 = tmp21974;
var inst_21922 = inst_21930;
var state_21957__$1 = (function (){var statearr_21978 = state_21957;
(statearr_21978[(7)] = inst_21921__$1);

(statearr_21978[(8)] = inst_21922);

return statearr_21978;
})();
var statearr_21979_22005 = state_21957__$1;
(statearr_21979_22005[(2)] = null);

(statearr_21979_22005[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__19275__auto___21991,out))
;
return ((function (switch__19163__auto__,c__19275__auto___21991,out){
return (function() {
var cljs$core$async$state_machine__19164__auto__ = null;
var cljs$core$async$state_machine__19164__auto____0 = (function (){
var statearr_21983 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_21983[(0)] = cljs$core$async$state_machine__19164__auto__);

(statearr_21983[(1)] = (1));

return statearr_21983;
});
var cljs$core$async$state_machine__19164__auto____1 = (function (state_21957){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_21957);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e21984){if((e21984 instanceof Object)){
var ex__19167__auto__ = e21984;
var statearr_21985_22006 = state_21957;
(statearr_21985_22006[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_21957);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e21984;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__22007 = state_21957;
state_21957 = G__22007;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$state_machine__19164__auto__ = function(state_21957){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19164__auto____1.call(this,state_21957);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19164__auto____0;
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19164__auto____1;
return cljs$core$async$state_machine__19164__auto__;
})()
;})(switch__19163__auto__,c__19275__auto___21991,out))
})();
var state__19277__auto__ = (function (){var statearr_21986 = f__19276__auto__.call(null);
(statearr_21986[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto___21991);

return statearr_21986;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(c__19275__auto___21991,out))
);


return out;
});

cljs.core.async.partition.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var args22008 = [];
var len__8373__auto___22082 = arguments.length;
var i__8374__auto___22083 = (0);
while(true){
if((i__8374__auto___22083 < len__8373__auto___22082)){
args22008.push((arguments[i__8374__auto___22083]));

var G__22084 = (i__8374__auto___22083 + (1));
i__8374__auto___22083 = G__22084;
continue;
} else {
}
break;
}

var G__22010 = args22008.length;
switch (G__22010) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args22008.length)].join('')));

}
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.call(null,f,ch,null);
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__19275__auto___22086 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__19275__auto___22086,out){
return (function (){
var f__19276__auto__ = (function (){var switch__19163__auto__ = ((function (c__19275__auto___22086,out){
return (function (state_22052){
var state_val_22053 = (state_22052[(1)]);
if((state_val_22053 === (7))){
var inst_22048 = (state_22052[(2)]);
var state_22052__$1 = state_22052;
var statearr_22054_22087 = state_22052__$1;
(statearr_22054_22087[(2)] = inst_22048);

(statearr_22054_22087[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22053 === (1))){
var inst_22011 = [];
var inst_22012 = inst_22011;
var inst_22013 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_22052__$1 = (function (){var statearr_22055 = state_22052;
(statearr_22055[(7)] = inst_22013);

(statearr_22055[(8)] = inst_22012);

return statearr_22055;
})();
var statearr_22056_22088 = state_22052__$1;
(statearr_22056_22088[(2)] = null);

(statearr_22056_22088[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22053 === (4))){
var inst_22016 = (state_22052[(9)]);
var inst_22016__$1 = (state_22052[(2)]);
var inst_22017 = (inst_22016__$1 == null);
var inst_22018 = cljs.core.not.call(null,inst_22017);
var state_22052__$1 = (function (){var statearr_22057 = state_22052;
(statearr_22057[(9)] = inst_22016__$1);

return statearr_22057;
})();
if(inst_22018){
var statearr_22058_22089 = state_22052__$1;
(statearr_22058_22089[(1)] = (5));

} else {
var statearr_22059_22090 = state_22052__$1;
(statearr_22059_22090[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22053 === (15))){
var inst_22042 = (state_22052[(2)]);
var state_22052__$1 = state_22052;
var statearr_22060_22091 = state_22052__$1;
(statearr_22060_22091[(2)] = inst_22042);

(statearr_22060_22091[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22053 === (13))){
var state_22052__$1 = state_22052;
var statearr_22061_22092 = state_22052__$1;
(statearr_22061_22092[(2)] = null);

(statearr_22061_22092[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22053 === (6))){
var inst_22012 = (state_22052[(8)]);
var inst_22037 = inst_22012.length;
var inst_22038 = (inst_22037 > (0));
var state_22052__$1 = state_22052;
if(cljs.core.truth_(inst_22038)){
var statearr_22062_22093 = state_22052__$1;
(statearr_22062_22093[(1)] = (12));

} else {
var statearr_22063_22094 = state_22052__$1;
(statearr_22063_22094[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22053 === (3))){
var inst_22050 = (state_22052[(2)]);
var state_22052__$1 = state_22052;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_22052__$1,inst_22050);
} else {
if((state_val_22053 === (12))){
var inst_22012 = (state_22052[(8)]);
var inst_22040 = cljs.core.vec.call(null,inst_22012);
var state_22052__$1 = state_22052;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_22052__$1,(15),out,inst_22040);
} else {
if((state_val_22053 === (2))){
var state_22052__$1 = state_22052;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_22052__$1,(4),ch);
} else {
if((state_val_22053 === (11))){
var inst_22016 = (state_22052[(9)]);
var inst_22020 = (state_22052[(10)]);
var inst_22030 = (state_22052[(2)]);
var inst_22031 = [];
var inst_22032 = inst_22031.push(inst_22016);
var inst_22012 = inst_22031;
var inst_22013 = inst_22020;
var state_22052__$1 = (function (){var statearr_22064 = state_22052;
(statearr_22064[(7)] = inst_22013);

(statearr_22064[(8)] = inst_22012);

(statearr_22064[(11)] = inst_22030);

(statearr_22064[(12)] = inst_22032);

return statearr_22064;
})();
var statearr_22065_22095 = state_22052__$1;
(statearr_22065_22095[(2)] = null);

(statearr_22065_22095[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22053 === (9))){
var inst_22012 = (state_22052[(8)]);
var inst_22028 = cljs.core.vec.call(null,inst_22012);
var state_22052__$1 = state_22052;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_22052__$1,(11),out,inst_22028);
} else {
if((state_val_22053 === (5))){
var inst_22013 = (state_22052[(7)]);
var inst_22016 = (state_22052[(9)]);
var inst_22020 = (state_22052[(10)]);
var inst_22020__$1 = f.call(null,inst_22016);
var inst_22021 = cljs.core._EQ_.call(null,inst_22020__$1,inst_22013);
var inst_22022 = cljs.core.keyword_identical_QMARK_.call(null,inst_22013,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var inst_22023 = (inst_22021) || (inst_22022);
var state_22052__$1 = (function (){var statearr_22066 = state_22052;
(statearr_22066[(10)] = inst_22020__$1);

return statearr_22066;
})();
if(cljs.core.truth_(inst_22023)){
var statearr_22067_22096 = state_22052__$1;
(statearr_22067_22096[(1)] = (8));

} else {
var statearr_22068_22097 = state_22052__$1;
(statearr_22068_22097[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22053 === (14))){
var inst_22045 = (state_22052[(2)]);
var inst_22046 = cljs.core.async.close_BANG_.call(null,out);
var state_22052__$1 = (function (){var statearr_22070 = state_22052;
(statearr_22070[(13)] = inst_22045);

return statearr_22070;
})();
var statearr_22071_22098 = state_22052__$1;
(statearr_22071_22098[(2)] = inst_22046);

(statearr_22071_22098[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22053 === (10))){
var inst_22035 = (state_22052[(2)]);
var state_22052__$1 = state_22052;
var statearr_22072_22099 = state_22052__$1;
(statearr_22072_22099[(2)] = inst_22035);

(statearr_22072_22099[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_22053 === (8))){
var inst_22012 = (state_22052[(8)]);
var inst_22016 = (state_22052[(9)]);
var inst_22020 = (state_22052[(10)]);
var inst_22025 = inst_22012.push(inst_22016);
var tmp22069 = inst_22012;
var inst_22012__$1 = tmp22069;
var inst_22013 = inst_22020;
var state_22052__$1 = (function (){var statearr_22073 = state_22052;
(statearr_22073[(7)] = inst_22013);

(statearr_22073[(8)] = inst_22012__$1);

(statearr_22073[(14)] = inst_22025);

return statearr_22073;
})();
var statearr_22074_22100 = state_22052__$1;
(statearr_22074_22100[(2)] = null);

(statearr_22074_22100[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__19275__auto___22086,out))
;
return ((function (switch__19163__auto__,c__19275__auto___22086,out){
return (function() {
var cljs$core$async$state_machine__19164__auto__ = null;
var cljs$core$async$state_machine__19164__auto____0 = (function (){
var statearr_22078 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_22078[(0)] = cljs$core$async$state_machine__19164__auto__);

(statearr_22078[(1)] = (1));

return statearr_22078;
});
var cljs$core$async$state_machine__19164__auto____1 = (function (state_22052){
while(true){
var ret_value__19165__auto__ = (function (){try{while(true){
var result__19166__auto__ = switch__19163__auto__.call(null,state_22052);
if(cljs.core.keyword_identical_QMARK_.call(null,result__19166__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__19166__auto__;
}
break;
}
}catch (e22079){if((e22079 instanceof Object)){
var ex__19167__auto__ = e22079;
var statearr_22080_22101 = state_22052;
(statearr_22080_22101[(5)] = ex__19167__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_22052);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e22079;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__19165__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__22102 = state_22052;
state_22052 = G__22102;
continue;
} else {
return ret_value__19165__auto__;
}
break;
}
});
cljs$core$async$state_machine__19164__auto__ = function(state_22052){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__19164__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__19164__auto____1.call(this,state_22052);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__19164__auto____0;
cljs$core$async$state_machine__19164__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__19164__auto____1;
return cljs$core$async$state_machine__19164__auto__;
})()
;})(switch__19163__auto__,c__19275__auto___22086,out))
})();
var state__19277__auto__ = (function (){var statearr_22081 = f__19276__auto__.call(null);
(statearr_22081[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__19275__auto___22086);

return statearr_22081;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__19277__auto__);
});})(c__19275__auto___22086,out))
);


return out;
});

cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3;

