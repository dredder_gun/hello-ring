// Compiled by ClojureScript 1.9.521 {}
goog.provide('cljs.spec.impl.gen');
goog.require('cljs.core');
goog.require('cljs.core');

/**
* @constructor
 * @implements {cljs.core.IDeref}
*/
cljs.spec.impl.gen.LazyVar = (function (f,cached){
this.f = f;
this.cached = cached;
this.cljs$lang$protocol_mask$partition0$ = 32768;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.spec.impl.gen.LazyVar.prototype.cljs$core$IDeref$_deref$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
if(!((self__.cached == null))){
return self__.cached;
} else {
var x = self__.f.call(null);
if((x == null)){
} else {
self__.cached = x;
}

return x;
}
});

cljs.spec.impl.gen.LazyVar.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),cljs.core.with_meta(new cljs.core.Symbol(null,"cached","cached",-1216707864,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"mutable","mutable",875778266),true], null))], null);
});

cljs.spec.impl.gen.LazyVar.cljs$lang$type = true;

cljs.spec.impl.gen.LazyVar.cljs$lang$ctorStr = "cljs.spec.impl.gen/LazyVar";

cljs.spec.impl.gen.LazyVar.cljs$lang$ctorPrWriter = (function (this__8005__auto__,writer__8006__auto__,opt__8007__auto__){
return cljs.core._write.call(null,writer__8006__auto__,"cljs.spec.impl.gen/LazyVar");
});

cljs.spec.impl.gen.__GT_LazyVar = (function cljs$spec$impl$gen$__GT_LazyVar(f,cached){
return (new cljs.spec.impl.gen.LazyVar(f,cached));
});

cljs.spec.impl.gen.quick_check_ref = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check.quick_check !== 'undefined')){
return clojure.test.check.quick_check;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check","quick-check","clojure.test.check/quick-check",-810344251,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check","quick-check","clojure.test.check/quick-check",-810344251,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
cljs.spec.impl.gen.quick_check = (function cljs$spec$impl$gen$quick_check(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18015 = arguments.length;
var i__8508__auto___18016 = (0);
while(true){
if((i__8508__auto___18016 < len__8507__auto___18015)){
args__8514__auto__.push((arguments[i__8508__auto___18016]));

var G__18017 = (i__8508__auto___18016 + (1));
i__8508__auto___18016 = G__18017;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.quick_check.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});

cljs.spec.impl.gen.quick_check.cljs$core$IFn$_invoke$arity$variadic = (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,cljs.spec.impl.gen.quick_check_ref),args);
});

cljs.spec.impl.gen.quick_check.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.quick_check.cljs$lang$applyTo = (function (seq18014){
return cljs.spec.impl.gen.quick_check.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18014));
});

cljs.spec.impl.gen.for_all_STAR__ref = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.properties.for_all_STAR_ !== 'undefined')){
return clojure.test.check.properties.for_all_STAR_;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.properties","for-all*","clojure.test.check.properties/for-all*",67088845,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.properties","for-all*","clojure.test.check.properties/for-all*",67088845,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Dynamically loaded clojure.test.check.properties/for-all*.
 */
cljs.spec.impl.gen.for_all_STAR_ = (function cljs$spec$impl$gen$for_all_STAR_(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18019 = arguments.length;
var i__8508__auto___18020 = (0);
while(true){
if((i__8508__auto___18020 < len__8507__auto___18019)){
args__8514__auto__.push((arguments[i__8508__auto___18020]));

var G__18021 = (i__8508__auto___18020 + (1));
i__8508__auto___18020 = G__18021;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.for_all_STAR_.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});

cljs.spec.impl.gen.for_all_STAR_.cljs$core$IFn$_invoke$arity$variadic = (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,cljs.spec.impl.gen.for_all_STAR__ref),args);
});

cljs.spec.impl.gen.for_all_STAR_.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.for_all_STAR_.cljs$lang$applyTo = (function (seq18018){
return cljs.spec.impl.gen.for_all_STAR_.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18018));
});

var g_QMARK__18022 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.generator_QMARK_ !== 'undefined')){
return clojure.test.check.generators.generator_QMARK_;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","generator?","clojure.test.check.generators/generator?",-1378210460,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","generator?","clojure.test.check.generators/generator?",-1378210460,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
var g_18023 = (new cljs.spec.impl.gen.LazyVar(((function (g_QMARK__18022){
return (function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.generate !== 'undefined')){
return clojure.test.check.generators.generate;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","generate","clojure.test.check.generators/generate",-690390711,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","generate","clojure.test.check.generators/generate",-690390711,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
});})(g_QMARK__18022))
,null));
var mkg_18024 = (new cljs.spec.impl.gen.LazyVar(((function (g_QMARK__18022,g_18023){
return (function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.__GT_Generator !== 'undefined')){
return clojure.test.check.generators.__GT_Generator;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","->Generator","clojure.test.check.generators/->Generator",-1179475051,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","->Generator","clojure.test.check.generators/->Generator",-1179475051,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
});})(g_QMARK__18022,g_18023))
,null));
cljs.spec.impl.gen.generator_QMARK_ = ((function (g_QMARK__18022,g_18023,mkg_18024){
return (function cljs$spec$impl$gen$generator_QMARK_(x){
return cljs.core.deref.call(null,g_QMARK__18022).call(null,x);
});})(g_QMARK__18022,g_18023,mkg_18024))
;

cljs.spec.impl.gen.generator = ((function (g_QMARK__18022,g_18023,mkg_18024){
return (function cljs$spec$impl$gen$generator(gfn){
return cljs.core.deref.call(null,mkg_18024).call(null,gfn);
});})(g_QMARK__18022,g_18023,mkg_18024))
;

/**
 * Generate a single value using generator.
 */
cljs.spec.impl.gen.generate = ((function (g_QMARK__18022,g_18023,mkg_18024){
return (function cljs$spec$impl$gen$generate(generator){
return cljs.core.deref.call(null,g_18023).call(null,generator);
});})(g_QMARK__18022,g_18023,mkg_18024))
;
cljs.spec.impl.gen.delay_impl = (function cljs$spec$impl$gen$delay_impl(gfnd){
return cljs.spec.impl.gen.generator.call(null,(function (rnd,size){
return new cljs.core.Keyword(null,"gen","gen",142575302).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,gfnd)).call(null,rnd,size);
}));
});
var g__17986__auto___18044 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.hash_map !== 'undefined')){
return clojure.test.check.generators.hash_map;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","hash-map","clojure.test.check.generators/hash-map",1961346626,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","hash-map","clojure.test.check.generators/hash-map",1961346626,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/hash-map
 */
cljs.spec.impl.gen.hash_map = ((function (g__17986__auto___18044){
return (function cljs$spec$impl$gen$hash_map(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18045 = arguments.length;
var i__8508__auto___18046 = (0);
while(true){
if((i__8508__auto___18046 < len__8507__auto___18045)){
args__8514__auto__.push((arguments[i__8508__auto___18046]));

var G__18047 = (i__8508__auto___18046 + (1));
i__8508__auto___18046 = G__18047;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.hash_map.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17986__auto___18044))
;

cljs.spec.impl.gen.hash_map.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17986__auto___18044){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__17986__auto___18044),args);
});})(g__17986__auto___18044))
;

cljs.spec.impl.gen.hash_map.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.hash_map.cljs$lang$applyTo = ((function (g__17986__auto___18044){
return (function (seq18025){
return cljs.spec.impl.gen.hash_map.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18025));
});})(g__17986__auto___18044))
;


var g__17986__auto___18048 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.list !== 'undefined')){
return clojure.test.check.generators.list;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","list","clojure.test.check.generators/list",506971058,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","list","clojure.test.check.generators/list",506971058,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/list
 */
cljs.spec.impl.gen.list = ((function (g__17986__auto___18048){
return (function cljs$spec$impl$gen$list(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18049 = arguments.length;
var i__8508__auto___18050 = (0);
while(true){
if((i__8508__auto___18050 < len__8507__auto___18049)){
args__8514__auto__.push((arguments[i__8508__auto___18050]));

var G__18051 = (i__8508__auto___18050 + (1));
i__8508__auto___18050 = G__18051;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.list.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17986__auto___18048))
;

cljs.spec.impl.gen.list.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17986__auto___18048){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__17986__auto___18048),args);
});})(g__17986__auto___18048))
;

cljs.spec.impl.gen.list.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.list.cljs$lang$applyTo = ((function (g__17986__auto___18048){
return (function (seq18026){
return cljs.spec.impl.gen.list.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18026));
});})(g__17986__auto___18048))
;


var g__17986__auto___18052 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.map !== 'undefined')){
return clojure.test.check.generators.map;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","map","clojure.test.check.generators/map",45738796,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","map","clojure.test.check.generators/map",45738796,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/map
 */
cljs.spec.impl.gen.map = ((function (g__17986__auto___18052){
return (function cljs$spec$impl$gen$map(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18053 = arguments.length;
var i__8508__auto___18054 = (0);
while(true){
if((i__8508__auto___18054 < len__8507__auto___18053)){
args__8514__auto__.push((arguments[i__8508__auto___18054]));

var G__18055 = (i__8508__auto___18054 + (1));
i__8508__auto___18054 = G__18055;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.map.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17986__auto___18052))
;

cljs.spec.impl.gen.map.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17986__auto___18052){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__17986__auto___18052),args);
});})(g__17986__auto___18052))
;

cljs.spec.impl.gen.map.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.map.cljs$lang$applyTo = ((function (g__17986__auto___18052){
return (function (seq18027){
return cljs.spec.impl.gen.map.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18027));
});})(g__17986__auto___18052))
;


var g__17986__auto___18056 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.not_empty !== 'undefined')){
return clojure.test.check.generators.not_empty;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","not-empty","clojure.test.check.generators/not-empty",-876211682,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","not-empty","clojure.test.check.generators/not-empty",-876211682,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/not-empty
 */
cljs.spec.impl.gen.not_empty = ((function (g__17986__auto___18056){
return (function cljs$spec$impl$gen$not_empty(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18057 = arguments.length;
var i__8508__auto___18058 = (0);
while(true){
if((i__8508__auto___18058 < len__8507__auto___18057)){
args__8514__auto__.push((arguments[i__8508__auto___18058]));

var G__18059 = (i__8508__auto___18058 + (1));
i__8508__auto___18058 = G__18059;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.not_empty.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17986__auto___18056))
;

cljs.spec.impl.gen.not_empty.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17986__auto___18056){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__17986__auto___18056),args);
});})(g__17986__auto___18056))
;

cljs.spec.impl.gen.not_empty.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.not_empty.cljs$lang$applyTo = ((function (g__17986__auto___18056){
return (function (seq18028){
return cljs.spec.impl.gen.not_empty.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18028));
});})(g__17986__auto___18056))
;


var g__17986__auto___18060 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.set !== 'undefined')){
return clojure.test.check.generators.set;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","set","clojure.test.check.generators/set",-1027639543,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","set","clojure.test.check.generators/set",-1027639543,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/set
 */
cljs.spec.impl.gen.set = ((function (g__17986__auto___18060){
return (function cljs$spec$impl$gen$set(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18061 = arguments.length;
var i__8508__auto___18062 = (0);
while(true){
if((i__8508__auto___18062 < len__8507__auto___18061)){
args__8514__auto__.push((arguments[i__8508__auto___18062]));

var G__18063 = (i__8508__auto___18062 + (1));
i__8508__auto___18062 = G__18063;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.set.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17986__auto___18060))
;

cljs.spec.impl.gen.set.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17986__auto___18060){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__17986__auto___18060),args);
});})(g__17986__auto___18060))
;

cljs.spec.impl.gen.set.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.set.cljs$lang$applyTo = ((function (g__17986__auto___18060){
return (function (seq18029){
return cljs.spec.impl.gen.set.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18029));
});})(g__17986__auto___18060))
;


var g__17986__auto___18064 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.vector !== 'undefined')){
return clojure.test.check.generators.vector;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","vector","clojure.test.check.generators/vector",1081775325,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","vector","clojure.test.check.generators/vector",1081775325,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/vector
 */
cljs.spec.impl.gen.vector = ((function (g__17986__auto___18064){
return (function cljs$spec$impl$gen$vector(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18065 = arguments.length;
var i__8508__auto___18066 = (0);
while(true){
if((i__8508__auto___18066 < len__8507__auto___18065)){
args__8514__auto__.push((arguments[i__8508__auto___18066]));

var G__18067 = (i__8508__auto___18066 + (1));
i__8508__auto___18066 = G__18067;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.vector.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17986__auto___18064))
;

cljs.spec.impl.gen.vector.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17986__auto___18064){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__17986__auto___18064),args);
});})(g__17986__auto___18064))
;

cljs.spec.impl.gen.vector.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.vector.cljs$lang$applyTo = ((function (g__17986__auto___18064){
return (function (seq18030){
return cljs.spec.impl.gen.vector.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18030));
});})(g__17986__auto___18064))
;


var g__17986__auto___18068 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.vector_distinct !== 'undefined')){
return clojure.test.check.generators.vector_distinct;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","vector-distinct","clojure.test.check.generators/vector-distinct",1656877834,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","vector-distinct","clojure.test.check.generators/vector-distinct",1656877834,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/vector-distinct
 */
cljs.spec.impl.gen.vector_distinct = ((function (g__17986__auto___18068){
return (function cljs$spec$impl$gen$vector_distinct(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18069 = arguments.length;
var i__8508__auto___18070 = (0);
while(true){
if((i__8508__auto___18070 < len__8507__auto___18069)){
args__8514__auto__.push((arguments[i__8508__auto___18070]));

var G__18071 = (i__8508__auto___18070 + (1));
i__8508__auto___18070 = G__18071;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.vector_distinct.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17986__auto___18068))
;

cljs.spec.impl.gen.vector_distinct.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17986__auto___18068){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__17986__auto___18068),args);
});})(g__17986__auto___18068))
;

cljs.spec.impl.gen.vector_distinct.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.vector_distinct.cljs$lang$applyTo = ((function (g__17986__auto___18068){
return (function (seq18031){
return cljs.spec.impl.gen.vector_distinct.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18031));
});})(g__17986__auto___18068))
;


var g__17986__auto___18072 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.fmap !== 'undefined')){
return clojure.test.check.generators.fmap;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","fmap","clojure.test.check.generators/fmap",1957997092,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","fmap","clojure.test.check.generators/fmap",1957997092,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/fmap
 */
cljs.spec.impl.gen.fmap = ((function (g__17986__auto___18072){
return (function cljs$spec$impl$gen$fmap(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18073 = arguments.length;
var i__8508__auto___18074 = (0);
while(true){
if((i__8508__auto___18074 < len__8507__auto___18073)){
args__8514__auto__.push((arguments[i__8508__auto___18074]));

var G__18075 = (i__8508__auto___18074 + (1));
i__8508__auto___18074 = G__18075;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.fmap.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17986__auto___18072))
;

cljs.spec.impl.gen.fmap.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17986__auto___18072){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__17986__auto___18072),args);
});})(g__17986__auto___18072))
;

cljs.spec.impl.gen.fmap.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.fmap.cljs$lang$applyTo = ((function (g__17986__auto___18072){
return (function (seq18032){
return cljs.spec.impl.gen.fmap.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18032));
});})(g__17986__auto___18072))
;


var g__17986__auto___18076 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.elements !== 'undefined')){
return clojure.test.check.generators.elements;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","elements","clojure.test.check.generators/elements",438991326,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","elements","clojure.test.check.generators/elements",438991326,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/elements
 */
cljs.spec.impl.gen.elements = ((function (g__17986__auto___18076){
return (function cljs$spec$impl$gen$elements(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18077 = arguments.length;
var i__8508__auto___18078 = (0);
while(true){
if((i__8508__auto___18078 < len__8507__auto___18077)){
args__8514__auto__.push((arguments[i__8508__auto___18078]));

var G__18079 = (i__8508__auto___18078 + (1));
i__8508__auto___18078 = G__18079;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.elements.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17986__auto___18076))
;

cljs.spec.impl.gen.elements.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17986__auto___18076){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__17986__auto___18076),args);
});})(g__17986__auto___18076))
;

cljs.spec.impl.gen.elements.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.elements.cljs$lang$applyTo = ((function (g__17986__auto___18076){
return (function (seq18033){
return cljs.spec.impl.gen.elements.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18033));
});})(g__17986__auto___18076))
;


var g__17986__auto___18080 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.bind !== 'undefined')){
return clojure.test.check.generators.bind;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","bind","clojure.test.check.generators/bind",-361313906,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","bind","clojure.test.check.generators/bind",-361313906,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/bind
 */
cljs.spec.impl.gen.bind = ((function (g__17986__auto___18080){
return (function cljs$spec$impl$gen$bind(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18081 = arguments.length;
var i__8508__auto___18082 = (0);
while(true){
if((i__8508__auto___18082 < len__8507__auto___18081)){
args__8514__auto__.push((arguments[i__8508__auto___18082]));

var G__18083 = (i__8508__auto___18082 + (1));
i__8508__auto___18082 = G__18083;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.bind.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17986__auto___18080))
;

cljs.spec.impl.gen.bind.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17986__auto___18080){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__17986__auto___18080),args);
});})(g__17986__auto___18080))
;

cljs.spec.impl.gen.bind.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.bind.cljs$lang$applyTo = ((function (g__17986__auto___18080){
return (function (seq18034){
return cljs.spec.impl.gen.bind.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18034));
});})(g__17986__auto___18080))
;


var g__17986__auto___18084 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.choose !== 'undefined')){
return clojure.test.check.generators.choose;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","choose","clojure.test.check.generators/choose",909997832,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","choose","clojure.test.check.generators/choose",909997832,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/choose
 */
cljs.spec.impl.gen.choose = ((function (g__17986__auto___18084){
return (function cljs$spec$impl$gen$choose(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18085 = arguments.length;
var i__8508__auto___18086 = (0);
while(true){
if((i__8508__auto___18086 < len__8507__auto___18085)){
args__8514__auto__.push((arguments[i__8508__auto___18086]));

var G__18087 = (i__8508__auto___18086 + (1));
i__8508__auto___18086 = G__18087;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.choose.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17986__auto___18084))
;

cljs.spec.impl.gen.choose.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17986__auto___18084){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__17986__auto___18084),args);
});})(g__17986__auto___18084))
;

cljs.spec.impl.gen.choose.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.choose.cljs$lang$applyTo = ((function (g__17986__auto___18084){
return (function (seq18035){
return cljs.spec.impl.gen.choose.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18035));
});})(g__17986__auto___18084))
;


var g__17986__auto___18088 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.one_of !== 'undefined')){
return clojure.test.check.generators.one_of;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","one-of","clojure.test.check.generators/one-of",-183339191,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","one-of","clojure.test.check.generators/one-of",-183339191,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/one-of
 */
cljs.spec.impl.gen.one_of = ((function (g__17986__auto___18088){
return (function cljs$spec$impl$gen$one_of(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18089 = arguments.length;
var i__8508__auto___18090 = (0);
while(true){
if((i__8508__auto___18090 < len__8507__auto___18089)){
args__8514__auto__.push((arguments[i__8508__auto___18090]));

var G__18091 = (i__8508__auto___18090 + (1));
i__8508__auto___18090 = G__18091;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.one_of.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17986__auto___18088))
;

cljs.spec.impl.gen.one_of.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17986__auto___18088){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__17986__auto___18088),args);
});})(g__17986__auto___18088))
;

cljs.spec.impl.gen.one_of.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.one_of.cljs$lang$applyTo = ((function (g__17986__auto___18088){
return (function (seq18036){
return cljs.spec.impl.gen.one_of.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18036));
});})(g__17986__auto___18088))
;


var g__17986__auto___18092 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.such_that !== 'undefined')){
return clojure.test.check.generators.such_that;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","such-that","clojure.test.check.generators/such-that",-1754178732,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","such-that","clojure.test.check.generators/such-that",-1754178732,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/such-that
 */
cljs.spec.impl.gen.such_that = ((function (g__17986__auto___18092){
return (function cljs$spec$impl$gen$such_that(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18093 = arguments.length;
var i__8508__auto___18094 = (0);
while(true){
if((i__8508__auto___18094 < len__8507__auto___18093)){
args__8514__auto__.push((arguments[i__8508__auto___18094]));

var G__18095 = (i__8508__auto___18094 + (1));
i__8508__auto___18094 = G__18095;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.such_that.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17986__auto___18092))
;

cljs.spec.impl.gen.such_that.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17986__auto___18092){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__17986__auto___18092),args);
});})(g__17986__auto___18092))
;

cljs.spec.impl.gen.such_that.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.such_that.cljs$lang$applyTo = ((function (g__17986__auto___18092){
return (function (seq18037){
return cljs.spec.impl.gen.such_that.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18037));
});})(g__17986__auto___18092))
;


var g__17986__auto___18096 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.tuple !== 'undefined')){
return clojure.test.check.generators.tuple;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","tuple","clojure.test.check.generators/tuple",-143711557,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","tuple","clojure.test.check.generators/tuple",-143711557,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/tuple
 */
cljs.spec.impl.gen.tuple = ((function (g__17986__auto___18096){
return (function cljs$spec$impl$gen$tuple(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18097 = arguments.length;
var i__8508__auto___18098 = (0);
while(true){
if((i__8508__auto___18098 < len__8507__auto___18097)){
args__8514__auto__.push((arguments[i__8508__auto___18098]));

var G__18099 = (i__8508__auto___18098 + (1));
i__8508__auto___18098 = G__18099;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.tuple.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17986__auto___18096))
;

cljs.spec.impl.gen.tuple.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17986__auto___18096){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__17986__auto___18096),args);
});})(g__17986__auto___18096))
;

cljs.spec.impl.gen.tuple.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.tuple.cljs$lang$applyTo = ((function (g__17986__auto___18096){
return (function (seq18038){
return cljs.spec.impl.gen.tuple.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18038));
});})(g__17986__auto___18096))
;


var g__17986__auto___18100 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.sample !== 'undefined')){
return clojure.test.check.generators.sample;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","sample","clojure.test.check.generators/sample",-382944992,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","sample","clojure.test.check.generators/sample",-382944992,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/sample
 */
cljs.spec.impl.gen.sample = ((function (g__17986__auto___18100){
return (function cljs$spec$impl$gen$sample(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18101 = arguments.length;
var i__8508__auto___18102 = (0);
while(true){
if((i__8508__auto___18102 < len__8507__auto___18101)){
args__8514__auto__.push((arguments[i__8508__auto___18102]));

var G__18103 = (i__8508__auto___18102 + (1));
i__8508__auto___18102 = G__18103;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.sample.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17986__auto___18100))
;

cljs.spec.impl.gen.sample.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17986__auto___18100){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__17986__auto___18100),args);
});})(g__17986__auto___18100))
;

cljs.spec.impl.gen.sample.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.sample.cljs$lang$applyTo = ((function (g__17986__auto___18100){
return (function (seq18039){
return cljs.spec.impl.gen.sample.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18039));
});})(g__17986__auto___18100))
;


var g__17986__auto___18104 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.return$ !== 'undefined')){
return clojure.test.check.generators.return$;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","return","clojure.test.check.generators/return",1744522038,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","return","clojure.test.check.generators/return",1744522038,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/return
 */
cljs.spec.impl.gen.return$ = ((function (g__17986__auto___18104){
return (function cljs$spec$impl$gen$return(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18105 = arguments.length;
var i__8508__auto___18106 = (0);
while(true){
if((i__8508__auto___18106 < len__8507__auto___18105)){
args__8514__auto__.push((arguments[i__8508__auto___18106]));

var G__18107 = (i__8508__auto___18106 + (1));
i__8508__auto___18106 = G__18107;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.return$.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17986__auto___18104))
;

cljs.spec.impl.gen.return$.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17986__auto___18104){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__17986__auto___18104),args);
});})(g__17986__auto___18104))
;

cljs.spec.impl.gen.return$.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.return$.cljs$lang$applyTo = ((function (g__17986__auto___18104){
return (function (seq18040){
return cljs.spec.impl.gen.return$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18040));
});})(g__17986__auto___18104))
;


var g__17986__auto___18108 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.large_integer_STAR_ !== 'undefined')){
return clojure.test.check.generators.large_integer_STAR_;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","large-integer*","clojure.test.check.generators/large-integer*",-437830670,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","large-integer*","clojure.test.check.generators/large-integer*",-437830670,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/large-integer*
 */
cljs.spec.impl.gen.large_integer_STAR_ = ((function (g__17986__auto___18108){
return (function cljs$spec$impl$gen$large_integer_STAR_(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18109 = arguments.length;
var i__8508__auto___18110 = (0);
while(true){
if((i__8508__auto___18110 < len__8507__auto___18109)){
args__8514__auto__.push((arguments[i__8508__auto___18110]));

var G__18111 = (i__8508__auto___18110 + (1));
i__8508__auto___18110 = G__18111;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.large_integer_STAR_.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17986__auto___18108))
;

cljs.spec.impl.gen.large_integer_STAR_.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17986__auto___18108){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__17986__auto___18108),args);
});})(g__17986__auto___18108))
;

cljs.spec.impl.gen.large_integer_STAR_.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.large_integer_STAR_.cljs$lang$applyTo = ((function (g__17986__auto___18108){
return (function (seq18041){
return cljs.spec.impl.gen.large_integer_STAR_.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18041));
});})(g__17986__auto___18108))
;


var g__17986__auto___18112 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.double_STAR_ !== 'undefined')){
return clojure.test.check.generators.double_STAR_;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","double*","clojure.test.check.generators/double*",841542265,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","double*","clojure.test.check.generators/double*",841542265,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/double*
 */
cljs.spec.impl.gen.double_STAR_ = ((function (g__17986__auto___18112){
return (function cljs$spec$impl$gen$double_STAR_(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18113 = arguments.length;
var i__8508__auto___18114 = (0);
while(true){
if((i__8508__auto___18114 < len__8507__auto___18113)){
args__8514__auto__.push((arguments[i__8508__auto___18114]));

var G__18115 = (i__8508__auto___18114 + (1));
i__8508__auto___18114 = G__18115;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.double_STAR_.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17986__auto___18112))
;

cljs.spec.impl.gen.double_STAR_.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17986__auto___18112){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__17986__auto___18112),args);
});})(g__17986__auto___18112))
;

cljs.spec.impl.gen.double_STAR_.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.double_STAR_.cljs$lang$applyTo = ((function (g__17986__auto___18112){
return (function (seq18042){
return cljs.spec.impl.gen.double_STAR_.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18042));
});})(g__17986__auto___18112))
;


var g__17986__auto___18116 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.frequency !== 'undefined')){
return clojure.test.check.generators.frequency;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","frequency","clojure.test.check.generators/frequency",2090703177,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","frequency","clojure.test.check.generators/frequency",2090703177,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/frequency
 */
cljs.spec.impl.gen.frequency = ((function (g__17986__auto___18116){
return (function cljs$spec$impl$gen$frequency(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18117 = arguments.length;
var i__8508__auto___18118 = (0);
while(true){
if((i__8508__auto___18118 < len__8507__auto___18117)){
args__8514__auto__.push((arguments[i__8508__auto___18118]));

var G__18119 = (i__8508__auto___18118 + (1));
i__8508__auto___18118 = G__18119;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.frequency.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17986__auto___18116))
;

cljs.spec.impl.gen.frequency.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17986__auto___18116){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__17986__auto___18116),args);
});})(g__17986__auto___18116))
;

cljs.spec.impl.gen.frequency.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.frequency.cljs$lang$applyTo = ((function (g__17986__auto___18116){
return (function (seq18043){
return cljs.spec.impl.gen.frequency.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18043));
});})(g__17986__auto___18116))
;

var g__17999__auto___18141 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.any !== 'undefined')){
return clojure.test.check.generators.any;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","any","clojure.test.check.generators/any",1883743710,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","any","clojure.test.check.generators/any",1883743710,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/any
 */
cljs.spec.impl.gen.any = ((function (g__17999__auto___18141){
return (function cljs$spec$impl$gen$any(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18142 = arguments.length;
var i__8508__auto___18143 = (0);
while(true){
if((i__8508__auto___18143 < len__8507__auto___18142)){
args__8514__auto__.push((arguments[i__8508__auto___18143]));

var G__18144 = (i__8508__auto___18143 + (1));
i__8508__auto___18143 = G__18144;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.any.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18141))
;

cljs.spec.impl.gen.any.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18141){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18141);
});})(g__17999__auto___18141))
;

cljs.spec.impl.gen.any.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.any.cljs$lang$applyTo = ((function (g__17999__auto___18141){
return (function (seq18120){
return cljs.spec.impl.gen.any.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18120));
});})(g__17999__auto___18141))
;


var g__17999__auto___18145 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.any_printable !== 'undefined')){
return clojure.test.check.generators.any_printable;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","any-printable","clojure.test.check.generators/any-printable",-1570493991,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","any-printable","clojure.test.check.generators/any-printable",-1570493991,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/any-printable
 */
cljs.spec.impl.gen.any_printable = ((function (g__17999__auto___18145){
return (function cljs$spec$impl$gen$any_printable(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18146 = arguments.length;
var i__8508__auto___18147 = (0);
while(true){
if((i__8508__auto___18147 < len__8507__auto___18146)){
args__8514__auto__.push((arguments[i__8508__auto___18147]));

var G__18148 = (i__8508__auto___18147 + (1));
i__8508__auto___18147 = G__18148;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.any_printable.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18145))
;

cljs.spec.impl.gen.any_printable.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18145){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18145);
});})(g__17999__auto___18145))
;

cljs.spec.impl.gen.any_printable.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.any_printable.cljs$lang$applyTo = ((function (g__17999__auto___18145){
return (function (seq18121){
return cljs.spec.impl.gen.any_printable.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18121));
});})(g__17999__auto___18145))
;


var g__17999__auto___18149 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.boolean$ !== 'undefined')){
return clojure.test.check.generators.boolean$;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","boolean","clojure.test.check.generators/boolean",1586992347,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","boolean","clojure.test.check.generators/boolean",1586992347,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/boolean
 */
cljs.spec.impl.gen.boolean$ = ((function (g__17999__auto___18149){
return (function cljs$spec$impl$gen$boolean(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18150 = arguments.length;
var i__8508__auto___18151 = (0);
while(true){
if((i__8508__auto___18151 < len__8507__auto___18150)){
args__8514__auto__.push((arguments[i__8508__auto___18151]));

var G__18152 = (i__8508__auto___18151 + (1));
i__8508__auto___18151 = G__18152;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.boolean$.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18149))
;

cljs.spec.impl.gen.boolean$.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18149){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18149);
});})(g__17999__auto___18149))
;

cljs.spec.impl.gen.boolean$.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.boolean$.cljs$lang$applyTo = ((function (g__17999__auto___18149){
return (function (seq18122){
return cljs.spec.impl.gen.boolean$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18122));
});})(g__17999__auto___18149))
;


var g__17999__auto___18153 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.char$ !== 'undefined')){
return clojure.test.check.generators.char$;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","char","clojure.test.check.generators/char",-1426343459,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","char","clojure.test.check.generators/char",-1426343459,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/char
 */
cljs.spec.impl.gen.char$ = ((function (g__17999__auto___18153){
return (function cljs$spec$impl$gen$char(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18154 = arguments.length;
var i__8508__auto___18155 = (0);
while(true){
if((i__8508__auto___18155 < len__8507__auto___18154)){
args__8514__auto__.push((arguments[i__8508__auto___18155]));

var G__18156 = (i__8508__auto___18155 + (1));
i__8508__auto___18155 = G__18156;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.char$.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18153))
;

cljs.spec.impl.gen.char$.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18153){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18153);
});})(g__17999__auto___18153))
;

cljs.spec.impl.gen.char$.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.char$.cljs$lang$applyTo = ((function (g__17999__auto___18153){
return (function (seq18123){
return cljs.spec.impl.gen.char$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18123));
});})(g__17999__auto___18153))
;


var g__17999__auto___18157 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.char_alpha !== 'undefined')){
return clojure.test.check.generators.char_alpha;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","char-alpha","clojure.test.check.generators/char-alpha",615785796,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","char-alpha","clojure.test.check.generators/char-alpha",615785796,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/char-alpha
 */
cljs.spec.impl.gen.char_alpha = ((function (g__17999__auto___18157){
return (function cljs$spec$impl$gen$char_alpha(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18158 = arguments.length;
var i__8508__auto___18159 = (0);
while(true){
if((i__8508__auto___18159 < len__8507__auto___18158)){
args__8514__auto__.push((arguments[i__8508__auto___18159]));

var G__18160 = (i__8508__auto___18159 + (1));
i__8508__auto___18159 = G__18160;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.char_alpha.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18157))
;

cljs.spec.impl.gen.char_alpha.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18157){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18157);
});})(g__17999__auto___18157))
;

cljs.spec.impl.gen.char_alpha.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.char_alpha.cljs$lang$applyTo = ((function (g__17999__auto___18157){
return (function (seq18124){
return cljs.spec.impl.gen.char_alpha.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18124));
});})(g__17999__auto___18157))
;


var g__17999__auto___18161 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.char_alphanumeric !== 'undefined')){
return clojure.test.check.generators.char_alphanumeric;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","char-alphanumeric","clojure.test.check.generators/char-alphanumeric",1383091431,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","char-alphanumeric","clojure.test.check.generators/char-alphanumeric",1383091431,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/char-alphanumeric
 */
cljs.spec.impl.gen.char_alphanumeric = ((function (g__17999__auto___18161){
return (function cljs$spec$impl$gen$char_alphanumeric(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18162 = arguments.length;
var i__8508__auto___18163 = (0);
while(true){
if((i__8508__auto___18163 < len__8507__auto___18162)){
args__8514__auto__.push((arguments[i__8508__auto___18163]));

var G__18164 = (i__8508__auto___18163 + (1));
i__8508__auto___18163 = G__18164;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.char_alphanumeric.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18161))
;

cljs.spec.impl.gen.char_alphanumeric.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18161){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18161);
});})(g__17999__auto___18161))
;

cljs.spec.impl.gen.char_alphanumeric.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.char_alphanumeric.cljs$lang$applyTo = ((function (g__17999__auto___18161){
return (function (seq18125){
return cljs.spec.impl.gen.char_alphanumeric.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18125));
});})(g__17999__auto___18161))
;


var g__17999__auto___18165 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.char_ascii !== 'undefined')){
return clojure.test.check.generators.char_ascii;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","char-ascii","clojure.test.check.generators/char-ascii",-899908538,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","char-ascii","clojure.test.check.generators/char-ascii",-899908538,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/char-ascii
 */
cljs.spec.impl.gen.char_ascii = ((function (g__17999__auto___18165){
return (function cljs$spec$impl$gen$char_ascii(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18166 = arguments.length;
var i__8508__auto___18167 = (0);
while(true){
if((i__8508__auto___18167 < len__8507__auto___18166)){
args__8514__auto__.push((arguments[i__8508__auto___18167]));

var G__18168 = (i__8508__auto___18167 + (1));
i__8508__auto___18167 = G__18168;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.char_ascii.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18165))
;

cljs.spec.impl.gen.char_ascii.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18165){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18165);
});})(g__17999__auto___18165))
;

cljs.spec.impl.gen.char_ascii.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.char_ascii.cljs$lang$applyTo = ((function (g__17999__auto___18165){
return (function (seq18126){
return cljs.spec.impl.gen.char_ascii.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18126));
});})(g__17999__auto___18165))
;


var g__17999__auto___18169 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.double$ !== 'undefined')){
return clojure.test.check.generators.double$;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","double","clojure.test.check.generators/double",668331090,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","double","clojure.test.check.generators/double",668331090,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/double
 */
cljs.spec.impl.gen.double$ = ((function (g__17999__auto___18169){
return (function cljs$spec$impl$gen$double(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18170 = arguments.length;
var i__8508__auto___18171 = (0);
while(true){
if((i__8508__auto___18171 < len__8507__auto___18170)){
args__8514__auto__.push((arguments[i__8508__auto___18171]));

var G__18172 = (i__8508__auto___18171 + (1));
i__8508__auto___18171 = G__18172;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.double$.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18169))
;

cljs.spec.impl.gen.double$.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18169){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18169);
});})(g__17999__auto___18169))
;

cljs.spec.impl.gen.double$.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.double$.cljs$lang$applyTo = ((function (g__17999__auto___18169){
return (function (seq18127){
return cljs.spec.impl.gen.double$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18127));
});})(g__17999__auto___18169))
;


var g__17999__auto___18173 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.int$ !== 'undefined')){
return clojure.test.check.generators.int$;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","int","clojure.test.check.generators/int",1756228469,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","int","clojure.test.check.generators/int",1756228469,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/int
 */
cljs.spec.impl.gen.int$ = ((function (g__17999__auto___18173){
return (function cljs$spec$impl$gen$int(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18174 = arguments.length;
var i__8508__auto___18175 = (0);
while(true){
if((i__8508__auto___18175 < len__8507__auto___18174)){
args__8514__auto__.push((arguments[i__8508__auto___18175]));

var G__18176 = (i__8508__auto___18175 + (1));
i__8508__auto___18175 = G__18176;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.int$.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18173))
;

cljs.spec.impl.gen.int$.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18173){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18173);
});})(g__17999__auto___18173))
;

cljs.spec.impl.gen.int$.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.int$.cljs$lang$applyTo = ((function (g__17999__auto___18173){
return (function (seq18128){
return cljs.spec.impl.gen.int$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18128));
});})(g__17999__auto___18173))
;


var g__17999__auto___18177 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.keyword !== 'undefined')){
return clojure.test.check.generators.keyword;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","keyword","clojure.test.check.generators/keyword",24530530,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","keyword","clojure.test.check.generators/keyword",24530530,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/keyword
 */
cljs.spec.impl.gen.keyword = ((function (g__17999__auto___18177){
return (function cljs$spec$impl$gen$keyword(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18178 = arguments.length;
var i__8508__auto___18179 = (0);
while(true){
if((i__8508__auto___18179 < len__8507__auto___18178)){
args__8514__auto__.push((arguments[i__8508__auto___18179]));

var G__18180 = (i__8508__auto___18179 + (1));
i__8508__auto___18179 = G__18180;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.keyword.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18177))
;

cljs.spec.impl.gen.keyword.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18177){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18177);
});})(g__17999__auto___18177))
;

cljs.spec.impl.gen.keyword.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.keyword.cljs$lang$applyTo = ((function (g__17999__auto___18177){
return (function (seq18129){
return cljs.spec.impl.gen.keyword.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18129));
});})(g__17999__auto___18177))
;


var g__17999__auto___18181 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.keyword_ns !== 'undefined')){
return clojure.test.check.generators.keyword_ns;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","keyword-ns","clojure.test.check.generators/keyword-ns",-1492628482,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","keyword-ns","clojure.test.check.generators/keyword-ns",-1492628482,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/keyword-ns
 */
cljs.spec.impl.gen.keyword_ns = ((function (g__17999__auto___18181){
return (function cljs$spec$impl$gen$keyword_ns(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18182 = arguments.length;
var i__8508__auto___18183 = (0);
while(true){
if((i__8508__auto___18183 < len__8507__auto___18182)){
args__8514__auto__.push((arguments[i__8508__auto___18183]));

var G__18184 = (i__8508__auto___18183 + (1));
i__8508__auto___18183 = G__18184;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.keyword_ns.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18181))
;

cljs.spec.impl.gen.keyword_ns.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18181){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18181);
});})(g__17999__auto___18181))
;

cljs.spec.impl.gen.keyword_ns.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.keyword_ns.cljs$lang$applyTo = ((function (g__17999__auto___18181){
return (function (seq18130){
return cljs.spec.impl.gen.keyword_ns.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18130));
});})(g__17999__auto___18181))
;


var g__17999__auto___18185 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.large_integer !== 'undefined')){
return clojure.test.check.generators.large_integer;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","large-integer","clojure.test.check.generators/large-integer",-865967138,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","large-integer","clojure.test.check.generators/large-integer",-865967138,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/large-integer
 */
cljs.spec.impl.gen.large_integer = ((function (g__17999__auto___18185){
return (function cljs$spec$impl$gen$large_integer(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18186 = arguments.length;
var i__8508__auto___18187 = (0);
while(true){
if((i__8508__auto___18187 < len__8507__auto___18186)){
args__8514__auto__.push((arguments[i__8508__auto___18187]));

var G__18188 = (i__8508__auto___18187 + (1));
i__8508__auto___18187 = G__18188;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.large_integer.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18185))
;

cljs.spec.impl.gen.large_integer.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18185){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18185);
});})(g__17999__auto___18185))
;

cljs.spec.impl.gen.large_integer.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.large_integer.cljs$lang$applyTo = ((function (g__17999__auto___18185){
return (function (seq18131){
return cljs.spec.impl.gen.large_integer.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18131));
});})(g__17999__auto___18185))
;


var g__17999__auto___18189 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.ratio !== 'undefined')){
return clojure.test.check.generators.ratio;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","ratio","clojure.test.check.generators/ratio",1540966915,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","ratio","clojure.test.check.generators/ratio",1540966915,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/ratio
 */
cljs.spec.impl.gen.ratio = ((function (g__17999__auto___18189){
return (function cljs$spec$impl$gen$ratio(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18190 = arguments.length;
var i__8508__auto___18191 = (0);
while(true){
if((i__8508__auto___18191 < len__8507__auto___18190)){
args__8514__auto__.push((arguments[i__8508__auto___18191]));

var G__18192 = (i__8508__auto___18191 + (1));
i__8508__auto___18191 = G__18192;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.ratio.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18189))
;

cljs.spec.impl.gen.ratio.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18189){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18189);
});})(g__17999__auto___18189))
;

cljs.spec.impl.gen.ratio.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.ratio.cljs$lang$applyTo = ((function (g__17999__auto___18189){
return (function (seq18132){
return cljs.spec.impl.gen.ratio.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18132));
});})(g__17999__auto___18189))
;


var g__17999__auto___18193 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.simple_type !== 'undefined')){
return clojure.test.check.generators.simple_type;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","simple-type","clojure.test.check.generators/simple-type",892572284,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","simple-type","clojure.test.check.generators/simple-type",892572284,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/simple-type
 */
cljs.spec.impl.gen.simple_type = ((function (g__17999__auto___18193){
return (function cljs$spec$impl$gen$simple_type(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18194 = arguments.length;
var i__8508__auto___18195 = (0);
while(true){
if((i__8508__auto___18195 < len__8507__auto___18194)){
args__8514__auto__.push((arguments[i__8508__auto___18195]));

var G__18196 = (i__8508__auto___18195 + (1));
i__8508__auto___18195 = G__18196;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.simple_type.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18193))
;

cljs.spec.impl.gen.simple_type.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18193){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18193);
});})(g__17999__auto___18193))
;

cljs.spec.impl.gen.simple_type.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.simple_type.cljs$lang$applyTo = ((function (g__17999__auto___18193){
return (function (seq18133){
return cljs.spec.impl.gen.simple_type.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18133));
});})(g__17999__auto___18193))
;


var g__17999__auto___18197 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.simple_type_printable !== 'undefined')){
return clojure.test.check.generators.simple_type_printable;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","simple-type-printable","clojure.test.check.generators/simple-type-printable",-58489962,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","simple-type-printable","clojure.test.check.generators/simple-type-printable",-58489962,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/simple-type-printable
 */
cljs.spec.impl.gen.simple_type_printable = ((function (g__17999__auto___18197){
return (function cljs$spec$impl$gen$simple_type_printable(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18198 = arguments.length;
var i__8508__auto___18199 = (0);
while(true){
if((i__8508__auto___18199 < len__8507__auto___18198)){
args__8514__auto__.push((arguments[i__8508__auto___18199]));

var G__18200 = (i__8508__auto___18199 + (1));
i__8508__auto___18199 = G__18200;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.simple_type_printable.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18197))
;

cljs.spec.impl.gen.simple_type_printable.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18197){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18197);
});})(g__17999__auto___18197))
;

cljs.spec.impl.gen.simple_type_printable.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.simple_type_printable.cljs$lang$applyTo = ((function (g__17999__auto___18197){
return (function (seq18134){
return cljs.spec.impl.gen.simple_type_printable.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18134));
});})(g__17999__auto___18197))
;


var g__17999__auto___18201 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.string !== 'undefined')){
return clojure.test.check.generators.string;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","string","clojure.test.check.generators/string",-1704750979,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","string","clojure.test.check.generators/string",-1704750979,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/string
 */
cljs.spec.impl.gen.string = ((function (g__17999__auto___18201){
return (function cljs$spec$impl$gen$string(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18202 = arguments.length;
var i__8508__auto___18203 = (0);
while(true){
if((i__8508__auto___18203 < len__8507__auto___18202)){
args__8514__auto__.push((arguments[i__8508__auto___18203]));

var G__18204 = (i__8508__auto___18203 + (1));
i__8508__auto___18203 = G__18204;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.string.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18201))
;

cljs.spec.impl.gen.string.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18201){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18201);
});})(g__17999__auto___18201))
;

cljs.spec.impl.gen.string.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.string.cljs$lang$applyTo = ((function (g__17999__auto___18201){
return (function (seq18135){
return cljs.spec.impl.gen.string.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18135));
});})(g__17999__auto___18201))
;


var g__17999__auto___18205 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.string_ascii !== 'undefined')){
return clojure.test.check.generators.string_ascii;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","string-ascii","clojure.test.check.generators/string-ascii",-2009877640,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","string-ascii","clojure.test.check.generators/string-ascii",-2009877640,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/string-ascii
 */
cljs.spec.impl.gen.string_ascii = ((function (g__17999__auto___18205){
return (function cljs$spec$impl$gen$string_ascii(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18206 = arguments.length;
var i__8508__auto___18207 = (0);
while(true){
if((i__8508__auto___18207 < len__8507__auto___18206)){
args__8514__auto__.push((arguments[i__8508__auto___18207]));

var G__18208 = (i__8508__auto___18207 + (1));
i__8508__auto___18207 = G__18208;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.string_ascii.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18205))
;

cljs.spec.impl.gen.string_ascii.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18205){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18205);
});})(g__17999__auto___18205))
;

cljs.spec.impl.gen.string_ascii.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.string_ascii.cljs$lang$applyTo = ((function (g__17999__auto___18205){
return (function (seq18136){
return cljs.spec.impl.gen.string_ascii.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18136));
});})(g__17999__auto___18205))
;


var g__17999__auto___18209 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.string_alphanumeric !== 'undefined')){
return clojure.test.check.generators.string_alphanumeric;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","string-alphanumeric","clojure.test.check.generators/string-alphanumeric",836374939,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","string-alphanumeric","clojure.test.check.generators/string-alphanumeric",836374939,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/string-alphanumeric
 */
cljs.spec.impl.gen.string_alphanumeric = ((function (g__17999__auto___18209){
return (function cljs$spec$impl$gen$string_alphanumeric(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18210 = arguments.length;
var i__8508__auto___18211 = (0);
while(true){
if((i__8508__auto___18211 < len__8507__auto___18210)){
args__8514__auto__.push((arguments[i__8508__auto___18211]));

var G__18212 = (i__8508__auto___18211 + (1));
i__8508__auto___18211 = G__18212;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.string_alphanumeric.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18209))
;

cljs.spec.impl.gen.string_alphanumeric.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18209){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18209);
});})(g__17999__auto___18209))
;

cljs.spec.impl.gen.string_alphanumeric.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.string_alphanumeric.cljs$lang$applyTo = ((function (g__17999__auto___18209){
return (function (seq18137){
return cljs.spec.impl.gen.string_alphanumeric.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18137));
});})(g__17999__auto___18209))
;


var g__17999__auto___18213 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.symbol !== 'undefined')){
return clojure.test.check.generators.symbol;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","symbol","clojure.test.check.generators/symbol",-1305461065,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","symbol","clojure.test.check.generators/symbol",-1305461065,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/symbol
 */
cljs.spec.impl.gen.symbol = ((function (g__17999__auto___18213){
return (function cljs$spec$impl$gen$symbol(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18214 = arguments.length;
var i__8508__auto___18215 = (0);
while(true){
if((i__8508__auto___18215 < len__8507__auto___18214)){
args__8514__auto__.push((arguments[i__8508__auto___18215]));

var G__18216 = (i__8508__auto___18215 + (1));
i__8508__auto___18215 = G__18216;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.symbol.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18213))
;

cljs.spec.impl.gen.symbol.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18213){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18213);
});})(g__17999__auto___18213))
;

cljs.spec.impl.gen.symbol.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.symbol.cljs$lang$applyTo = ((function (g__17999__auto___18213){
return (function (seq18138){
return cljs.spec.impl.gen.symbol.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18138));
});})(g__17999__auto___18213))
;


var g__17999__auto___18217 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.symbol_ns !== 'undefined')){
return clojure.test.check.generators.symbol_ns;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","symbol-ns","clojure.test.check.generators/symbol-ns",-862629490,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","symbol-ns","clojure.test.check.generators/symbol-ns",-862629490,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/symbol-ns
 */
cljs.spec.impl.gen.symbol_ns = ((function (g__17999__auto___18217){
return (function cljs$spec$impl$gen$symbol_ns(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18218 = arguments.length;
var i__8508__auto___18219 = (0);
while(true){
if((i__8508__auto___18219 < len__8507__auto___18218)){
args__8514__auto__.push((arguments[i__8508__auto___18219]));

var G__18220 = (i__8508__auto___18219 + (1));
i__8508__auto___18219 = G__18220;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.symbol_ns.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18217))
;

cljs.spec.impl.gen.symbol_ns.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18217){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18217);
});})(g__17999__auto___18217))
;

cljs.spec.impl.gen.symbol_ns.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.symbol_ns.cljs$lang$applyTo = ((function (g__17999__auto___18217){
return (function (seq18139){
return cljs.spec.impl.gen.symbol_ns.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18139));
});})(g__17999__auto___18217))
;


var g__17999__auto___18221 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.uuid !== 'undefined')){
return clojure.test.check.generators.uuid;
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Var "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol("clojure.test.check.generators","uuid","clojure.test.check.generators/uuid",1589373144,null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" does not exist, "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","uuid","clojure.test.check.generators/uuid",1589373144,null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/uuid
 */
cljs.spec.impl.gen.uuid = ((function (g__17999__auto___18221){
return (function cljs$spec$impl$gen$uuid(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18222 = arguments.length;
var i__8508__auto___18223 = (0);
while(true){
if((i__8508__auto___18223 < len__8507__auto___18222)){
args__8514__auto__.push((arguments[i__8508__auto___18223]));

var G__18224 = (i__8508__auto___18223 + (1));
i__8508__auto___18223 = G__18224;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.uuid.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});})(g__17999__auto___18221))
;

cljs.spec.impl.gen.uuid.cljs$core$IFn$_invoke$arity$variadic = ((function (g__17999__auto___18221){
return (function (args){
return cljs.core.deref.call(null,g__17999__auto___18221);
});})(g__17999__auto___18221))
;

cljs.spec.impl.gen.uuid.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.uuid.cljs$lang$applyTo = ((function (g__17999__auto___18221){
return (function (seq18140){
return cljs.spec.impl.gen.uuid.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18140));
});})(g__17999__auto___18221))
;

/**
 * Returns a generator of a sequence catenated from results of
 * gens, each of which should generate something sequential.
 */
cljs.spec.impl.gen.cat = (function cljs$spec$impl$gen$cat(var_args){
var args__8514__auto__ = [];
var len__8507__auto___18227 = arguments.length;
var i__8508__auto___18228 = (0);
while(true){
if((i__8508__auto___18228 < len__8507__auto___18227)){
args__8514__auto__.push((arguments[i__8508__auto___18228]));

var G__18229 = (i__8508__auto___18228 + (1));
i__8508__auto___18228 = G__18229;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((0) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.cat.cljs$core$IFn$_invoke$arity$variadic(argseq__8515__auto__);
});

cljs.spec.impl.gen.cat.cljs$core$IFn$_invoke$arity$variadic = (function (gens){
return cljs.spec.impl.gen.fmap.call(null,(function (p1__18225_SHARP_){
return cljs.core.apply.call(null,cljs.core.concat,p1__18225_SHARP_);
}),cljs.core.apply.call(null,cljs.spec.impl.gen.tuple,gens));
});

cljs.spec.impl.gen.cat.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.cat.cljs$lang$applyTo = (function (seq18226){
return cljs.spec.impl.gen.cat.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq18226));
});

cljs.spec.impl.gen.qualified_QMARK_ = (function cljs$spec$impl$gen$qualified_QMARK_(ident){
return !((cljs.core.namespace.call(null,ident) == null));
});
cljs.spec.impl.gen.gen_builtins = (new cljs.core.Delay((function (){
var simple = cljs.spec.impl.gen.simple_type_printable.call(null);
return cljs.core.PersistentHashMap.fromArrays([cljs.core.qualified_keyword_QMARK_,cljs.core.seq_QMARK_,cljs.core.vector_QMARK_,cljs.core.any_QMARK_,cljs.core.boolean_QMARK_,cljs.core.char_QMARK_,cljs.core.inst_QMARK_,cljs.core.simple_symbol_QMARK_,cljs.core.sequential_QMARK_,cljs.core.float_QMARK_,cljs.core.set_QMARK_,cljs.core.map_QMARK_,cljs.core.empty_QMARK_,cljs.core.string_QMARK_,cljs.core.double_QMARK_,cljs.core.int_QMARK_,cljs.core.associative_QMARK_,cljs.core.keyword_QMARK_,cljs.core.indexed_QMARK_,cljs.core.zero_QMARK_,cljs.core.simple_keyword_QMARK_,cljs.core.neg_int_QMARK_,cljs.core.nil_QMARK_,cljs.core.ident_QMARK_,cljs.core.qualified_ident_QMARK_,cljs.core.true_QMARK_,cljs.core.integer_QMARK_,cljs.core.nat_int_QMARK_,cljs.core.pos_int_QMARK_,cljs.core.uuid_QMARK_,cljs.core.false_QMARK_,cljs.core.list_QMARK_,cljs.core.simple_ident_QMARK_,cljs.core.number_QMARK_,cljs.core.qualified_symbol_QMARK_,cljs.core.seqable_QMARK_,cljs.core.symbol_QMARK_,cljs.core.coll_QMARK_],[cljs.spec.impl.gen.such_that.call(null,cljs.spec.impl.gen.qualified_QMARK_,cljs.spec.impl.gen.keyword_ns.call(null)),cljs.spec.impl.gen.list.call(null,simple),cljs.spec.impl.gen.vector.call(null,simple),cljs.spec.impl.gen.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.impl.gen.return$.call(null,null),cljs.spec.impl.gen.any_printable.call(null)], null)),cljs.spec.impl.gen.boolean$.call(null),cljs.spec.impl.gen.char$.call(null),cljs.spec.impl.gen.fmap.call(null,((function (simple){
return (function (p1__18230_SHARP_){
return (new Date(p1__18230_SHARP_));
});})(simple))
,cljs.spec.impl.gen.large_integer.call(null)),cljs.spec.impl.gen.symbol.call(null),cljs.spec.impl.gen.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.impl.gen.list.call(null,simple),cljs.spec.impl.gen.vector.call(null,simple)], null)),cljs.spec.impl.gen.double$.call(null),cljs.spec.impl.gen.set.call(null,simple),cljs.spec.impl.gen.map.call(null,simple,simple),cljs.spec.impl.gen.elements.call(null,new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,cljs.core.List.EMPTY,cljs.core.PersistentVector.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentHashSet.EMPTY], null)),cljs.spec.impl.gen.string_alphanumeric.call(null),cljs.spec.impl.gen.double$.call(null),cljs.spec.impl.gen.large_integer.call(null),cljs.spec.impl.gen.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.impl.gen.map.call(null,simple,simple),cljs.spec.impl.gen.vector.call(null,simple)], null)),cljs.spec.impl.gen.keyword_ns.call(null),cljs.spec.impl.gen.vector.call(null,simple),cljs.spec.impl.gen.return$.call(null,(0)),cljs.spec.impl.gen.keyword.call(null),cljs.spec.impl.gen.large_integer_STAR_.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"max","max",61366548),(-1)], null)),cljs.spec.impl.gen.return$.call(null,null),cljs.spec.impl.gen.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.impl.gen.keyword_ns.call(null),cljs.spec.impl.gen.symbol_ns.call(null)], null)),cljs.spec.impl.gen.such_that.call(null,cljs.spec.impl.gen.qualified_QMARK_,cljs.spec.impl.gen.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.impl.gen.keyword_ns.call(null),cljs.spec.impl.gen.symbol_ns.call(null)], null))),cljs.spec.impl.gen.return$.call(null,true),cljs.spec.impl.gen.large_integer.call(null),cljs.spec.impl.gen.large_integer_STAR_.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"min","min",444991522),(0)], null)),cljs.spec.impl.gen.large_integer_STAR_.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"min","min",444991522),(1)], null)),cljs.spec.impl.gen.uuid.call(null),cljs.spec.impl.gen.return$.call(null,false),cljs.spec.impl.gen.list.call(null,simple),cljs.spec.impl.gen.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.impl.gen.keyword.call(null),cljs.spec.impl.gen.symbol.call(null)], null)),cljs.spec.impl.gen.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.impl.gen.large_integer.call(null),cljs.spec.impl.gen.double$.call(null)], null)),cljs.spec.impl.gen.such_that.call(null,cljs.spec.impl.gen.qualified_QMARK_,cljs.spec.impl.gen.symbol_ns.call(null)),cljs.spec.impl.gen.one_of.call(null,new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.impl.gen.return$.call(null,null),cljs.spec.impl.gen.list.call(null,simple),cljs.spec.impl.gen.vector.call(null,simple),cljs.spec.impl.gen.map.call(null,simple,simple),cljs.spec.impl.gen.set.call(null,simple),cljs.spec.impl.gen.string_alphanumeric.call(null)], null)),cljs.spec.impl.gen.symbol_ns.call(null),cljs.spec.impl.gen.one_of.call(null,new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.impl.gen.map.call(null,simple,simple),cljs.spec.impl.gen.list.call(null,simple),cljs.spec.impl.gen.vector.call(null,simple),cljs.spec.impl.gen.set.call(null,simple)], null))]);
}),null));
/**
 * Given a predicate, returns a built-in generator if one exists.
 */
cljs.spec.impl.gen.gen_for_pred = (function cljs$spec$impl$gen$gen_for_pred(pred){
if(cljs.core.set_QMARK_.call(null,pred)){
return cljs.spec.impl.gen.elements.call(null,pred);
} else {
return cljs.core.get.call(null,cljs.core.deref.call(null,cljs.spec.impl.gen.gen_builtins),pred);
}
});
