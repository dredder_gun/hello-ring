// Compiled by ClojureScript 1.9.521 {}
goog.provide('ajax.xml_http_request');
goog.require('cljs.core');
goog.require('ajax.protocols');
ajax.xml_http_request.ready_state = (function ajax$xml_http_request$ready_state(e){
return new cljs.core.PersistentArrayMap(null, 5, [(0),new cljs.core.Keyword(null,"not-initialized","not-initialized",-1937378906),(1),new cljs.core.Keyword(null,"connection-established","connection-established",-1403749733),(2),new cljs.core.Keyword(null,"request-received","request-received",2110590540),(3),new cljs.core.Keyword(null,"processing-request","processing-request",-264947221),(4),new cljs.core.Keyword(null,"response-ready","response-ready",245208276)], null).call(null,e.target.readyState);
});
XMLHttpRequest.prototype.ajax$protocols$AjaxImpl$ = cljs.core.PROTOCOL_SENTINEL;

XMLHttpRequest.prototype.ajax$protocols$AjaxImpl$_js_ajax_request$arity$3 = (function (this$,p__11325,handler){
var map__11326 = p__11325;
var map__11326__$1 = ((((!((map__11326 == null)))?((((map__11326.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__11326.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__11326):map__11326);
var uri = cljs.core.get.call(null,map__11326__$1,new cljs.core.Keyword(null,"uri","uri",-774711847));
var method = cljs.core.get.call(null,map__11326__$1,new cljs.core.Keyword(null,"method","method",55703592));
var body = cljs.core.get.call(null,map__11326__$1,new cljs.core.Keyword(null,"body","body",-2049205669));
var headers = cljs.core.get.call(null,map__11326__$1,new cljs.core.Keyword(null,"headers","headers",-835030129));
var timeout = cljs.core.get.call(null,map__11326__$1,new cljs.core.Keyword(null,"timeout","timeout",-318625318),(0));
var with_credentials = cljs.core.get.call(null,map__11326__$1,new cljs.core.Keyword(null,"with-credentials","with-credentials",-1163127235),false);
var response_format = cljs.core.get.call(null,map__11326__$1,new cljs.core.Keyword(null,"response-format","response-format",1664465322));
var this$__$1 = this;
this$__$1.withCredentials = with_credentials;

this$__$1.onreadystatechange = ((function (this$__$1,map__11326,map__11326__$1,uri,method,body,headers,timeout,with_credentials,response_format){
return (function (p1__11324_SHARP_){
if(cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"response-ready","response-ready",245208276),ajax.xml_http_request.ready_state.call(null,p1__11324_SHARP_))){
return handler.call(null,this$__$1);
} else {
return null;
}
});})(this$__$1,map__11326,map__11326__$1,uri,method,body,headers,timeout,with_credentials,response_format))
;

this$__$1.open(method,uri,true);

this$__$1.timeout = timeout;

var temp__4657__auto___11338 = new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(response_format);
if(cljs.core.truth_(temp__4657__auto___11338)){
var response_type_11339 = temp__4657__auto___11338;
this$__$1.responseType = cljs.core.name.call(null,response_type_11339);
} else {
}

var seq__11328_11340 = cljs.core.seq.call(null,headers);
var chunk__11329_11341 = null;
var count__11330_11342 = (0);
var i__11331_11343 = (0);
while(true){
if((i__11331_11343 < count__11330_11342)){
var vec__11332_11344 = cljs.core._nth.call(null,chunk__11329_11341,i__11331_11343);
var k_11345 = cljs.core.nth.call(null,vec__11332_11344,(0),null);
var v_11346 = cljs.core.nth.call(null,vec__11332_11344,(1),null);
this$__$1.setRequestHeader(k_11345,v_11346);

var G__11347 = seq__11328_11340;
var G__11348 = chunk__11329_11341;
var G__11349 = count__11330_11342;
var G__11350 = (i__11331_11343 + (1));
seq__11328_11340 = G__11347;
chunk__11329_11341 = G__11348;
count__11330_11342 = G__11349;
i__11331_11343 = G__11350;
continue;
} else {
var temp__4657__auto___11351 = cljs.core.seq.call(null,seq__11328_11340);
if(temp__4657__auto___11351){
var seq__11328_11352__$1 = temp__4657__auto___11351;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__11328_11352__$1)){
var c__8213__auto___11353 = cljs.core.chunk_first.call(null,seq__11328_11352__$1);
var G__11354 = cljs.core.chunk_rest.call(null,seq__11328_11352__$1);
var G__11355 = c__8213__auto___11353;
var G__11356 = cljs.core.count.call(null,c__8213__auto___11353);
var G__11357 = (0);
seq__11328_11340 = G__11354;
chunk__11329_11341 = G__11355;
count__11330_11342 = G__11356;
i__11331_11343 = G__11357;
continue;
} else {
var vec__11335_11358 = cljs.core.first.call(null,seq__11328_11352__$1);
var k_11359 = cljs.core.nth.call(null,vec__11335_11358,(0),null);
var v_11360 = cljs.core.nth.call(null,vec__11335_11358,(1),null);
this$__$1.setRequestHeader(k_11359,v_11360);

var G__11361 = cljs.core.next.call(null,seq__11328_11352__$1);
var G__11362 = null;
var G__11363 = (0);
var G__11364 = (0);
seq__11328_11340 = G__11361;
chunk__11329_11341 = G__11362;
count__11330_11342 = G__11363;
i__11331_11343 = G__11364;
continue;
}
} else {
}
}
break;
}

this$__$1.send((function (){var or__7394__auto__ = body;
if(cljs.core.truth_(or__7394__auto__)){
return or__7394__auto__;
} else {
return "";
}
})());

return this$__$1;
});

XMLHttpRequest.prototype.ajax$protocols$AjaxRequest$ = cljs.core.PROTOCOL_SENTINEL;

XMLHttpRequest.prototype.ajax$protocols$AjaxRequest$_abort$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1.abort();
});

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$ = cljs.core.PROTOCOL_SENTINEL;

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$_body$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1.response;
});

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$_status$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1.status;
});

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$_status_text$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1.statusText;
});

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$_get_response_header$arity$2 = (function (this$,header){
var this$__$1 = this;
return this$__$1.getResponseHeader(header);
});

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$_was_aborted$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core._EQ_.call(null,(0),this$__$1.readyState);
});
