// Compiled by ClojureScript 1.9.521 {}
goog.provide('frontend.form_autorization');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('reagent.session');
goog.require('secretary.core');
frontend.form_autorization.input_element = (function frontend$form_autorization$input_element(id,name,type,value,in_focus){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"on-focus","on-focus",-13737624),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"value","value",305978217),new cljs.core.Keyword(null,"on-blur","on-blur",814300747),new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"class","class",-2030961996),new cljs.core.Keyword(null,"on-change","on-change",-732046149),new cljs.core.Keyword(null,"required","required",1807647006)],[(function (){
return cljs.core.swap_BANG_.call(null,in_focus,cljs.core.not);
}),name,cljs.core.deref.call(null,value),(function (){
return cljs.core.swap_BANG_.call(null,in_focus,cljs.core.not);
}),type,id,"form-control",(function (p1__12225_SHARP_){
return cljs.core.reset_BANG_.call(null,value,p1__12225_SHARP_.target.value);
}),""])], null);
});
frontend.form_autorization.input_and_prompt = (function frontend$form_autorization$input_and_prompt(label_value,input_name,input_type,input_element_arg,prompt_element,required_QMARK_){
var input_focus = reagent.core.atom.call(null,false);
return ((function (input_focus){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label","label",1718410804),label_value], null),(cljs.core.truth_(cljs.core.deref.call(null,input_focus))?prompt_element:new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632)], null)),new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [frontend.form_autorization.input_element,input_name,input_name,input_type,input_element_arg,input_focus], null),(cljs.core.truth_((function (){var and__7382__auto__ = required_QMARK_;
if(cljs.core.truth_(and__7382__auto__)){
return cljs.core._EQ_.call(null,"",cljs.core.deref.call(null,input_element_arg));
} else {
return and__7382__auto__;
}
})())?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),"Field is required!"], null):new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632)], null)),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),cljs.core.deref.call(null,input_element_arg)], null)], null);
});
;})(input_focus))
});
/**
 * A prompt that will animate to help the user with a given input
 */
frontend.form_autorization.prompt_message = (function frontend$form_autorization$prompt_message(message){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"class","class",-2030961996),"my-messages"], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"class","class",-2030961996),"prompt message-animation"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),message], null)], null)], null);
});
frontend.form_autorization.name_form = (function frontend$form_autorization$name_form(name_atom){
return frontend.form_autorization.input_and_prompt.call(null,"name","name","text",name_atom,frontend.form_autorization.prompt_message.call(null,"What is yout name!&"),true);
});
/**
 * Check if the value is nil, then apply the predicate
 */
frontend.form_autorization.check_nil_then_predicate = (function frontend$form_autorization$check_nil_then_predicate(value,predicate){
if((value == null)){
return false;
} else {
return predicate.call(null,value);
}
});
frontend.form_autorization.eight_or_more_characters_QMARK_ = (function frontend$form_autorization$eight_or_more_characters_QMARK_(word){
return frontend.form_autorization.check_nil_then_predicate.call(null,word,(function (arg){
return (cljs.core.count.call(null,arg) > (7));
}));
});
frontend.form_autorization.has_special_character_QMARK_ = (function frontend$form_autorization$has_special_character_QMARK_(word){
return frontend.form_autorization.check_nil_then_predicate.call(null,word,(function (arg){
return cljs.core.boolean$.call(null,cljs.core.first.call(null,cljs.core.re_seq.call(null,/\W+/,arg)));
}));
});
frontend.form_autorization.has_number_QMARK_ = (function frontend$form_autorization$has_number_QMARK_(word){
return frontend.form_autorization.check_nil_then_predicate.call(null,word,(function (arg){
return cljs.core.boolean$.call(null,cljs.core.re_seq.call(null,/\d+/,arg));
}));
});
frontend.form_autorization.wrap_as_element_in_form = (function frontend$form_autorization$wrap_as_element_in_form(element){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"class=","class=",1799263312),"row form-group"], null),element], null);
});
/**
 * A list to describe which password requirements have been met so far
 */
frontend.form_autorization.password_requirements = (function frontend$form_autorization$password_requirements(password,requirements){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ul","ul",-1349521403),cljs.core.map.call(null,(function (req){
return cljs.core.with_meta(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(req)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),req], null));
}),cljs.core.doall.call(null,cljs.core.filter.call(null,(function (req){
return cljs.core.not.call(null,new cljs.core.Keyword(null,"check-fn","check-fn",-1710398015).cljs$core$IFn$_invoke$arity$1(req).call(null,cljs.core.deref.call(null,password)));
}),requirements)))], null)], null);
});
frontend.form_autorization.password_form = (function frontend$form_autorization$password_form(password){
var password_type_atom = reagent.core.atom.call(null,"password");
return ((function (password_type_atom){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [frontend.form_autorization.input_and_prompt.call(null,"password","password",cljs.core.deref.call(null,password_type_atom),password,frontend.form_autorization.prompt_message.call(null,"What's your password"),true)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [frontend.form_autorization.password_requirements,password,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"message","message",-406056002),"8 or more characters",new cljs.core.Keyword(null,"check-fn","check-fn",-1710398015),frontend.form_autorization.eight_or_more_characters_QMARK_], null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"message","message",-406056002),"At least one special character",new cljs.core.Keyword(null,"check-fn","check-fn",-1710398015),frontend.form_autorization.has_special_character_QMARK_], null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"message","message",-406056002),"At least one number",new cljs.core.Keyword(null,"check-fn","check-fn",-1710398015),frontend.form_autorization.has_number_QMARK_], null)], null)], null)], null);
});
;})(password_type_atom))
});
frontend.form_autorization.email_form = (function frontend$form_autorization$email_form(email_address_atom){
return new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [frontend.form_autorization.input_and_prompt,"\u0422\u0443\u0442 \u0432\u044B \u0432\u0432\u043E\u0434\u0438\u0442\u0435 email","email","email",email_address_atom,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [frontend.form_autorization.prompt_message,"What`s your email?"], null),true], null);
});
frontend.form_autorization.email_prompt = (function frontend$form_autorization$email_prompt(){
return frontend.form_autorization.prompt_message.call(null,"What's your email address?");
});
frontend.form_autorization.home_page = (function frontend$form_autorization$home_page(){
var email_address = reagent.core.atom.call(null,null);
var name = reagent.core.atom.call(null,null);
var password = reagent.core.atom.call(null,null);
return ((function (email_address,name,password){
return (function (){
new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button","button",1456579943),"\u041A\u043D\u043E\u043F\u043A\u0430"], null);

return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"class","class",-2030961996),"signup-wrapper"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h2","h2",-372662728),"Welcome to TestChimp"], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"form","form",-1624062471),frontend.form_autorization.wrap_as_element_in_form.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [frontend.form_autorization.email_form,email_address], null)),frontend.form_autorization.wrap_as_element_in_form.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [frontend.form_autorization.name_form,name], null)),frontend.form_autorization.wrap_as_element_in_form.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [frontend.form_autorization.password_form,password], null))], null)], null);
});
;})(email_address,name,password))
});
frontend.form_autorization.about_page = (function frontend$form_autorization$about_page(){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h2","h2",-372662728),"About reagent-hello-world"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),"/"], null),"go to the home page"], null)], null)], null);
});
frontend.form_autorization.current_page = (function frontend$form_autorization$current_page(){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [reagent.session.get.call(null,new cljs.core.Keyword(null,"current-page","current-page",-101294180))], null)], null);
});
var action__12060__auto___12231 = (function (params__12061__auto__){
if(cljs.core.map_QMARK_.call(null,params__12061__auto__)){
var map__12226 = params__12061__auto__;
var map__12226__$1 = ((((!((map__12226 == null)))?((((map__12226.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__12226.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__12226):map__12226);
return reagent.session.put_BANG_.call(null,new cljs.core.Keyword(null,"current-page","current-page",-101294180),new cljs.core.Var(function(){return frontend.form_autorization.home_page;},new cljs.core.Symbol("frontend.form-autorization","home-page","frontend.form-autorization/home-page",-1836595369,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"frontend.form-autorization","frontend.form-autorization",746743591,null),new cljs.core.Symbol(null,"home-page","home-page",-850279575,null),"src/frontend/form-autorization.cljs",16,1,120,120,cljs.core.list(cljs.core.PersistentVector.EMPTY),null,(cljs.core.truth_(frontend.form_autorization.home_page)?frontend.form_autorization.home_page.cljs$lang$test:null)])));
} else {
if(cljs.core.vector_QMARK_.call(null,params__12061__auto__)){
var vec__12228 = params__12061__auto__;
return reagent.session.put_BANG_.call(null,new cljs.core.Keyword(null,"current-page","current-page",-101294180),new cljs.core.Var(function(){return frontend.form_autorization.home_page;},new cljs.core.Symbol("frontend.form-autorization","home-page","frontend.form-autorization/home-page",-1836595369,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"frontend.form-autorization","frontend.form-autorization",746743591,null),new cljs.core.Symbol(null,"home-page","home-page",-850279575,null),"src/frontend/form-autorization.cljs",16,1,120,120,cljs.core.list(cljs.core.PersistentVector.EMPTY),null,(cljs.core.truth_(frontend.form_autorization.home_page)?frontend.form_autorization.home_page.cljs$lang$test:null)])));
} else {
return null;
}
}
});
secretary.core.add_route_BANG_.call(null,"/",action__12060__auto___12231);

var action__12060__auto___12237 = (function (params__12061__auto__){
if(cljs.core.map_QMARK_.call(null,params__12061__auto__)){
var map__12232 = params__12061__auto__;
var map__12232__$1 = ((((!((map__12232 == null)))?((((map__12232.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__12232.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__12232):map__12232);
return reagent.session.put_BANG_.call(null,new cljs.core.Keyword(null,"current-page","current-page",-101294180),new cljs.core.Var(function(){return frontend.form_autorization.about_page;},new cljs.core.Symbol("frontend.form-autorization","about-page","frontend.form-autorization/about-page",-965632805,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"frontend.form-autorization","frontend.form-autorization",746743591,null),new cljs.core.Symbol(null,"about-page","about-page",2116788009,null),"src/frontend/form-autorization.cljs",17,1,133,133,cljs.core.list(cljs.core.PersistentVector.EMPTY),null,(cljs.core.truth_(frontend.form_autorization.about_page)?frontend.form_autorization.about_page.cljs$lang$test:null)])));
} else {
if(cljs.core.vector_QMARK_.call(null,params__12061__auto__)){
var vec__12234 = params__12061__auto__;
return reagent.session.put_BANG_.call(null,new cljs.core.Keyword(null,"current-page","current-page",-101294180),new cljs.core.Var(function(){return frontend.form_autorization.about_page;},new cljs.core.Symbol("frontend.form-autorization","about-page","frontend.form-autorization/about-page",-965632805,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"frontend.form-autorization","frontend.form-autorization",746743591,null),new cljs.core.Symbol(null,"about-page","about-page",2116788009,null),"src/frontend/form-autorization.cljs",17,1,133,133,cljs.core.list(cljs.core.PersistentVector.EMPTY),null,(cljs.core.truth_(frontend.form_autorization.about_page)?frontend.form_autorization.about_page.cljs$lang$test:null)])));
} else {
return null;
}
}
});
secretary.core.add_route_BANG_.call(null,"/about",action__12060__auto___12237);

frontend.form_autorization.mount_root = (function frontend$form_autorization$mount_root(){
return reagent.core.render.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [frontend.form_autorization.current_page], null),document.getElementById("app"));
});
