// Compiled by ClojureScript 1.9.521 {}
goog.provide('frontend.ajax_handlers');
goog.require('cljs.core');
frontend.ajax_handlers.handler = (function frontend$ajax_handlers$handler(response){
return console.log([cljs.core.str.cljs$core$IFn$_invoke$arity$1(response)].join(''));
});
frontend.ajax_handlers.error_handler = (function frontend$ajax_handlers$error_handler(p__12240){
var map__12243 = p__12240;
var map__12243__$1 = ((((!((map__12243 == null)))?((((map__12243.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__12243.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__12243):map__12243);
var status = cljs.core.get.call(null,map__12243__$1,new cljs.core.Keyword(null,"status","status",-1997798413));
var status_text = cljs.core.get.call(null,map__12243__$1,new cljs.core.Keyword(null,"status-text","status-text",-1834235478));
return console.log([cljs.core.str.cljs$core$IFn$_invoke$arity$1("something bad happened: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(status),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(status_text)].join(''));
});
