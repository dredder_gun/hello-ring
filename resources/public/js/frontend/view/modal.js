// Compiled by ClojureScript 1.9.521 {}
goog.provide('frontend.view.modal');
goog.require('cljs.core');
goog.require('re_com.core');
goog.require('free_form.bootstrap_3');
frontend.view.modal.dialog_markup = (function frontend$view$modal$dialog_markup(var_args){
var args__8514__auto__ = [];
var len__8507__auto___17171 = arguments.length;
var i__8508__auto___17172 = (0);
while(true){
if((i__8508__auto___17172 < len__8507__auto___17171)){
args__8514__auto__.push((arguments[i__8508__auto___17172]));

var G__17173 = (i__8508__auto___17172 + (1));
i__8508__auto___17172 = G__17173;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((5) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((5)),(0),null)):null);
return frontend.view.modal.dialog_markup.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),argseq__8515__auto__);
});

frontend.view.modal.dialog_markup.cljs$core$IFn$_invoke$arity$variadic = (function (form_data,process_ok,process_cancel,main_label,button_label,p__17168){
var map__17169 = p__17168;
var map__17169__$1 = ((((!((map__17169 == null)))?((((map__17169.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__17169.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__17169):map__17169);
var ajax_handler = cljs.core.get.call(null,map__17169__$1,new cljs.core.Keyword(null,"ajax-handler","ajax-handler",1607322897));
var incorrect_data = cljs.core.get.call(null,map__17169__$1,new cljs.core.Keyword(null,"incorrect-data","incorrect-data",-839694326));
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_com.core.border,new cljs.core.Keyword(null,"border","border",1444987323),"1px solid #eee",new cljs.core.Keyword(null,"child","child",623967545),new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_com.core.v_box,new cljs.core.Keyword(null,"padding","padding",1660304693),"10px",new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"background-color","background-color",570434026),"cornsilk"], null),new cljs.core.Keyword(null,"children","children",-940561982),new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_com.core.title,new cljs.core.Keyword(null,"label","label",1718410804),main_label,new cljs.core.Keyword(null,"level","level",1290497552),new cljs.core.Keyword(null,"level2","level2",-2044031830)], null),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_com.core.v_box,new cljs.core.Keyword(null,"class","class",-2030961996),"form-group",new cljs.core.Keyword(null,"children","children",-940561982),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label","label",1718410804),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"for","for",-1323786319),"pf-login"], null),"\u041D\u043E\u0432\u044B\u0439 \u043B\u043E\u0433\u0438\u043D"], null),new cljs.core.PersistentVector(null, 11, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_com.core.input_text,new cljs.core.Keyword(null,"model","model",331153215),new cljs.core.Keyword(null,"login","login",55217519).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,form_data)),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (map__17169,map__17169__$1,ajax_handler,incorrect_data){
return (function (p1__17160_SHARP_){
return cljs.core.swap_BANG_.call(null,form_data,cljs.core.assoc,new cljs.core.Keyword(null,"login","login",55217519),p1__17160_SHARP_);
});})(map__17169,map__17169__$1,ajax_handler,incorrect_data))
,new cljs.core.Keyword(null,"placeholder","placeholder",-104873083),"\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u043B\u043E\u0433\u0438\u043D",new cljs.core.Keyword(null,"class","class",-2030961996),"form-control",new cljs.core.Keyword(null,"attr","attr",-604132353),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"id","id",-1388402092),"login"], null)], null)], null)], null),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_com.core.v_box,new cljs.core.Keyword(null,"class","class",-2030961996),"form-group",new cljs.core.Keyword(null,"children","children",-940561982),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label","label",1718410804),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"for","for",-1323786319),"pf-password"], null),"\u041D\u043E\u0432\u044B\u0439 \u043F\u0430\u0440\u043E\u043B\u044C"], null),new cljs.core.PersistentVector(null, 11, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_com.core.input_text,new cljs.core.Keyword(null,"model","model",331153215),new cljs.core.Keyword(null,"password","password",417022471).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,form_data)),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (map__17169,map__17169__$1,ajax_handler,incorrect_data){
return (function (p1__17161_SHARP_){
return cljs.core.swap_BANG_.call(null,form_data,cljs.core.assoc,new cljs.core.Keyword(null,"password","password",417022471),p1__17161_SHARP_);
});})(map__17169,map__17169__$1,ajax_handler,incorrect_data))
,new cljs.core.Keyword(null,"placeholder","placeholder",-104873083),"\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u043F\u0430\u0440\u043E\u043B\u044C",new cljs.core.Keyword(null,"class","class",-2030961996),"form-control",new cljs.core.Keyword(null,"attr","attr",-604132353),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"id","id",-1388402092),"pf-password",new cljs.core.Keyword(null,"type","type",1174270348),"password"], null)], null)], null)], null),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_com.core.line,new cljs.core.Keyword(null,"color","color",1011675173),"#ddd",new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"margin","margin",-995903681),"10px 0 10px"], null)], null),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_com.core.h_box,new cljs.core.Keyword(null,"gap","gap",80255254),"12px",new cljs.core.Keyword(null,"children","children",-940561982),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_com.core.button,new cljs.core.Keyword(null,"label","label",1718410804),button_label,new cljs.core.Keyword(null,"class","class",-2030961996),"btn-primary",new cljs.core.Keyword(null,"on-click","on-click",1632826543),(((ajax_handler == null))?process_ok:ajax_handler)], null),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_com.core.button,new cljs.core.Keyword(null,"label","label",1718410804),"\u041E\u0442\u043C\u0435\u043D\u0430",new cljs.core.Keyword(null,"on-click","on-click",1632826543),process_cancel], null)], null)], null),(((cljs.core.not_EQ_.call(null,incorrect_data,null)) && (cljs.core._EQ_.call(null,cljs.core.deref.call(null,incorrect_data),"true")))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.error-message","p.error-message",770926858),"\u041D\u0435\u0432\u0435\u0440\u043D\u044B\u0435 \u0434\u0430\u043D\u043D\u044B\u0435"], null):null)], null)], null)], null);
});

frontend.view.modal.dialog_markup.cljs$lang$maxFixedArity = (5);

frontend.view.modal.dialog_markup.cljs$lang$applyTo = (function (seq17162){
var G__17163 = cljs.core.first.call(null,seq17162);
var seq17162__$1 = cljs.core.next.call(null,seq17162);
var G__17164 = cljs.core.first.call(null,seq17162__$1);
var seq17162__$2 = cljs.core.next.call(null,seq17162__$1);
var G__17165 = cljs.core.first.call(null,seq17162__$2);
var seq17162__$3 = cljs.core.next.call(null,seq17162__$2);
var G__17166 = cljs.core.first.call(null,seq17162__$3);
var seq17162__$4 = cljs.core.next.call(null,seq17162__$3);
var G__17167 = cljs.core.first.call(null,seq17162__$4);
var seq17162__$5 = cljs.core.next.call(null,seq17162__$4);
return frontend.view.modal.dialog_markup.cljs$core$IFn$_invoke$arity$variadic(G__17163,G__17164,G__17165,G__17166,G__17167,seq17162__$5);
});

