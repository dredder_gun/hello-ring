// Compiled by ClojureScript 1.9.521 {}
goog.provide('frontend.core');
goog.require('cljs.core');
goog.require('goog.events');
goog.require('reagent.core');
goog.require('re_frame.core');
goog.require('secretary.core');
goog.require('frontend.events');
goog.require('frontend.subs');
goog.require('frontend.views');
goog.require('devtools.core');
goog.require('goog.History');
goog.require('goog.history.EventType');
devtools.core.install_BANG_.call(null);
cljs.core.enable_console_print_BANG_.call(null);
var action__16312__auto___20263 = (function (params__16313__auto__){
if(cljs.core.map_QMARK_.call(null,params__16313__auto__)){
var map__20258 = params__16313__auto__;
var map__20258__$1 = ((((!((map__20258 == null)))?((((map__20258.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__20258.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__20258):map__20258);
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-showing","set-showing",-429468401),new cljs.core.Keyword(null,"all","all",892129742)], null));
} else {
if(cljs.core.vector_QMARK_.call(null,params__16313__auto__)){
var vec__20260 = params__16313__auto__;
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-showing","set-showing",-429468401),new cljs.core.Keyword(null,"all","all",892129742)], null));
} else {
return null;
}
}
});
secretary.core.add_route_BANG_.call(null,"/",action__16312__auto___20263);

var action__16312__auto___20269 = (function (params__16313__auto__){
if(cljs.core.map_QMARK_.call(null,params__16313__auto__)){
var map__20264 = params__16313__auto__;
var map__20264__$1 = ((((!((map__20264 == null)))?((((map__20264.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__20264.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__20264):map__20264);
var filter = cljs.core.get.call(null,map__20264__$1,new cljs.core.Keyword(null,"filter","filter",-948537934));
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-showing","set-showing",-429468401),cljs.core.keyword.call(null,filter)], null));
} else {
if(cljs.core.vector_QMARK_.call(null,params__16313__auto__)){
var vec__20266 = params__16313__auto__;
var filter = cljs.core.nth.call(null,vec__20266,(0),null);
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-showing","set-showing",-429468401),cljs.core.keyword.call(null,filter)], null));
} else {
return null;
}
}
});
secretary.core.add_route_BANG_.call(null,"/:filter",action__16312__auto___20269);

frontend.core.history = (function (){var G__20270 = (new goog.History());
goog.events.listen(G__20270,goog.history.EventType.NAVIGATE,((function (G__20270){
return (function (event){
return secretary.core.dispatch_BANG_.call(null,event.token);
});})(G__20270))
);

G__20270.setEnabled(true);

return G__20270;
})();
frontend.core.main = (function frontend$core$main(){
re_frame.core.dispatch_sync.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"initialise-db","initialise-db",-533872578)], null));

return reagent.core.render.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [frontend.views.todo_app], null),document.getElementById("app"));
});
goog.exportSymbol('frontend.core.main', frontend.core.main);
