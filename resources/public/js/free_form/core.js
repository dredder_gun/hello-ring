// Compiled by ClojureScript 1.9.521 {}
goog.provide('free_form.core');
goog.require('cljs.core');
goog.require('free_form.bootstrap_3');
goog.require('free_form.debug');
goog.require('clojure.string');
goog.require('clojure.walk');
goog.require('free_form.extension');
goog.require('free_form.util');
free_form.core.extract_attributes = (function free_form$core$extract_attributes(node,key){
var attributes = cljs.core.get.call(null,node,free_form.util.attributes_index);
var re_attributes = key.call(null,attributes);
var attributes__$1 = cljs.core.dissoc.call(null,attributes,key);
var keys = (function (){var or__7394__auto__ = new cljs.core.Keyword(null,"keys","keys",1068423698).cljs$core$IFn$_invoke$arity$1(re_attributes);
if(cljs.core.truth_(or__7394__auto__)){
return or__7394__auto__;
} else {
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(re_attributes)], null);
}
})();
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [attributes__$1,re_attributes,keys], null);
});
free_form.core.input_QMARK_ = (function free_form$core$input_QMARK_(node){
return (cljs.core.coll_QMARK_.call(null,node)) && (cljs.core.contains_QMARK_.call(null,cljs.core.second.call(null,node),new cljs.core.Keyword("free-form","input","free-form/input",228451328)));
});
free_form.core.js_event_value = (function free_form$core$js_event_value(event){
return event.target.value;
});
free_form.core.bind_input = (function free_form$core$bind_input(values,on_change,node){
if(cljs.core.not.call(null,free_form.core.input_QMARK_.call(null,node))){
return node;
} else {
var vec__16929 = free_form.core.extract_attributes.call(null,node,new cljs.core.Keyword("free-form","input","free-form/input",228451328));
var attributes = cljs.core.nth.call(null,vec__16929,(0),null);
var _ = cljs.core.nth.call(null,vec__16929,(1),null);
var keys = cljs.core.nth.call(null,vec__16929,(2),null);
return cljs.core.assoc.call(null,node,free_form.util.attributes_index,cljs.core.assoc.call(null,attributes,new cljs.core.Keyword(null,"value","value",305978217),(function (){var or__7394__auto__ = cljs.core.get_in.call(null,values,keys);
if(cljs.core.truth_(or__7394__auto__)){
return or__7394__auto__;
} else {
return "";
}
})(),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (vec__16929,attributes,_,keys){
return (function (p1__16925_SHARP_){
return on_change.call(null,keys,free_form.core.js_event_value.call(null,p1__16925_SHARP_));
});})(vec__16929,attributes,_,keys))
));
}
});
/**
 * Tests whether the node should be marked with an error class should the field have an associated error.
 */
free_form.core.error_class_QMARK_ = (function free_form$core$error_class_QMARK_(node){
return (cljs.core.coll_QMARK_.call(null,node)) && (cljs.core.contains_QMARK_.call(null,cljs.core.second.call(null,node),new cljs.core.Keyword("free-form","error-class","free-form/error-class",159754118)));
});
free_form.core.bind_error_class = (function free_form$core$bind_error_class(errors,node){
if(cljs.core.not.call(null,free_form.core.error_class_QMARK_.call(null,node))){
return node;
} else {
var vec__16937 = free_form.core.extract_attributes.call(null,node,new cljs.core.Keyword("free-form","error-class","free-form/error-class",159754118));
var attributes = cljs.core.nth.call(null,vec__16937,(0),null);
var re_attributes = cljs.core.nth.call(null,vec__16937,(1),null);
var keys = cljs.core.nth.call(null,vec__16937,(2),null);
return cljs.core.assoc.call(null,node,free_form.util.attributes_index,((cljs.core.not_any_QMARK_.call(null,((function (vec__16937,attributes,re_attributes,keys){
return (function (p1__16932_SHARP_){
return cljs.core.get_in.call(null,errors,p1__16932_SHARP_);
});})(vec__16937,attributes,re_attributes,keys))
,cljs.core.conj.call(null,new cljs.core.Keyword(null,"extra-keys","extra-keys",-1845607319).cljs$core$IFn$_invoke$arity$1(re_attributes),keys)))?attributes:cljs.core.update.call(null,attributes,new cljs.core.Keyword(null,"class","class",-2030961996),((function (vec__16937,attributes,re_attributes,keys){
return (function (p1__16933_SHARP_){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__7394__auto__ = new cljs.core.Keyword(null,"error","error",-978969032).cljs$core$IFn$_invoke$arity$1(re_attributes);
if(cljs.core.truth_(or__7394__auto__)){
return or__7394__auto__;
} else {
return "error";
}
})()),cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__16933_SHARP_)].join('');
});})(vec__16937,attributes,re_attributes,keys))
)));
}
});
free_form.core.error_messages_QMARK_ = (function free_form$core$error_messages_QMARK_(node){
return (cljs.core.coll_QMARK_.call(null,node)) && (cljs.core.contains_QMARK_.call(null,cljs.core.second.call(null,node),new cljs.core.Keyword("free-form","error-message","free-form/error-message",-1957745210)));
});
free_form.core.bind_error_messages = (function free_form$core$bind_error_messages(errors,node){
if(cljs.core.not.call(null,free_form.core.error_messages_QMARK_.call(null,node))){
return node;
} else {
var vec__16944 = free_form.core.extract_attributes.call(null,node,new cljs.core.Keyword("free-form","error-message","free-form/error-message",-1957745210));
var attributes = cljs.core.nth.call(null,vec__16944,(0),null);
var _ = cljs.core.nth.call(null,vec__16944,(1),null);
var keys = cljs.core.nth.call(null,vec__16944,(2),null);
var temp__4655__auto__ = cljs.core.get_in.call(null,errors,keys);
if(cljs.core.truth_(temp__4655__auto__)){
var errors__$1 = temp__4655__auto__;
return cljs.core.vec.call(null,cljs.core.concat.call(null,cljs.core.drop_last.call(null,cljs.core.assoc.call(null,node,free_form.util.attributes_index,attributes)),cljs.core.map.call(null,((function (errors__$1,temp__4655__auto__,vec__16944,attributes,_,keys){
return (function (p1__16940_SHARP_){
return cljs.core.conj.call(null,cljs.core.get.call(null,node,(2)),p1__16940_SHARP_);
});})(errors__$1,temp__4655__auto__,vec__16944,attributes,_,keys))
,errors__$1)));
} else {
return null;
}
}
});
free_form.core.warn_of_leftovers = (function free_form$core$warn_of_leftovers(node){
var attrs_16948 = cljs.core.get.call(null,node,free_form.util.attributes_index);
if(cljs.core.truth_((function (){var and__7382__auto__ = cljs.core.map_QMARK_.call(null,attrs_16948);
if(and__7382__auto__){
return cljs.core.some.call(null,((function (and__7382__auto__,attrs_16948){
return (function (p1__16947_SHARP_){
return cljs.core._EQ_.call(null,"free-form",cljs.core.namespace.call(null,p1__16947_SHARP_));
});})(and__7382__auto__,attrs_16948))
,cljs.core.keys.call(null,attrs_16948));
} else {
return and__7382__auto__;
}
})())){
console.error("There are free-form-looking leftovers on",cljs.core.pr_str.call(null,node));
} else {
}

return node;
});
free_form.core.form = (function free_form$core$form(var_args){
var args16955 = [];
var len__8507__auto___16958 = arguments.length;
var i__8508__auto___16959 = (0);
while(true){
if((i__8508__auto___16959 < len__8507__auto___16958)){
args16955.push((arguments[i__8508__auto___16959]));

var G__16960 = (i__8508__auto___16959 + (1));
i__8508__auto___16959 = G__16960;
continue;
} else {
}
break;
}

var G__16957 = args16955.length;
switch (G__16957) {
case 4:
return free_form.core.form.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return free_form.core.form.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args16955.length)].join('')));

}
});

free_form.core.form.cljs$core$IFn$_invoke$arity$4 = (function (values,errors,on_change,html){
return free_form.core.form.call(null,values,errors,on_change,cljs.core.PersistentVector.EMPTY,html);
});

free_form.core.form.cljs$core$IFn$_invoke$arity$5 = (function (values,errors,on_change,extensions,html){
var errors__$1 = (function (){var or__7394__auto__ = errors;
if(cljs.core.truth_(or__7394__auto__)){
return or__7394__auto__;
} else {
return cljs.core.PersistentArrayMap.EMPTY;
}
})();
var extensions__$1 = ((cljs.core.sequential_QMARK_.call(null,extensions))?extensions:new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [extensions], null));
var inner_fn = ((function (errors__$1,extensions__$1){
return (function (html__$1){
return clojure.walk.postwalk.call(null,((function (errors__$1,extensions__$1){
return (function (p1__16951_SHARP_){
return free_form.core.bind_error_messages.call(null,errors__$1,p1__16951_SHARP_);
});})(errors__$1,extensions__$1))
,clojure.walk.postwalk.call(null,((function (errors__$1,extensions__$1){
return (function (p1__16950_SHARP_){
return free_form.core.bind_error_class.call(null,errors__$1,p1__16950_SHARP_);
});})(errors__$1,extensions__$1))
,clojure.walk.postwalk.call(null,((function (errors__$1,extensions__$1){
return (function (p1__16949_SHARP_){
return free_form.core.bind_input.call(null,values,on_change,p1__16949_SHARP_);
});})(errors__$1,extensions__$1))
,html__$1)));
});})(errors__$1,extensions__$1))
;
return clojure.walk.postwalk.call(null,((function (errors__$1,extensions__$1,inner_fn){
return (function (p1__16952_SHARP_){
return free_form.core.warn_of_leftovers.call(null,p1__16952_SHARP_);
});})(errors__$1,extensions__$1,inner_fn))
,cljs.core.reduce.call(null,((function (errors__$1,extensions__$1,inner_fn){
return (function (p1__16954_SHARP_,p2__16953_SHARP_){
return free_form.extension.extension.call(null,p2__16953_SHARP_,p1__16954_SHARP_);
});})(errors__$1,extensions__$1,inner_fn))
,inner_fn,extensions__$1).call(null,html));
});

free_form.core.form.cljs$lang$maxFixedArity = 5;

