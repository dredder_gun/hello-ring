// Compiled by ClojureScript 1.9.521 {}
goog.provide('free_form.debug');
goog.require('cljs.core');
goog.require('free_form.extension');
cljs.core._add_method.call(null,free_form.extension.extension,new cljs.core.Keyword(null,"debug","debug",-1608172596),(function (_extension_name,inner_fn){
return (function (html){
cljs.core.println.call(null,"Before:",html);

var html__$1 = inner_fn.call(null,html);
cljs.core.println.call(null,"After",html__$1);

return html__$1;
});
}));
