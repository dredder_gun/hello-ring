// Compiled by ClojureScript 1.9.521 {}
goog.provide('re_frame.fx');
goog.require('cljs.core');
goog.require('re_frame.router');
goog.require('re_frame.db');
goog.require('re_frame.interceptor');
goog.require('re_frame.interop');
goog.require('re_frame.events');
goog.require('re_frame.registrar');
goog.require('re_frame.loggers');
re_frame.fx.kind = new cljs.core.Keyword(null,"fx","fx",-1237829572);
if(cljs.core.truth_(re_frame.registrar.kinds.call(null,re_frame.fx.kind))){
} else {
throw (new Error("Assert failed: (re-frame.registrar/kinds kind)"));
}
re_frame.fx.register = cljs.core.partial.call(null,re_frame.registrar.register_handler,re_frame.fx.kind);
/**
 * An interceptor which actions a `context's` (side) `:effects`.
 * 
 *   For each key in the `:effects` map, call the `effects handler` previously
 *   registered using `reg-fx`.
 * 
 *   So, if `:effects` was:
 *    {:dispatch  [:hello 42]
 *     :db        {...}
 *     :undo      "set flag"}
 *   call the registered effects handlers for each of the map's keys:
 *   `:dispatch`, `:undo` and `:db`.
 */
re_frame.fx.do_fx = re_frame.interceptor.__GT_interceptor.call(null,new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"do-fx","do-fx",1194163050),new cljs.core.Keyword(null,"after","after",594996914),(function re_frame$fx$do_fx_after(context){
var seq__17194 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"effects","effects",-282369292).cljs$core$IFn$_invoke$arity$1(context));
var chunk__17195 = null;
var count__17196 = (0);
var i__17197 = (0);
while(true){
if((i__17197 < count__17196)){
var vec__17198 = cljs.core._nth.call(null,chunk__17195,i__17197);
var effect_k = cljs.core.nth.call(null,vec__17198,(0),null);
var value = cljs.core.nth.call(null,vec__17198,(1),null);
var temp__4655__auto___17204 = re_frame.registrar.get_handler.call(null,re_frame.fx.kind,effect_k,true);
if(cljs.core.truth_(temp__4655__auto___17204)){
var effect_fn_17205 = temp__4655__auto___17204;
effect_fn_17205.call(null,value);
} else {
}

var G__17206 = seq__17194;
var G__17207 = chunk__17195;
var G__17208 = count__17196;
var G__17209 = (i__17197 + (1));
seq__17194 = G__17206;
chunk__17195 = G__17207;
count__17196 = G__17208;
i__17197 = G__17209;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__17194);
if(temp__4657__auto__){
var seq__17194__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__17194__$1)){
var c__8213__auto__ = cljs.core.chunk_first.call(null,seq__17194__$1);
var G__17210 = cljs.core.chunk_rest.call(null,seq__17194__$1);
var G__17211 = c__8213__auto__;
var G__17212 = cljs.core.count.call(null,c__8213__auto__);
var G__17213 = (0);
seq__17194 = G__17210;
chunk__17195 = G__17211;
count__17196 = G__17212;
i__17197 = G__17213;
continue;
} else {
var vec__17201 = cljs.core.first.call(null,seq__17194__$1);
var effect_k = cljs.core.nth.call(null,vec__17201,(0),null);
var value = cljs.core.nth.call(null,vec__17201,(1),null);
var temp__4655__auto___17214 = re_frame.registrar.get_handler.call(null,re_frame.fx.kind,effect_k,true);
if(cljs.core.truth_(temp__4655__auto___17214)){
var effect_fn_17215 = temp__4655__auto___17214;
effect_fn_17215.call(null,value);
} else {
}

var G__17216 = cljs.core.next.call(null,seq__17194__$1);
var G__17217 = null;
var G__17218 = (0);
var G__17219 = (0);
seq__17194 = G__17216;
chunk__17195 = G__17217;
count__17196 = G__17218;
i__17197 = G__17219;
continue;
}
} else {
return null;
}
}
break;
}
}));
re_frame.fx.register.call(null,new cljs.core.Keyword(null,"dispatch-later","dispatch-later",291951390),(function (value){
var seq__17220 = cljs.core.seq.call(null,value);
var chunk__17221 = null;
var count__17222 = (0);
var i__17223 = (0);
while(true){
if((i__17223 < count__17222)){
var map__17224 = cljs.core._nth.call(null,chunk__17221,i__17223);
var map__17224__$1 = ((((!((map__17224 == null)))?((((map__17224.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__17224.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__17224):map__17224);
var effect = map__17224__$1;
var ms = cljs.core.get.call(null,map__17224__$1,new cljs.core.Keyword(null,"ms","ms",-1152709733));
var dispatch = cljs.core.get.call(null,map__17224__$1,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009));
if((cljs.core.empty_QMARK_.call(null,dispatch)) || (!(typeof ms === 'number'))){
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: ignoring bad :dispatch-later value:",effect);
} else {
re_frame.interop.set_timeout_BANG_.call(null,((function (seq__17220,chunk__17221,count__17222,i__17223,map__17224,map__17224__$1,effect,ms,dispatch){
return (function (){
return re_frame.router.dispatch.call(null,dispatch);
});})(seq__17220,chunk__17221,count__17222,i__17223,map__17224,map__17224__$1,effect,ms,dispatch))
,ms);
}

var G__17228 = seq__17220;
var G__17229 = chunk__17221;
var G__17230 = count__17222;
var G__17231 = (i__17223 + (1));
seq__17220 = G__17228;
chunk__17221 = G__17229;
count__17222 = G__17230;
i__17223 = G__17231;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__17220);
if(temp__4657__auto__){
var seq__17220__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__17220__$1)){
var c__8213__auto__ = cljs.core.chunk_first.call(null,seq__17220__$1);
var G__17232 = cljs.core.chunk_rest.call(null,seq__17220__$1);
var G__17233 = c__8213__auto__;
var G__17234 = cljs.core.count.call(null,c__8213__auto__);
var G__17235 = (0);
seq__17220 = G__17232;
chunk__17221 = G__17233;
count__17222 = G__17234;
i__17223 = G__17235;
continue;
} else {
var map__17226 = cljs.core.first.call(null,seq__17220__$1);
var map__17226__$1 = ((((!((map__17226 == null)))?((((map__17226.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__17226.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__17226):map__17226);
var effect = map__17226__$1;
var ms = cljs.core.get.call(null,map__17226__$1,new cljs.core.Keyword(null,"ms","ms",-1152709733));
var dispatch = cljs.core.get.call(null,map__17226__$1,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009));
if((cljs.core.empty_QMARK_.call(null,dispatch)) || (!(typeof ms === 'number'))){
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: ignoring bad :dispatch-later value:",effect);
} else {
re_frame.interop.set_timeout_BANG_.call(null,((function (seq__17220,chunk__17221,count__17222,i__17223,map__17226,map__17226__$1,effect,ms,dispatch,seq__17220__$1,temp__4657__auto__){
return (function (){
return re_frame.router.dispatch.call(null,dispatch);
});})(seq__17220,chunk__17221,count__17222,i__17223,map__17226,map__17226__$1,effect,ms,dispatch,seq__17220__$1,temp__4657__auto__))
,ms);
}

var G__17236 = cljs.core.next.call(null,seq__17220__$1);
var G__17237 = null;
var G__17238 = (0);
var G__17239 = (0);
seq__17220 = G__17236;
chunk__17221 = G__17237;
count__17222 = G__17238;
i__17223 = G__17239;
continue;
}
} else {
return null;
}
}
break;
}
}));
re_frame.fx.register.call(null,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009),(function (value){
if(!(cljs.core.vector_QMARK_.call(null,value))){
return re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: ignoring bad :dispatch value. Expected a vector, but got:",value);
} else {
return re_frame.router.dispatch.call(null,value);
}
}));
re_frame.fx.register.call(null,new cljs.core.Keyword(null,"dispatch-n","dispatch-n",-504469236),(function (value){
if(!(cljs.core.sequential_QMARK_.call(null,value))){
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: ignoring bad :dispatch-n value. Expected a collection, got got:",value);
} else {
}

var seq__17240 = cljs.core.seq.call(null,value);
var chunk__17241 = null;
var count__17242 = (0);
var i__17243 = (0);
while(true){
if((i__17243 < count__17242)){
var event = cljs.core._nth.call(null,chunk__17241,i__17243);
re_frame.router.dispatch.call(null,event);

var G__17244 = seq__17240;
var G__17245 = chunk__17241;
var G__17246 = count__17242;
var G__17247 = (i__17243 + (1));
seq__17240 = G__17244;
chunk__17241 = G__17245;
count__17242 = G__17246;
i__17243 = G__17247;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__17240);
if(temp__4657__auto__){
var seq__17240__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__17240__$1)){
var c__8213__auto__ = cljs.core.chunk_first.call(null,seq__17240__$1);
var G__17248 = cljs.core.chunk_rest.call(null,seq__17240__$1);
var G__17249 = c__8213__auto__;
var G__17250 = cljs.core.count.call(null,c__8213__auto__);
var G__17251 = (0);
seq__17240 = G__17248;
chunk__17241 = G__17249;
count__17242 = G__17250;
i__17243 = G__17251;
continue;
} else {
var event = cljs.core.first.call(null,seq__17240__$1);
re_frame.router.dispatch.call(null,event);

var G__17252 = cljs.core.next.call(null,seq__17240__$1);
var G__17253 = null;
var G__17254 = (0);
var G__17255 = (0);
seq__17240 = G__17252;
chunk__17241 = G__17253;
count__17242 = G__17254;
i__17243 = G__17255;
continue;
}
} else {
return null;
}
}
break;
}
}));
re_frame.fx.register.call(null,new cljs.core.Keyword(null,"deregister-event-handler","deregister-event-handler",-1096518994),(function (value){
var clear_event = cljs.core.partial.call(null,re_frame.registrar.clear_handlers,re_frame.events.kind);
if(cljs.core.sequential_QMARK_.call(null,value)){
var seq__17256 = cljs.core.seq.call(null,((cljs.core.sequential_QMARK_.call(null,value))?value:new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [value], null)));
var chunk__17257 = null;
var count__17258 = (0);
var i__17259 = (0);
while(true){
if((i__17259 < count__17258)){
var event = cljs.core._nth.call(null,chunk__17257,i__17259);
clear_event.call(null,event);

var G__17260 = seq__17256;
var G__17261 = chunk__17257;
var G__17262 = count__17258;
var G__17263 = (i__17259 + (1));
seq__17256 = G__17260;
chunk__17257 = G__17261;
count__17258 = G__17262;
i__17259 = G__17263;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__17256);
if(temp__4657__auto__){
var seq__17256__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__17256__$1)){
var c__8213__auto__ = cljs.core.chunk_first.call(null,seq__17256__$1);
var G__17264 = cljs.core.chunk_rest.call(null,seq__17256__$1);
var G__17265 = c__8213__auto__;
var G__17266 = cljs.core.count.call(null,c__8213__auto__);
var G__17267 = (0);
seq__17256 = G__17264;
chunk__17257 = G__17265;
count__17258 = G__17266;
i__17259 = G__17267;
continue;
} else {
var event = cljs.core.first.call(null,seq__17256__$1);
clear_event.call(null,event);

var G__17268 = cljs.core.next.call(null,seq__17256__$1);
var G__17269 = null;
var G__17270 = (0);
var G__17271 = (0);
seq__17256 = G__17268;
chunk__17257 = G__17269;
count__17258 = G__17270;
i__17259 = G__17271;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
}));
re_frame.fx.register.call(null,new cljs.core.Keyword(null,"db","db",993250759),(function (value){
return cljs.core.reset_BANG_.call(null,re_frame.db.app_db,value);
}));
