// Compiled by ClojureScript 1.9.521 {}
goog.provide('reagent.session');
goog.require('cljs.core');
goog.require('reagent.core');
reagent.session.state = reagent.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
/**
 * Returns a cursor from the state atom.
 */
reagent.session.cursor = (function reagent$session$cursor(ks){
return reagent.core.cursor.call(null,reagent.session.state,ks);
});
/**
 * Get the key's value from the session, returns nil if it doesn't exist.
 */
reagent.session.get = (function reagent$session$get(var_args){
var args__8514__auto__ = [];
var len__8507__auto___19427 = arguments.length;
var i__8508__auto___19428 = (0);
while(true){
if((i__8508__auto___19428 < len__8507__auto___19427)){
args__8514__auto__.push((arguments[i__8508__auto___19428]));

var G__19429 = (i__8508__auto___19428 + (1));
i__8508__auto___19428 = G__19429;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((1) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((1)),(0),null)):null);
return reagent.session.get.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__8515__auto__);
});

reagent.session.get.cljs$core$IFn$_invoke$arity$variadic = (function (k,p__19423){
var vec__19424 = p__19423;
var default$ = cljs.core.nth.call(null,vec__19424,(0),null);
var temp_a = reagent.session.cursor.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [k], null));
if(!((cljs.core.deref.call(null,temp_a) == null))){
return cljs.core.deref.call(null,temp_a);
} else {
return default$;
}
});

reagent.session.get.cljs$lang$maxFixedArity = (1);

reagent.session.get.cljs$lang$applyTo = (function (seq19421){
var G__19422 = cljs.core.first.call(null,seq19421);
var seq19421__$1 = cljs.core.next.call(null,seq19421);
return reagent.session.get.cljs$core$IFn$_invoke$arity$variadic(G__19422,seq19421__$1);
});

reagent.session.put_BANG_ = (function reagent$session$put_BANG_(k,v){
return cljs.core.swap_BANG_.call(null,reagent.session.state,cljs.core.assoc,k,v);
});
/**
 * Gets the value at the path specified by the vector ks from the session,
 *   returns nil if it doesn't exist.
 */
reagent.session.get_in = (function reagent$session$get_in(var_args){
var args__8514__auto__ = [];
var len__8507__auto___19436 = arguments.length;
var i__8508__auto___19437 = (0);
while(true){
if((i__8508__auto___19437 < len__8507__auto___19436)){
args__8514__auto__.push((arguments[i__8508__auto___19437]));

var G__19438 = (i__8508__auto___19437 + (1));
i__8508__auto___19437 = G__19438;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((1) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((1)),(0),null)):null);
return reagent.session.get_in.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__8515__auto__);
});

reagent.session.get_in.cljs$core$IFn$_invoke$arity$variadic = (function (ks,p__19432){
var vec__19433 = p__19432;
var default$ = cljs.core.nth.call(null,vec__19433,(0),null);
var or__7394__auto__ = cljs.core.deref.call(null,reagent.session.cursor.call(null,ks));
if(cljs.core.truth_(or__7394__auto__)){
return or__7394__auto__;
} else {
return default$;
}
});

reagent.session.get_in.cljs$lang$maxFixedArity = (1);

reagent.session.get_in.cljs$lang$applyTo = (function (seq19430){
var G__19431 = cljs.core.first.call(null,seq19430);
var seq19430__$1 = cljs.core.next.call(null,seq19430);
return reagent.session.get_in.cljs$core$IFn$_invoke$arity$variadic(G__19431,seq19430__$1);
});

/**
 * Replace the current session's value with the result of executing f with
 *   the current value and args.
 */
reagent.session.swap_BANG_ = (function reagent$session$swap_BANG_(var_args){
var args__8514__auto__ = [];
var len__8507__auto___19441 = arguments.length;
var i__8508__auto___19442 = (0);
while(true){
if((i__8508__auto___19442 < len__8507__auto___19441)){
args__8514__auto__.push((arguments[i__8508__auto___19442]));

var G__19443 = (i__8508__auto___19442 + (1));
i__8508__auto___19442 = G__19443;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((1) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((1)),(0),null)):null);
return reagent.session.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__8515__auto__);
});

reagent.session.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (f,args){
return cljs.core.apply.call(null,cljs.core.swap_BANG_,reagent.session.state,f,args);
});

reagent.session.swap_BANG_.cljs$lang$maxFixedArity = (1);

reagent.session.swap_BANG_.cljs$lang$applyTo = (function (seq19439){
var G__19440 = cljs.core.first.call(null,seq19439);
var seq19439__$1 = cljs.core.next.call(null,seq19439);
return reagent.session.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__19440,seq19439__$1);
});

/**
 * Remove all data from the session and start over cleanly.
 */
reagent.session.clear_BANG_ = (function reagent$session$clear_BANG_(){
return cljs.core.reset_BANG_.call(null,reagent.session.state,cljs.core.PersistentArrayMap.EMPTY);
});
reagent.session.reset_BANG_ = (function reagent$session$reset_BANG_(m){
return cljs.core.reset_BANG_.call(null,reagent.session.state,m);
});
/**
 * Remove a key from the session
 */
reagent.session.remove_BANG_ = (function reagent$session$remove_BANG_(k){
return cljs.core.swap_BANG_.call(null,reagent.session.state,cljs.core.dissoc,k);
});
/**
 * Associates a value in the session, where ks is a
 * sequence of keys and v is the new value and returns
 * a new nested structure. If any levels do not exist,
 * hash-maps will be created.
 */
reagent.session.assoc_in_BANG_ = (function reagent$session$assoc_in_BANG_(ks,v){
return cljs.core.swap_BANG_.call(null,reagent.session.state,cljs.core.assoc_in,ks,v);
});
/**
 * Destructive get from the session. This returns the current value of the key
 *   and then removes it from the session.
 */
reagent.session.get_BANG_ = (function reagent$session$get_BANG_(var_args){
var args__8514__auto__ = [];
var len__8507__auto___19450 = arguments.length;
var i__8508__auto___19451 = (0);
while(true){
if((i__8508__auto___19451 < len__8507__auto___19450)){
args__8514__auto__.push((arguments[i__8508__auto___19451]));

var G__19452 = (i__8508__auto___19451 + (1));
i__8508__auto___19451 = G__19452;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((1) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((1)),(0),null)):null);
return reagent.session.get_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__8515__auto__);
});

reagent.session.get_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (k,p__19446){
var vec__19447 = p__19446;
var default$ = cljs.core.nth.call(null,vec__19447,(0),null);
var cur = reagent.session.get.call(null,k,default$);
reagent.session.remove_BANG_.call(null,k);

return cur;
});

reagent.session.get_BANG_.cljs$lang$maxFixedArity = (1);

reagent.session.get_BANG_.cljs$lang$applyTo = (function (seq19444){
var G__19445 = cljs.core.first.call(null,seq19444);
var seq19444__$1 = cljs.core.next.call(null,seq19444);
return reagent.session.get_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__19445,seq19444__$1);
});

/**
 * Destructive get from the session. This returns the current value of the path
 *   specified by the vector ks and then removes it from the session.
 */
reagent.session.get_in_BANG_ = (function reagent$session$get_in_BANG_(var_args){
var args__8514__auto__ = [];
var len__8507__auto___19459 = arguments.length;
var i__8508__auto___19460 = (0);
while(true){
if((i__8508__auto___19460 < len__8507__auto___19459)){
args__8514__auto__.push((arguments[i__8508__auto___19460]));

var G__19461 = (i__8508__auto___19460 + (1));
i__8508__auto___19460 = G__19461;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((1) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((1)),(0),null)):null);
return reagent.session.get_in_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__8515__auto__);
});

reagent.session.get_in_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ks,p__19455){
var vec__19456 = p__19455;
var default$ = cljs.core.nth.call(null,vec__19456,(0),null);
var cur = reagent.session.get_in.call(null,ks,default$);
reagent.session.assoc_in_BANG_.call(null,ks,null);

return cur;
});

reagent.session.get_in_BANG_.cljs$lang$maxFixedArity = (1);

reagent.session.get_in_BANG_.cljs$lang$applyTo = (function (seq19453){
var G__19454 = cljs.core.first.call(null,seq19453);
var seq19453__$1 = cljs.core.next.call(null,seq19453);
return reagent.session.get_in_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__19454,seq19453__$1);
});

/**
 * Updates a value in session where k is a key and f
 * is the function that takes the old value along with any
 * supplied args and return the new value. If key is not
 * present it will be added.
 */
reagent.session.update_BANG_ = (function reagent$session$update_BANG_(var_args){
var args__8514__auto__ = [];
var len__8507__auto___19466 = arguments.length;
var i__8508__auto___19467 = (0);
while(true){
if((i__8508__auto___19467 < len__8507__auto___19466)){
args__8514__auto__.push((arguments[i__8508__auto___19467]));

var G__19468 = (i__8508__auto___19467 + (1));
i__8508__auto___19467 = G__19468;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((2) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((2)),(0),null)):null);
return reagent.session.update_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__8515__auto__);
});

reagent.session.update_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (k,f,args){
return cljs.core.swap_BANG_.call(null,reagent.session.state,(function (p1__19462_SHARP_){
return cljs.core.apply.call(null,cljs.core.partial.call(null,cljs.core.update,p1__19462_SHARP_,k,f),args);
}));
});

reagent.session.update_BANG_.cljs$lang$maxFixedArity = (2);

reagent.session.update_BANG_.cljs$lang$applyTo = (function (seq19463){
var G__19464 = cljs.core.first.call(null,seq19463);
var seq19463__$1 = cljs.core.next.call(null,seq19463);
var G__19465 = cljs.core.first.call(null,seq19463__$1);
var seq19463__$2 = cljs.core.next.call(null,seq19463__$1);
return reagent.session.update_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__19464,G__19465,seq19463__$2);
});

/**
 * 'Updates a value in the session, where ks is a
 * sequence of keys and f is a function that will
 * take the old value along with any supplied args and return
 * the new value. If any levels do not exist, hash-maps
 * will be created.
 */
reagent.session.update_in_BANG_ = (function reagent$session$update_in_BANG_(var_args){
var args__8514__auto__ = [];
var len__8507__auto___19473 = arguments.length;
var i__8508__auto___19474 = (0);
while(true){
if((i__8508__auto___19474 < len__8507__auto___19473)){
args__8514__auto__.push((arguments[i__8508__auto___19474]));

var G__19475 = (i__8508__auto___19474 + (1));
i__8508__auto___19474 = G__19475;
continue;
} else {
}
break;
}

var argseq__8515__auto__ = ((((2) < args__8514__auto__.length))?(new cljs.core.IndexedSeq(args__8514__auto__.slice((2)),(0),null)):null);
return reagent.session.update_in_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__8515__auto__);
});

reagent.session.update_in_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ks,f,args){
return cljs.core.swap_BANG_.call(null,reagent.session.state,(function (p1__19469_SHARP_){
return cljs.core.apply.call(null,cljs.core.partial.call(null,cljs.core.update_in,p1__19469_SHARP_,ks,f),args);
}));
});

reagent.session.update_in_BANG_.cljs$lang$maxFixedArity = (2);

reagent.session.update_in_BANG_.cljs$lang$applyTo = (function (seq19470){
var G__19471 = cljs.core.first.call(null,seq19470);
var seq19470__$1 = cljs.core.next.call(null,seq19470);
var G__19472 = cljs.core.first.call(null,seq19470__$1);
var seq19470__$2 = cljs.core.next.call(null,seq19470__$1);
return reagent.session.update_in_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__19471,G__19472,seq19470__$2);
});

