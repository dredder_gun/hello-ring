// Compiled by ClojureScript 1.9.521 {}
goog.provide('reagent.debug');
goog.require('cljs.core');
reagent.debug.has_console = typeof console !== 'undefined';
reagent.debug.tracking = false;
if(typeof reagent.debug.warnings !== 'undefined'){
} else {
reagent.debug.warnings = cljs.core.atom.call(null,null);
}
if(typeof reagent.debug.track_console !== 'undefined'){
} else {
reagent.debug.track_console = (function (){var o = ({});
o.warn = ((function (o){
return (function() { 
var G__14042__delegate = function (args){
return cljs.core.swap_BANG_.call(null,reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"warn","warn",-436710552)], null),cljs.core.conj,cljs.core.apply.call(null,cljs.core.str,args));
};
var G__14042 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__14043__i = 0, G__14043__a = new Array(arguments.length -  0);
while (G__14043__i < G__14043__a.length) {G__14043__a[G__14043__i] = arguments[G__14043__i + 0]; ++G__14043__i;}
  args = new cljs.core.IndexedSeq(G__14043__a,0);
} 
return G__14042__delegate.call(this,args);};
G__14042.cljs$lang$maxFixedArity = 0;
G__14042.cljs$lang$applyTo = (function (arglist__14044){
var args = cljs.core.seq(arglist__14044);
return G__14042__delegate(args);
});
G__14042.cljs$core$IFn$_invoke$arity$variadic = G__14042__delegate;
return G__14042;
})()
;})(o))
;

o.error = ((function (o){
return (function() { 
var G__14045__delegate = function (args){
return cljs.core.swap_BANG_.call(null,reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"error","error",-978969032)], null),cljs.core.conj,cljs.core.apply.call(null,cljs.core.str,args));
};
var G__14045 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__14046__i = 0, G__14046__a = new Array(arguments.length -  0);
while (G__14046__i < G__14046__a.length) {G__14046__a[G__14046__i] = arguments[G__14046__i + 0]; ++G__14046__i;}
  args = new cljs.core.IndexedSeq(G__14046__a,0);
} 
return G__14045__delegate.call(this,args);};
G__14045.cljs$lang$maxFixedArity = 0;
G__14045.cljs$lang$applyTo = (function (arglist__14047){
var args = cljs.core.seq(arglist__14047);
return G__14045__delegate(args);
});
G__14045.cljs$core$IFn$_invoke$arity$variadic = G__14045__delegate;
return G__14045;
})()
;})(o))
;

return o;
})();
}
reagent.debug.track_warnings = (function reagent$debug$track_warnings(f){
reagent.debug.tracking = true;

cljs.core.reset_BANG_.call(null,reagent.debug.warnings,null);

f.call(null);

var warns = cljs.core.deref.call(null,reagent.debug.warnings);
cljs.core.reset_BANG_.call(null,reagent.debug.warnings,null);

reagent.debug.tracking = false;

return warns;
});
