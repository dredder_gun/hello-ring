// Compiled by ClojureScript 1.9.521 {}
goog.provide('todomvc.subs');
goog.require('cljs.core');
goog.require('re_frame.core');
goog.require('reagent.ratom');
re_frame.core.reg_sub.call(null,new cljs.core.Keyword(null,"showing","showing",798009604),(function (db,_){
return new cljs.core.Keyword(null,"showing","showing",798009604).cljs$core$IFn$_invoke$arity$1(db);
}));
todomvc.subs.sorted_todos = (function todomvc$subs$sorted_todos(db,_){
return new cljs.core.Keyword(null,"todos","todos",630308868).cljs$core$IFn$_invoke$arity$1(db);
});
re_frame.core.reg_sub.call(null,new cljs.core.Keyword(null,"sorted-todos","sorted-todos",-1662564733),todomvc.subs.sorted_todos);
todomvc.subs.password_login = (function todomvc$subs$password_login(db,_){
return new cljs.core.Keyword(null,"passed?","passed?",1858749938).cljs$core$IFn$_invoke$arity$1(db);
});
re_frame.core.reg_sub.call(null,new cljs.core.Keyword(null,"login-password","login-password",1067260115),todomvc.subs.password_login);
re_frame.core.reg_sub.call(null,new cljs.core.Keyword(null,"validated-logpass","validated-logpass",-1789019307),(function (input_lp,_){
return re_frame.core.subscribe.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"login-password","login-password",1067260115)], null));
}),(function (login_data,query_v,_){
if(cljs.core._EQ_.call(null,login_data,true)){
return true;
} else {
return false;
}
}));
re_frame.core.reg_sub.call(null,new cljs.core.Keyword(null,"valid-data","valid-data",-1156260581),(function (db,_){
return new cljs.core.Keyword(null,"incorrect-data","incorrect-data",-839694326).cljs$core$IFn$_invoke$arity$1(db);
}));
re_frame.core.reg_sub.call(null,new cljs.core.Keyword(null,"todos","todos",630308868),(function (query_v,_){
return re_frame.core.subscribe.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"sorted-todos","sorted-todos",-1662564733)], null));
}),(function (sorted_todos,query_v,_){
return cljs.core.vals.call(null,sorted_todos);
}));
re_frame.core.reg_sub.call(null,new cljs.core.Keyword(null,"visible-todos","visible-todos",-694632253),(function (query_v,_){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_frame.core.subscribe.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"todos","todos",630308868)], null)),re_frame.core.subscribe.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"showing","showing",798009604)], null))], null);
}),(function (p__21968,_){
var vec__21969 = p__21968;
var todos = cljs.core.nth.call(null,vec__21969,(0),null);
var showing = cljs.core.nth.call(null,vec__21969,(1),null);
var filter_fn = (function (){var G__21972 = (((showing instanceof cljs.core.Keyword))?showing.fqn:null);
switch (G__21972) {
case "active":
return cljs.core.complement.call(null,new cljs.core.Keyword(null,"done","done",-889844188));

break;
case "done":
return new cljs.core.Keyword(null,"done","done",-889844188);

break;
case "all":
return cljs.core.identity;

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("No matching clause: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(showing)].join('')));

}
})();
return cljs.core.filter.call(null,filter_fn,todos);
}));
re_frame.core.reg_sub.call(null,new cljs.core.Keyword(null,"all-complete?","all-complete?",85738111),new cljs.core.Keyword(null,"<-","<-",760412998),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"todos","todos",630308868)], null),(function (todos,_){
return cljs.core.seq.call(null,todos);
}));
re_frame.core.reg_sub.call(null,new cljs.core.Keyword(null,"completed-count","completed-count",484261033),new cljs.core.Keyword(null,"<-","<-",760412998),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"todos","todos",630308868)], null),(function (todos,_){
return cljs.core.count.call(null,cljs.core.filter.call(null,new cljs.core.Keyword(null,"done","done",-889844188),todos));
}));
re_frame.core.reg_sub.call(null,new cljs.core.Keyword(null,"footer-counts","footer-counts",179032732),new cljs.core.Keyword(null,"<-","<-",760412998),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"todos","todos",630308868)], null),new cljs.core.Keyword(null,"<-","<-",760412998),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"completed-count","completed-count",484261033)], null),(function (p__21974,_){
var vec__21975 = p__21974;
var todos = cljs.core.nth.call(null,vec__21975,(0),null);
var completed = cljs.core.nth.call(null,vec__21975,(1),null);
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(cljs.core.count.call(null,todos) - completed),completed], null);
}));
