// Compiled by ClojureScript 1.9.521 {}
goog.provide('todomvc.events');
goog.require('cljs.core');
goog.require('todomvc.db');
goog.require('re_frame.core');
goog.require('cljs.spec');
/**
 * throw an exception if db doesn't match the spec
 */
todomvc.events.check_and_throw = (function todomvc$events$check_and_throw(a_spec,db){
if(cljs.core.truth_(cljs.spec.valid_QMARK_.call(null,a_spec,db))){
return null;
} else {
throw cljs.core.ex_info.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1("spec check failed: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.spec.explain_str.call(null,a_spec,db))].join(''),cljs.core.PersistentArrayMap.EMPTY);
}
});
todomvc.events.check_spec_interceptor = re_frame.core.after.call(null,cljs.core.partial.call(null,todomvc.events.check_and_throw,new cljs.core.Keyword("todomvc.db","db","todomvc.db/db",2057481519)));
todomvc.events.__GT_local_store = re_frame.core.after.call(null,todomvc.db.todos__GT_local_store);
todomvc.events.__GT_local_autorized = re_frame.core.after.call(null,todomvc.db.autorized__GT_local_store);
todomvc.events.interceptor_for_query = re_frame.core.__GT_interceptor.call(null,new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"interceptor-for-query","interceptor-for-query",2143781581),new cljs.core.Keyword(null,"before","before",-1633692388),(function (context){
return cljs.core.update_in.call(null,context,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"coeffects","coeffects",497912985),new cljs.core.Keyword(null,"event","event",301435442)], null),(function (p1__19478_SHARP_){
return cljs.core.into.call(null,cljs.core.PersistentArrayMap.EMPTY,p1__19478_SHARP_);
}));
}));
todomvc.events.todo_interceptors = new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [todomvc.events.check_spec_interceptor,re_frame.core.path.call(null,new cljs.core.Keyword(null,"todos","todos",630308868)),todomvc.events.__GT_local_store,(cljs.core.truth_(goog.DEBUG)?re_frame.core.debug:null),re_frame.core.trim_v], null);
todomvc.events.interceptor_validation_for_form = re_frame.core.__GT_interceptor.call(null,new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"interceptor-validation-for-form","interceptor-validation-for-form",-650222223),new cljs.core.Keyword(null,"before","before",-1633692388),(function (context,second){
cljs.core.println.call(null,"\u041F\u0435\u0440\u0432\u044B\u0439 \u043F\u0430\u0440\u0430\u043C\u0435\u0442\u0440 \u0432 error-login",context);

cljs.core.println.call(null,"\u0412\u0442\u043E\u0440\u043E\u0439 \u043F\u0430\u0440\u0430\u043C\u0435\u0442\u0440 \u0432 error-login",second);

return context;
}));
todomvc.events.validation_form = cljs.core.PersistentVector.EMPTY;
/**
 * Returns the next todo id.
 *   Assumes todos are sorted.
 *   Returns one more than the current largest id.
 */
todomvc.events.allocate_next_id = (function todomvc$events$allocate_next_id(todos){
return cljs.core.fnil.call(null,cljs.core.inc,(0)).call(null,cljs.core.last.call(null,cljs.core.keys.call(null,todos)));
});
todomvc.events.ui_dispatch = (function todomvc$events$ui_dispatch(js_event,event){
js_event.preventDefault();

return re_frame.core.dispatch.call(null,event);
});
re_frame.core.reg_event_fx.call(null,new cljs.core.Keyword(null,"initialise-db","initialise-db",-533872578),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_frame.core.inject_cofx.call(null,new cljs.core.Keyword(null,"local-store-todos","local-store-todos",-1573636976)),re_frame.core.inject_cofx.call(null,new cljs.core.Keyword(null,"local-store-authorized?","local-store-authorized?",1073145115)),todomvc.events.check_spec_interceptor], null),(function (p__19479,_){
var map__19480 = p__19479;
var map__19480__$1 = ((((!((map__19480 == null)))?((((map__19480.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__19480.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__19480):map__19480);
var db = cljs.core.get.call(null,map__19480__$1,new cljs.core.Keyword(null,"db","db",993250759));
var local_store_todos = cljs.core.get.call(null,map__19480__$1,new cljs.core.Keyword(null,"local-store-todos","local-store-todos",-1573636976));
var local_store_authorized_QMARK_ = cljs.core.get.call(null,map__19480__$1,new cljs.core.Keyword(null,"local-store-authorized?","local-store-authorized?",1073145115));
cljs.core.println.call(null,"\u042D\u0442\u043E \u0437\u0430\u043F\u0438\u0441\u044B\u0432\u0430\u0435\u0442\u0441\u044F \u0432 app-db \u043F\u0440\u0438 \u0441\u0442\u0430\u0440\u0442\u0435",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"db","db",993250759),cljs.core.assoc.call(null,todomvc.db.default_value,new cljs.core.Keyword(null,"todos","todos",630308868),local_store_todos,new cljs.core.Keyword(null,"passed?","passed?",1858749938),local_store_authorized_QMARK_)], null));

return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"db","db",993250759),cljs.core.assoc.call(null,todomvc.db.default_value,new cljs.core.Keyword(null,"todos","todos",630308868),local_store_todos,new cljs.core.Keyword(null,"passed?","passed?",1858749938),local_store_authorized_QMARK_)], null);
}));
re_frame.core.reg_event_db.call(null,new cljs.core.Keyword(null,"set-showing","set-showing",-429468401),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [todomvc.events.check_spec_interceptor,re_frame.core.path.call(null,new cljs.core.Keyword(null,"showing","showing",798009604)),re_frame.core.trim_v], null),(function (old_keyword,p__19482){
var vec__19483 = p__19482;
var new_filter_kw = cljs.core.nth.call(null,vec__19483,(0),null);
return new_filter_kw;
}));
re_frame.core.reg_event_db.call(null,new cljs.core.Keyword(null,"example-event","example-event",-1134001239),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_frame.core.trim_v,todomvc.events.interceptor_for_query,todomvc.events.__GT_local_autorized], null),(function (db,query_v){
cljs.core.println.call(null,"\u041F\u0435\u0440\u0432\u044B\u0439 \u043F\u0430\u0440\u0430\u043C\u0435\u0442\u0440 \u0432 \u0441\u043E\u0431\u044B\u0442\u0438\u0438 :example-event",db);

var login = cljs.core.reduce.call(null,(function (acc,el){
if((cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"log","log",-1595516004),cljs.core.key.call(null,el))) || (cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"pass","pass",1574159993),cljs.core.key.call(null,el)))){
return cljs.core.merge.call(null,acc,el);
} else {
return acc;
}
}),cljs.core.PersistentArrayMap.EMPTY,db);
if((cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"password","password",417022471).cljs$core$IFn$_invoke$arity$1(query_v),new cljs.core.Keyword(null,"pass","pass",1574159993).cljs$core$IFn$_invoke$arity$1(login))) && (cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"login","login",55217519).cljs$core$IFn$_invoke$arity$1(query_v),new cljs.core.Keyword(null,"log","log",-1595516004).cljs$core$IFn$_invoke$arity$1(login)))){
return cljs.core.assoc.call(null,db,new cljs.core.Keyword(null,"passed?","passed?",1858749938),"true",new cljs.core.Keyword(null,"incorrect-data","incorrect-data",-839694326),"false");
} else {
return cljs.core.assoc.call(null,db,new cljs.core.Keyword(null,"incorrect-data","incorrect-data",-839694326),"true");
}
}));
re_frame.core.reg_event_db.call(null,new cljs.core.Keyword(null,"update-re-frame-bootstrap-3","update-re-frame-bootstrap-3",469652828),cljs.core.PersistentVector.EMPTY,(function (db,p__19486){
var vec__19487 = p__19486;
var _ = cljs.core.nth.call(null,vec__19487,(0),null);
var keys = cljs.core.nth.call(null,vec__19487,(1),null);
var value = cljs.core.nth.call(null,vec__19487,(2),null);
var event = vec__19487;
cljs.core.println.call(null,"\u041F\u0435\u0440\u0432\u044B\u0439 \u0430\u0440\u0433\u0443\u043C\u0435\u043D\u0442 \u0432 \u0441\u043E\u0431\u044B\u0442\u0438\u0438 :update-re-frame-bootstrap-3",db);

cljs.core.println.call(null,"\u0412\u0442\u043E\u0440\u043E\u0439 \u0430\u0440\u0433\u0443\u043C\u0435\u043D\u0442 \u0432 \u0441\u043E\u0431\u044B\u0442\u0438\u0438 :update-re-frame-bootstrap-3",event);

return db;
}));
re_frame.core.reg_event_db.call(null,new cljs.core.Keyword(null,"add-validation-errors-to-form","add-validation-errors-to-form",686350594),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [todomvc.events.__GT_local_store], null),(function (db,p__19491){
var vec__19492 = p__19491;
var _ = cljs.core.nth.call(null,vec__19492,(0),null);
var target = cljs.core.nth.call(null,vec__19492,(1),null);
var what_is_it = vec__19492;
var error_field = cljs.core.keyword.call(null,new cljs.core.Keyword(null,"field","field",-1302436500).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"validation-error","validation-error",1040492454).cljs$core$IFn$_invoke$arity$1(db)));
var error_message = "Idiot ebaniy! Invalid input!";
return cljs.core.update_in.call(null,db,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"re-frame-bootstrap-3","re-frame-bootstrap-3",-954658978),new cljs.core.Keyword(null,"-errors","-errors",1378571364),new cljs.core.Keyword(null,"password","password",417022471)], null),((function (error_field,error_message,vec__19492,_,target,what_is_it){
return (function (p1__19490_SHARP_){
return cljs.core.cons.call(null,error_message,p1__19490_SHARP_);
});})(error_field,error_message,vec__19492,_,target,what_is_it))
);
}));
re_frame.core.reg_event_db.call(null,new cljs.core.Keyword(null,"exit-event","exit-event",160198835),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [todomvc.events.__GT_local_autorized], null),(function (db,query_from_view){
return cljs.core.assoc.call(null,db,new cljs.core.Keyword(null,"passed?","passed?",1858749938),false);
}));
re_frame.core.reg_event_db.call(null,new cljs.core.Keyword(null,"add-todo","add-todo",-1657891401),todomvc.events.todo_interceptors,(function (todos,p__19495){
var vec__19496 = p__19495;
var text = cljs.core.nth.call(null,vec__19496,(0),null);
var id = todomvc.events.allocate_next_id.call(null,todos);
return cljs.core.assoc.call(null,todos,id,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"title","title",636505583),text,new cljs.core.Keyword(null,"done","done",-889844188),false], null));
}));
re_frame.core.reg_event_db.call(null,new cljs.core.Keyword(null,"toggle-done","toggle-done",-77894994),todomvc.events.todo_interceptors,(function (todos,p__19499){
var vec__19500 = p__19499;
var id = cljs.core.nth.call(null,vec__19500,(0),null);
return cljs.core.update_in.call(null,todos,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [id,new cljs.core.Keyword(null,"done","done",-889844188)], null),cljs.core.not);
}));
re_frame.core.reg_event_db.call(null,new cljs.core.Keyword(null,"save","save",1850079149),todomvc.events.todo_interceptors,(function (todos,p__19503){
var vec__19504 = p__19503;
var id = cljs.core.nth.call(null,vec__19504,(0),null);
var title = cljs.core.nth.call(null,vec__19504,(1),null);
return cljs.core.assoc_in.call(null,todos,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [id,new cljs.core.Keyword(null,"title","title",636505583)], null),title);
}));
re_frame.core.reg_event_db.call(null,new cljs.core.Keyword(null,"delete-todo","delete-todo",-134034723),todomvc.events.todo_interceptors,(function (todos,p__19507){
var vec__19508 = p__19507;
var id = cljs.core.nth.call(null,vec__19508,(0),null);
cljs.core.println.call(null,"\u041F\u0435\u0440\u0432\u044B\u0439 \u043F\u0430\u0440\u0430\u043C\u0435\u0442\u0440 \u0432 \u0441\u043E\u0431\u044B\u0442\u0438\u0438 :delete-todo",todos);

return cljs.core.dissoc.call(null,todos,id);
}));
re_frame.core.reg_event_db.call(null,new cljs.core.Keyword(null,"clear-completed","clear-completed",1314054961),todomvc.events.todo_interceptors,(function (todos,_){
return cljs.core.reduce.call(null,cljs.core.dissoc,todos,cljs.core.map.call(null,new cljs.core.Keyword(null,"id","id",-1388402092),cljs.core.filter.call(null,new cljs.core.Keyword(null,"done","done",-889844188),cljs.core.vals.call(null,todos))));
}));
re_frame.core.reg_event_db.call(null,new cljs.core.Keyword(null,"complete-all-toggle","complete-all-toggle",1745771156),todomvc.events.todo_interceptors,(function (todos,_){
var new_done = cljs.core.not_every_QMARK_.call(null,new cljs.core.Keyword(null,"done","done",-889844188),cljs.core.vals.call(null,todos));
return cljs.core.reduce.call(null,((function (new_done){
return (function (p1__19511_SHARP_,p2__19512_SHARP_){
return cljs.core.assoc_in.call(null,p1__19511_SHARP_,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p2__19512_SHARP_,new cljs.core.Keyword(null,"done","done",-889844188)], null),new_done);
});})(new_done))
,todos,cljs.core.keys.call(null,todos));
}));
