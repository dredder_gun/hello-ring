// Compiled by ClojureScript 1.9.521 {}
goog.provide('todomvc.ajax_handlers');
goog.require('cljs.core');
todomvc.ajax_handlers.handler = (function todomvc$ajax_handlers$handler(response){
return console.log([cljs.core.str.cljs$core$IFn$_invoke$arity$1(response)].join(''));
});
todomvc.ajax_handlers.error_handler = (function todomvc$ajax_handlers$error_handler(p__22239){
var map__22242 = p__22239;
var map__22242__$1 = ((((!((map__22242 == null)))?((((map__22242.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__22242.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__22242):map__22242);
var status = cljs.core.get.call(null,map__22242__$1,new cljs.core.Keyword(null,"status","status",-1997798413));
var status_text = cljs.core.get.call(null,map__22242__$1,new cljs.core.Keyword(null,"status-text","status-text",-1834235478));
return console.log([cljs.core.str.cljs$core$IFn$_invoke$arity$1("something bad happened: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(status),cljs.core.str.cljs$core$IFn$_invoke$arity$1(" "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(status_text)].join(''));
});
