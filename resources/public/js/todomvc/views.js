// Compiled by ClojureScript 1.9.521 {}
goog.provide('todomvc.views');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('re_frame.core');
goog.require('re_com.modal_panel');
goog.require('re_com.core');
goog.require('free_form.re_frame');
goog.require('free_form.bootstrap_3');
goog.require('ajax.core');
goog.require('todomvc.frontend.view.modal');
goog.require('todomvc.frontend.view.notes');
todomvc.views.todo_input = (function todomvc$views$todo_input(p__17765){
var map__17769 = p__17765;
var map__17769__$1 = ((((!((map__17769 == null)))?((((map__17769.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__17769.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__17769):map__17769);
var title = cljs.core.get.call(null,map__17769__$1,new cljs.core.Keyword(null,"title","title",636505583));
var on_save = cljs.core.get.call(null,map__17769__$1,new cljs.core.Keyword(null,"on-save","on-save",1618176266));
var on_stop = cljs.core.get.call(null,map__17769__$1,new cljs.core.Keyword(null,"on-stop","on-stop",1520114515));
var val = reagent.core.atom.call(null,title);
var stop = ((function (val,map__17769,map__17769__$1,title,on_save,on_stop){
return (function (){
cljs.core.reset_BANG_.call(null,val,"");

if(cljs.core.truth_(on_stop)){
return on_stop.call(null);
} else {
return null;
}
});})(val,map__17769,map__17769__$1,title,on_save,on_stop))
;
var save = ((function (val,stop,map__17769,map__17769__$1,title,on_save,on_stop){
return (function (){
var v = clojure.string.trim.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,val))].join(''));
if(cljs.core.seq.call(null,v)){
on_save.call(null,v);
} else {
}

return stop.call(null);
});})(val,stop,map__17769,map__17769__$1,title,on_save,on_stop))
;
return ((function (val,stop,save,map__17769,map__17769__$1,title,on_save,on_stop){
return (function (props){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),cljs.core.merge.call(null,props,new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"type","type",1174270348),"text",new cljs.core.Keyword(null,"value","value",305978217),cljs.core.deref.call(null,val),new cljs.core.Keyword(null,"auto-focus","auto-focus",1250006231),true,new cljs.core.Keyword(null,"on-blur","on-blur",814300747),save,new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (val,stop,save,map__17769,map__17769__$1,title,on_save,on_stop){
return (function (p1__17763_SHARP_){
return cljs.core.reset_BANG_.call(null,val,p1__17763_SHARP_.target.value);
});})(val,stop,save,map__17769,map__17769__$1,title,on_save,on_stop))
,new cljs.core.Keyword(null,"on-key-down","on-key-down",-1374733765),((function (val,stop,save,map__17769,map__17769__$1,title,on_save,on_stop){
return (function (p1__17764_SHARP_){
var G__17771 = p1__17764_SHARP_.which;
switch (G__17771) {
case (13):
return save.call(null);

break;
case (27):
return stop.call(null);

break;
default:
return null;

}
});})(val,stop,save,map__17769,map__17769__$1,title,on_save,on_stop))
], null))], null);
});
;})(val,stop,save,map__17769,map__17769__$1,title,on_save,on_stop))
});
todomvc.views.todo_item = (function todomvc$views$todo_item(){
var editing = reagent.core.atom.call(null,false);
return ((function (editing){
return (function (p__17777){
var map__17778 = p__17777;
var map__17778__$1 = ((((!((map__17778 == null)))?((((map__17778.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__17778.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__17778):map__17778);
var id = cljs.core.get.call(null,map__17778__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var done = cljs.core.get.call(null,map__17778__$1,new cljs.core.Keyword(null,"done","done",-889844188));
var title = cljs.core.get.call(null,map__17778__$1,new cljs.core.Keyword(null,"title","title",636505583));
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"class","class",-2030961996),[cljs.core.str.cljs$core$IFn$_invoke$arity$1((cljs.core.truth_(done)?"completed ":null)),cljs.core.str.cljs$core$IFn$_invoke$arity$1((cljs.core.truth_(cljs.core.deref.call(null,editing))?"editing":null))].join('')], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.view","div.view",-1680900976),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input.toggle","input.toggle",-519545942),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),"checkbox",new cljs.core.Keyword(null,"checked","checked",-50955819),done,new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (map__17778,map__17778__$1,id,done,title,editing){
return (function (){
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"toggle-done","toggle-done",-77894994),id], null));
});})(map__17778,map__17778__$1,id,done,title,editing))
], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label","label",1718410804),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-double-click","on-double-click",1434856980),((function (map__17778,map__17778__$1,id,done,title,editing){
return (function (){
return cljs.core.reset_BANG_.call(null,editing,true);
});})(map__17778,map__17778__$1,id,done,title,editing))
], null),title], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.destroy","button.destroy",1044866871),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (map__17778,map__17778__$1,id,done,title,editing){
return (function (){
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"delete-todo","delete-todo",-134034723),id], null));
});})(map__17778,map__17778__$1,id,done,title,editing))
], null)], null)], null),(cljs.core.truth_(cljs.core.deref.call(null,editing))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [todomvc.views.todo_input,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"class","class",-2030961996),"edit",new cljs.core.Keyword(null,"title","title",636505583),title,new cljs.core.Keyword(null,"on-save","on-save",1618176266),((function (map__17778,map__17778__$1,id,done,title,editing){
return (function (p1__17773_SHARP_){
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"save","save",1850079149),id,p1__17773_SHARP_], null));
});})(map__17778,map__17778__$1,id,done,title,editing))
,new cljs.core.Keyword(null,"on-stop","on-stop",1520114515),((function (map__17778,map__17778__$1,id,done,title,editing){
return (function (){
return cljs.core.reset_BANG_.call(null,editing,false);
});})(map__17778,map__17778__$1,id,done,title,editing))
], null)], null):null)], null);
});
;})(editing))
});
todomvc.views.task_list = (function todomvc$views$task_list(){
var visible_todos = cljs.core.deref.call(null,re_frame.core.subscribe.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"visible-todos","visible-todos",-694632253)], null)));
var all_complete_QMARK_ = cljs.core.deref.call(null,re_frame.core.subscribe.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"all-complete?","all-complete?",85738111)], null)));
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"section#main","section#main",559170339),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input#toggle-all","input#toggle-all",-512330812),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),"checkbox",new cljs.core.Keyword(null,"checked","checked",-50955819),all_complete_QMARK_,new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (visible_todos,all_complete_QMARK_){
return (function (){
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"complete-all-toggle","complete-all-toggle",1745771156),cljs.core.not.call(null,all_complete_QMARK_)], null));
});})(visible_todos,all_complete_QMARK_))
], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label","label",1718410804),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"for","for",-1323786319),"toggle-all"], null),"\u041F\u043E\u043C\u0435\u0442\u0438\u0442\u044C \u0432\u0441\u0435"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ul#todo-list","ul#todo-list",-1648361723),(function (){var iter__8182__auto__ = ((function (visible_todos,all_complete_QMARK_){
return (function todomvc$views$task_list_$_iter__17784(s__17785){
return (new cljs.core.LazySeq(null,((function (visible_todos,all_complete_QMARK_){
return (function (){
var s__17785__$1 = s__17785;
while(true){
var temp__4657__auto__ = cljs.core.seq.call(null,s__17785__$1);
if(temp__4657__auto__){
var s__17785__$2 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__17785__$2)){
var c__8180__auto__ = cljs.core.chunk_first.call(null,s__17785__$2);
var size__8181__auto__ = cljs.core.count.call(null,c__8180__auto__);
var b__17787 = cljs.core.chunk_buffer.call(null,size__8181__auto__);
if((function (){var i__17786 = (0);
while(true){
if((i__17786 < size__8181__auto__)){
var todo = cljs.core._nth.call(null,c__8180__auto__,i__17786);
cljs.core.chunk_append.call(null,b__17787,cljs.core.with_meta(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [todomvc.views.todo_item,todo], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(todo)], null)));

var G__17788 = (i__17786 + (1));
i__17786 = G__17788;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__17787),todomvc$views$task_list_$_iter__17784.call(null,cljs.core.chunk_rest.call(null,s__17785__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__17787),null);
}
} else {
var todo = cljs.core.first.call(null,s__17785__$2);
return cljs.core.cons.call(null,cljs.core.with_meta(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [todomvc.views.todo_item,todo], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(todo)], null)),todomvc$views$task_list_$_iter__17784.call(null,cljs.core.rest.call(null,s__17785__$2)));
}
} else {
return null;
}
break;
}
});})(visible_todos,all_complete_QMARK_))
,null,null));
});})(visible_todos,all_complete_QMARK_))
;
return iter__8182__auto__.call(null,visible_todos);
})()], null)], null);
});
todomvc.views.footer_controls = (function todomvc$views$footer_controls(){
var vec__17793 = cljs.core.deref.call(null,re_frame.core.subscribe.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"footer-counts","footer-counts",179032732)], null)));
var active = cljs.core.nth.call(null,vec__17793,(0),null);
var done = cljs.core.nth.call(null,vec__17793,(1),null);
var showing = cljs.core.deref.call(null,re_frame.core.subscribe.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"showing","showing",798009604)], null)));
var a_fn = ((function (vec__17793,active,done,showing){
return (function (filter_kw,txt){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"class","class",-2030961996),((cljs.core._EQ_.call(null,filter_kw,showing))?"selected":null),new cljs.core.Keyword(null,"href","href",-793805698),[cljs.core.str.cljs$core$IFn$_invoke$arity$1("#/"),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.name.call(null,filter_kw))].join('')], null),txt], null);
});})(vec__17793,active,done,showing))
;
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"footer#footer","footer#footer",-1164052935),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span#todo-count","span#todo-count",-1116128108),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"strong","strong",269529000),active], null)," ",(function (){var G__17796 = active;
switch (G__17796) {
case (1):
return "item";

break;
default:
return "items";

}
})()," \u043E\u0441\u0442\u0430\u043B\u043E\u0441\u044C"], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ul#filters","ul#filters",-899831395),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),a_fn.call(null,new cljs.core.Keyword(null,"all","all",892129742),"\u0412\u0441\u0435")], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),a_fn.call(null,new cljs.core.Keyword(null,"active","active",1895962068),"\u0410\u043A\u0442.")], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),a_fn.call(null,new cljs.core.Keyword(null,"done","done",-889844188),"\u0412\u044B\u043F.")], null)], null),(((done > (0)))?new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button#clear-completed","button#clear-completed",-1698725142),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (vec__17793,active,done,showing,a_fn){
return (function (){
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"clear-completed","clear-completed",1314054961)], null));
});})(vec__17793,active,done,showing,a_fn))
], null),"\u0423\u0431\u0440\u0430\u0442\u044C \u0432\u044B\u043F\u043E\u043B\u043D\u0435\u043D\u043D\u044B\u0435"], null):null)], null);
});
todomvc.views.task_entry = (function todomvc$views$task_entry(){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"header#header","header#header",1650878621),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h1","h1",-1896887462),"\u0437\u0430\u043C\u0435\u0442\u043A\u0438"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [todomvc.views.todo_input,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"id","id",-1388402092),"new-todo",new cljs.core.Keyword(null,"placeholder","placeholder",-104873083),"\u0427\u0442\u043E \u043D\u0443\u0436\u043D\u043E \u0441\u0434\u0435\u043B\u0430\u0442\u044C?",new cljs.core.Keyword(null,"on-save","on-save",1618176266),(function (p1__17798_SHARP_){
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"add-todo","add-todo",-1657891401),p1__17798_SHARP_], null));
})], null)], null)], null);
});
todomvc.views.handler = (function todomvc$views$handler(p__17799){
var vec__17803 = p__17799;
var ok = cljs.core.nth.call(null,vec__17803,(0),null);
var response = cljs.core.nth.call(null,vec__17803,(1),null);
if(cljs.core.truth_(ok)){
return console.log([cljs.core.str.cljs$core$IFn$_invoke$arity$1(response)].join(''));
} else {
return console.error([cljs.core.str.cljs$core$IFn$_invoke$arity$1(response)].join(''));
}
});
/**
 * Create a button to test the modal component for modal dialogs
 */
todomvc.views.modal_dialog = (function todomvc$views$modal_dialog(){
var show_QMARK_ = reagent.core.atom.call(null,false);
var incorrect_data = re_frame.core.subscribe.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"valid-data","valid-data",-1156260581)], null));
var form_data = reagent.core.atom.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"login","login",55217519),"login",new cljs.core.Keyword(null,"password","password",417022471),"0000",new cljs.core.Keyword(null,"remember-me","remember-me",-1046194083),true], null));
var save_form_data = reagent.core.atom.call(null,null);
var process_ok = ((function (show_QMARK_,incorrect_data,form_data,save_form_data){
return (function (event){
cljs.core.prn.call(null,"process-ok");

cljs.core.reset_BANG_.call(null,save_form_data,cljs.core.deref.call(null,form_data));

re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"example-event","example-event",-1134001239),cljs.core.deref.call(null,save_form_data)], null));

if(cljs.core._EQ_.call(null,cljs.core.deref.call(null,incorrect_data),"true")){
cljs.core.reset_BANG_.call(null,show_QMARK_,false);
} else {
}

return false;
});})(show_QMARK_,incorrect_data,form_data,save_form_data))
;
var process_cancel = ((function (show_QMARK_,incorrect_data,form_data,save_form_data,process_ok){
return (function (event){
cljs.core.reset_BANG_.call(null,form_data,cljs.core.deref.call(null,save_form_data));

cljs.core.reset_BANG_.call(null,show_QMARK_,false);

return false;
});})(show_QMARK_,incorrect_data,form_data,save_form_data,process_ok))
;
return ((function (show_QMARK_,incorrect_data,form_data,save_form_data,process_ok,process_cancel){
return (function (){
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_com.core.h_box,new cljs.core.Keyword(null,"justify","justify",-722524056),new cljs.core.Keyword(null,"around","around",-265975553),new cljs.core.Keyword(null,"children","children",-940561982),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_com.core.button,new cljs.core.Keyword(null,"label","label",1718410804),"\u0412\u043E\u0439\u0442\u0438 \u0432 \u0441\u0438\u0441\u0442\u0435\u043C\u0443",new cljs.core.Keyword(null,"class","class",-2030961996),"btn-info",new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (show_QMARK_,incorrect_data,form_data,save_form_data,process_ok,process_cancel){
return (function (){
cljs.core.reset_BANG_.call(null,save_form_data,cljs.core.deref.call(null,form_data));

return cljs.core.reset_BANG_.call(null,show_QMARK_,"input");
});})(show_QMARK_,incorrect_data,form_data,save_form_data,process_ok,process_cancel))
], null),new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_com.core.button,new cljs.core.Keyword(null,"label","label",1718410804),"\u0417\u0430\u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0438\u0440\u043E\u0432\u0430\u0442\u044C\u0441\u044F",new cljs.core.Keyword(null,"class","class",-2030961996),"btn-success",new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (show_QMARK_,incorrect_data,form_data,save_form_data,process_ok,process_cancel){
return (function (){
cljs.core.reset_BANG_.call(null,save_form_data,cljs.core.deref.call(null,form_data));

return cljs.core.reset_BANG_.call(null,show_QMARK_,"reg");
});})(show_QMARK_,incorrect_data,form_data,save_form_data,process_ok,process_cancel))
], null),(function (){var G__17807 = cljs.core.deref.call(null,show_QMARK_);
switch (G__17807) {
case "input":
return new cljs.core.PersistentVector(null, 9, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_com.core.modal_panel,new cljs.core.Keyword(null,"backdrop-color","backdrop-color",1921200717),"grey",new cljs.core.Keyword(null,"backdrop-opacity","backdrop-opacity",1467395653),0.4,new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"font-family","font-family",-667419874),"Consolas"], null),new cljs.core.Keyword(null,"child","child",623967545),new cljs.core.PersistentVector(null, 8, 5, cljs.core.PersistentVector.EMPTY_NODE, [todomvc.frontend.view.modal.dialog_markup,form_data,process_ok,process_cancel,"\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u0441\u0432\u043E\u0438 \u0434\u0430\u043D\u043D\u044B\u0435","\u0412\u043E\u0439\u0442\u0438",new cljs.core.Keyword(null,"incorrect-data","incorrect-data",-839694326),incorrect_data], null)], null);

break;
case "reg":
return new cljs.core.PersistentVector(null, 9, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_com.core.modal_panel,new cljs.core.Keyword(null,"backdrop-color","backdrop-color",1921200717),"grey",new cljs.core.Keyword(null,"backdrop-opacity","backdrop-opacity",1467395653),0.4,new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"font-family","font-family",-667419874),"Consolas"], null),new cljs.core.Keyword(null,"child","child",623967545),new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [todomvc.frontend.view.modal.dialog_markup,form_data,process_ok,process_cancel,"\u0420\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F \u043F\u0440\u043E\u0444\u0438\u043B\u044F","\u0417\u0430\u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0438\u0440\u043E\u0432\u0430\u0442\u044C",new cljs.core.Keyword(null,"ajax-handler","ajax-handler",1607322897),((function (G__17807,show_QMARK_,incorrect_data,form_data,save_form_data,process_ok,process_cancel){
return (function (){
return ajax.core.ajax_request.call(null,new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"uri","uri",-774711847),"/send-message",new cljs.core.Keyword(null,"method","method",55703592),new cljs.core.Keyword(null,"post","post",269697687),new cljs.core.Keyword(null,"params","params",710516235),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"login","login",55217519),new cljs.core.Keyword(null,"login","login",55217519).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,form_data)),new cljs.core.Keyword(null,"password","password",417022471),new cljs.core.Keyword(null,"password","password",417022471).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,form_data))], null),new cljs.core.Keyword(null,"handler","handler",-195596612),todomvc.views.handler,new cljs.core.Keyword(null,"format","format",-1306924766),ajax.core.url_request_format.call(null),new cljs.core.Keyword(null,"response-format","response-format",1664465322),ajax.core.json_response_format.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"keywords?","keywords?",764949733),true], null))], null));
});})(G__17807,show_QMARK_,incorrect_data,form_data,save_form_data,process_ok,process_cancel))
,new cljs.core.Keyword(null,"incorrect-data","incorrect-data",-839694326),incorrect_data], null)], null);

break;
default:
return null;

}
})()], null)], null);
});
;})(show_QMARK_,incorrect_data,form_data,save_form_data,process_ok,process_cancel))
});
todomvc.views.todo_app = (function todomvc$views$todo_app(){
var aut_QMARK_ = cljs.core.deref.call(null,re_frame.core.subscribe.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"login-password","login-password",1067260115)], null)));
var data = re_frame.core.subscribe.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"re-frame-bootstrap-3","re-frame-bootstrap-3",-954658978)], null));
if(cljs.core._EQ_.call(null,aut_QMARK_,"true")){
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [re_com.core.button,new cljs.core.Keyword(null,"label","label",1718410804),"\u0412\u044B\u0439\u0442\u0438",new cljs.core.Keyword(null,"class","class",-2030961996),"btn-primary",new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (aut_QMARK_,data){
return (function (){
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"exit-event","exit-event",160198835)], null));
});})(aut_QMARK_,data))
], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [todomvc.frontend.view.notes.list_todos], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"section#todoapp","section#todoapp",41606040),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [todomvc.views.task_entry], null),((cljs.core.seq.call(null,cljs.core.deref.call(null,re_frame.core.subscribe.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"todos","todos",630308868)], null)))))?new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [todomvc.views.task_list], null):null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [todomvc.views.footer_controls], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"footer#info","footer#info",1634811413),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),"\u0414\u0432\u043E\u0439\u043D\u043E\u0439 \u043A\u043B\u0438\u043A \u0434\u043B\u044F \u0440\u0435\u0434\u0430\u043A\u0442\u0438\u0440\u043E\u0432\u0430\u043D\u0438\u044F \u0437\u0430\u043C\u0435\u0442\u043A\u0438"], null)], null)], null);
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h1#enter-header","h1#enter-header",2107433050),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"text-align","text-align",1786091845),"center"], null)], null),"\u0437\u0430\u043C\u043522\u0442\u043A\u0438"], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [todomvc.views.modal_dialog], null)], null);
}
});
