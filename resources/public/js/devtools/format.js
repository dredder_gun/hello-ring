// Compiled by ClojureScript 1.9.521 {}
goog.provide('devtools.format');
goog.require('cljs.core');

/**
 * @interface
 */
devtools.format.IDevtoolsFormat = function(){};

devtools.format._header = (function devtools$format$_header(value){
if((!((value == null))) && (!((value.devtools$format$IDevtoolsFormat$_header$arity$1 == null)))){
return value.devtools$format$IDevtoolsFormat$_header$arity$1(value);
} else {
var x__7928__auto__ = (((value == null))?null:value);
var m__7929__auto__ = (devtools.format._header[goog.typeOf(x__7928__auto__)]);
if(!((m__7929__auto__ == null))){
return m__7929__auto__.call(null,value);
} else {
var m__7929__auto____$1 = (devtools.format._header["_"]);
if(!((m__7929__auto____$1 == null))){
return m__7929__auto____$1.call(null,value);
} else {
throw cljs.core.missing_protocol.call(null,"IDevtoolsFormat.-header",value);
}
}
}
});

devtools.format._has_body = (function devtools$format$_has_body(value){
if((!((value == null))) && (!((value.devtools$format$IDevtoolsFormat$_has_body$arity$1 == null)))){
return value.devtools$format$IDevtoolsFormat$_has_body$arity$1(value);
} else {
var x__7928__auto__ = (((value == null))?null:value);
var m__7929__auto__ = (devtools.format._has_body[goog.typeOf(x__7928__auto__)]);
if(!((m__7929__auto__ == null))){
return m__7929__auto__.call(null,value);
} else {
var m__7929__auto____$1 = (devtools.format._has_body["_"]);
if(!((m__7929__auto____$1 == null))){
return m__7929__auto____$1.call(null,value);
} else {
throw cljs.core.missing_protocol.call(null,"IDevtoolsFormat.-has-body",value);
}
}
}
});

devtools.format._body = (function devtools$format$_body(value){
if((!((value == null))) && (!((value.devtools$format$IDevtoolsFormat$_body$arity$1 == null)))){
return value.devtools$format$IDevtoolsFormat$_body$arity$1(value);
} else {
var x__7928__auto__ = (((value == null))?null:value);
var m__7929__auto__ = (devtools.format._body[goog.typeOf(x__7928__auto__)]);
if(!((m__7929__auto__ == null))){
return m__7929__auto__.call(null,value);
} else {
var m__7929__auto____$1 = (devtools.format._body["_"]);
if(!((m__7929__auto____$1 == null))){
return m__7929__auto____$1.call(null,value);
} else {
throw cljs.core.missing_protocol.call(null,"IDevtoolsFormat.-body",value);
}
}
}
});

devtools.format.setup_BANG_ = (function devtools$format$setup_BANG_(){
if(cljs.core.truth_(devtools.format._STAR_setup_done_STAR_)){
return null;
} else {
devtools.format._STAR_setup_done_STAR_ = true;

devtools.format.make_template_fn = (function (){var temp__4657__auto__ = goog.object.get(window,"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__14666__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__14666__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__14666__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__14666__auto____$1,"templating");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__14665__auto__ = temp__4657__auto____$2;
return goog.object.get(o__14665__auto__,"make_template");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

devtools.format.make_group_fn = (function (){var temp__4657__auto__ = goog.object.get(window,"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__14666__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__14666__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__14666__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__14666__auto____$1,"templating");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__14665__auto__ = temp__4657__auto____$2;
return goog.object.get(o__14665__auto__,"make_group");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

devtools.format.make_reference_fn = (function (){var temp__4657__auto__ = goog.object.get(window,"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__14666__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__14666__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__14666__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__14666__auto____$1,"templating");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__14665__auto__ = temp__4657__auto____$2;
return goog.object.get(o__14665__auto__,"make_reference");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

devtools.format.make_surrogate_fn = (function (){var temp__4657__auto__ = goog.object.get(window,"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__14666__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__14666__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__14666__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__14666__auto____$1,"templating");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__14665__auto__ = temp__4657__auto____$2;
return goog.object.get(o__14665__auto__,"make_surrogate");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

devtools.format.render_markup_fn = (function (){var temp__4657__auto__ = goog.object.get(window,"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__14666__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__14666__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__14666__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__14666__auto____$1,"templating");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__14665__auto__ = temp__4657__auto____$2;
return goog.object.get(o__14665__auto__,"render_markup");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

devtools.format._LT_header_GT__fn = (function (){var temp__4657__auto__ = goog.object.get(window,"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__14666__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__14666__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__14666__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__14666__auto____$1,"markup");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__14665__auto__ = temp__4657__auto____$2;
return goog.object.get(o__14665__auto__,"_LT_header_GT_");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

devtools.format._LT_standard_body_GT__fn = (function (){var temp__4657__auto__ = goog.object.get(window,"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__14666__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__14666__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__14666__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__14666__auto____$1,"markup");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__14665__auto__ = temp__4657__auto____$2;
return goog.object.get(o__14665__auto__,"_LT_standard_body_GT_");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

if(cljs.core.truth_(devtools.format.make_template_fn)){
} else {
throw (new Error("Assert failed: make-template-fn"));
}

if(cljs.core.truth_(devtools.format.make_group_fn)){
} else {
throw (new Error("Assert failed: make-group-fn"));
}

if(cljs.core.truth_(devtools.format.make_reference_fn)){
} else {
throw (new Error("Assert failed: make-reference-fn"));
}

if(cljs.core.truth_(devtools.format.make_surrogate_fn)){
} else {
throw (new Error("Assert failed: make-surrogate-fn"));
}

if(cljs.core.truth_(devtools.format.render_markup_fn)){
} else {
throw (new Error("Assert failed: render-markup-fn"));
}

if(cljs.core.truth_(devtools.format._LT_header_GT__fn)){
} else {
throw (new Error("Assert failed: <header>-fn"));
}

if(cljs.core.truth_(devtools.format._LT_standard_body_GT__fn)){
return null;
} else {
throw (new Error("Assert failed: <standard-body>-fn"));
}
}
});
devtools.format.render_markup = (function devtools$format$render_markup(var_args){
var args__8380__auto__ = [];
var len__8373__auto___14687 = arguments.length;
var i__8374__auto___14688 = (0);
while(true){
if((i__8374__auto___14688 < len__8373__auto___14687)){
args__8380__auto__.push((arguments[i__8374__auto___14688]));

var G__14689 = (i__8374__auto___14688 + (1));
i__8374__auto___14688 = G__14689;
continue;
} else {
}
break;
}

var argseq__8381__auto__ = ((((0) < args__8380__auto__.length))?(new cljs.core.IndexedSeq(args__8380__auto__.slice((0)),(0),null)):null);
return devtools.format.render_markup.cljs$core$IFn$_invoke$arity$variadic(argseq__8381__auto__);
});

devtools.format.render_markup.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.render_markup_fn,args);
});

devtools.format.render_markup.cljs$lang$maxFixedArity = (0);

devtools.format.render_markup.cljs$lang$applyTo = (function (seq14686){
return devtools.format.render_markup.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq14686));
});

devtools.format.make_template = (function devtools$format$make_template(var_args){
var args__8380__auto__ = [];
var len__8373__auto___14691 = arguments.length;
var i__8374__auto___14692 = (0);
while(true){
if((i__8374__auto___14692 < len__8373__auto___14691)){
args__8380__auto__.push((arguments[i__8374__auto___14692]));

var G__14693 = (i__8374__auto___14692 + (1));
i__8374__auto___14692 = G__14693;
continue;
} else {
}
break;
}

var argseq__8381__auto__ = ((((0) < args__8380__auto__.length))?(new cljs.core.IndexedSeq(args__8380__auto__.slice((0)),(0),null)):null);
return devtools.format.make_template.cljs$core$IFn$_invoke$arity$variadic(argseq__8381__auto__);
});

devtools.format.make_template.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_template_fn,args);
});

devtools.format.make_template.cljs$lang$maxFixedArity = (0);

devtools.format.make_template.cljs$lang$applyTo = (function (seq14690){
return devtools.format.make_template.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq14690));
});

devtools.format.make_group = (function devtools$format$make_group(var_args){
var args__8380__auto__ = [];
var len__8373__auto___14695 = arguments.length;
var i__8374__auto___14696 = (0);
while(true){
if((i__8374__auto___14696 < len__8373__auto___14695)){
args__8380__auto__.push((arguments[i__8374__auto___14696]));

var G__14697 = (i__8374__auto___14696 + (1));
i__8374__auto___14696 = G__14697;
continue;
} else {
}
break;
}

var argseq__8381__auto__ = ((((0) < args__8380__auto__.length))?(new cljs.core.IndexedSeq(args__8380__auto__.slice((0)),(0),null)):null);
return devtools.format.make_group.cljs$core$IFn$_invoke$arity$variadic(argseq__8381__auto__);
});

devtools.format.make_group.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_group_fn,args);
});

devtools.format.make_group.cljs$lang$maxFixedArity = (0);

devtools.format.make_group.cljs$lang$applyTo = (function (seq14694){
return devtools.format.make_group.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq14694));
});

devtools.format.make_surrogate = (function devtools$format$make_surrogate(var_args){
var args__8380__auto__ = [];
var len__8373__auto___14699 = arguments.length;
var i__8374__auto___14700 = (0);
while(true){
if((i__8374__auto___14700 < len__8373__auto___14699)){
args__8380__auto__.push((arguments[i__8374__auto___14700]));

var G__14701 = (i__8374__auto___14700 + (1));
i__8374__auto___14700 = G__14701;
continue;
} else {
}
break;
}

var argseq__8381__auto__ = ((((0) < args__8380__auto__.length))?(new cljs.core.IndexedSeq(args__8380__auto__.slice((0)),(0),null)):null);
return devtools.format.make_surrogate.cljs$core$IFn$_invoke$arity$variadic(argseq__8381__auto__);
});

devtools.format.make_surrogate.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_surrogate_fn,args);
});

devtools.format.make_surrogate.cljs$lang$maxFixedArity = (0);

devtools.format.make_surrogate.cljs$lang$applyTo = (function (seq14698){
return devtools.format.make_surrogate.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq14698));
});

devtools.format.template = (function devtools$format$template(var_args){
var args__8380__auto__ = [];
var len__8373__auto___14703 = arguments.length;
var i__8374__auto___14704 = (0);
while(true){
if((i__8374__auto___14704 < len__8373__auto___14703)){
args__8380__auto__.push((arguments[i__8374__auto___14704]));

var G__14705 = (i__8374__auto___14704 + (1));
i__8374__auto___14704 = G__14705;
continue;
} else {
}
break;
}

var argseq__8381__auto__ = ((((0) < args__8380__auto__.length))?(new cljs.core.IndexedSeq(args__8380__auto__.slice((0)),(0),null)):null);
return devtools.format.template.cljs$core$IFn$_invoke$arity$variadic(argseq__8381__auto__);
});

devtools.format.template.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_template_fn,args);
});

devtools.format.template.cljs$lang$maxFixedArity = (0);

devtools.format.template.cljs$lang$applyTo = (function (seq14702){
return devtools.format.template.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq14702));
});

devtools.format.group = (function devtools$format$group(var_args){
var args__8380__auto__ = [];
var len__8373__auto___14707 = arguments.length;
var i__8374__auto___14708 = (0);
while(true){
if((i__8374__auto___14708 < len__8373__auto___14707)){
args__8380__auto__.push((arguments[i__8374__auto___14708]));

var G__14709 = (i__8374__auto___14708 + (1));
i__8374__auto___14708 = G__14709;
continue;
} else {
}
break;
}

var argseq__8381__auto__ = ((((0) < args__8380__auto__.length))?(new cljs.core.IndexedSeq(args__8380__auto__.slice((0)),(0),null)):null);
return devtools.format.group.cljs$core$IFn$_invoke$arity$variadic(argseq__8381__auto__);
});

devtools.format.group.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_group_fn,args);
});

devtools.format.group.cljs$lang$maxFixedArity = (0);

devtools.format.group.cljs$lang$applyTo = (function (seq14706){
return devtools.format.group.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq14706));
});

devtools.format.surrogate = (function devtools$format$surrogate(var_args){
var args__8380__auto__ = [];
var len__8373__auto___14711 = arguments.length;
var i__8374__auto___14712 = (0);
while(true){
if((i__8374__auto___14712 < len__8373__auto___14711)){
args__8380__auto__.push((arguments[i__8374__auto___14712]));

var G__14713 = (i__8374__auto___14712 + (1));
i__8374__auto___14712 = G__14713;
continue;
} else {
}
break;
}

var argseq__8381__auto__ = ((((0) < args__8380__auto__.length))?(new cljs.core.IndexedSeq(args__8380__auto__.slice((0)),(0),null)):null);
return devtools.format.surrogate.cljs$core$IFn$_invoke$arity$variadic(argseq__8381__auto__);
});

devtools.format.surrogate.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_surrogate_fn,args);
});

devtools.format.surrogate.cljs$lang$maxFixedArity = (0);

devtools.format.surrogate.cljs$lang$applyTo = (function (seq14710){
return devtools.format.surrogate.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq14710));
});

devtools.format.reference = (function devtools$format$reference(var_args){
var args__8380__auto__ = [];
var len__8373__auto___14721 = arguments.length;
var i__8374__auto___14722 = (0);
while(true){
if((i__8374__auto___14722 < len__8373__auto___14721)){
args__8380__auto__.push((arguments[i__8374__auto___14722]));

var G__14723 = (i__8374__auto___14722 + (1));
i__8374__auto___14722 = G__14723;
continue;
} else {
}
break;
}

var argseq__8381__auto__ = ((((1) < args__8380__auto__.length))?(new cljs.core.IndexedSeq(args__8380__auto__.slice((1)),(0),null)):null);
return devtools.format.reference.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__8381__auto__);
});

devtools.format.reference.cljs$core$IFn$_invoke$arity$variadic = (function (object,p__14717){
var vec__14718 = p__14717;
var state_override = cljs.core.nth.call(null,vec__14718,(0),null);
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_reference_fn,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [object,((function (vec__14718,state_override){
return (function (p1__14714_SHARP_){
return cljs.core.merge.call(null,p1__14714_SHARP_,state_override);
});})(vec__14718,state_override))
], null));
});

devtools.format.reference.cljs$lang$maxFixedArity = (1);

devtools.format.reference.cljs$lang$applyTo = (function (seq14715){
var G__14716 = cljs.core.first.call(null,seq14715);
var seq14715__$1 = cljs.core.next.call(null,seq14715);
return devtools.format.reference.cljs$core$IFn$_invoke$arity$variadic(G__14716,seq14715__$1);
});

devtools.format.standard_reference = (function devtools$format$standard_reference(target){
devtools.format.setup_BANG_.call(null);

return devtools.format.make_template_fn.call(null,new cljs.core.Keyword(null,"ol","ol",932524051),new cljs.core.Keyword(null,"standard-ol-style","standard-ol-style",2143825615),devtools.format.make_template_fn.call(null,new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.Keyword(null,"standard-li-style","standard-li-style",413442955),devtools.format.make_reference_fn.call(null,target)));
});
devtools.format.build_header = (function devtools$format$build_header(var_args){
var args__8380__auto__ = [];
var len__8373__auto___14725 = arguments.length;
var i__8374__auto___14726 = (0);
while(true){
if((i__8374__auto___14726 < len__8373__auto___14725)){
args__8380__auto__.push((arguments[i__8374__auto___14726]));

var G__14727 = (i__8374__auto___14726 + (1));
i__8374__auto___14726 = G__14727;
continue;
} else {
}
break;
}

var argseq__8381__auto__ = ((((0) < args__8380__auto__.length))?(new cljs.core.IndexedSeq(args__8380__auto__.slice((0)),(0),null)):null);
return devtools.format.build_header.cljs$core$IFn$_invoke$arity$variadic(argseq__8381__auto__);
});

devtools.format.build_header.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return devtools.format.render_markup.call(null,cljs.core.apply.call(null,devtools.format._LT_header_GT__fn,args));
});

devtools.format.build_header.cljs$lang$maxFixedArity = (0);

devtools.format.build_header.cljs$lang$applyTo = (function (seq14724){
return devtools.format.build_header.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq14724));
});

devtools.format.standard_body_template = (function devtools$format$standard_body_template(var_args){
var args__8380__auto__ = [];
var len__8373__auto___14730 = arguments.length;
var i__8374__auto___14731 = (0);
while(true){
if((i__8374__auto___14731 < len__8373__auto___14730)){
args__8380__auto__.push((arguments[i__8374__auto___14731]));

var G__14732 = (i__8374__auto___14731 + (1));
i__8374__auto___14731 = G__14732;
continue;
} else {
}
break;
}

var argseq__8381__auto__ = ((((1) < args__8380__auto__.length))?(new cljs.core.IndexedSeq(args__8380__auto__.slice((1)),(0),null)):null);
return devtools.format.standard_body_template.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__8381__auto__);
});

devtools.format.standard_body_template.cljs$core$IFn$_invoke$arity$variadic = (function (lines,rest){
devtools.format.setup_BANG_.call(null);

var args = cljs.core.concat.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.map.call(null,(function (x){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [x], null);
}),lines)], null),rest);
return devtools.format.render_markup.call(null,cljs.core.apply.call(null,devtools.format._LT_standard_body_GT__fn,args));
});

devtools.format.standard_body_template.cljs$lang$maxFixedArity = (1);

devtools.format.standard_body_template.cljs$lang$applyTo = (function (seq14728){
var G__14729 = cljs.core.first.call(null,seq14728);
var seq14728__$1 = cljs.core.next.call(null,seq14728);
return devtools.format.standard_body_template.cljs$core$IFn$_invoke$arity$variadic(G__14729,seq14728__$1);
});

