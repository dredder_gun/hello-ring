// Compiled by ClojureScript 1.9.521 {}
goog.provide('devtools.formatters.templating');
goog.require('cljs.core');
goog.require('cljs.pprint');
goog.require('clojure.walk');
goog.require('devtools.util');
goog.require('devtools.protocols');
goog.require('devtools.formatters.helpers');
goog.require('devtools.formatters.state');
goog.require('clojure.string');
devtools.formatters.templating.mark_as_group_BANG_ = (function devtools$formatters$templating$mark_as_group_BANG_(value){
var x17124_17125 = value;
x17124_17125.devtools$protocols$IGroup$ = cljs.core.PROTOCOL_SENTINEL;


return value;
});
devtools.formatters.templating.group_QMARK_ = (function devtools$formatters$templating$group_QMARK_(value){
if(!((value == null))){
if((false) || ((cljs.core.PROTOCOL_SENTINEL === value.devtools$protocols$IGroup$))){
return true;
} else {
if((!value.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_.call(null,devtools.protocols.IGroup,value);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_.call(null,devtools.protocols.IGroup,value);
}
});
devtools.formatters.templating.mark_as_template_BANG_ = (function devtools$formatters$templating$mark_as_template_BANG_(value){
var x17129_17130 = value;
x17129_17130.devtools$protocols$ITemplate$ = cljs.core.PROTOCOL_SENTINEL;


return value;
});
devtools.formatters.templating.template_QMARK_ = (function devtools$formatters$templating$template_QMARK_(value){
if(!((value == null))){
if((false) || ((cljs.core.PROTOCOL_SENTINEL === value.devtools$protocols$ITemplate$))){
return true;
} else {
if((!value.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_.call(null,devtools.protocols.ITemplate,value);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_.call(null,devtools.protocols.ITemplate,value);
}
});
devtools.formatters.templating.mark_as_surrogate_BANG_ = (function devtools$formatters$templating$mark_as_surrogate_BANG_(value){
var x17134_17135 = value;
x17134_17135.devtools$protocols$ISurrogate$ = cljs.core.PROTOCOL_SENTINEL;


return value;
});
devtools.formatters.templating.surrogate_QMARK_ = (function devtools$formatters$templating$surrogate_QMARK_(value){
if(!((value == null))){
if((false) || ((cljs.core.PROTOCOL_SENTINEL === value.devtools$protocols$ISurrogate$))){
return true;
} else {
if((!value.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_.call(null,devtools.protocols.ISurrogate,value);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_.call(null,devtools.protocols.ISurrogate,value);
}
});
devtools.formatters.templating.reference_QMARK_ = (function devtools$formatters$templating$reference_QMARK_(value){
var and__7248__auto__ = devtools.formatters.templating.group_QMARK_.call(null,value);
if(cljs.core.truth_(and__7248__auto__)){
return cljs.core._EQ_.call(null,(value[(0)]),"object");
} else {
return and__7248__auto__;
}
});
devtools.formatters.templating.make_group = (function devtools$formatters$templating$make_group(var_args){
var args__8380__auto__ = [];
var len__8373__auto___17143 = arguments.length;
var i__8374__auto___17144 = (0);
while(true){
if((i__8374__auto___17144 < len__8373__auto___17143)){
args__8380__auto__.push((arguments[i__8374__auto___17144]));

var G__17145 = (i__8374__auto___17144 + (1));
i__8374__auto___17144 = G__17145;
continue;
} else {
}
break;
}

var argseq__8381__auto__ = ((((0) < args__8380__auto__.length))?(new cljs.core.IndexedSeq(args__8380__auto__.slice((0)),(0),null)):null);
return devtools.formatters.templating.make_group.cljs$core$IFn$_invoke$arity$variadic(argseq__8381__auto__);
});

devtools.formatters.templating.make_group.cljs$core$IFn$_invoke$arity$variadic = (function (items){
var group = devtools.formatters.templating.mark_as_group_BANG_.call(null,[]);
var seq__17139_17146 = cljs.core.seq.call(null,items);
var chunk__17140_17147 = null;
var count__17141_17148 = (0);
var i__17142_17149 = (0);
while(true){
if((i__17142_17149 < count__17141_17148)){
var item_17150 = cljs.core._nth.call(null,chunk__17140_17147,i__17142_17149);
if(!((item_17150 == null))){
if(cljs.core.coll_QMARK_.call(null,item_17150)){
(group["push"]).apply(group,devtools.formatters.templating.mark_as_group_BANG_.call(null,cljs.core.into_array.call(null,item_17150)));
} else {
group.push(devtools.formatters.helpers.pref.call(null,item_17150));
}
} else {
}

var G__17151 = seq__17139_17146;
var G__17152 = chunk__17140_17147;
var G__17153 = count__17141_17148;
var G__17154 = (i__17142_17149 + (1));
seq__17139_17146 = G__17151;
chunk__17140_17147 = G__17152;
count__17141_17148 = G__17153;
i__17142_17149 = G__17154;
continue;
} else {
var temp__4657__auto___17155 = cljs.core.seq.call(null,seq__17139_17146);
if(temp__4657__auto___17155){
var seq__17139_17156__$1 = temp__4657__auto___17155;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__17139_17156__$1)){
var c__8079__auto___17157 = cljs.core.chunk_first.call(null,seq__17139_17156__$1);
var G__17158 = cljs.core.chunk_rest.call(null,seq__17139_17156__$1);
var G__17159 = c__8079__auto___17157;
var G__17160 = cljs.core.count.call(null,c__8079__auto___17157);
var G__17161 = (0);
seq__17139_17146 = G__17158;
chunk__17140_17147 = G__17159;
count__17141_17148 = G__17160;
i__17142_17149 = G__17161;
continue;
} else {
var item_17162 = cljs.core.first.call(null,seq__17139_17156__$1);
if(!((item_17162 == null))){
if(cljs.core.coll_QMARK_.call(null,item_17162)){
(group["push"]).apply(group,devtools.formatters.templating.mark_as_group_BANG_.call(null,cljs.core.into_array.call(null,item_17162)));
} else {
group.push(devtools.formatters.helpers.pref.call(null,item_17162));
}
} else {
}

var G__17163 = cljs.core.next.call(null,seq__17139_17156__$1);
var G__17164 = null;
var G__17165 = (0);
var G__17166 = (0);
seq__17139_17146 = G__17163;
chunk__17140_17147 = G__17164;
count__17141_17148 = G__17165;
i__17142_17149 = G__17166;
continue;
}
} else {
}
}
break;
}

return group;
});

devtools.formatters.templating.make_group.cljs$lang$maxFixedArity = (0);

devtools.formatters.templating.make_group.cljs$lang$applyTo = (function (seq17138){
return devtools.formatters.templating.make_group.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq17138));
});

devtools.formatters.templating.make_template = (function devtools$formatters$templating$make_template(var_args){
var args__8380__auto__ = [];
var len__8373__auto___17174 = arguments.length;
var i__8374__auto___17175 = (0);
while(true){
if((i__8374__auto___17175 < len__8373__auto___17174)){
args__8380__auto__.push((arguments[i__8374__auto___17175]));

var G__17176 = (i__8374__auto___17175 + (1));
i__8374__auto___17175 = G__17176;
continue;
} else {
}
break;
}

var argseq__8381__auto__ = ((((2) < args__8380__auto__.length))?(new cljs.core.IndexedSeq(args__8380__auto__.slice((2)),(0),null)):null);
return devtools.formatters.templating.make_template.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__8381__auto__);
});

devtools.formatters.templating.make_template.cljs$core$IFn$_invoke$arity$variadic = (function (tag,style,children){
var tag__$1 = devtools.formatters.helpers.pref.call(null,tag);
var style__$1 = devtools.formatters.helpers.pref.call(null,style);
var template = devtools.formatters.templating.mark_as_template_BANG_.call(null,[tag__$1,((cljs.core.empty_QMARK_.call(null,style__$1))?({}):({"style": style__$1}))]);
var seq__17170_17177 = cljs.core.seq.call(null,children);
var chunk__17171_17178 = null;
var count__17172_17179 = (0);
var i__17173_17180 = (0);
while(true){
if((i__17173_17180 < count__17172_17179)){
var child_17181 = cljs.core._nth.call(null,chunk__17171_17178,i__17173_17180);
if(!((child_17181 == null))){
if(cljs.core.coll_QMARK_.call(null,child_17181)){
(template["push"]).apply(template,devtools.formatters.templating.mark_as_template_BANG_.call(null,cljs.core.into_array.call(null,cljs.core.keep.call(null,devtools.formatters.helpers.pref,child_17181))));
} else {
var temp__4655__auto___17182 = devtools.formatters.helpers.pref.call(null,child_17181);
if(cljs.core.truth_(temp__4655__auto___17182)){
var child_value_17183 = temp__4655__auto___17182;
template.push(child_value_17183);
} else {
}
}
} else {
}

var G__17184 = seq__17170_17177;
var G__17185 = chunk__17171_17178;
var G__17186 = count__17172_17179;
var G__17187 = (i__17173_17180 + (1));
seq__17170_17177 = G__17184;
chunk__17171_17178 = G__17185;
count__17172_17179 = G__17186;
i__17173_17180 = G__17187;
continue;
} else {
var temp__4657__auto___17188 = cljs.core.seq.call(null,seq__17170_17177);
if(temp__4657__auto___17188){
var seq__17170_17189__$1 = temp__4657__auto___17188;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__17170_17189__$1)){
var c__8079__auto___17190 = cljs.core.chunk_first.call(null,seq__17170_17189__$1);
var G__17191 = cljs.core.chunk_rest.call(null,seq__17170_17189__$1);
var G__17192 = c__8079__auto___17190;
var G__17193 = cljs.core.count.call(null,c__8079__auto___17190);
var G__17194 = (0);
seq__17170_17177 = G__17191;
chunk__17171_17178 = G__17192;
count__17172_17179 = G__17193;
i__17173_17180 = G__17194;
continue;
} else {
var child_17195 = cljs.core.first.call(null,seq__17170_17189__$1);
if(!((child_17195 == null))){
if(cljs.core.coll_QMARK_.call(null,child_17195)){
(template["push"]).apply(template,devtools.formatters.templating.mark_as_template_BANG_.call(null,cljs.core.into_array.call(null,cljs.core.keep.call(null,devtools.formatters.helpers.pref,child_17195))));
} else {
var temp__4655__auto___17196 = devtools.formatters.helpers.pref.call(null,child_17195);
if(cljs.core.truth_(temp__4655__auto___17196)){
var child_value_17197 = temp__4655__auto___17196;
template.push(child_value_17197);
} else {
}
}
} else {
}

var G__17198 = cljs.core.next.call(null,seq__17170_17189__$1);
var G__17199 = null;
var G__17200 = (0);
var G__17201 = (0);
seq__17170_17177 = G__17198;
chunk__17171_17178 = G__17199;
count__17172_17179 = G__17200;
i__17173_17180 = G__17201;
continue;
}
} else {
}
}
break;
}

return template;
});

devtools.formatters.templating.make_template.cljs$lang$maxFixedArity = (2);

devtools.formatters.templating.make_template.cljs$lang$applyTo = (function (seq17167){
var G__17168 = cljs.core.first.call(null,seq17167);
var seq17167__$1 = cljs.core.next.call(null,seq17167);
var G__17169 = cljs.core.first.call(null,seq17167__$1);
var seq17167__$2 = cljs.core.next.call(null,seq17167__$1);
return devtools.formatters.templating.make_template.cljs$core$IFn$_invoke$arity$variadic(G__17168,G__17169,seq17167__$2);
});

devtools.formatters.templating.concat_templates_BANG_ = (function devtools$formatters$templating$concat_templates_BANG_(var_args){
var args__8380__auto__ = [];
var len__8373__auto___17204 = arguments.length;
var i__8374__auto___17205 = (0);
while(true){
if((i__8374__auto___17205 < len__8373__auto___17204)){
args__8380__auto__.push((arguments[i__8374__auto___17205]));

var G__17206 = (i__8374__auto___17205 + (1));
i__8374__auto___17205 = G__17206;
continue;
} else {
}
break;
}

var argseq__8381__auto__ = ((((1) < args__8380__auto__.length))?(new cljs.core.IndexedSeq(args__8380__auto__.slice((1)),(0),null)):null);
return devtools.formatters.templating.concat_templates_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__8381__auto__);
});

devtools.formatters.templating.concat_templates_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (template,templates){
return devtools.formatters.templating.mark_as_template_BANG_.call(null,goog.object.get(template,"concat").apply(template,cljs.core.into_array.call(null,cljs.core.map.call(null,cljs.core.into_array,cljs.core.keep.call(null,devtools.formatters.helpers.pref,templates)))));
});

devtools.formatters.templating.concat_templates_BANG_.cljs$lang$maxFixedArity = (1);

devtools.formatters.templating.concat_templates_BANG_.cljs$lang$applyTo = (function (seq17202){
var G__17203 = cljs.core.first.call(null,seq17202);
var seq17202__$1 = cljs.core.next.call(null,seq17202);
return devtools.formatters.templating.concat_templates_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__17203,seq17202__$1);
});

devtools.formatters.templating.extend_template_BANG_ = (function devtools$formatters$templating$extend_template_BANG_(var_args){
var args__8380__auto__ = [];
var len__8373__auto___17209 = arguments.length;
var i__8374__auto___17210 = (0);
while(true){
if((i__8374__auto___17210 < len__8373__auto___17209)){
args__8380__auto__.push((arguments[i__8374__auto___17210]));

var G__17211 = (i__8374__auto___17210 + (1));
i__8374__auto___17210 = G__17211;
continue;
} else {
}
break;
}

var argseq__8381__auto__ = ((((1) < args__8380__auto__.length))?(new cljs.core.IndexedSeq(args__8380__auto__.slice((1)),(0),null)):null);
return devtools.formatters.templating.extend_template_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__8381__auto__);
});

devtools.formatters.templating.extend_template_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (template,args){
return devtools.formatters.templating.concat_templates_BANG_.call(null,template,args);
});

devtools.formatters.templating.extend_template_BANG_.cljs$lang$maxFixedArity = (1);

devtools.formatters.templating.extend_template_BANG_.cljs$lang$applyTo = (function (seq17207){
var G__17208 = cljs.core.first.call(null,seq17207);
var seq17207__$1 = cljs.core.next.call(null,seq17207);
return devtools.formatters.templating.extend_template_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__17208,seq17207__$1);
});

devtools.formatters.templating.make_surrogate = (function devtools$formatters$templating$make_surrogate(var_args){
var args17212 = [];
var len__8373__auto___17217 = arguments.length;
var i__8374__auto___17218 = (0);
while(true){
if((i__8374__auto___17218 < len__8373__auto___17217)){
args17212.push((arguments[i__8374__auto___17218]));

var G__17219 = (i__8374__auto___17218 + (1));
i__8374__auto___17218 = G__17219;
continue;
} else {
}
break;
}

var G__17214 = args17212.length;
switch (G__17214) {
case 1:
return devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Invalid arity: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(args17212.length)].join('')));

}
});

devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$1 = (function (object){
return devtools.formatters.templating.make_surrogate.call(null,object,null);
});

devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$2 = (function (object,header){
return devtools.formatters.templating.make_surrogate.call(null,object,header,null);
});

devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$3 = (function (object,header,body){
return devtools.formatters.templating.make_surrogate.call(null,object,header,body,(0));
});

devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$4 = (function (object,header,body,start_index){
return devtools.formatters.templating.mark_as_surrogate_BANG_.call(null,(function (){var obj17216 = {"target":object,"header":header,"body":body,"startIndex":(function (){var or__7260__auto__ = start_index;
if(cljs.core.truth_(or__7260__auto__)){
return or__7260__auto__;
} else {
return (0);
}
})()};
return obj17216;
})());
});

devtools.formatters.templating.make_surrogate.cljs$lang$maxFixedArity = 4;

devtools.formatters.templating.get_surrogate_target = (function devtools$formatters$templating$get_surrogate_target(surrogate){
if(cljs.core.truth_(devtools.formatters.templating.surrogate_QMARK_.call(null,surrogate))){
} else {
throw (new Error("Assert failed: (surrogate? surrogate)"));
}

return goog.object.get(surrogate,"target");
});
devtools.formatters.templating.get_surrogate_header = (function devtools$formatters$templating$get_surrogate_header(surrogate){
if(cljs.core.truth_(devtools.formatters.templating.surrogate_QMARK_.call(null,surrogate))){
} else {
throw (new Error("Assert failed: (surrogate? surrogate)"));
}

return goog.object.get(surrogate,"header");
});
devtools.formatters.templating.get_surrogate_body = (function devtools$formatters$templating$get_surrogate_body(surrogate){
if(cljs.core.truth_(devtools.formatters.templating.surrogate_QMARK_.call(null,surrogate))){
} else {
throw (new Error("Assert failed: (surrogate? surrogate)"));
}

return goog.object.get(surrogate,"body");
});
devtools.formatters.templating.get_surrogate_start_index = (function devtools$formatters$templating$get_surrogate_start_index(surrogate){
if(cljs.core.truth_(devtools.formatters.templating.surrogate_QMARK_.call(null,surrogate))){
} else {
throw (new Error("Assert failed: (surrogate? surrogate)"));
}

return goog.object.get(surrogate,"startIndex");
});
devtools.formatters.templating.make_reference = (function devtools$formatters$templating$make_reference(var_args){
var args__8380__auto__ = [];
var len__8373__auto___17227 = arguments.length;
var i__8374__auto___17228 = (0);
while(true){
if((i__8374__auto___17228 < len__8373__auto___17227)){
args__8380__auto__.push((arguments[i__8374__auto___17228]));

var G__17229 = (i__8374__auto___17228 + (1));
i__8374__auto___17228 = G__17229;
continue;
} else {
}
break;
}

var argseq__8381__auto__ = ((((1) < args__8380__auto__.length))?(new cljs.core.IndexedSeq(args__8380__auto__.slice((1)),(0),null)):null);
return devtools.formatters.templating.make_reference.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__8381__auto__);
});

devtools.formatters.templating.make_reference.cljs$core$IFn$_invoke$arity$variadic = (function (object,p__17223){
var vec__17224 = p__17223;
var state_override_fn = cljs.core.nth.call(null,vec__17224,(0),null);
if(((state_override_fn == null)) || (cljs.core.fn_QMARK_.call(null,state_override_fn))){
} else {
throw (new Error("Assert failed: (or (nil? state-override-fn) (fn? state-override-fn))"));
}

if((object == null)){
return devtools.formatters.templating.make_template.call(null,new cljs.core.Keyword(null,"span","span",1394872991),new cljs.core.Keyword(null,"nil-style","nil-style",-1505044832),new cljs.core.Keyword(null,"nil-label","nil-label",-587789203));
} else {
var sub_state = ((!((state_override_fn == null)))?state_override_fn.call(null,devtools.formatters.state.get_current_state.call(null)):devtools.formatters.state.get_current_state.call(null));
return devtools.formatters.templating.make_group.call(null,"object",({"object": object, "config": sub_state}));
}
});

devtools.formatters.templating.make_reference.cljs$lang$maxFixedArity = (1);

devtools.formatters.templating.make_reference.cljs$lang$applyTo = (function (seq17221){
var G__17222 = cljs.core.first.call(null,seq17221);
var seq17221__$1 = cljs.core.next.call(null,seq17221);
return devtools.formatters.templating.make_reference.cljs$core$IFn$_invoke$arity$variadic(G__17222,seq17221__$1);
});

devtools.formatters.templating._STAR_current_render_stack_STAR_ = cljs.core.PersistentVector.EMPTY;
devtools.formatters.templating._STAR_current_render_path_STAR_ = cljs.core.PersistentVector.EMPTY;
devtools.formatters.templating.pprint_str = (function devtools$formatters$templating$pprint_str(markup){
var sb__8254__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR_17233_17236 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR_17234_17237 = cljs.core._STAR_print_fn_STAR_;
cljs.core._STAR_print_newline_STAR_ = true;

cljs.core._STAR_print_fn_STAR_ = ((function (_STAR_print_newline_STAR_17233_17236,_STAR_print_fn_STAR_17234_17237,sb__8254__auto__){
return (function (x__8255__auto__){
return sb__8254__auto__.append(x__8255__auto__);
});})(_STAR_print_newline_STAR_17233_17236,_STAR_print_fn_STAR_17234_17237,sb__8254__auto__))
;

try{var _STAR_print_level_STAR_17235_17238 = cljs.core._STAR_print_level_STAR_;
cljs.core._STAR_print_level_STAR_ = (300);

try{cljs.pprint.pprint.call(null,markup);
}finally {cljs.core._STAR_print_level_STAR_ = _STAR_print_level_STAR_17235_17238;
}}finally {cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR_17234_17237;

cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR_17233_17236;
}
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__8254__auto__)].join('');
});
devtools.formatters.templating.print_preview = (function devtools$formatters$templating$print_preview(markup){
var _STAR_print_level_STAR_17240 = cljs.core._STAR_print_level_STAR_;
cljs.core._STAR_print_level_STAR_ = (1);

try{return cljs.core.pr_str.call(null,markup);
}finally {cljs.core._STAR_print_level_STAR_ = _STAR_print_level_STAR_17240;
}});
devtools.formatters.templating.add_stack_separators = (function devtools$formatters$templating$add_stack_separators(stack){
return cljs.core.interpose.call(null,"-------------",stack);
});
devtools.formatters.templating.replace_fns_with_markers = (function devtools$formatters$templating$replace_fns_with_markers(stack){
var f = (function (v){
if(cljs.core.fn_QMARK_.call(null,v)){
return "##fn##";
} else {
return v;
}
});
return clojure.walk.prewalk.call(null,f,stack);
});
devtools.formatters.templating.pprint_render_calls = (function devtools$formatters$templating$pprint_render_calls(stack){
return cljs.core.map.call(null,devtools.formatters.templating.pprint_str,stack);
});
devtools.formatters.templating.pprint_render_stack = (function devtools$formatters$templating$pprint_render_stack(stack){
return clojure.string.join.call(null,"\n",devtools.formatters.templating.add_stack_separators.call(null,devtools.formatters.templating.pprint_render_calls.call(null,devtools.formatters.templating.replace_fns_with_markers.call(null,cljs.core.reverse.call(null,stack)))));
});
devtools.formatters.templating.pprint_render_path = (function devtools$formatters$templating$pprint_render_path(path){
return devtools.formatters.templating.pprint_str.call(null,path);
});
devtools.formatters.templating.assert_markup_error = (function devtools$formatters$templating$assert_markup_error(msg){
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Assert failed: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1([cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg),cljs.core.str.cljs$core$IFn$_invoke$arity$1("\n"),cljs.core.str.cljs$core$IFn$_invoke$arity$1("Render path: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(devtools.formatters.templating.pprint_render_path.call(null,devtools.formatters.templating._STAR_current_render_path_STAR_)),cljs.core.str.cljs$core$IFn$_invoke$arity$1("\n"),cljs.core.str.cljs$core$IFn$_invoke$arity$1("Render stack:\n"),cljs.core.str.cljs$core$IFn$_invoke$arity$1(devtools.formatters.templating.pprint_render_stack.call(null,devtools.formatters.templating._STAR_current_render_stack_STAR_))].join('')),cljs.core.str.cljs$core$IFn$_invoke$arity$1("\n"),cljs.core.str.cljs$core$IFn$_invoke$arity$1("false")].join('')));

});
devtools.formatters.templating.surrogate_markup_QMARK_ = (function devtools$formatters$templating$surrogate_markup_QMARK_(markup){
return (cljs.core.sequential_QMARK_.call(null,markup)) && (cljs.core._EQ_.call(null,cljs.core.first.call(null,markup),"surrogate"));
});
devtools.formatters.templating.render_special = (function devtools$formatters$templating$render_special(name,args){
var G__17242 = name;
switch (G__17242) {
case "surrogate":
var obj = cljs.core.first.call(null,args);
var converted_args = cljs.core.map.call(null,devtools.formatters.templating.render_json_ml_STAR_,cljs.core.rest.call(null,args));
return cljs.core.apply.call(null,devtools.formatters.templating.make_surrogate,cljs.core.concat.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [obj], null),converted_args));

break;
case "reference":
var obj = cljs.core.first.call(null,args);
var converted_obj = (cljs.core.truth_(devtools.formatters.templating.surrogate_markup_QMARK_.call(null,obj))?devtools.formatters.templating.render_json_ml_STAR_.call(null,obj):obj);
return cljs.core.apply.call(null,devtools.formatters.templating.make_reference,cljs.core.concat.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [converted_obj], null),cljs.core.rest.call(null,args)));

break;
default:
return devtools.formatters.templating.assert_markup_error.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1("no matching special tag name: '"),cljs.core.str.cljs$core$IFn$_invoke$arity$1(name),cljs.core.str.cljs$core$IFn$_invoke$arity$1("'")].join(''));

}
});
devtools.formatters.templating.emptyish_QMARK_ = (function devtools$formatters$templating$emptyish_QMARK_(v){
if((cljs.core.seqable_QMARK_.call(null,v)) || (cljs.core.array_QMARK_.call(null,v)) || (typeof v === 'string')){
return cljs.core.empty_QMARK_.call(null,v);
} else {
return false;
}
});
devtools.formatters.templating.render_subtree = (function devtools$formatters$templating$render_subtree(tag,children){
var vec__17247 = tag;
var html_tag = cljs.core.nth.call(null,vec__17247,(0),null);
var style = cljs.core.nth.call(null,vec__17247,(1),null);
return cljs.core.apply.call(null,devtools.formatters.templating.make_template,html_tag,style,cljs.core.map.call(null,devtools.formatters.templating.render_json_ml_STAR_,cljs.core.remove.call(null,devtools.formatters.templating.emptyish_QMARK_,cljs.core.map.call(null,devtools.formatters.helpers.pref,children))));
});
devtools.formatters.templating.render_json_ml_STAR_ = (function devtools$formatters$templating$render_json_ml_STAR_(markup){
if(!(cljs.core.sequential_QMARK_.call(null,markup))){
return markup;
} else {
var _STAR_current_render_path_STAR_17251 = devtools.formatters.templating._STAR_current_render_path_STAR_;
devtools.formatters.templating._STAR_current_render_path_STAR_ = cljs.core.conj.call(null,devtools.formatters.templating._STAR_current_render_path_STAR_,cljs.core.first.call(null,markup));

try{var tag = devtools.formatters.helpers.pref.call(null,cljs.core.first.call(null,markup));
if(typeof tag === 'string'){
return devtools.formatters.templating.render_special.call(null,tag,cljs.core.rest.call(null,markup));
} else {
if(cljs.core.sequential_QMARK_.call(null,tag)){
return devtools.formatters.templating.render_subtree.call(null,tag,cljs.core.rest.call(null,markup));
} else {
return devtools.formatters.templating.assert_markup_error.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1("invalid json-ml markup at "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(devtools.formatters.templating.print_preview.call(null,markup)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(":")].join(''));

}
}
}finally {devtools.formatters.templating._STAR_current_render_path_STAR_ = _STAR_current_render_path_STAR_17251;
}}
});
devtools.formatters.templating.render_json_ml = (function devtools$formatters$templating$render_json_ml(markup){
var _STAR_current_render_stack_STAR_17254 = devtools.formatters.templating._STAR_current_render_stack_STAR_;
var _STAR_current_render_path_STAR_17255 = devtools.formatters.templating._STAR_current_render_path_STAR_;
devtools.formatters.templating._STAR_current_render_stack_STAR_ = cljs.core.conj.call(null,devtools.formatters.templating._STAR_current_render_stack_STAR_,markup);

devtools.formatters.templating._STAR_current_render_path_STAR_ = cljs.core.conj.call(null,devtools.formatters.templating._STAR_current_render_path_STAR_,"<render-json-ml>");

try{return devtools.formatters.templating.render_json_ml_STAR_.call(null,markup);
}finally {devtools.formatters.templating._STAR_current_render_path_STAR_ = _STAR_current_render_path_STAR_17255;

devtools.formatters.templating._STAR_current_render_stack_STAR_ = _STAR_current_render_stack_STAR_17254;
}});
devtools.formatters.templating.assert_failed_markup_rendering = (function devtools$formatters$templating$assert_failed_markup_rendering(initial_value,value){
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1("Assert failed: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1([cljs.core.str.cljs$core$IFn$_invoke$arity$1("result of markup rendering must be a template,\n"),cljs.core.str.cljs$core$IFn$_invoke$arity$1("resolved to "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(devtools.formatters.templating.pprint_str.call(null,value)),cljs.core.str.cljs$core$IFn$_invoke$arity$1("initial value: "),cljs.core.str.cljs$core$IFn$_invoke$arity$1(devtools.formatters.templating.pprint_str.call(null,initial_value))].join('')),cljs.core.str.cljs$core$IFn$_invoke$arity$1("\n"),cljs.core.str.cljs$core$IFn$_invoke$arity$1("false")].join('')));

});
devtools.formatters.templating.render_markup_STAR_ = (function devtools$formatters$templating$render_markup_STAR_(initial_value,value){
while(true){
if(cljs.core.fn_QMARK_.call(null,value)){
var G__17256 = initial_value;
var G__17257 = value.call(null);
initial_value = G__17256;
value = G__17257;
continue;
} else {
if((value instanceof cljs.core.Keyword)){
var G__17258 = initial_value;
var G__17259 = devtools.formatters.helpers.pref.call(null,value);
initial_value = G__17258;
value = G__17259;
continue;
} else {
if(cljs.core.sequential_QMARK_.call(null,value)){
var G__17260 = initial_value;
var G__17261 = devtools.formatters.templating.render_json_ml.call(null,value);
initial_value = G__17260;
value = G__17261;
continue;
} else {
if(cljs.core.truth_(devtools.formatters.templating.template_QMARK_.call(null,value))){
return value;
} else {
if(cljs.core.truth_(devtools.formatters.templating.surrogate_QMARK_.call(null,value))){
return value;
} else {
if(cljs.core.truth_(devtools.formatters.templating.reference_QMARK_.call(null,value))){
return value;
} else {
return devtools.formatters.templating.assert_failed_markup_rendering.call(null,initial_value,value);

}
}
}
}
}
}
break;
}
});
devtools.formatters.templating.render_markup = (function devtools$formatters$templating$render_markup(value){
return devtools.formatters.templating.render_markup_STAR_.call(null,value,value);
});
