(defproject todos "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :source-paths ["src"]
  :plugins [[lein-ring "0.12.0"]
            [lein-cljsbuild "1.1.6"]]
  :ring {:handler todos.core/app}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [ring/ring-core "1.6.1"]
                 [ring/ring-defaults "0.3.0"]
                 [ring/ring-jetty-adapter "1.6.1"]
                 [compojure "1.6.0"]
                 [korma "0.4.3"]
                 [environ "0.4.0"]
                 [buddy "1.3.0"]
                 ; подключение к БД
                 [org.clojure/java.jdbc "0.3.7"]
                 [postgresql/postgresql "9.1-901-1.jdbc4"]
                 ;
                 [honeysql "0.9.0-beta2"]
                 ; логгирование
                 [org.clojure/tools.logging "0.3.1"]
                 [log4j/log4j "1.2.17" :exclusions [javax.mail/mail
                                                    javax.jms/jms
                                                    com.sun.jmdk/jmxtools
                                                    com.sun.jmx/jmxri]]
                 [org.clojure/clojurescript "1.9.521"]
                 [prismatic/schema "1.1.6"]
                 ; frontend
                 [reagent "0.6.0-rc"]
                 [reagent-utils "0.2.1"]
                 [re-frame "0.9.0"]
                 [binaryage/devtools "0.8.1"]
                 [secretary "1.2.3"]
                 [re-com "2.0.0"]
                 [cljs-ajax "0.5.9"]
                 [com.pupeno/free-form "0.5.0"]]

  :cljsbuild {
     :builds [{:source-paths ["src/frontend"]
               :compiler {:output-to  "resources/public/js/client.js"
                          :output-dir "resources/public/js"
                          :optimizations :whitespace
                          :source-map false
                          :main "frontend.core"
                          :asset-path "js"
                          ; :source-map "resources/public/js/out/todos/core.js.map"
                          :pretty-print true}}]}
  :main todos.core)
