(ns todos.models.db
  (:require [honeysql.core :as sql]
            [honeysql.helpers :refer :all]
            [clojure.java.jdbc :as jdbc]))

(def db-spec
  {:classname "com.mysql.jdbc.Driver"
  :subprotocol "postgresql"
  :subname "//127.0.0.1:5432/aleksandr_todos"
  :user "todos_user"
  :password "0000"})

  ; (defdb db (postgres {:db (get env :restful-db "aleksandr_todos")
  ;                      :user (get env :restful-db-user "todos_user")
  ;                      :password (get env :restful-db-pass "0000")
  ;                      :host (get env :restful-db-host "localhost")
  ;                      :port (get env :restful-db-port 5432)}))
