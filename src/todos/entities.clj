(ns todos.entities
  (:use korma.core
        todos.models.db))

(declare users lists items)

(defentity users
  (pk :id)
  (table :users)
  (has-many lists)
  (entity-fields :login :password_digest))

(defentity lists
  (pk :id)
  (table :lists)
  (has-many items)
  (belongs-to users {:fk :user_id})
  (entity-fields :title))

(defentity items
  (pk :id)
  (table :items)
  (belongs-to lists {:fk :list_id})
  (entity-fields :text))
