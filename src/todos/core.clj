(ns todos.core
  (:require [compojure.core :refer :all]
            [clojure.test :refer :all]
            [compojure.route :as route]
            [ring.util.response :as resp]
            [clojure.tools.logging :as log]
            [honeysql.core :as sql]
            [honeysql.helpers :refer :all]
            [clojure.java.jdbc :as jdbc]
            [buddy.hashers :as hashers]
            [todos.models.users :as models-users])
  (:use ring.middleware.file
        ring.middleware.resource
        ring.middleware.content-type
        ring.middleware.not-modified
        ring.middleware.defaults))

(def root "/home/aleksandr/projects/todos/resources/public")

(defn wrap-debug [handler]
 (fn [request]
   (prn "handler result" (handler request))
   (assoc (handler request) :status 200)))

(defn handler
  [request]
  {:status 200
    :headers {"Content-Type" "text/plain"}
    :body "Hello world"})

(defroutes my-routes
  (GET "/" [] (resp/content-type (resp/resource-response "index.html" {:root "public"}) "text/html"))
  ; (route/files "/" {:root root})
  (POST "/send-message" [] models-users/create-user)
  (route/resources "/")
  (route/not-found "Not Found"))
  
(def app
  (-> my-routes
      (wrap-resource root)
      (wrap-content-type)
      (wrap-not-modified)
      (wrap-defaults
        (assoc-in site-defaults [:security :anti-forgery] false))))
