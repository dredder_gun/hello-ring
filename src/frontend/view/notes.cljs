(ns frontend.view.notes
  (:require [re-com.core :refer [h-box v-box box gap line input-text input-password input-textarea label checkbox radio-button slider title p button border modal-panel]]
            free-form.bootstrap-3
            [ajax.core :refer [GET POST ajax-request json-request-format json-response-format url-request-format]]))

(defn list-todos
 []
 [:div#list-todos
   [:h4#list-todos-headers "Шаблоны заметок"]
   [:ul
     [:li [:a {:href "#"} "Заметка на работчий день"]]
     [:li [:a {:href "#"} "Заметка начальнику"]]
     [:li [:a {:href "#"} "Заметка жене"]]]])
