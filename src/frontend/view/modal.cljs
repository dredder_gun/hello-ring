(ns frontend.view.modal
  (:require [re-com.core :refer [h-box v-box box gap line input-text
                                  input-password input-textarea label checkbox
                                  radio-button slider title p button border
                                  modal-panel]]
            free-form.bootstrap-3))

(defn dialog-markup
  [form-data process-ok process-cancel main-label button-label
    & {:keys [ajax-handler
              incorrect-data]}]
  [border
   :border "1px solid #eee"
   :child  [v-box
            :padding  "10px"
            :style    {:background-color "cornsilk"}
            :children [[title :label main-label :level :level2]
                       [v-box
                        :class    "form-group"
                        :children [[:label {:for "pf-login"} "Новый логин"]
                                   [input-text
                                    :model       (:login @form-data)
                                    :on-change   #(swap! form-data assoc :login %)
                                    :placeholder "Введите логин"
                                    :class       "form-control"
                                    :attr        {:id "login"}]]]
                       [v-box
                        :class    "form-group"
                        :children [[:label {:for "pf-password"} "Новый пароль"]
                                   [input-text
                                    :model       (:password @form-data)
                                    :on-change   #(swap! form-data assoc :password %)
                                    :placeholder "Введите пароль"
                                    :class       "form-control"
                                    :attr        {:id "pf-password" :type "password"}]]]
                      ; [checkbox
                      ;  :label     "Запомнить"
                      ;  :model     (:remember-me @form-data)
                      ;  :on-change #(swap! form-data assoc :remember-me %)]
                       [line :color "#ddd" :style {:margin "10px 0 10px"}]
                       [h-box
                        :gap      "12px"
                        :children [[button
                                    :label    button-label
                                    :class    "btn-primary"
                                    :on-click (if (nil? ajax-handler) process-ok ajax-handler)]
                                   [button
                                    :label    "Отмена"
                                    :on-click process-cancel]]]
                        (when (and (not= incorrect-data nil) (= @incorrect-data "true"))
                          [:p.error-message "Неверные данные"])]]])
