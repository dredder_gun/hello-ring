(ns frontend.views
  (:require [reagent.core       :as reagent]
            [re-frame.core      :refer [subscribe dispatch]]
            [re-com.modal-panel :refer [modal-panel-args-desc]]
            [re-com.core   :refer [h-box v-box box gap line input-text input-password input-textarea label checkbox radio-button slider title p button border modal-panel]]
            [reagent.core       :as reagent]
            [free-form.re-frame :as free-form]
            free-form.bootstrap-3
            [ajax.core :refer [GET POST ajax-request json-request-format json-response-format url-request-format]]

            [frontend.view.modal :refer [dialog-markup]]
            [frontend.view.notes :refer [list-todos]]))

(defn todo-input [{:keys [title on-save on-stop]}]
  (let [val (reagent/atom title)
        stop #(do (reset! val "")
                  (when on-stop (on-stop)))
        save #(let [v (-> @val str clojure.string/trim)]
               (when (seq v) (on-save v))
               (stop))]
    (fn [props]
      [:input (merge props
                     {:type "text"
                      :value @val
                      :auto-focus true
                      :on-blur save
                      :on-change #(reset! val (-> % .-target .-value))
                      :on-key-down #(case (.-which %)
                                     13 (save)
                                     27 (stop)
                                     nil)})])))

(defn todo-item
  []
  (let [editing (reagent/atom false)]
    (fn [{:keys [id done title]}]
      [:li {:class (str (when done "completed ")
                        (when @editing "editing"))}
        [:div.view
          [:input.toggle
            {:type "checkbox"
             :checked done
             :on-change #(dispatch [:toggle-done id])}]
          [:label
            {:on-double-click #(reset! editing true)}
            title]
          [:button.destroy
            {:on-click #(dispatch [:delete-todo id])}]]
        (when @editing
          [todo-input
            {:class "edit"
             :title title
             :on-save #(dispatch [:save id %])
             :on-stop #(reset! editing false)}])])))

(defn task-list
  []
  (let [visible-todos @(subscribe [:visible-todos])
        all-complete? @(subscribe [:all-complete?])]
      [:section#main
        [:input#toggle-all
          {:type "checkbox"
           :checked all-complete?
           :on-change #(dispatch [:complete-all-toggle (not all-complete?)])}]
        [:label
          {:for "toggle-all"}
          "Пометить все"]
        [:ul#todo-list
          (for [todo  visible-todos]
            ^{:key (:id todo)} [todo-item todo])]]))

(defn footer-controls
  []
  (let [[active done] @(subscribe [:footer-counts])
        showing       @(subscribe [:showing])
        a-fn          (fn [filter-kw txt]
                        [:a {:class (when (= filter-kw showing) "selected")
                             :href (str "#/" (name filter-kw))} txt])]
    [:footer#footer
     [:span#todo-count
      [:strong active] " " (case active 1 "item" "items") " осталось"]
     [:ul#filters
      [:li (a-fn :all    "Все")]
      [:li (a-fn :active "Акт.")]
      [:li (a-fn :done   "Вып.")]]
     (when (pos? done)
       [:button#clear-completed {:on-click #(dispatch [:clear-completed])}
        "Убрать выполненные"])]))

(defn task-entry
  []
  [:header#header
    [:h1 "заметки"]
    [todo-input
      {:id "new-todo"
       :placeholder "Что нужно сделать?"
       :on-save #(dispatch [:add-todo %])}]])

(defn handler [[ok response]]
 (if ok
   (.log js/console (str response))
   (.error js/console (str response))))

(defn modal-dialog
  "Create a button to test the modal component for modal dialogs"
  []
  (let [show?          (reagent/atom false)
        incorrect-data (subscribe [:valid-data])
        form-data      (reagent/atom {:login       "login"
                                      :password    "0000"
                                      :remember-me true})
        save-form-data (reagent/atom nil)
        process-ok     (fn [event]
                         (reset! save-form-data @form-data)
                         (dispatch [:example-event @save-form-data])
                         (when (= @incorrect-data "true")
                           (reset! show? false))
                         ;; ***** PROCESS THE RETURNED DATA HERE
                         false) ;; Prevent default "GET" form submission (if used)
        process-cancel (fn [event]
                         (reset! form-data @save-form-data)
                         (reset! show? false)
                         false)]
    (fn []
      [h-box
       :justify :around
       :children [[button
                   :label    "Войти в систему"
                   :class    "btn-info"
                   :on-click #(do
                               (reset! save-form-data @form-data)
                               (reset! show? "input"))]
                  [button
                    :label "Зарегистрироваться"
                    :class "btn-success"
                    :on-click #(do
                                (reset! save-form-data @form-data)
                                (reset! show? "reg"))]

                  (case @show?
                    "input" [modal-panel
                                     :backdrop-color   "grey"
                                     :backdrop-opacity 0.4
                                     :style            {:font-family "Consolas"}
                                     :child            [dialog-markup
                                                        form-data
                                                        process-ok
                                                        process-cancel
                                                        "Введите свои данные"
                                                        "Войти"
                                                        :incorrect-data incorrect-data]]
                    "reg" [modal-panel
                                     :backdrop-color   "grey"
                                     :backdrop-opacity 0.4
                                     :style            {:font-family "Consolas"}
                                     :child            [dialog-markup
                                                         form-data
                                                         process-ok
                                                         process-cancel
                                                         "Регистрация профиля"
                                                         "Зарегистрировать"
                                                         :ajax-handler
                                                           #(ajax-request
                                                               {:uri "/send-message"
                                                                :method :post
                                                                :params {:login    (:login @form-data)
                                                                         :password (:password @form-data)}
                                                                :handler handler
                                                                :format (url-request-format)
                                                                :response-format (json-response-format {:keywords? true})})
                                                          :incorrect-data incorrect-data]]
                    nil)]])))

(defn todo-app
  []
  (let [aut? @(subscribe [:login-password])
      data (subscribe [:re-frame-bootstrap-3])]
    (if (= aut? "true")
      [:div
       [button
        :label "Выйти"
        :class "btn-primary"
        :on-click #(dispatch [:exit-event])]
       [list-todos]
       [:section#todoapp
        [task-entry]
        (when (seq @(subscribe [:todos]))
          [task-list])
        [footer-controls]]
       [:footer#info
        [:p "Двойной клик для редактирования заметки"]]]
       [:div
         [:h1#enter-header {:style {:text-align "center" }} "заметки"]
         [modal-dialog]])))
