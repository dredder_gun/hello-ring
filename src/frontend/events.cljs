(ns frontend.events
  (:require
    [frontend.db    :refer [default-value todos->local-store autorized->local-store]]
    [re-frame.core :refer [reg-event-db reg-event-fx inject-cofx path trim-v
                           after debug dispatch]]
    [cljs.spec     :as s]))


;; -- Interceptors --------------------------------------------------------------
;;

(defn check-and-throw
  "throw an exception if db doesn't match the spec"
  [a-spec db]
  (when-not (s/valid? a-spec db)
    (throw (ex-info (str "spec check failed: " (s/explain-str a-spec db)) {}))))

;; Event handlers change state, that's their job. But what happens if there's
;; a bug which corrupts app state in some subtle way? This interceptor is run after
;; each event handler has finished, and it checks app-db against a spec.  This
;; helps us detect event handler bugs early.
(def check-spec-interceptor (after (partial check-and-throw :frontend.db/db)))

;; this interceptor stores todos into local storage
;; we attach it to each event handler which could update todos
(def ->local-store (after todos->local-store))
(def ->local-autorized (after autorized->local-store))

(def interceptor-for-query
  (re-frame.core/->interceptor
    :id      :interceptor-for-query
    :before  (fn [context]
               (update-in context [:coeffects :event] #(into {} %)))))

;; Each event handler can have its own set of interceptors (middleware)
;; But we use the same set of interceptors for all event habdlers related
;; to manipulating todos.
;; A chain of interceptors is a vector.
(def todo-interceptors [check-spec-interceptor               ;; ensure the spec is still valid
                        (path :todos)                        ;; 1st param to handler will be the value from this path
                        ->local-store                        ;; write todos to localstore
                        (when ^boolean js/goog.DEBUG debug)  ;; look in your browser console for debug logs
                        trim-v])                             ;; removes first (event id) element from the event vec

(def interceptor-validation-for-form
  (re-frame.core/->interceptor
    :id      :interceptor-validation-for-form
    :before  (fn [context second]
               (println "Первый параметр в error-login" context)
               (println "Второй параметр в error-login" second)
               context)))

(def validation-form [])

;; -- Helpers -----------------------------------------------------------------

(defn allocate-next-id
  "Returns the next todo id.
  Assumes todos are sorted.
  Returns one more than the current largest id."
  [todos]
  ((fnil inc 0) (last (keys todos))))


;; -- Event Handlers ----------------------------------------------------------

(defn ui-dispatch
  [js-event event]
  (.preventDefault js-event)
  (dispatch event))

;; usage:  (dispatch [:initialise-db])
(reg-event-fx                     ;; on app startup, create initial state
  :initialise-db                  ;; event id being handled
  [(inject-cofx :local-store-todos)  ;; obtain todos from localstore
  (inject-cofx :local-store-authorized?)
   check-spec-interceptor]                                  ;; after the event handler runs, check that app-db matches the spec
  (fn [{:keys [db local-store-todos local-store-authorized?]} _]                    ;; the handler being registered
    (println "Это записывается в app-db при старте" {:db (assoc default-value :todos local-store-todos :passed? local-store-authorized?)})
    {:db (assoc default-value :todos local-store-todos :passed? local-store-authorized?)}))  ;; all hail the new state

;; usage:  (dispatch [:set-showing  :active])
(reg-event-db                     ;; this handler changes the todo filter
  :set-showing                    ;; event-id

  ;; this chain of two interceptors wrap the handler
  [check-spec-interceptor (path :showing) trim-v]

  ;; The event handler
  ;; Because of the path interceptor above, the 1st parameter to
  ;; the handler below won't be the entire 'db', and instead will
  ;; be the value at a certain path within db, namely :showing.
  ;; Also, the use of the 'trim-v' interceptor means we can omit
  ;; the leading underscore from the 2nd parameter (event vector).
  (fn [old-keyword [new-filter-kw]]  ;; handler
    new-filter-kw))                  ;; return new state for the path

(reg-event-db
  :example-event
  [trim-v interceptor-for-query ->local-autorized]
  (fn [db query-v]
    (println "Первый параметр в событии :example-event" db)
    (let [login (reduce
                  (fn [acc el]
                    (if (or (= :log (key el)) (= :pass (key el)))
                      (merge acc el)
                      acc))
                  {}
                  db)]

      (if (and (= (:password query-v) (:pass login)) (= (:login query-v) (:log login)))
        (assoc db :passed? "true" :incorrect-data "false")
        (assoc db :incorrect-data "true")))))

(reg-event-db
  :update-re-frame-bootstrap-3
  []
  (fn [db [_ keys value :as event]]
    (println "Первый аргумент в событии :update-re-frame-bootstrap-3" db)
    (println "Второй аргумент в событии :update-re-frame-bootstrap-3" event)
    db
    ; (-> db
    ;     (assoc-in (cons :re-frame-bootstrap-3 keys) value)
    ;     (update :event-log #(conj % event)))
        ))

(reg-event-db
  :add-validation-errors-to-form
  [->local-store]
  (fn [db [_ target :as what-is-it]]
    (let [error-field (keyword (:field (:validation-error db)))
          error-message "Idiot ebaniy! Invalid input!"]
      (update-in db [:re-frame-bootstrap-3 :-errors :password] #(cons error-message %1)))))

(reg-event-db
  :exit-event
  [->local-autorized]
  (fn [db query-from-view]
    (assoc db :passed? false)))

;; usage:  (dispatch [:add-todo  "Finish comments"])
(reg-event-db                     ;; given the text, create a new todo
  :add-todo

  ;; The standard set of interceptors, defined above, which we
  ;; apply to all todos-modifiing event handlers. Looks after
  ;; writing todos to local store, etc.
  todo-interceptors

  ;; The event handler function.
  ;; The "path" interceptor in `todo-interceptors` means 1st parameter is :todos
  (fn [todos [text]]
    ; (println "Элемент todos" todos) тут мапа элемент перед добавлением нового элемента
    ; (println "Элемент [text]" [text])
    (let [id (allocate-next-id todos)]
      (assoc todos id {:id id :title text :done false}))))


(reg-event-db
  :toggle-done
  todo-interceptors
  (fn [todos [id]]
    (update-in todos [id :done] not)))


(reg-event-db
  :save
  todo-interceptors
  (fn [todos [id title]]
    (assoc-in todos [id :title] title)))


(reg-event-db
  :delete-todo
  todo-interceptors
  (fn [todos [id]]
    (println "Первый параметр в событии :delete-todo" todos)
    (dissoc todos id)))


(reg-event-db
  :clear-completed
  todo-interceptors
  (fn [todos _]
    (->> (vals todos)                ;; find the ids of all todos where :done is true
         (filter :done)
         (map :id)
         (reduce dissoc todos))))    ;; now delete these ids


(reg-event-db
  :complete-all-toggle
  todo-interceptors
  (fn [todos _]
    (let [new-done (not-every? :done (vals todos))]   ;; work out: toggle true or false?
      (reduce #(assoc-in %1 [%2 :done] new-done)
              todos
              (keys todos)))))
